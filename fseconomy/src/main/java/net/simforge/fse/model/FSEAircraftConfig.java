package net.simforge.fse.model;

import javax.persistence.*;

@Entity
@Table(name = "fse_aircraft_config")
public class FSEAircraftConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fse_aircraft_config_id_seq")
    @SequenceGenerator(name = "fse_aircraft_config_id_seq", sequenceName = "fse_aircraft_config_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column(name = "make_model")
    private String makeModel;
    @Column
    private int crew;
    @Column
    private int seats;
    @Column(name = "cruise_speed")
    private int cruiseSpeed;
    @Column
    private int gph;
    @Column(name = "fuel_type")
    private int fuelType;
    @Column
    private int mtow;
    @Column(name = "empty_weight")
    private int emptyWeight;
    @Column
    private double price;
    @Column
    private int ext1;
    @Column(name = "l_tip")
    private int lTip;
    @Column(name = "l_aux")
    private int lAux;
    @Column(name = "l_main")
    private int lMain;
    @Column
    private int center1;
    @Column
    private int center2;
    @Column
    private int center3;
    @Column(name = "r_main")
    private int rMain;
    @Column(name = "r_aux")
    private int rAux;
    @Column(name = "r_tip")
    private int rTip;
    @Column(name = "r_ext2")
    private int rExt2;
    @Column
    private int engines;
    @Column(name = "engine_price")
    private double enginePrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public int getCrew() {
        return crew;
    }

    public void setCrew(int crew) {
        this.crew = crew;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getCruiseSpeed() {
        return cruiseSpeed;
    }

    public void setCruiseSpeed(int cruiseSpeed) {
        this.cruiseSpeed = cruiseSpeed;
    }

    public int getGph() {
        return gph;
    }

    public void setGph(int gph) {
        this.gph = gph;
    }

    public int getFuelType() {
        return fuelType;
    }

    public void setFuelType(int fuelType) {
        this.fuelType = fuelType;
    }

    public int getMtow() {
        return mtow;
    }

    public void setMtow(int mtow) {
        this.mtow = mtow;
    }

    public int getEmptyWeight() {
        return emptyWeight;
    }

    public void setEmptyWeight(int emptyWeight) {
        this.emptyWeight = emptyWeight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getExt1() {
        return ext1;
    }

    public void setExt1(int ext1) {
        this.ext1 = ext1;
    }

    public int getLTip() {
        return lTip;
    }

    public void setLTip(int lTip) {
        this.lTip = lTip;
    }

    public int getLAux() {
        return lAux;
    }

    public void setLAux(int lAux) {
        this.lAux = lAux;
    }

    public int getLMain() {
        return lMain;
    }

    public void setLMain(int lMain) {
        this.lMain = lMain;
    }

    public int getCenter1() {
        return center1;
    }

    public void setCenter1(int center1) {
        this.center1 = center1;
    }

    public int getCenter2() {
        return center2;
    }

    public void setCenter2(int center2) {
        this.center2 = center2;
    }

    public int getCenter3() {
        return center3;
    }

    public void setCenter3(int center3) {
        this.center3 = center3;
    }

    public int getRMain() {
        return rMain;
    }

    public void setRMain(int rMain) {
        this.rMain = rMain;
    }

    public int getRAux() {
        return rAux;
    }

    public void setRAux(int rAux) {
        this.rAux = rAux;
    }

    public int getRTip() {
        return rTip;
    }

    public void setRTip(int rTip) {
        this.rTip = rTip;
    }

    public int getRExt2() {
        return rExt2;
    }

    public void setRExt2(int rExt2) {
        this.rExt2 = rExt2;
    }

    public int getEngines() {
        return engines;
    }

    public void setEngines(int engines) {
        this.engines = engines;
    }

    public double getEnginePrice() {
        return enginePrice;
    }

    public void setEnginePrice(double enginePrice) {
        this.enginePrice = enginePrice;
    }
}
