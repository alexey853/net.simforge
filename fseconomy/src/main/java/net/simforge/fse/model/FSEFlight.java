package net.simforge.fse.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "fse_flight")
public class FSEFlight {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fse_flight_id_seq")
    @SequenceGenerator(name = "fse_flight_id_seq", sequenceName = "fse_flight_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column(name = "fse_account_id")
    private int fseAccountId;
    @Column(name = "fse_id")
    private int fseId;
    @Column
    private LocalDateTime dt;
    @Column(name = "pilot_name")
    private String pilotName;
    @Column
    private String aircraft;
    @Column(name = "make_model")
    private String makeModel;
    @Column(name = "from_airport")
    private String fromAirport;
    @Column(name = "to_airport")
    private String toAirport;
    @Column(name = "flight_time")
    private String flightTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public int getFseAccountId() {
        return fseAccountId;
    }

    public void setFseAccountId(int fseAccountId) {
        this.fseAccountId = fseAccountId;
    }

    public int getFseId() {
        return fseId;
    }

    public void setFseId(int fseId) {
        this.fseId = fseId;
    }

    public LocalDateTime getDt() {
        return dt;
    }

    public void setDt(LocalDateTime dt) {
        this.dt = dt;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public String getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

    public String getToAirport() {
        return toAirport;
    }

    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }
}
