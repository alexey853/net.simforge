package net.simforge.fse.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "fse_feeder_query")
public class FSEFeederQuery {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fse_feeder_query_id_seq")
    @SequenceGenerator(name = "fse_feeder_query_id_seq", sequenceName = "fse_feeder_query_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column
    private Integer status;
    @Column(name = "status_dt")
    private LocalDateTime statusDt;
    @Column
    private String url;
    @Column
    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getStatusDt() {
        return statusDt;
    }

    public void setStatusDt(LocalDateTime statusDt) {
        this.statusDt = statusDt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static class Status {
        public final static int Pending = 0;
        public final static int OnHold = 1;
        public final static int Processing = 2;
        public final static int Done = 3;
        public final static int Error = 4;
    }
}
