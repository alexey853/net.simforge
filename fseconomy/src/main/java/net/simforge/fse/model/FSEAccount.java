package net.simforge.fse.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "fse_account")
public class FSEAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fse_account_id_seq")
    @SequenceGenerator(name = "fse_account_id_seq", sequenceName = "fse_account_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column(name = "pilot_name")
    private String pilotName;
    @Column(name = "access_key")
    private String accessKey;
    @Column(name = "next_time_dt")
    private LocalDateTime nextTimeDt;
    @Column(name = "feeder_query_id")
    private Integer feederQueryId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public LocalDateTime getNextTimeDt() {
        return nextTimeDt;
    }

    public void setNextTimeDt(LocalDateTime nextTimeDt) {
        this.nextTimeDt = nextTimeDt;
    }

    public Integer getFeederQueryId() {
        return feederQueryId;
    }

    public void setFeederQueryId(Integer feederQueryId) {
        this.feederQueryId = feederQueryId;
    }
}
