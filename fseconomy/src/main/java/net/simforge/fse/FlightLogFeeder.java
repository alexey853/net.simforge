package net.simforge.fse;

import net.simforge.commons.io.Csv;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Str;
import net.simforge.commons.runtime.BaseTask;
import net.simforge.fse.model.FSEAccount;
import net.simforge.fse.model.FSEFeederQuery;
import net.simforge.fse.model.FSEFlight;
import org.hibernate.Session;

import java.time.LocalDateTime;

public class FlightLogFeeder extends BaseTask {
    private Session session;

    public FlightLogFeeder() {
        super("FSEFlightLogFeeder");
    }

    @Override
    protected void process() {
        try (Session session = FSEApp.getSessionFactory().openSession()) {
            this.session = session;

            FSEAccount account = session.get(FSEAccount.class, 1);

            if (account.getNextTimeDt() == null || account.getNextTimeDt().isBefore(JavaTime.nowUtc())) {
                process(account);
            }
        }
    }

    private void process(FSEAccount account) {
        String pilotName = account.getPilotName();

        logger.info(String.format("Account '%s': processing", pilotName));


        session.getTransaction().begin();

        Integer feederQueryId = account.getFeederQueryId();
        if (feederQueryId != null) {
            FSEFeederQuery query = session.get(FSEFeederQuery.class, feederQueryId);
            Integer status = query.getStatus();

            logger.info(String.format("Account '%s': query status is %s", pilotName, status));
            switch (status) {
                case FSEFeederQuery.Status.Done:
                    logger.info(String.format("Account '%s': start parsing...", pilotName));
                    try {
                        parseFlights(account, query.getContent());
                    } catch (Exception e) {
                        logger.error("Error during flight parsing", e);
                    }
                    account.setFeederQueryId(null);
                    account.setNextTimeDt(JavaTime.nowUtc().plusMinutes(30));
                    break;
                case FSEFeederQuery.Status.Error:
                    logger.info(String.format("Account '%s': forgetting about this query", pilotName));
                    account.setFeederQueryId(null);
                    account.setNextTimeDt(JavaTime.nowUtc().plusMinutes(30));
                    break;
                default:
                    logger.info(String.format("Account '%s': waiting for next time", pilotName));
                    account.setNextTimeDt(JavaTime.nowUtc().plusMinutes(1));
            }
        } else {
            String accessKey = account.getAccessKey();

            //noinspection JpaQlInspection
            FSEFlight lastFlight = (FSEFlight) session
                    .createQuery("select f from FSEFlight as f where f.fseAccountId = :fseAccountId order by id desc")
                    .setInteger("fseAccountId", account.getId())
                    .setMaxResults(1)
                    .uniqueResult();
            int fromId = lastFlight == null ?
                    0 : // lets start from beginning if last flight is absent
                    lastFlight.getFseId() - 10; // some overlapping to avoid any issues

            FSEFeederQuery feederQuery = FSEFeeder.createFlightLogByIdQuery(accessKey, fromId);
            session.save(feederQuery);

            account.setFeederQueryId(feederQuery.getId());
            account.setNextTimeDt(JavaTime.nowUtc());

            logger.info(String.format("Account '%s': new query to feeder queue added", pilotName));
        }

        session.update(account);
        session.getTransaction().commit();

        logger.info(String.format("Account '%s': next processing at %s", pilotName, account.getNextTimeDt()));
    }

    private void parseFlights(FSEAccount account, String content) {
        String pilotName = account.getPilotName();

        Csv csv = Csv.fromContent(content);
        logger.info(String.format("Found %s row(s)", csv.rowCount()));
        for (int i = 0; i < csv.rowCount(); i++) {
            if (csv.rowWidth(i) < 24) {
                logger.warn(String.format("  Row %s, length %s, skipping", i, csv.rowWidth(i)));
                continue;
            }

            String eachType = csv.value(i, 1);
            if (!eachType.equals("flight")) {
                continue;
            }

            String eachPilotName = csv.value(i, 4);
            if (!eachPilotName.equals(pilotName)) {
                continue;
            }

            FSEFlight fseFlight = new FSEFlight();
            fseFlight.setFseAccountId(account.getId());
            fseFlight.setFseId(Integer.parseInt(csv.value(i, "Id")));
            fseFlight.setDt(LocalDateTime.parse(csv.value(i, "Time"), FSE.DTformatJT8));
            fseFlight.setPilotName(pilotName);
            fseFlight.setAircraft(csv.value(i, "Aircraft"));
            fseFlight.setMakeModel(csv.value(i, "MakeModel"));
            fseFlight.setFromAirport(csv.value(i, "From"));
            fseFlight.setToAirport(csv.value(i, "To"));
            fseFlight.setFlightTime(csv.value(i, "FlightTime"));

            try (Session anotherSession = FSEApp.getSessionFactory().openSession()) {
                //noinspection JpaQlInspection
                FSEFlight existingFseFlight = (FSEFlight) session
                        .createQuery("from FSEFlight f where f.fseId = :fseId")
                        .setInteger("fseId", fseFlight.getFseId())
                        .uniqueResult();
                if (existingFseFlight != null) {
                    continue;
                }

                anotherSession.getTransaction().begin();
                anotherSession.save(fseFlight);
                anotherSession.getTransaction().commit();

                logger.info(String.format("New flight: %s", fseFlightToString(fseFlight)));
            }
        }
    }

    private static String fseFlightToString(FSEFlight fseFlight) {
        return Str.al(String.valueOf(fseFlight.getFseId()), 10)
                + Str.al(fseFlight.getDt().toString(), 35)
                + Str.al(fseFlight.getPilotName(), 25)
                + Str.al(fseFlight.getAircraft(), 10)
                + Str.al(fseFlight.getMakeModel(), 30)
                + Str.al(fseFlight.getFromAirport(), 10)
                + Str.al(fseFlight.getToAirport(), 10)
                + Str.al(fseFlight.getFlightTime(), 10);
    }
}
