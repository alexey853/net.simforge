package net.simforge.fse;

import net.simforge.commons.io.Csv;
import net.simforge.fse.model.FSEAircraftConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.File;
import java.io.IOException;

@Deprecated
public class ImportAircraftConfigs {
    public static void main(String[] args) throws IOException {
        SessionFactory sessionFactory = FSE.buildSessionFactory();
        Session session = sessionFactory.openSession();

        String filename = args.length > 0 ? args[0] : "./data/aircraftconfigs.csv";
        File file = new File(filename);
        System.out.println("Using file " + file.getAbsolutePath());

        Csv csv = Csv.load(file);
        System.out.println("Loaded rows " + csv.rowCount());
        for (int i = 0; i < csv.rowCount(); i++) {
            if (csv.rowWidth(i) != 23) {
                System.out.println("Skipping row " + i);
                continue;
            }

            String makeModel = csv.value(i, 0);
            System.out.println("MakeModel " + makeModel);

            FSEAircraftConfig aircraftConfig = FSE.findAircraftConfig(session, makeModel);
            if (aircraftConfig != null) {
                System.out.println("Found, skipping");
                continue;
            }

            aircraftConfig = new FSEAircraftConfig();
            aircraftConfig.setMakeModel(makeModel);
            aircraftConfig.setCrew(Integer.parseInt(csv.value(i, 1)));
            aircraftConfig.setSeats(Integer.parseInt(csv.value(i, 2)));
            aircraftConfig.setCruiseSpeed(Integer.parseInt(csv.value(i, 3)));
            aircraftConfig.setGph(Integer.parseInt(csv.value(i, 4)));
            aircraftConfig.setFuelType(Integer.parseInt(csv.value(i, 5)));
            aircraftConfig.setMtow(Integer.parseInt(csv.value(i, 6)));
            aircraftConfig.setEmptyWeight(Integer.parseInt(csv.value(i, 7)));
            aircraftConfig.setPrice(Double.parseDouble(csv.value(i, 8)));
            aircraftConfig.setExt1(Integer.parseInt(csv.value(i, 9)));
            aircraftConfig.setLTip(Integer.parseInt(csv.value(i, 10)));
            aircraftConfig.setLAux(Integer.parseInt(csv.value(i, 11)));
            aircraftConfig.setLMain(Integer.parseInt(csv.value(i, 12)));
            aircraftConfig.setCenter1(Integer.parseInt(csv.value(i, 13)));
            aircraftConfig.setCenter2(Integer.parseInt(csv.value(i, 14)));
            aircraftConfig.setCenter3(Integer.parseInt(csv.value(i, 15)));
            aircraftConfig.setRMain(Integer.parseInt(csv.value(i, 16)));
            aircraftConfig.setRAux(Integer.parseInt(csv.value(i, 17)));
            aircraftConfig.setRTip(Integer.parseInt(csv.value(i, 18)));
            aircraftConfig.setRExt2(Integer.parseInt(csv.value(i, 19)));
            aircraftConfig.setEngines(Integer.parseInt(csv.value(i, 20)));
            aircraftConfig.setEnginePrice(Double.parseDouble(csv.value(i, 21)));

            session.getTransaction().begin();
            session.save(aircraftConfig);
            session.getTransaction().commit();

            System.out.println("Created");
        }

        session.close();
        sessionFactory.close();
    }
}
