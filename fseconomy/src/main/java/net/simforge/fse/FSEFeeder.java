package net.simforge.fse;

import net.simforge.commons.io.IOHelper;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.runtime.BaseTask;
import net.simforge.fse.model.FSEFeederQuery;
import org.hibernate.Session;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Iterator;

public class FSEFeeder extends BaseTask {
    private Session session;

    public FSEFeeder() {
        super("FSEFeeder");
    }

    @Override
    protected void process() {
        try (Session session = FSEApp.getSessionFactory().openSession()) {
            this.session = session;

            FSEFeederQuery query = loadFirstPending();

            if (query == null) {
                logger.debug("No pending queries found");
                return;
            }

            query = updateQueryStatus(query, FSEFeederQuery.Status.Processing);

            try {
                String content = download(query);
                query.setContent(content);

                updateQueryStatus(query, FSEFeederQuery.Status.Done);
            } catch (Exception e) {
                logger.error("Unable to download content", e);

                updateQueryStatus(query, FSEFeederQuery.Status.Error);
            }
        }
    }

    private String download(FSEFeederQuery query) throws IOException {
        String url = query.getUrl();
        logger.info(String.format("Query %s: Downloading url '%s'...", query.getId(), url));
        String content = IOHelper.download(url);
        logger.info(String.format("Query %s: Downloaded %s bytes", query.getId(), content.length()));
        return content;
    }

    private FSEFeederQuery loadFirstPending() {
        //noinspection JpaQlInspection,UnnecessaryLocalVariable
        FSEFeederQuery query = (FSEFeederQuery) session
                .createQuery("select q " +
                        "from FSEFeederQuery as q " +
                        "where q.status = :pending order by q.statusDt")
                .setInteger("pending", FSEFeederQuery.Status.Pending)
                .setMaxResults(1)
                .uniqueResult();
        return query;
    }

    private FSEFeederQuery updateQueryStatus(FSEFeederQuery query, int status) {
        query.setStatus(status);
        query.setStatusDt(JavaTime.nowUtc());

        session.getTransaction().begin();
        session.update(query);
        session.getTransaction().commit();

        logger.info(String.format("Query %s: status = %s", query.getId(), status));

        return session.load(FSEFeederQuery.class, query.getId());
    }

    public static FSEFeederQuery createFlightLogByIdQuery(String accessKey, int fromId) {
        return createQuery(
                String.format(
                        "%s/data?userkey=%s&format=csv&query=flightlogs&search=id&readaccesskey=%s&fromid=%s",
                        getBaseUrl(),
                        accessKey,
                        accessKey,
                        fromId));
    }

    public static FSEFeederQuery createAircraftByMakeModelQuery(String accessKey, String makeModel) {
        try {
            return createQuery(
                    String.format(
                            "%s/data?userkey=%s&format=csv&query=aircraft&search=makemodel&makemodel=%s",
                            getBaseUrl(),
                            accessKey,
                            URLEncoder.encode(makeModel, "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static FSEFeederQuery createAssignmentsByIcaosQuery(String accessKey, Collection<String> icaos) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = icaos.iterator();
        while (it.hasNext()) {
            String icao = it.next();
            sb.append(icao);
            if (it.hasNext()) {
                sb.append("-");
            }
        }

        return createQuery(
                String.format(
                        "%s/data?userkey=%s&format=csv&query=icao&search=jobsfrom&icaos=%s",
                        getBaseUrl(),
                        accessKey,
                        sb.toString()));
    }

/*    public static FSEFeederQuery createAircraftForSaleQuery(String accesskey) throws SQLException {
        return createQuery(
                String.format(
                        "%s/data?userkey=%s&format=csv&query=aircraft&search=forsale",
                        getBaseUrl(),
                        accesskey));
    }*/

    private static String getBaseUrl() {
        return "http://server.fseconomy.net";
    }

    private static FSEFeederQuery createQuery(String url) {
        FSEFeederQuery query = new FSEFeederQuery();
        query.setStatus(FSEFeederQuery.Status.Pending);
        query.setStatusDt(JavaTime.nowUtc());
        query.setUrl(url);
        return query;
    }
}
