package net.simforge.fse;

import org.hibernate.SessionFactory;

public class FSEApp {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void setSessionFactory(SessionFactory sessionFactory) {
        FSEApp.sessionFactory = sessionFactory;
    }
}
