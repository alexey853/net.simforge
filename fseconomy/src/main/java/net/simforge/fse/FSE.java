package net.simforge.fse;

import net.simforge.commons.hibernate.SessionFactoryBuilder;
import net.simforge.fse.model.FSEAccount;
import net.simforge.fse.model.FSEAircraftConfig;
import net.simforge.fse.model.FSEFeederQuery;
import net.simforge.fse.model.FSEFlight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.time.Duration;

public class FSE {

    public static final Class[] entities = {
            FSEAccount.class,
            FSEAircraftConfig.class,
            FSEFeederQuery.class,
            FSEFlight.class
    };

    public static SessionFactory buildSessionFactory() {
        return SessionFactoryBuilder
                .forDatabase("fslog")
                .entities(entities)
                .build();
    }

    public static final DateTimeFormatter DTformat = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
    public static final java.time.format.DateTimeFormatter DTformatJT8 = java.time.format.DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    public static final DateTimeFormatter filenameTimestampFormat = DateTimeFormat.forPattern("yyyyMMddHHmmss");

    public static Duration parseFlightTime(String flightTime) {
        int separatorIndex = flightTime.indexOf(':');
        String hStr = flightTime.substring(0, separatorIndex);
        String mStr = flightTime.substring(separatorIndex + 1);
        int h = Integer.parseInt(hStr);
        int m = Integer.parseInt(mStr);
        return Duration.ofHours(h).plusMinutes(m);
    }

    public static FSEAircraftConfig findAircraftConfig(Session session, String makeModel) {
        //noinspection JpaQlInspection
        return (FSEAircraftConfig) session
                .createQuery("from FSEAircraftConfig where makeModel = :makeModel")
                .setString("makeModel", makeModel)
                .uniqueResult();
    }
}
