

CREATE SEQUENCE fse_feeder_query_id_seq;

CREATE TABLE fse_feeder_query
(
  id  int DEFAULT nextval('fse_feeder_query_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  status smallint NOT NULL,
  status_dt timestamp without time zone NOT NULL,
  url character varying(500),
  content text,
  CONSTRAINT pk_fse_feeder_query PRIMARY KEY (id)
);




ALTER TABLE fse_account
  ADD COLUMN feeder_query_id integer;

ALTER TABLE fse_account
  ADD CONSTRAINT fk_feeder_query_id FOREIGN KEY (feeder_query_id) REFERENCES fse_feeder_query (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
