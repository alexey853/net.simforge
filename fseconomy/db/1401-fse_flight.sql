
CREATE SEQUENCE fse_flight_id_seq;

CREATE TABLE fse_flight
(
  id int DEFAULT nextval('fse_flight_id_seq'::regclass) NOT NULL,
  version smallint,
  fse_id integer NOT NULL,
  dt timestamp without time zone NOT NULL,
  pilot_name character varying(20) NOT NULL,
  aircraft character varying(10) NOT NULL,
  make_model character varying(30) NOT NULL,
  from_airport character varying(5) NOT NULL,
  to_airport character varying(5) NOT NULL,
  flight_time character varying(5) NOT NULL,
  CONSTRAINT pk_fse_flight PRIMARY KEY (id),
  CONSTRAINT uk_fse_id UNIQUE (fse_id)
);
