
-- Sequence: fse_account_id_seq

-- DROP SEQUENCE fse_account_id_seq;

CREATE SEQUENCE fse_account_id_seq;
ALTER TABLE fse_account_id_seq
  OWNER TO postgres;


-- Table: fse_account

-- DROP TABLE fse_account;

CREATE TABLE fse_account
(
  id  int DEFAULT nextval('fse_account_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  pilot_name character varying(20) NOT NULL,
  access_key character varying(20) NOT NULL,
  next_time_dt timestamp without time zone,
  CONSTRAINT pk_fse_account PRIMARY KEY (id),
  CONSTRAINT uk_pilot_name UNIQUE (pilot_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fse_account
  OWNER TO postgres;


INSERT INTO fse_account(
            pilot_name, access_key, next_time_dt)
    VALUES ('Alexey853', 'XCHOXGAKWC', null);


-- Sequence: fse_flight_id_seq

-- DROP SEQUENCE fse_flight_id_seq;

CREATE SEQUENCE fse_flight_id_seq;
ALTER TABLE fse_flight_id_seq
  OWNER TO postgres;


-- Table: fse_flight

-- DROP TABLE fse_flight;

CREATE TABLE fse_flight
(
  id int DEFAULT nextval('fse_flight_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  fse_account_id integer NOT NULL,
  fse_id integer NOT NULL,
  dt timestamp without time zone NOT NULL,
  pilot_name character varying(20) NOT NULL,
  aircraft character varying(10) NOT NULL,
  make_model character varying(30) NOT NULL,
  from_airport character varying(5) NOT NULL,
  to_airport character varying(5) NOT NULL,
  flight_time character varying(5) NOT NULL,
  CONSTRAINT pk_fse_flight PRIMARY KEY (id),
  CONSTRAINT fk_fse_account_id FOREIGN KEY (fse_account_id)
      REFERENCES fse_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_fse_id UNIQUE (fse_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fse_flight
  OWNER TO postgres;


-- Sequence: fse_aircraft_config_id_seq

-- DROP SEQUENCE fse_aircraft_config_id_seq;

CREATE SEQUENCE fse_aircraft_config_id_seq;
ALTER TABLE fse_aircraft_config_id_seq
  OWNER TO postgres;


-- Table: fse_aircraft_config

-- DROP TABLE fse_aircraft_config;

CREATE TABLE fse_aircraft_config
(
  id int DEFAULT nextval('fse_aircraft_config_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  make_model character varying(100) NOT NULL,
  crew integer NOT NULL,
  seats integer NOT NULL,
  cruise_speed integer NOT NULL,
  gph integer NOT NULL,
  fuel_type integer NOT NULL,
  mtow integer NOT NULL,
  empty_weight integer NOT NULL,
  price real NOT NULL,
  ext1 integer NOT NULL,
  l_tip integer NOT NULL,
  l_aux integer NOT NULL,
  l_main integer NOT NULL,
  center1 integer NOT NULL,
  center2 integer NOT NULL,
  center3 integer NOT NULL,
  r_main integer NOT NULL,
  r_aux integer NOT NULL,
  r_tip integer NOT NULL,
  r_ext2 integer NOT NULL,
  engines integer NOT NULL,
  engine_price real NOT NULL,
  CONSTRAINT pk_fse_aircraft_config PRIMARY KEY (id),
  CONSTRAINT uk_make_model UNIQUE (make_model)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fse_aircraft_config
  OWNER TO postgres;
