
ALTER TABLE fslog_flight ADD aircraft_model_id int;

ALTER TABLE fslog_flight
      ADD CONSTRAINT fk_flight_aircraft_model_id FOREIGN KEY (aircraft_model_id)
      REFERENCES refdata_aircraft_makemodel (id);






CREATE SEQUENCE fslog_aircraft_model_fse_mapping_id_seq;

CREATE TABLE fslog_aircraft_model_fse_mapping
(
  id  int DEFAULT nextval('fslog_aircraft_model_fse_mapping_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  fse_aircraft_config_id integer NOT NULL,
  aircraft_model_id integer NOT NULL,
  CONSTRAINT pk_fslog_aircraft_model_fse_mapping PRIMARY KEY (id),
  CONSTRAINT fk_fse_aircraft_config_id FOREIGN KEY (fse_aircraft_config_id)
      REFERENCES fse_aircraft_config (id),
  CONSTRAINT fk_aircraft_model_id FOREIGN KEY (aircraft_model_id)
      REFERENCES refdata_aircraft_makemodel (id),
  CONSTRAINT uk_fslog_aircraft_model_mapping_fse_config_id UNIQUE (fse_aircraft_config_id)
);





CREATE OR REPLACE FUNCTION fslog_add_fse_aircraft_model_mapping(
  fse_make_model character varying(100),
  makemodel_id integer)
RETURNS void AS $$
DECLARE fse_id int;
DECLARE cnt int;
BEGIN
  fse_id := (SELECT id FROM fse_aircraft_config where make_model = fse_make_model);
  cnt := (SELECT count(id) FROM fslog_aircraft_model_fse_mapping where fse_aircraft_config_id = fse_id);
  IF (cnt = 0) THEN
    INSERT INTO fslog_aircraft_model_fse_mapping(fse_aircraft_config_id, aircraft_model_id) VALUES(fse_id, makemodel_id);
  END IF;
END;
$$ LANGUAGE plpgsql;
