
ALTER TABLE fslog_account ADD username character varying(20);
ALTER TABLE fslog_account ADD password character varying(50);

UPDATE fslog_account
    SET username = 'Alexey853',
        password = '1'
    WHERE id = 1;

ALTER TABLE fslog_account ALTER COLUMN username SET NOT NULL;
ALTER TABLE fslog_account ALTER COLUMN password SET NOT NULL;

ALTER TABLE fslog_account
      ADD CONSTRAINT uk_username UNIQUE (username);
