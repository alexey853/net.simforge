
CREATE TABLE fslog_flight_source
(
  id int NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  active boolean NOT NULL DEFAULT true,
  name character varying(100),
  CONSTRAINT pk_fslog_flight_source PRIMARY KEY (id)
);

ALTER TABLE fslog_flight_source
  OWNER TO postgres;


insert into fslog_flight_source(id, name) values(0, 'User');
insert into fslog_flight_source(id, name) values(1, 'VATSIM');
insert into fslog_flight_source(id, name) values(2, 'IVAO');
insert into fslog_flight_source(id, name) values(9, 'FSE');

ALTER TABLE fslog_flight ADD source_id int;

ALTER TABLE fslog_flight
      ADD CONSTRAINT fk_source_id FOREIGN KEY (source_id)
      REFERENCES fslog_flight_source (id);

update fslog_flight
    set source_id = 9,
        version = fslog_flight.version + 1
    from fslog_flight_fse
    where fslog_flight_fse.flight_id = fslog_flight.id;


update fslog_flight
    set source_id = 0,
        version = version + 1
    where source_id is null;

ALTER TABLE fslog_flight
    ALTER COLUMN source_id SET NOT NULL;
