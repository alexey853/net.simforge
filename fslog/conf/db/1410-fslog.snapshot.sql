
-- Sequence: fslog_account_id_seq

-- DROP SEQUENCE fslog_account_id_seq;

CREATE SEQUENCE fslog_account_id_seq;
ALTER TABLE fslog_account_id_seq
  OWNER TO postgres;

-- Table: fslog_account

-- DROP TABLE fslog_account;

CREATE TABLE fslog_account
(
  id  int DEFAULT nextval('fslog_account_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  fse_account_id integer,
  CONSTRAINT pk_fslog_account PRIMARY KEY (id),
  CONSTRAINT fk_fse_account_id FOREIGN KEY (fse_account_id)
      REFERENCES fse_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fslog_account
  OWNER TO postgres;


INSERT INTO fslog_account(
            fse_account_id)
    VALUES (1);



-- Sequence: fslog_aircraft_type_id_seq

-- DROP SEQUENCE fslog_aircraft_type_id_seq;

CREATE SEQUENCE fslog_aircraft_type_id_seq;
ALTER TABLE fslog_aircraft_type_id_seq
  OWNER TO postgres;

-- Table: fslog_aircraft_type

-- DROP TABLE fslog_aircraft_type;

CREATE TABLE fslog_aircraft_type
(
  id  int DEFAULT nextval('fslog_aircraft_type_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  icao_code character varying(4) NOT NULL,
  CONSTRAINT pk_fslog_aircraft_type PRIMARY KEY (id),
  CONSTRAINT uk_icao_code UNIQUE (icao_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fslog_aircraft_type
  OWNER TO postgres;



-- Sequence: fslog_aircraft_type_fse_mapping_id_seq

-- DROP SEQUENCE fslog_aircraft_type_fse_mapping_id_seq;

CREATE SEQUENCE fslog_aircraft_type_fse_mapping_id_seq;
ALTER TABLE fslog_aircraft_type_fse_mapping_id_seq
  OWNER TO postgres;

-- Table: fslog_aircraft_type_fse_mapping

-- DROP TABLE fslog_aircraft_type_fse_mapping;

CREATE TABLE fslog_aircraft_type_fse_mapping
(
  id  int DEFAULT nextval('fslog_aircraft_type_fse_mapping_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  fse_aircraft_config_id integer NOT NULL,
  aircraft_type_id integer NOT NULL,
  CONSTRAINT pk_fslog_aircraft_type_fse_mapping PRIMARY KEY (id),
  CONSTRAINT fk_fse_aircraft_config_id FOREIGN KEY (fse_aircraft_config_id)
      REFERENCES fse_aircraft_config (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_aircraft_type_id FOREIGN KEY (aircraft_type_id)
      REFERENCES fslog_aircraft_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_fse_aircraft_config_id UNIQUE (fse_aircraft_config_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fslog_aircraft_type_fse_mapping
  OWNER TO postgres;



-- Sequence: fslog_flight_id_seq

-- DROP SEQUENCE fslog_flight_id_seq;

CREATE SEQUENCE fslog_flight_id_seq;
ALTER TABLE fslog_flight_id_seq
  OWNER TO postgres;

-- Table: fslog_flight

-- DROP TABLE fslog_flight;

CREATE TABLE fslog_flight
(
  id  int DEFAULT nextval('fslog_flight_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  account_id integer NOT NULL,
  aircraft_type_id integer,
  tail_number character varying(10),
  flight_number character varying(10),
  callsign character varying(10),
  from_icao character varying(30),
  to_icao character varying(30),
  departure_utc timestamp without time zone,
  arrival_utc timestamp without time zone,
  flight_time character varying(5),
  comments character varying(10000),
  CONSTRAINT pk_fslog_flight PRIMARY KEY (id),
  CONSTRAINT fk_account_id FOREIGN KEY (account_id)
      REFERENCES fslog_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_aircraft_type_id FOREIGN KEY (aircraft_type_id)
      REFERENCES fslog_aircraft_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fslog_flight
  OWNER TO postgres;



-- Sequence: fslog_flight_fse_id_seq

-- DROP SEQUENCE fslog_flight_fse_id_seq;

CREATE SEQUENCE fslog_flight_fse_id_seq;
ALTER TABLE fslog_flight_fse_id_seq
  OWNER TO postgres;

-- Table: fslog_flight_fse

-- DROP TABLE fslog_flight_fse;

CREATE TABLE fslog_flight_fse
(
  id  int DEFAULT nextval('fslog_flight_fse_id_seq'::regclass) NOT NULL,
  version smallint DEFAULT 0 NOT NULL,
  fse_flight_id integer NOT NULL,
  flight_id integer NOT NULL,
  CONSTRAINT pk_fslog_flight_fse PRIMARY KEY (id),
  CONSTRAINT fk_fse_flight_id FOREIGN KEY (fse_flight_id)
      REFERENCES fse_flight (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_flight_id FOREIGN KEY (flight_id)
      REFERENCES fslog_flight (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_fse_flight_id UNIQUE (fse_flight_id),
  CONSTRAINT uk_flight_id UNIQUE (flight_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fslog_flight_fse
  OWNER TO postgres;


