--FSE Make Model,Icao Code


--Airbus A318,A318
--Airbus A319,A319
--Airbus A320,A320
--Airbus A321,A321


--ATR 72-500,AT75


select fslog_add_fse_aircraft_model_mapping('Beechcraft Baron 58', 12002);
select fslog_add_fse_aircraft_model_mapping('Beechcraft Baron 58 - tip tanks (Dreamfleet)', 12002);
select fslog_add_fse_aircraft_model_mapping('Beechcraft King Air 350', 12001);


select fslog_add_fse_aircraft_model_mapping('Boeing Vertol CH-47 Chinook', 20700);


select fslog_add_fse_aircraft_model_mapping('Cessna 172 Skyhawk', 10004);
select fslog_add_fse_aircraft_model_mapping('Cessna 182 Skylane', 10006);
select fslog_add_fse_aircraft_model_mapping('Cessna 206 Stationair', 10011);
select fslog_add_fse_aircraft_model_mapping('Cessna 207 Stationair 8', 10012);
select fslog_add_fse_aircraft_model_mapping('Cessna 208 Caravan', 10013);
select fslog_add_fse_aircraft_model_mapping('Cessna 210 Centurion', 10014);
--Cessna 310,C310
select fslog_add_fse_aircraft_model_mapping('Cessna 404 Titan', 10016);
select fslog_add_fse_aircraft_model_mapping('Cessna Citation X', 10015);


--Diamond DA20 Katana,DV20

--Dornier Do-27 A4,DO27
--Dornier Do-27 B1,DO27


select fslog_add_fse_aircraft_model_mapping('Embraer ERJ-135LR', 31502);
select fslog_add_fse_aircraft_model_mapping('Embraer ERJ-145LR', 31504);


select fslog_add_fse_aircraft_model_mapping('Lear 45', 30007);


select fslog_add_fse_aircraft_model_mapping('Piper PA-28 Warrior', 11303);

--Robin DR400,DR40
