package net.simforge.fslog;

import net.simforge.fse.FSEApp;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FSLogApp {
    private static Logger logger = LoggerFactory.getLogger(FSLogApp.class.getName());

    private static SessionFactory sessionFactory;

    public static class StartupAction implements Runnable {
        @Override
        public void run() {
            logger.info("creating session factory");
            sessionFactory = FSLog.buildSessionFactory();
            FSEApp.setSessionFactory(sessionFactory);
        }
    }

    public static class ShutdownAction implements Runnable {
        @Override
        public void run() {
            logger.info("killing session factory");
            SessionFactory _sessionFactory = sessionFactory;
            sessionFactory = null;
            FSEApp.setSessionFactory(null);
            _sessionFactory.close();
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
