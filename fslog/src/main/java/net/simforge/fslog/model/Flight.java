package net.simforge.fslog.model;

import net.simforge.refdata.aircraft.model.AircraftMakeModel;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "fslog_flight")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fslog_flight_id_seq")
    @SequenceGenerator(name = "fslog_flight_id_seq", sequenceName = "fslog_flight_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;
    @Column
    private Boolean active;


    @Column(name = "source_id")
    private Integer sourceId;
    @Column(name = "account_id")
    private Integer accountId;
    @Column(name = "aircraft_type_id")
    private Integer aircraftTypeId;
    @ManyToOne
    @JoinColumn(name = "aircraft_model_id")
    private AircraftMakeModel aircraftModel;
    @Column(name = "tail_number")
    private String tailNumber;
    @Column(name = "flight_number")
    private String flightNumber;
    @Column(name = "callsign")
    private String callsign;
    @Column(name = "from_icao")
    private String fromIcao;
    @Column(name = "to_icao")
    private String toIcao;
    @Column(name = "departure_utc")
    private LocalDateTime departureUtc;
    @Column(name = "arrival_utc")
    private LocalDateTime arrivalUtc;
    @Column(name = "flight_time")
    private String flightTime;
    @Column(name = "comment", length = 10000)
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getAircraftTypeId() {
        return aircraftTypeId;
    }

    public void setAircraftTypeId(Integer aircraftTypeId) {
        this.aircraftTypeId = aircraftTypeId;
    }

    public AircraftMakeModel getAircraftModel() {
        return aircraftModel;
    }

    public void setAircraftModel(AircraftMakeModel aircraftModel) {
        this.aircraftModel = aircraftModel;
    }

    public String getTailNumber() {
        return tailNumber;
    }

    public void setTailNumber(String tailNumber) {
        this.tailNumber = tailNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getFromIcao() {
        return fromIcao;
    }

    public void setFromIcao(String fromIcao) {
        this.fromIcao = fromIcao;
    }

    public String getToIcao() {
        return toIcao;
    }

    public void setToIcao(String toIcao) {
        this.toIcao = toIcao;
    }

    public LocalDateTime getDepartureUtc() {
        return departureUtc;
    }

    public void setDepartureUtc(LocalDateTime departureUtc) {
        this.departureUtc = departureUtc;
    }

    public LocalDateTime getArrivalUtc() {
        return arrivalUtc;
    }

    public void setArrivalUtc(LocalDateTime arrivalUtc) {
        this.arrivalUtc = arrivalUtc;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
