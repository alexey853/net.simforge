package net.simforge.fslog.model;

import javax.persistence.*;

@Deprecated
@Entity
@Table(name = "fslog_aircraft_type")
public class AircraftType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fslog_aircraft_type_id_seq")
    @SequenceGenerator(name = "fslog_aircraft_type_id_seq", sequenceName = "fslog_aircraft_type_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column(name = "icao_code")
    private String icaoCode;
    @Column
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AircraftType{" +
                "icaoCode='" + icaoCode + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
