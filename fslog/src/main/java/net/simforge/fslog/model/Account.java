package net.simforge.fslog.model;

import javax.persistence.*;

@Entity
@Table(name = "fslog_account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fslog_account_id_seq")
    @SequenceGenerator(name = "fslog_account_id_seq", sequenceName = "fslog_account_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column
    private String username;
    @Column
    private String password;
    @Column(name = "fse_account_id")
    private Integer fseAccountId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getFseAccountId() {
        return fseAccountId;
    }

    public void setFseAccountId(Integer fseAccountId) {
        this.fseAccountId = fseAccountId;
    }
}
