package net.simforge.fslog.model;

import net.simforge.refdata.aircraft.model.AircraftMakeModel;

import javax.persistence.*;

@Entity
@Table(name = "fslog_aircraft_model_fse_mapping")
public class AircraftModelFSEMapping {
    @Id
    private Integer id;
    @Version
    private Integer version;

    @Column(name = "fse_aircraft_config_id")
    private Integer fseAircraftConfigId;
    @ManyToOne
    @JoinColumn(name = "aircraft_model_id")
    private AircraftMakeModel aircraftModel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getFseAircraftConfigId() {
        return fseAircraftConfigId;
    }

    public void setFseAircraftConfigId(Integer fseAircraftConfigId) {
        this.fseAircraftConfigId = fseAircraftConfigId;
    }

    public AircraftMakeModel getAircraftModel() {
        return aircraftModel;
    }

    public void setAircraftModel(AircraftMakeModel aircraftModel) {
        this.aircraftModel = aircraftModel;
    }
}
