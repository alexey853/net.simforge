package net.simforge.fslog.model;

import javax.persistence.*;

@Entity
@Table(name = "fslog_flight_fse")
public class Flight2FSE {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fslog_flight_fse_id_seq")
    @SequenceGenerator(name = "fslog_flight_fse_id_seq", sequenceName = "fslog_flight_fse_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;


    @Column(name = "fse_flight_id")
    private Integer fseFlightId;
    @Column(name = "flight_id")
    private Integer flightId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getFseFlightId() {
        return fseFlightId;
    }

    public void setFseFlightId(Integer fseFlightId) {
        this.fseFlightId = fseFlightId;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }
}
