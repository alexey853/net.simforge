package net.simforge.fslog;

import net.simforge.commons.hibernate.SessionFactoryBuilder;
import net.simforge.fse.FSE;
import net.simforge.fslog.model.*;
import net.simforge.refdata.aircraft.AircraftRefData;
import net.simforge.refdata.aircraft.model.AircraftMakeModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.format.DateTimeFormatter;

public class FSLog {

    public static final Class[] entities = {
            Account.class,
            Flight.class,
            AircraftModelFSEMapping.class,
            AircraftType.class,
            Flight2FSE.class
    };

    public static final DateTimeFormatter ddmmyyyy = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static SessionFactory buildSessionFactory() {
        return SessionFactoryBuilder
                .forDatabase("fslog")
                .entities(FSE.entities)
                .entities(AircraftRefData.entities)
                .entities(entities)
                .build();
    }

    public enum Source {
        User  (0, "User"),
        VATSIM(1, "VATSIM"),
        IVAO  (2, "IVAO"),
        FSE   (9, "FSE");

        private int id;
        private String aName;

        Source(int id, String aName) {
            this.id = id;
            this.aName = aName;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return aName;
        }

        public static Source byId(int sourceId) {
            for (Source source : Source.values()) {
                if (source.getId() == sourceId) {
                    return source;
                }
            }

            return null;
        }
    }

    public static AircraftMakeModel findAircraftModel(Session session, Integer modelId) {
        return session.get(AircraftMakeModel.class, modelId);
    }

    public static Account findAccountByFseAccountId(Session session, int fseAccountId) {
        //noinspection JpaQlInspection,UnnecessaryLocalVariable
        Account account = (Account) session
                .createQuery("from Account " +
                        "where fseAccountId = :fseAccountId")
                .setInteger("fseAccountId", fseAccountId)
                .uniqueResult();
        return account;
    }
}
