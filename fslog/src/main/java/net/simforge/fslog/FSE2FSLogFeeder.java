package net.simforge.fslog;

import net.simforge.commons.io.Marker;
import net.simforge.commons.legacy.misc.Mailer;
import net.simforge.commons.legacy.misc.Settings;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Str;
import net.simforge.commons.runtime.BaseTask;
import net.simforge.fse.FSE;
import net.simforge.fse.model.FSEAircraftConfig;
import net.simforge.fse.model.FSEFlight;
import net.simforge.fslog.model.*;
import org.hibernate.Session;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FSE2FSLogFeeder extends BaseTask {

    private Session session;

    private final String email;
    private final String emailTag;

    public FSE2FSLogFeeder() {
        super("FSE2FSLogFeeder");

        setBaseSleepTime(TimeUnit.SECONDS.toMillis(10));

        email = Settings.get("fslog.email");
        emailTag = Str.mn(Settings.get("fslog.email.tag"));
    }

    @Override
    protected void process() {
        try (Session session = FSLogApp.getSessionFactory().openSession()) {
            this.session = session;

            Flight2FSE lastProcessedFlight2FSE = loadLastProcessedFlight2FSE();

            FSEFlight nextFSEFlight;
            if (lastProcessedFlight2FSE == null) {
                nextFSEFlight = loadFirstFSEFlight();
            } else {
                nextFSEFlight = loadNextFSEFlight(lastProcessedFlight2FSE.getFseFlightId());
            }

            if (nextFSEFlight != null) {
                process(nextFSEFlight);

                setNextSleepTime(10);
            }
        } catch (Exception e) {
            logger.error("Error during processing", e);
            setNextSleepTime(TimeUnit.MINUTES.toMillis(1));
        }
    }

    private void process(FSEFlight fseFlight) {
        logger.info(String.format("FSE Flight: id %s, pilot %s, type %s, date %s, route %s-%s",
                fseFlight.getId(), fseFlight.getPilotName(), fseFlight.getMakeModel(),
                fseFlight.getDt(), fseFlight.getFromAirport(), fseFlight.getToAirport()));

        int fseAccountId = fseFlight.getFseAccountId();

        Account account = FSLog.findAccountByFseAccountId(session, fseAccountId);
        if (account == null) {
            throw new IllegalStateException("Unable to find account by FSE account " + fseAccountId);
        }

        Flight2FSE flight2fse = findFlight2FSEByFseFlightId(fseFlight.getId());
        if (flight2fse != null) {
            logger.warn(String.format("FSE Flight %s is already processed", fseFlight.getId()));
            return;
        }

        String fseMakeModel = fseFlight.getMakeModel();
        FSEAircraftConfig fseAircraftConfig = FSE.findAircraftConfig(session, fseMakeModel);
        if (fseAircraftConfig == null) {
            String msg = String.format("Unable to find FSE Make Model '%s'", fseMakeModel);
            sendEmail(msg);
            throw new IllegalStateException(msg);
        }

        AircraftModelFSEMapping aircraftModelMapping = findAircraftModelFSEMapping(fseAircraftConfig);
        if (aircraftModelMapping == null) {
            String msg = String.format("Unable to find mapping for FSE Make Model '%s'", fseMakeModel);
            sendEmail(msg);
            throw new IllegalStateException(msg);
        }

        Flight flight = new Flight();
        flight.setActive(true);
        flight.setSourceId(FSLog.Source.FSE.getId());
        flight.setAccountId(account.getId());
        flight.setAircraftModel(aircraftModelMapping.getAircraftModel());
        flight.setTailNumber(fseFlight.getAircraft());
        flight.setFromIcao(fseFlight.getFromAirport());
        flight.setToIcao(fseFlight.getToAirport());

        // Conversion from FSE time fields to FSLog time fields
        LocalDateTime arrivalUtc = fseFlight.getDt();
        Duration flightTime = FSE.parseFlightTime(fseFlight.getFlightTime());
        LocalDateTime departureUtc = arrivalUtc.minus(flightTime);

        flight.setDepartureUtc(departureUtc);
        flight.setArrivalUtc(arrivalUtc);
        flight.setFlightTime(JavaTime.toHhmm(flightTime));

        session.getTransaction().begin();

        session.save(flight);

        flight2fse = new Flight2FSE();
        flight2fse.setFseFlightId(fseFlight.getId());
        flight2fse.setFlightId(flight.getId());

        session.save(flight2fse);

        session.getTransaction().commit();

        logger.info("            added");
    }

    private void sendEmail(String msg) {
        if (email == null) {
            return;
        }

        Marker marker = new Marker("FSE2FSLogFeeder-lastEmailDt");
        Date date = marker.getDate();
        long time = date != null ? date.getTime() : 0;
        if (time + 4 * 3600 * 1000 > System.currentTimeMillis()) {
            return;
        }
        marker.setDate(new Date());

        String subject = "FSE2FSLogFeeder";
        String body = msg;
        try {
            logger.warn("Sending email with subject '{}'", subject);
            Mailer.sendPlainText(email, emailTag + " " + subject, body);
            logger.warn("Email sent");
        } catch (Throwable t) {
            logger.warn("Error on email processing", t);
        }
    }

    private AircraftModelFSEMapping findAircraftModelFSEMapping(FSEAircraftConfig fseAircraftConfig) {
        //noinspection JpaQlInspection
        return (AircraftModelFSEMapping) session
                .createQuery("from AircraftModelFSEMapping where fseAircraftConfigId = :fseAircraftConfigId")
                .setInteger("fseAircraftConfigId", fseAircraftConfig.getId())
                .uniqueResult();
    }

    private Flight2FSE loadLastProcessedFlight2FSE() {
        //noinspection JpaQlInspection
        return (Flight2FSE) session
                .createQuery("from Flight2FSE order by fseFlightId desc")
                .setMaxResults(1)
                .uniqueResult();
    }

    private Flight2FSE findFlight2FSEByFseFlightId(Integer fseFlightId) {
        //noinspection JpaQlInspection
        return (Flight2FSE) session
                .createQuery("from Flight2FSE where fseFlightId = :fseFlightId")
                .setInteger("fseFlightId", fseFlightId)
                .uniqueResult();
    }

    private FSEFlight loadFirstFSEFlight() {
        //noinspection JpaQlInspection
        return (FSEFlight) session
                .createQuery("from FSEFlight order by id asc")
                .setMaxResults(1)
                .uniqueResult();
    }

    private FSEFlight loadNextFSEFlight(int lastFSEFlightId) {
        //noinspection JpaQlInspection
        return (FSEFlight) session
                .createQuery("from FSEFlight where id > :lastFseFlightId order by id asc")
                .setInteger("lastFseFlightId", lastFSEFlightId)
                .setMaxResults(1)
                .uniqueResult();
    }
}
