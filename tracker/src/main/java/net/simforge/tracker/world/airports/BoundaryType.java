package net.simforge.tracker.world.airports;

public enum BoundaryType {
    Default,
    Circles,
    Polygon
}
