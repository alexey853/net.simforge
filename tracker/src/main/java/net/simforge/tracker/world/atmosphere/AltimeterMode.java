package net.simforge.tracker.world.atmosphere;

public enum AltimeterMode {
    STD, QNH, QFE
}
