package net.simforge.tracker.world.atmosphere;

public class Atmosphere {
    public static final int QNH_STD = 1013;
    public static final double QNH_STD_PRECISE = 1013.25;
}
