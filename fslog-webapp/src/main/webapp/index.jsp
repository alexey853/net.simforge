<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FSLog</title>

    <meta name="description" content="FSLog"/>

    <link rel="stylesheet" type="text/css" href="extjs/classic/theme-crisp/resources/theme-crisp-all.css">
    <link rel="stylesheet" type="text/css" href="fslog.css">

    <script type="text/javascript" src="extjs/ext-all.js"></script>
    <script type="text/javascript" src="extjs/classic/theme-crisp/theme-crisp.js"></script>

    <script type="text/javascript" src="charts/charts.js"></script>
    <link rel="stylesheet" type="text/css"  href="charts/crisp/resources/charts-all.css">

    <script type="text/javascript" src="js/_includes.js"></script>
    <script type="text/javascript" src="js/Auth.js"></script>
    <script type="text/javascript" src="js/CustomVTypes.js"></script>
    <script type="text/javascript" src="js/Flight.js"></script>
    <script type="text/javascript" src="js/FlightsGrid.js"></script>
    <script type="text/javascript" src="js/StatisticsPieChart.js"></script>
    <script type="text/javascript" src="js/StatisticsTab.js"></script>
    <script type="text/javascript" src="js/StatisticsTabController.js"></script>
    <script type="text/javascript" src="js/IntegrationsTab.js"></script>
    <%--<script type="text/javascript" src="js/FlightDetailsTab.js"></script>--%>
    <%--<script type="text/javascript" src="js/FlightDetailsTabController.js"></script>--%>
    <script type="text/javascript" src="js/_app.js"></script>
</head>
<body>
    <!--%@ include file="google-analytics.jsp" %-->
</body>
</html>
