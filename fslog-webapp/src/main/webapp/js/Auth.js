Ext.define('Auth', {
    getUIItems: function () {
        var that = this;
        return [
            {
                id: 'auth-user-label',
                xtype: 'label',
                text: '',
                margin: '0 10 0 0'
            },
            {
                id: 'auth-login-button',
                xtype: 'button',
                text: 'Login',
                handler: that.showLoginDialog,
                scope: that
            },
            {
                id: 'auth-logout-button',
                xtype: 'button',
                text: 'Logout',
                hidden: true,
                handler: that.doLogout,
                scope: that
            }
        ];
    },

    showLoginDialog: function () {
        var that = this;
        this.loginWindow = Ext.create('Ext.window.Window', {
            title: 'Login',
            // height: 200,
            width: 320,
            layout: 'fit',
            modal: true,
            items: [
                {
                    xtype: 'form',
                    border: false,
                    frame: true,

                    bodyPadding: 10,

                    defaultType: 'textfield',

                    items: [{
                        allowBlank: false,
                        fieldLabel: 'Username',
                        name: 'username',
                        emptyText: 'user name'
                    }, {
                        allowBlank: false,
                        fieldLabel: 'Password',
                        name: 'password',
                        emptyText: 'password',
                        inputType: 'password'
                    }/*, {
                     xtype:'checkbox',
                     fieldLabel: 'Remember me',
                     name: 'remember'
                     }*/],

                    buttons: [
                        {
                            text: 'Register'
                        }, {
                            text: 'Login',
                            handler: that.doLogin,
                            scope: that
                        }
                    ],

                    defaults: {
                        anchor: '100%',
                        labelWidth: 120
                    }
                }
            ]
        });
        this.loginWindow.show();
    },

    doLogin: function() {
        var username = this.loginWindow.down('textfield[name=username]').getValue();
        var password = this.loginWindow.down('textfield[name=password]').getValue();

        this.loginWindow.hide();
        Ext.getBody().mask('Logging in...');

        var that = this;
        Ext.Ajax.request({
            url: 'rest/auth/login',
            method: 'POST',
            params: {
                username: username,
                password: password
            },
            timeout: 120000,
            success: function (response) {
                Ext.getBody().unmask();

                var text = response.responseText;
                var result = Ext.decode(text);
                if (result.success) {
                    that.loginWindow.destroy();

                    that.onSuccessfulLogin(result.status);
                } else {
                    Ext.MessageBox.alert('Error', result.message, function () {
                        that.loginWindow.show();
                    });
                }
            },
            failure: function (response) {
                Ext.getBody().unmask();

                var text = response.responseText;
                Ext.MessageBox.alert('Error', text, function() {
                    that.loginWindow.show();
                });
            }
        });
    },

    doLogout: function() {
        Ext.getBody().mask('Logging out...');

        var that = this;
        Ext.Ajax.request({
            url: 'rest/auth/logout',
            method: 'POST',
            timeout: 120000,
            success: function (response) {
                Ext.getBody().unmask();

                var text = response.responseText;
                var result = Ext.decode(text);
                if (result.success) {
                    that.onSuccessfulLogout(result.status);
                } else {
                    Ext.MessageBox.alert('Error', result.message);
                }
            },
            failure: function (response) {
                Ext.getBody().unmask();

                var text = response.responseText;
                Ext.MessageBox.alert('Error', text);
            }
        });
    },

    onSuccessfulLogin: function (status) {
        Ext.getCmp('auth-login-button').hide();
        Ext.getCmp('auth-logout-button').show();
        Ext.getCmp('auth-user-label').setText(status.username);

        Ext.getCmp('main-panel').setActiveItem(1);
    },

    onSuccessfulLogout: function (status) {
        Ext.getCmp('main-panel').setActiveItem(0);

        Ext.getCmp('auth-login-button').show();
        Ext.getCmp('auth-logout-button').hide();
        Ext.getCmp('auth-user-label').setText('');
    },

    loadStatus: function (callback) {
        Ext.Ajax.request({
            url: 'rest/auth/status',
            method: 'POST',
            timeout: 120000,
            success: function (response) {
                var text = response.responseText;
                var result = Ext.decode(text);
                if (result.success) {
                    callback(result.status);
                } else {
                    Ext.MessageBox.alert('Error', result.message);
                }
            },
            failure: function (response) {
                var text = response.responseText;
                Ext.MessageBox.alert('Error', text);
            }
        });
    }
});
