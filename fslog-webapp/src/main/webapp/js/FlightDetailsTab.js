Ext.define('FlightDetailsTab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.flight-details-tab',

    layout: {
        type: 'border'
    },

    defaults: {
        border: true
    },

    items: [
        { // prev button panel
            region: 'west',
            width: 30,
            html: 'prev'
        }, { // next button panel
            region: 'east',
            width: 30,
            html: 'next'
        },
        { // central area
            region: 'center',
            layout: {
                type: 'vbox'
            },
            defaults: {
                border: true,
                // margin: '20 20 20 20'
                padding: '20 20 20 20',
                width: '100%'
            },
            items: [
                { // header
                    layout: {
                        type: 'table',
                        columns: 3
                    },
                    items: [
                        {
                            region: 'west',
                            html: '10 May 2017',
                            width: 100
                            // align: 'left'
                        }, {
                            region: 'center',
                            html: 'HK-ZCK',
                            width: 200
                            // align: 'center'
                        }, {
                            region: 'east',
                            html: 'EDBJ - EDDK',
                            width: 100
                            // align: 'right'
                        }
                    ]
                }, { // info & map
                    html: 'From  -  To<br>Aicraft<br>Reg No<br>DepTime<br>ArrTime<br>FlightTime<br>TrackDistance<br>MAP'
                }, { // comments
                    xtype: 'textarea',
                    layout: 'fit',
                    height: 70
                }
            ]
        }
    ]
});
