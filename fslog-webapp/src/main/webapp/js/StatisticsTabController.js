Ext.define('StatisticsTabController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.statistics-tab',

    currentPeriod: 'last-year',

    handleInit: function () {
        this.togglePeriodButtons();
    },

    togglePeriodButtons: function () {
        var period = this.currentPeriod;

        var buttons = this.lookupReference('charts-panel').query('button');
        Ext.each(buttons, function (each) {
            each.suspendEvents(false);
            each.setPressed(period == each.period);
            each.resumeEvents();
        });
    },

    onPeriodToggle: function (button, pressed) {
        this.currentPeriod = button.period;
        this.togglePeriodButtons(button);

        this.refresh();
    },

    refresh: function () {
        var period = this.currentPeriod;

        var chartsPanel = this.lookupReference('charts-panel');
        Ext.each(chartsPanel.items.items, function (each) {
            var polarChart = each.down('#polar-chart');
            polarChart.getStore().load({
                params: {
                    period: period
                }
            });
        });
    }
});
