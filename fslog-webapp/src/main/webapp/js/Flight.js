Ext.define('Flight', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'dateOfFlight', type: 'date', dateFormat: 'd/m/Y' },
        'aircraftTypeId',
        'aircraftModelId',
        'aircraftRegNo',
        'flightNo',
        'callsign',
        'departedFrom',
        'atd',
        'arrivedTo',
        'ata',
        'flightTime',
        'vatsimStatus',
        'ivaoStatus',
        'peStatus',
        'fseStatus',
        'validationStatus',
        'comment'
    ]
});
