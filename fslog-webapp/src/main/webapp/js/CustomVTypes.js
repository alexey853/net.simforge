
Ext.define('Override.form.field.VTypes', {
    override: 'Ext.form.field.VTypes',

    // vtype validation function
    time24: function(value) {
        return this.time24Re.test(value);
    },
    // RegExp for the value to be tested against within the validation function
    time24Re: /^(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$/i,
    // vtype Text property: The error text to display when the validation function returns false
    time24Text: 'Not a valid time.  Must be in the format "23:59".',
    // vtype Mask property: The keystroke filter mask
    time24Mask: /[\d\s:amp]/i
});
