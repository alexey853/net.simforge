Ext.define('StatisticsPieChart', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.statistics-pie-chart',

    constructor: function (config) {
        var url = config.url;
        var angleField = config.angleField;

        Ext.apply(config, {
            width: 800,
            height: 500,
            layout: 'fit',
            items: [
                {
                    xtype: 'polar',
                    itemId: 'polar-chart',
                    insetPadding: 40,
                    innerPadding: 20,
                    store: {
                        autoLoad: false,
                        proxy: {
                            type: 'ajax',
                            api: {
                                read: url
                            },
                            reader: {
                                type: 'json',
                                rootProperty: 'data'
                            }
                        }
                    },
                    legend: {
                        docked: 'right'
                    },
                    series: [
                        {
                            type: 'pie',
                            angleField: angleField,
                            label: {
                                field: 'category',
                                calloutLine: {
                                    length: 40,
                                    width: 2
                                }
                            },
                            highlight: true,
                            tooltip: {
                                trackMouse: true,
                                renderer: function (tooltip, record, item) {
                                    tooltip.setHtml(record.get('tooltip'));
                                }
                            }
                        }
                    ]
                }
            ]
        });

        this.callParent([config]);
    }
});
