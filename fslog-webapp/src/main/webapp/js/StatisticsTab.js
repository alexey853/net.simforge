Ext.define('StatisticsTab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.statistics-tab',
    controller: 'statistics-tab',

    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },

    autoScroll: true,

    items: [
        {
            reference: 'charts-panel',
            itemId: 'charts-panel',

            layout: {
                type: 'vbox',
                align: 'center',
                pack: 'center'
            },

            defaults: {
                border: true,
                margin: '10 10 10 10'
            },

            autoScroll: true,

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'left',

                defaults: {
                    enableToggle: true
                },

                items: [
                    {
                        xtype: 'button',
                        period: 'total',
                        text: 'Total',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    },
                    {
                        xtype: 'button',
                        period: 'last-year',
                        text: 'Last year',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2018',
                        text: '2018',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2017',
                        text: '2017',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2016',
                        text: '2016',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2015',
                        text: '2015',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2014',
                        text: '2014',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2013',
                        text: '2013',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2012',
                        text: '2012',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2011',
                        text: '2011',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2010',
                        text: '2010',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2009',
                        text: '2009',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2008',
                        text: '2008',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2007',
                        text: '2007',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2006',
                        text: '2006',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2005',
                        text: '2005',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2004',
                        text: '2004',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2003',
                        text: '2003',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2002',
                        text: '2002',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2001',
                        text: '2001',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }, {
                        xtype: 'button',
                        period: '2000',
                        text: '2000',
                        scale: 'medium',
                        listeners: {
                            toggle: 'onPeriodToggle'
                        }
                    }
                ]
            }],

            items: [
                {
                    xtype: 'statistics-pie-chart',
                    title: 'Time / Flights by Aircraft ICAO',
                    url: 'rest/statistics/byAircraftICAO',
                    angleField: 'hours'
                },
                {
                    xtype: 'statistics-pie-chart',
                    title: 'Time / Flights by Aircraft Family',
                    url: 'rest/statistics/byAircraftFamily',
                    angleField: 'hours'
                },
                {
                    xtype: 'statistics-pie-chart',
                    title: 'Time / Flights by Aircraft Manufacturer',
                    url: 'rest/statistics/byAircraftManufacturer',
                    angleField: 'hours'
                },
                {
                    xtype: 'statistics-pie-chart',
                    title: 'Takeoffs / Landings by ICAO code',
                    url: 'rest/statistics/byIcaoRegion',
                    angleField: 'total'
                },
                {
                    xtype: 'statistics-pie-chart',
                    title: 'Time / Flights by Source',
                    url: 'rest/statistics/bySource',
                    angleField: 'hours'
                }
            ]
        }
    ],

    onTabShown: function () {
        this.getController().handleInit();
        this.getController().refresh();
    }
});
