Ext.define('FlightsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.flights-grid',

    initComponent: function () {
        var me = this;

        this.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });

        this.aircraftTypeStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'icaoCode'],
            autoLoad: false,
            proxy: {
                type: 'ajax',
                api: {
                    read: 'rest/aircraft-types/list'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners: {
                load: function() {
                    me.getView().refresh();
                }
            }
        });

        this.aircraftModelStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'level', 'levelPx', 'name', 'icao'],
            autoLoad: false,
            proxy: {
                type: 'ajax',
                api: {
                    read: 'rest/aircraft-models/list'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners: {
                load: function() {
                    me.getView().refresh();
                }
            }
        });

        Ext.apply(this, {
            title: 'Flights',
            frame: true,

            plugins: [this.rowEditing],

            store: {
                model: 'Flight',
                autoLoad: false,
                autoSync: true,
                proxy: {
                    type: 'ajax',
                    api: {
                        read: 'rest/flights/list',
                        create: 'rest/flights/create',
                        update: 'rest/flights/update',
                        destroy: 'rest/flights/delete'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'data'
                    },
                    writer: {
                        type: 'json',
                        writeAllFields: true
                    },
                    listeners: {
                        exception: function (store, request, operation, eOpts) {
                            var response = Ext.decode(request.responseText);
                            Ext.Msg.alert("Error", response.message);
                        }
                    }
                }
            },

            sortableColumns: false,
            enableColumnHide: false,

            columns: {
                items: [
                    {
                        header: 'DOF',
                        xtype: 'datecolumn',
                        dataIndex: 'dateOfFlight',
                        format: 'd/m/Y',
                        width: 120,
                        editor: {
                            xtype: 'datefield',
                            allowBlank: false,
                            format: 'd/m/Y'
                        }
                    },
                    {
                        header: 'AIRCRAFT',
                        columns: [
                            {
                                header: 'Type',
                                dataIndex: 'aircraftTypeId',
                                sortable: false,
                                width: 80,
                                editor: {
                                    xtype: 'combo',
                                    store: this.aircraftTypeStore,
                                    displayField: 'icaoCode',
                                    valueField: 'id',
                                    allowBlank: true
                                },
                                renderer: function (value) {
                                    var idx = me.aircraftTypeStore.findExact('id', value);
                                    var rec = me.aircraftTypeStore.getAt(idx);
                                    return rec === null ? '' : rec.get('icaoCode');
                                }
                            },
                            {
                                header: 'Model',
                                dataIndex: 'aircraftModelId',
                                sortable: false,
                                width: 80,
                                editor: {
                                    xtype: 'combo',
                                    store: this.aircraftModelStore,
                                    displayField: 'icao',
                                    valueField: 'id',
                                    allowBlank: true,
                                    matchFieldWidth: false,
                                    listConfig: {
                                        width: 400,
                                        itemTpl: [
                                            '<div><span style="width: 50px; display: inline-block;">{icao}</span><span style="width: {levelPx}px; display: inline-block;"></span><span>{name}</span></div>'
                                        ]
                                    }
                                },
                                renderer: function (value) {
                                    var idx = me.aircraftModelStore.findExact('id', value);
                                    var rec = me.aircraftModelStore.getAt(idx);
                                    if (rec == null) {
                                        return '';
                                    }
                                    var icao = rec.get('icao');
                                    var name = rec.get('name');
                                    return icao || name;
                                }
                            },
                            {
                                header: 'Tail #',
                                dataIndex: 'aircraftRegNo',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: true
                                }
                            }
                        ]
                    },
                    {
                        header: 'FLIGHT DETAILS',
                        columns: [
                            {
                                header: 'Flight #',
                                dataIndex: 'flightNo',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: true
                                }
                            },
                            {
                                header: 'Callsign',
                                dataIndex: 'callsign',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: true
                                }
                            }
                        ]
                    },
                    {
                        header: 'ROUTE',
                        columns: [
                            {
                                header: 'From',
                                dataIndex: 'departedFrom',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: true
                                }
                            },
                            {
                                header: 'To',
                                dataIndex: 'arrivedTo',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: true
                                }
                            }
                        ]
                    },
                    {
                        header: 'TIME',
                        columns: [
                            {
                                header: 'ATD',
                                dataIndex: 'atd',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: false,
                                    vtype: 'time24',
                                    listeners: {
                                        change: function (cmp, newValue, oldValue, eOpts) {
                                            me.updateFlightTime();
                                        }
                                    }
                                }
                            },
                            {
                                header: 'ATA',
                                dataIndex: 'ata',
                                sortable: false,
                                width: 80,
                                editor: {
                                    allowBlank: false,
                                    vtype: 'time24',
                                    listeners: {
                                        change: function (cmp, newValue, oldValue, eOpts) {
                                            me.updateFlightTime();
                                        }
                                    }
                                }
                            },
                            {
                                header: 'Flight<br/>Time',
                                dataIndex: 'flightTime',
                                width: 80,
                                editor: {
                                    editable: false
                                }
                            }
                        ]
                    },
                    {
                        header: 'Tags',
                        width: 150
                    },
                    {
                        header: 'Source',
                        dataIndex: 'source',
                        align: 'center',
                        width: 70
                    },
                    {
                        header: 'Valid',
                        dataIndex: 'validationStatus',
                        width: 50,
                        align: 'center',
                        renderer: function (value, metadata, record) {
                            var okImg = '<img src="images/fatcow/16/bullet_green.png"/>';
                            var warningImg = '<img src="images/fatcow/16/warning.png"/>';
                            var errorImg = '<img src="images/fatcow/16/exclamation.png"/>';

                            var validationResults = value ? Ext.decode(value) : undefined;
                            var results = validationResults ? validationResults.results : undefined;

                            if (!results) {
                                return '';
                            }

                            var errorsText = '';
                            var hasErrors = false;

                            var warningsText = '';
                            var hasWarnings = false;

                            Ext.each(results, function (each) {
                                var type = each.type;

                                if (type == 'overlap' || type == 'noRequiredValue') {
                                    hasErrors = true;
                                    errorsText += errorImg + ' ' + each.message + '<br/>';
                                }

                                if (type == 'warning') {
                                    hasWarnings = true;
                                    warningsText += warningImg + ' ' + each.message + '<br/>';
                                }
                            });

                            if (hasErrors) {
                                metadata.tdAttr = "data-qtip='" + (errorsText + (hasWarnings ? warningsText : '')) + "'";
                                return errorImg;
                            } else if (hasWarnings) {
                                metadata.tdAttr = "data-qtip='" + warningsText + "'";
                                return warningImg;
                            } else {
                                metadata.tdAttr = 'data-qtip="OK"';
                                return okImg;
                            }
                        }
                    },
                    {
                        header: 'Comment',
                        dataIndex: 'comment',
                        editor: {
                            allowBlank: true
                        },
                        flex: 1,
                        minWidth: 250
                    }
                ],
                defaults: {
                    sortable: false,
                    hideable: false
                }
            },

            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: 'Add',
                    iconCls: 'icon-add',
                    scope: this,
                    handler: this.onAddClick
                }, {
                    itemId: 'delete',
                    text: 'Delete',
                    iconCls: 'icon-delete',
                    disabled: true,
                    scope: this,
                    handler: this.onDeleteClick
                }/*, {
                    text: 'Sync',
                    scope: this,
                    handler: this.onSync
                }*/]
            }]
        });

        this.callParent();
        this.getSelectionModel().on('selectionchange', this.onSelectionChange, this);
    },

    updateFlightTime: function () {
        var atdField = this.rowEditing.editor.down('textfield[name=atd]');
        var ataField = this.rowEditing.editor.down('textfield[name=ata]');
        var flightTimeField = this.rowEditing.editor.down('textfield[name=flightTime]');

        if (!atdField.isValid() || !ataField.isValid()) {
            flightTimeField.setValue('-');
            return;
        }

        var atdValue = atdField.getValue();
        var ataValue = ataField.getValue();

        var atdMinutes = this.hhmmToMinutes(atdValue);
        var ataMinutes = this.hhmmToMinutes(ataValue);

        var flightTimeMinutes = ataMinutes - atdMinutes;
        if (flightTimeMinutes < 0) {
            flightTimeMinutes += 24 * 60;
        }

        var flightTimeValue = this.minutesToHhmm(flightTimeMinutes);

        flightTimeField.setValue(flightTimeValue);
    },

    hhmmToMinutes: function (hhmm) {
        var hh = hhmm.substring(0, 2);
        var mm = hhmm.substring(3, 5);

        return parseInt(hh) * 60 + parseInt(mm);
    },

    minutesToHhmm: function (minutes) {
        var m = minutes % 60;
        var h = (minutes - m) / 60;

        return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
    },

    onSelectionChange: function (selModel, selections) {
        this.down('#delete').setDisabled(selections.length === 0);
    },

    onAddClick: function () {
        var rec = new Flight({
            source: 'User'
        });

        this.rowEditing.cancelEdit();
        this.store.insert(0, rec);
        this.rowEditing.startEdit(0, 0);
    },

    onDeleteClick: function () {
        var selection = this.getView().getSelectionModel().getSelection()[0];
        if (selection) {
            this.store.remove(selection);
        }
    },

    // onSync: function () {
    //     var selection = this.getView().getStore().sync();
    // },

    onTabShown: function () {
        if (!this.aircraftTypeStore.isLoaded()) 
            this.aircraftTypeStore.load();
        if (!this.aircraftModelStore.isLoaded())
            this.aircraftModelStore.load();
        if (!this.getStore().isLoaded())
            this.getStore().load();
    }
});
