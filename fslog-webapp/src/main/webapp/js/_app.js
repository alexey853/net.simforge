Ext.onReady(function () {

    auth = Ext.create('Auth');

    Ext.getBody().mask('Loading...');
    auth.loadStatus(function (status) {
        if (status.loggedIn) {
            auth.onSuccessfulLogin(status);
        }
        
        Ext.getBody().unmask();
    });

    Ext.tip.QuickTipManager.init();

    Ext.create('Ext.container.Viewport', {
        layout: 'fit',
        items: [
            {
                title: 'FSLog',
                cls: 'navigation',
                id: 'main-panel',

                layout: 'card',

                items: [
                    {
                        html: 'welcome page will be there'
                    },
                    {
                        // title: 'FSLog',
                        xtype: 'tabpanel',
                        // cls: 'navigation',

                        tabBar: {
                            layout: {
                                pack: 'center'
                            }
                        },

                        items: [
                            {
                                title: 'Logbook',
                                iconCls: 'logbook-tab-icon',
                                layout: 'fit',
                                items: [
                                    {
                                        xtype: 'flights-grid'
                                    }
                                ]
                            },
                            {
                                title: 'Statistics',
                                iconCls: 'statistics-tab-icon',
                                layout: 'fit',
                                items: [
                                    {
                                        xtype: 'statistics-tab'
                                    }
                                ]
                            }/*,
                            {
                                title: 'Integrations',
                                iconCls: 'integrations-tab-icon',
                                layout: 'fit',
                                items: [
                                    {
                                        xtype: 'integrations-tab'
                                    }
                                ]
                            },
                            {
                                title: 'Settings',
                                iconCls: 'settings-tab-icon'
                            }*/
/*                            , {
                                title: 'Flight details',
                                iconCls: 'logbook-tab-icon',
                                layout: 'fit',
                                items: [
                                    {
                                        xtype: 'flight-details-tab'
                                    }
                                ]
                            }*/
                        ],
                        listeners: {
                            activate: function( newActiveItem , tabpabel , oldActiveItem , eOpts) {
                                newActiveItem.fireEvent('tabchange', newActiveItem.getTabBar(), newActiveItem.getActiveTab());
                            },
                            tabchange: function ( tabBar , newTab , oldTab , eOpts )  {
                                Ext.each(newTab.items.items, function (each) {
                                    if (each.onTabShown) {
                                        each.onTabShown.apply(each);
                                    }
                                });
                            }
                        }
                    }
                ],

                header: {
                    titlePosition: 0,
                    items: auth.getUIItems()
                }
            }
        ]
    });

});
