Ext.define('IntegrationsTab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.integrations-tab',

    defaults: {
        bodyPadding: '20 20',
        border: true,
        margin: '10 10 10 10'
    },

    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },

    autoScroll: true,

    items: [
        {
            xtype: 'form',
            title: 'FSEconomy Integration',
            width: 800,
            height: 500,
            items: [
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Enabled',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            name: 'enabled',
                            checked: true,
                            readOnly: 'true'
                        }
                    ]
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'Username',
                    name: 'username',
                    readOnly: 'true'
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'Last refresh',
                    name: 'lastRefresh',
                    readOnly: 'true'
                }, {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Next refresh',
                            name: 'nextRefresh',
                            margin: '0 10 0 0',
                            readOnly: 'true'
                        }, {
                            xtype: 'button',
                            text: 'Refresh now',
                            width: 150
                        }
                    ]
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'Flights imported',
                    name: 'flightsImported',
                    readOnly: 'true'
                }
            ]
        }
    ]
});
