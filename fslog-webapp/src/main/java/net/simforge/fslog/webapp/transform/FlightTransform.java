package net.simforge.fslog.webapp.transform;

import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Str;
import net.simforge.fslog.FSLog;
import net.simforge.fslog.model.Flight;
import net.simforge.fslog.webapp.dto.FlightDto;
import net.simforge.refdata.aircraft.model.AircraftMakeModel;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class FlightTransform {
    private static final Logger logger = LoggerFactory.getLogger(FlightTransform.class.getName());

    public static FlightDto toDto(Flight flight) {
        FlightDto flightDto = new FlightDto();

        flightDto.setId(String.valueOf(flight.getId()));

        flightDto.setSource(FSLog.Source.byId(flight.getSourceId()).getName());

        flightDto.setAircraftTypeId(flight.getAircraftTypeId() != null ? String.valueOf(flight.getAircraftTypeId()) : null);
        flightDto.setAircraftModelId(flight.getAircraftModel() != null ? String.valueOf(flight.getAircraftModel().getId()) : null);
        flightDto.setAircraftRegNo(flight.getTailNumber());

        flightDto.setFlightNo(flight.getFlightNumber());
        flightDto.setCallsign(flight.getCallsign());

        flightDto.setDepartedFrom(flight.getFromIcao());
        flightDto.setArrivedTo(flight.getToIcao());

        if (flight.getDepartureUtc() != null && flight.getArrivalUtc() != null) {
            LocalDateTime departureUtc = flight.getDepartureUtc();
            flightDto.setDateOfFlight(FSLog.ddmmyyyy.format(departureUtc.toLocalDate()));
            flightDto.setAtd(JavaTime.toHhmm(departureUtc.toLocalTime()));
            flightDto.setAta(JavaTime.toHhmm(flight.getArrivalUtc().toLocalTime()));
            flightDto.setFlightTime(flight.getFlightTime());
        } else {
            flightDto.setDateOfFlight(null);
            flightDto.setAtd(null);
            flightDto.setAta(null);
            flightDto.setFlightTime(null);
        }

        flightDto.setComment(flight.getComment());

        return flightDto;
    }

    public static Flight fromDto(FlightDto flightDto, Session session) {
        Flight flight = new Flight();

        flight.setId(parseIntegerInput(flightDto.getId()));

        flight.setAircraftTypeId(parseIntegerInput(flightDto.getAircraftTypeId()));

        Integer aircraftModelId = parseIntegerInput(flightDto.getAircraftModelId());
        if (aircraftModelId != null) {
            AircraftMakeModel aircraftModel = FSLog.findAircraftModel(session, aircraftModelId);
            if (aircraftModel == null) {
                throw new RuntimeException("Could not find aircraft model by id " + aircraftModelId);
            }
            flight.setAircraftModel(aircraftModel);
        } else {
            flight.setAircraftModel(null);
        }

        flight.setTailNumber(parseStringInput(flightDto.getAircraftRegNo()));
        flight.setFlightNumber(parseStringInput(flightDto.getFlightNo()));
        flight.setCallsign(parseStringInput(flightDto.getCallsign()));

        flight.setFromIcao(parseStringInput(flightDto.getDepartedFrom()));
        flight.setToIcao(parseStringInput(flightDto.getArrivedTo()));

        try {
            LocalDate dateOfFlight = LocalDate.parse(flightDto.getDateOfFlight(), FSLog.ddmmyyyy);
            flight.setDepartureUtc(dateOfFlight.atTime(JavaTime.hhmmToLocalTime(flightDto.getAtd())));

            Duration flightTime = JavaTime.hhmmToDuration(flightDto.getFlightTime());
            flight.setArrivalUtc(flight.getDepartureUtc().plus(flightTime));

            flight.setFlightTime(JavaTime.toHhmm(flightTime));
        } catch (Exception e) {
            logger.warn("Unable to parse date-time fields for flight " + flightDto);

            flight.setDepartureUtc(null);
            flight.setArrivalUtc(null);
            flight.setFlightTime(null);
        }

        flight.setComment(parseStringInput(flightDto.getComment()));

        return flight;
    }

    private static String parseStringInput(String aircraftRegNo) {
        return !Str.isEmpty(aircraftRegNo) ? aircraftRegNo.trim() : null;
    }

    private static Integer parseIntegerInput(String input) {
        return input != null ? Integer.parseInt(input) : null;
    }
}
