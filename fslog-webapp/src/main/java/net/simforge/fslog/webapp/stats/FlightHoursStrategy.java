package net.simforge.fslog.webapp.stats;

import net.simforge.commons.misc.JavaTime;
import net.simforge.fslog.model.Flight;

public abstract class FlightHoursStrategy extends BaseStatsStrategy {

    @Override
    public Stats.Entry createEntry(String category) {
        return new Entry(category);
    }

    @Override
    public int compare(Stats.Entry entry1, Stats.Entry entry2) {
        return Entry.compare(entry1, entry2);
    }

    @Override
    protected void count(Flight flight, Stats.Entry entry) {
        ((Entry)entry).count(flight);
    }

    private static class Entry implements Stats.Entry {
        private String category;
        private int flights;
        private double hours;

        Entry(String category) {
            this.category = category;
        }

        @Override
        public String getCategory() {
            return category;
        }

        public int getFlights() {
            return flights;
        }

        public double getHours() {
            return hours;
        }

        @Override
        public String getTooltip() {
            return String.format("Time: %.1f hrs<br/>Flights: %s", hours, flights);
        }

        void count(Flight flight) {
            flights++;
            String flightTime = flight.getFlightTime();
            if (flightTime != null) {
                hours += JavaTime.hhmmToDuration(flightTime).toMinutes() / 60.0;
            }
        }

        @Override
        public Stats.Entry plus(Stats.Entry entry) {
            Entry result = new Entry(category);
            result.flights = flights + ((Entry)entry).flights;
            result.hours   = hours   + ((Entry)entry).hours;
            return result;
        }

        @Override
        public double divide(Stats.Entry entry) {
            return hours / ((Entry)entry).hours;
        }

        public static int compare(Stats.Entry entry1, Stats.Entry entry2) {
            return -Double.compare(((Entry)entry1).hours, ((Entry)entry2).hours);
        }
    }
}
