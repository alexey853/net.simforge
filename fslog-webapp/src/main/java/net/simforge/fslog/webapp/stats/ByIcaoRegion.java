package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.Flight;

public class ByIcaoRegion implements Stats.Strategy {
    @Override
    public void count(Stats.Visitor visitor, Flight flight) {
        String takeoffRegion = getRegion(flight.getFromIcao());
        ByRegionEntry entry = (ByRegionEntry) visitor.getOrCreate(takeoffRegion);
        entry.countTakeoff();

        String landingRegion = getRegion(flight.getToIcao());
        entry = (ByRegionEntry) visitor.getOrCreate(landingRegion);
        entry.countLanding();
    }

    private String getRegion(String icao) {
        if (icao == null || icao.trim().length() == 0) {
            return "Unknown";
        }

        icao = icao.trim().toUpperCase();

        if (icao.length() != 4) {
            return "Non-ICAO";
        }

        if (!Character.isLetter(icao.charAt(0))) {
            return "Non-ICAO";
        }

        return icao.substring(0, 1) + "xxx";
    }

    @Override
    public Stats.Entry createEntry(String category) {
        return new ByRegionEntry(category);
    }

    @Override
    public int compare(Stats.Entry entry1, Stats.Entry entry2) {
        return -Integer.compare(((ByRegionEntry)entry1).getTotal(), ((ByRegionEntry)entry2).getTotal());
    }

    @Override
    public double getMaxOtherPart() {
        return 0.01;
    }

    private class ByRegionEntry implements Stats.Entry {
        private String category;
        private int takeoffs;
        private int landings;

        private ByRegionEntry(String category) {
            this.category = category;
        }

        @Override
        public String getCategory() {
            return category;
        }

        public int getTakeoffs() {
            return takeoffs;
        }

        public int getLandings() {
            return landings;
        }

        public int getTotal() {
            return takeoffs + landings;
        }

        @Override
        public String getTooltip() {
            return String.format("Total: %s<br/>Takeoffs: %s<br/>Landings: %s", getTotal(), takeoffs, landings);
        }

        void countTakeoff() {
            takeoffs++;
        }

        void countLanding() {
            landings++;
        }

        @Override
        public Stats.Entry plus(Stats.Entry entry) {
            ByRegionEntry result = new ByRegionEntry(category);
            result.takeoffs = takeoffs + ((ByRegionEntry)entry).takeoffs;
            result.landings = landings + ((ByRegionEntry)entry).landings;
            return result;
        }

        @Override
        public double divide(Stats.Entry entry) {
            return getTotal() / (double)((ByRegionEntry)entry).getTotal();
        }
    }
}
