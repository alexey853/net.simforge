package net.simforge.fslog.webapp.dto;

public class FlightDto {
    private String id;
    private String source;
    private String dateOfFlight;
    private String aircraftTypeId;
    private String aircraftModelId;
    private String aircraftRegNo;
    private String flightNo;
    private String callsign;
    private String departedFrom;
    private String atd;
    private String arrivedTo;
    private String ata;
    private String flightTime;
    private String vatsimStatus;
    private String ivaoStatus;
    private String peStatus;
    private String fseStatus;
    private String validationStatus;
    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDateOfFlight() {
        return dateOfFlight;
    }

    public void setDateOfFlight(String dateOfFlight) {
        this.dateOfFlight = dateOfFlight;
    }

    public String getAircraftTypeId() {
        return aircraftTypeId;
    }

    public void setAircraftTypeId(String aircraftTypeId) {
        this.aircraftTypeId = aircraftTypeId;
    }

    public String getAircraftModelId() {
        return aircraftModelId;
    }

    public void setAircraftModelId(String aircraftModelId) {
        this.aircraftModelId = aircraftModelId;
    }

    public String getAircraftRegNo() {
        return aircraftRegNo;
    }

    public void setAircraftRegNo(String aircraftRegNo) {
        this.aircraftRegNo = aircraftRegNo;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getDepartedFrom() {
        return departedFrom;
    }

    public void setDepartedFrom(String departedFrom) {
        this.departedFrom = departedFrom;
    }

    public String getAtd() {
        return atd;
    }

    public void setAtd(String atd) {
        this.atd = atd;
    }

    public String getArrivedTo() {
        return arrivedTo;
    }

    public void setArrivedTo(String arrivedTo) {
        this.arrivedTo = arrivedTo;
    }

    public String getAta() {
        return ata;
    }

    public void setAta(String ata) {
        this.ata = ata;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public String getVatsimStatus() {
        return vatsimStatus;
    }

    public void setVatsimStatus(String vatsimStatus) {
        this.vatsimStatus = vatsimStatus;
    }

    public String getIvaoStatus() {
        return ivaoStatus;
    }

    public void setIvaoStatus(String ivaoStatus) {
        this.ivaoStatus = ivaoStatus;
    }

    public String getPeStatus() {
        return peStatus;
    }

    public void setPeStatus(String peStatus) {
        this.peStatus = peStatus;
    }

    public String getFseStatus() {
        return fseStatus;
    }

    public void setFseStatus(String fseStatus) {
        this.fseStatus = fseStatus;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "FlightDto{" +
                "id='" + id + '\'' +
                ", dateOfFlight='" + dateOfFlight + '\'' +
                ", departedFrom='" + departedFrom + '\'' +
                ", arrivedTo='" + arrivedTo + '\'' +
                '}';
    }
}
