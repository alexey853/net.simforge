package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.Flight;
import net.simforge.refdata.aircraft.model.AircraftMakeModel;

public class ByAircraftICAO extends FlightHoursStrategy {
    @Override
    public String getCategory(Flight flight) {
        AircraftMakeModel aircraftModel = flight.getAircraftModel();
        while (aircraftModel != null) {
            String icao = aircraftModel.getIcao();
            if (icao != null) {
                return icao;
            }

            aircraftModel = aircraftModel.getParent();
        }

        return "Unknown";
    }

    @Override
    public double getMaxOtherPart() {
        return 0.05;
    }
}
