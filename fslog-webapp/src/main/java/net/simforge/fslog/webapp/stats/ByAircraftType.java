package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.AircraftType;
import net.simforge.fslog.model.Flight;
import org.hibernate.Session;

public class ByAircraftType extends FlightHoursStrategy {
    private Session session;

    public ByAircraftType(Session session) {
        this.session = session;
    }

    @Override
    public String getCategory(Flight flight) {
        Integer aircraftTypeId = flight.getAircraftTypeId();
        if (aircraftTypeId != null) {
            return session.get(AircraftType.class, aircraftTypeId).getIcaoCode();
        } else {
            return "Unknown";
        }
    }

    @Override
    public double getMaxOtherPart() {
        return 0.05;
    }
}
