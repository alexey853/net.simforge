package net.simforge.fslog.webapp;

import net.simforge.fslog.model.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AuthHelper {
    private static final String FSLOG_ACCOUNT_ID = "FSLOG_ACCOUNT_ID";
    private static final String FSLOG_USERNAME = "FSLOG_USERNAME";

    private HttpSession session;

    public AuthHelper(HttpServletRequest request) {
        this.session = request.getSession(true);
    }

    public Integer getCurrentAccountId() {
        return (Integer) session.getAttribute(FSLOG_ACCOUNT_ID);
    }

    public String getCurrentUsername() {
        return (String) session.getAttribute(FSLOG_USERNAME);
    }

    public void checkLoggedIn() {
        if (!isLoggedIn()) {
            throw new IllegalStateException("User is not logged in");
        }
    }

    public boolean isLoggedIn() {
        return getCurrentAccountId() != null;
    }

    public void login(Account account) {
        session.setAttribute(FSLOG_ACCOUNT_ID, account.getId());
        session.setAttribute(FSLOG_USERNAME, account.getUsername());
    }

    public void logout() {
        session.setAttribute(FSLOG_ACCOUNT_ID, null);
        session.setAttribute(FSLOG_USERNAME, null);
    }
}
