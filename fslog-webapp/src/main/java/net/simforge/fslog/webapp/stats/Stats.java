package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.Flight;

import java.util.*;

public class Stats {
    public static Stats create(Strategy strategy) {
        Stats stats = new Stats();
        stats.add(strategy);
        return stats;
    }

    private Map<Strategy, Map<String, Entry>> allData = new HashMap<>();

    private Stats() {
    }

    public Stats add(Strategy strategy) {
        allData.put(strategy, new HashMap<>());
        return this;
    }

    public void count(Flight flight) {
        for (Strategy strategy : allData.keySet()) {
            strategy.count(new Visitor(strategy), flight);
        }
    }

    public List<Entry> getRawData(Strategy strategy) {
        Map<String, Entry> strategyData = allData.get(strategy);
        return new ArrayList<>(strategyData.values());
    }

    public List<Entry> getSortedData(Strategy strategy) {
        List<Entry> data = getRawData(strategy);
        data.sort(strategy::compare);
        return data;
    }

    public List<Entry> getTunedData(Strategy strategy) {
        List<Entry> data = getSortedData(strategy);

        Entry total = data.stream().reduce(Entry::plus).orElse(strategy.createEntry("No data"));

        double maxOtherPart = strategy.getMaxOtherPart();

        Entry other = strategy.createEntry("Other");
        int index = data.size() - 1;
        while (index >= 0) {
            Entry entry = data.get(index);

            Entry nextOther = other.plus(entry);
            if (nextOther.divide(total) > maxOtherPart) {
                break;
            }

            other = nextOther;
            index--;
        }

        if (index != data.size() - 1) {
            List<Stats.Entry> reducedList = new ArrayList<>(data.subList(0, index + 1));
            reducedList.add(other);
            return reducedList;
        } else {
            // return initial list
            return data;
        }
    }

    public class Visitor {
        private Strategy strategy;

        public Visitor(Strategy strategy) {
            this.strategy = strategy;
        }

        public Entry getOrCreate(String category) {
            Map<String, Entry> strategyData = allData.get(strategy);
            Entry entry = strategyData.get(category);
            if (entry == null) {
                entry = strategy.createEntry(category);
                strategyData.put(category, entry);
            }
            return entry;
        }
    }

    public interface Strategy {
        void count(Visitor visitor, Flight flight);
        Entry createEntry(String category);
        int compare(Entry entry1, Entry entry2);
        double getMaxOtherPart();
    }

    public interface Entry {
        String getCategory();
        String getTooltip();
        Entry plus(Entry entry);
        double divide(Entry entry);
    }
}
