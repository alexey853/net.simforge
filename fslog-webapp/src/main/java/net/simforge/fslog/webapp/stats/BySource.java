package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.FSLog;
import net.simforge.fslog.model.Flight;

public class BySource extends FlightHoursStrategy {
    @Override
    protected String getCategory(Flight flight) {
        Integer sourceId = flight.getSourceId();
        FSLog.Source source = sourceId != null ? FSLog.Source.byId(sourceId) : null;
        if (source != null) {
            return source.getName();
        } else {
            return "Unknown";
        }
    }
}
