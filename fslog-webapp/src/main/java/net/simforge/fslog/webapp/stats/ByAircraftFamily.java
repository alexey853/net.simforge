package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.Flight;
import net.simforge.refdata.aircraft.model.AircraftMakeModel;

public class ByAircraftFamily extends FlightHoursStrategy {
    @Override
    public String getCategory(Flight flight) {
        AircraftMakeModel aircraftModel = flight.getAircraftModel();
        while (aircraftModel != null) {
            if (AircraftMakeModel.Type.Family.equals(aircraftModel.getType())) {
                return aircraftModel.getName();
            }

            aircraftModel = aircraftModel.getParent();
        }

        aircraftModel = flight.getAircraftModel();
        while (aircraftModel != null) {
            if (AircraftMakeModel.Type.Manufacturer.equals(aircraftModel.getType())) {
                return aircraftModel.getName();
            }

            aircraftModel = aircraftModel.getParent();
        }


        return "Unknown";
    }

    @Override
    public double getMaxOtherPart() {
        return 0.05;
    }
}
