package net.simforge.fslog.webapp.stats;

import net.simforge.fslog.model.Flight;

public abstract class BaseStatsStrategy implements Stats.Strategy {

    @Override
    public void count(Stats.Visitor visitor, Flight flight) {
        String category = getCategory(flight);
        Stats.Entry entry = visitor.getOrCreate(category);
        count(flight, entry);
    }

    protected abstract String getCategory(Flight flight);

    protected abstract void count(Flight flight, Stats.Entry entry);

    @Override
    public double getMaxOtherPart() {
        return 0.01;
    }
}
