package net.simforge.fslog.webapp.transform;

import net.simforge.fslog.FSLog;
import net.simforge.fslog.model.Flight;
import net.simforge.fslog.webapp.dto.FlightDto;
import net.simforge.refdata.aircraft.model.AircraftMakeModel;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FlightTransformTest {

    private AircraftMakeModel aircraftModel10;
    private AircraftMakeModel aircraftModel12;

    @Before
    public void setUp() throws Exception {
        aircraftModel10 = new AircraftMakeModel();
        aircraftModel10.setId(10);

        aircraftModel12 = new AircraftMakeModel();
        aircraftModel12.setId(12);
    }

    @Test
    public void toDto() throws Exception {
        Flight flight = new Flight();

        flight.setId(1);
        flight.setSourceId(FSLog.Source.User.getId());
        flight.setAircraftTypeId(1);

        flight.setAircraftModel(aircraftModel10);

        flight.setTailNumber("N208FS");
        flight.setFromIcao("KBFL");
        flight.setDepartureUtc(LocalDateTime.of(2016, 10, 20, 10, 0));
        flight.setToIcao("L00");
        flight.setArrivalUtc(LocalDateTime.of(2016, 10, 20, 10, 30));
        flight.setFlightTime("00:30");
        flight.setComment("First flight");

        FlightDto flightDto = FlightTransform.toDto(flight);

        assertEquals("1", flightDto.getId());
        assertEquals("20/10/2016", flightDto.getDateOfFlight());
        assertEquals("1", flightDto.getAircraftTypeId());
        assertEquals("10", flightDto.getAircraftModelId());
        assertEquals("N208FS", flightDto.getAircraftRegNo());
        assertEquals(null, flightDto.getFlightNo());
        assertEquals(null, flightDto.getCallsign());
        assertEquals("KBFL", flightDto.getDepartedFrom());
        assertEquals("10:00", flightDto.getAtd());
        assertEquals("L00", flightDto.getArrivedTo());
        assertEquals("10:30", flightDto.getAta());
        assertEquals("00:30", flightDto.getFlightTime());
        assertEquals(null, flightDto.getVatsimStatus());
        assertEquals(null, flightDto.getIvaoStatus());
        assertEquals(null, flightDto.getPeStatus());
        assertEquals(null, flightDto.getFseStatus());
        assertEquals(null, flightDto.getValidationStatus());
        assertEquals("First flight", flightDto.getComment());
    }

    @Test
    public void toDto_emptyTimeFields() throws Exception {
        Flight flight = new Flight();

        flight.setId(1);
        flight.setSourceId(FSLog.Source.User.getId());
        flight.setAircraftTypeId(1);

        flight.setAircraftModel(aircraftModel10);

        flight.setTailNumber("N208FS");
        flight.setFromIcao("KBFL");
        flight.setDepartureUtc(null);
        flight.setToIcao("L00");
        flight.setArrivalUtc(null);
        flight.setFlightTime(null);
        flight.setComment("First flight");

        FlightDto flightDto = FlightTransform.toDto(flight);

        assertEquals("1", flightDto.getId());
        assertEquals(null, flightDto.getDateOfFlight());
        assertEquals("1", flightDto.getAircraftTypeId());
        assertEquals("10", flightDto.getAircraftModelId());
        assertEquals("N208FS", flightDto.getAircraftRegNo());
        assertEquals(null, flightDto.getFlightNo());
        assertEquals(null, flightDto.getCallsign());
        assertEquals("KBFL", flightDto.getDepartedFrom());
        assertEquals(null, flightDto.getAtd());
        assertEquals("L00", flightDto.getArrivedTo());
        assertEquals(null, flightDto.getAta());
        assertEquals(null, flightDto.getFlightTime());
        assertEquals(null, flightDto.getVatsimStatus());
        assertEquals(null, flightDto.getIvaoStatus());
        assertEquals(null, flightDto.getPeStatus());
        assertEquals(null, flightDto.getFseStatus());
        assertEquals(null, flightDto.getValidationStatus());
        assertEquals("First flight", flightDto.getComment());
    }

    @Test
    public void toDto_emptyAircraftType() throws Exception {
        Flight flight = new Flight();

        flight.setId(1);
        flight.setSourceId(FSLog.Source.User.getId());
        flight.setAircraftTypeId(null);
        flight.setAircraftModel(null);
        flight.setTailNumber("N208FS");
        flight.setFromIcao("KBFL");
        flight.setDepartureUtc(null);
        flight.setToIcao("L00");
        flight.setArrivalUtc(null);
        flight.setFlightTime(null);
        flight.setComment("First flight");

        FlightDto flightDto = FlightTransform.toDto(flight);

        assertEquals("1", flightDto.getId());
        assertEquals(null, flightDto.getDateOfFlight());
        assertEquals(null, flightDto.getAircraftTypeId());
        assertEquals(null, flightDto.getAircraftModelId());
        assertEquals("N208FS", flightDto.getAircraftRegNo());
        assertEquals(null, flightDto.getFlightNo());
        assertEquals(null, flightDto.getCallsign());
        assertEquals("KBFL", flightDto.getDepartedFrom());
        assertEquals(null, flightDto.getAtd());
        assertEquals("L00", flightDto.getArrivedTo());
        assertEquals(null, flightDto.getAta());
        assertEquals(null, flightDto.getFlightTime());
        assertEquals(null, flightDto.getVatsimStatus());
        assertEquals(null, flightDto.getIvaoStatus());
        assertEquals(null, flightDto.getPeStatus());
        assertEquals(null, flightDto.getFseStatus());
        assertEquals(null, flightDto.getValidationStatus());
        assertEquals("First flight", flightDto.getComment());
    }

    @Test
    public void fromDto() throws Exception {
        FlightDto flightDto = new FlightDto();
        flightDto.setId("1");
        flightDto.setDateOfFlight("20/10/2016");
        flightDto.setAircraftTypeId("1");
        flightDto.setAircraftModelId("12");
        flightDto.setAircraftRegNo("N208FS");
        flightDto.setFlightNo("");
        flightDto.setCallsign("");
        flightDto.setDepartedFrom("KBFL");
        flightDto.setAtd("10:00");
        flightDto.setArrivedTo("L00");
        flightDto.setAta("10:30");
        flightDto.setFlightTime("00:30");
        flightDto.setVatsimStatus("");
        flightDto.setIvaoStatus("");
        flightDto.setPeStatus("");
        flightDto.setFseStatus("YES");
        flightDto.setValidationStatus("");
        flightDto.setComment("First flight");

        Session session = mock(Session.class);
        when(session.get(AircraftMakeModel.class, 12)).thenReturn(aircraftModel12);
        Flight flight = FlightTransform.fromDto(flightDto, session);

        assertEquals(new Integer(1), flight.getId());
        assertEquals(new Integer(1), flight.getAircraftTypeId());
        assertEquals(new Integer(12), flight.getAircraftModel().getId());
        assertEquals("N208FS", flight.getTailNumber());
        assertNull(flight.getFlightNumber());
        assertNull(flight.getCallsign());
        assertEquals("KBFL", flight.getFromIcao());
        assertEquals(LocalDateTime.of(2016, 10, 20, 10, 0), flight.getDepartureUtc());
        assertEquals("L00", flight.getToIcao());
        assertEquals(LocalDateTime.of(2016, 10, 20, 10, 30), flight.getArrivalUtc());
        assertEquals("00:30", flight.getFlightTime());
        assertEquals("First flight", flight.getComment());
    }

    @Test
    public void fromDto_emptyDateOfFlight() throws Exception {
        FlightDto flightDto = new FlightDto();
        flightDto.setId("1");
        flightDto.setDateOfFlight("");
        flightDto.setAircraftTypeId("1");
        flightDto.setAircraftModelId("12");
        flightDto.setAircraftRegNo("N208FS");
        flightDto.setFlightNo("");
        flightDto.setCallsign("");
        flightDto.setDepartedFrom("KBFL");
        flightDto.setAtd("10:00");
        flightDto.setArrivedTo("L00");
        flightDto.setAta("10:30");
        flightDto.setFlightTime("00:30");
        flightDto.setVatsimStatus("");
        flightDto.setIvaoStatus("");
        flightDto.setPeStatus("");
        flightDto.setFseStatus("YES");
        flightDto.setValidationStatus("");
        flightDto.setComment("First flight");

        Session session = mock(Session.class);
        when(session.get(AircraftMakeModel.class, 12)).thenReturn(aircraftModel12);
        Flight flight = FlightTransform.fromDto(flightDto, session);

        assertEquals(new Integer(1), flight.getId());
        assertEquals(new Integer(1), flight.getAircraftTypeId());
        assertEquals(new Integer(12), flight.getAircraftModel().getId());
        assertEquals("N208FS", flight.getTailNumber());
        assertNull(flight.getFlightNumber());
        assertNull(flight.getCallsign());
        assertEquals("KBFL", flight.getFromIcao());
        assertEquals(null, flight.getDepartureUtc());
        assertEquals("L00", flight.getToIcao());
        assertEquals(null, flight.getArrivalUtc());
        assertEquals(null, flight.getFlightTime());
        assertEquals("First flight", flight.getComment());
    }
}
