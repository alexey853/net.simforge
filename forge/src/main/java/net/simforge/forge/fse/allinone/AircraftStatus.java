package net.simforge.forge.fse.allinone;

import java.time.LocalDateTime;

public class AircraftStatus {
    private String serial;
    private String registration;
    private String location;
    private String status; // In Flight, Locked

    private String assignmentDestination;
    private Integer assignmentPay;
    private LocalDateTime assignmentExpiration;
    private Integer assignmentDistance;
    private String assignmentEET;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssignmentDestination() {
        return assignmentDestination;
    }

    public void setAssignmentDestination(String assignmentDestination) {
        this.assignmentDestination = assignmentDestination;
    }

    public Integer getAssignmentPay() {
        return assignmentPay;
    }

    public void setAssignmentPay(Integer assignmentPay) {
        this.assignmentPay = assignmentPay;
    }

    public LocalDateTime getAssignmentExpiration() {
        return assignmentExpiration;
    }

    public void setAssignmentExpiration(LocalDateTime assignmentExpiration) {
        this.assignmentExpiration = assignmentExpiration;
    }

    public Integer getAssignmentDistance() {
        return assignmentDistance;
    }

    public void setAssignmentDistance(Integer assignmentDistance) {
        this.assignmentDistance = assignmentDistance;
    }

    public String getAssignmentEET() {
        return assignmentEET;
    }

    public void setAssignmentEET(String assignmentEET) {
        this.assignmentEET = assignmentEET;
    }
}
