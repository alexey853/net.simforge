package net.simforge.forge.fse.allinone;

import net.simforge.commons.hibernate.HibernateUtils;
import net.simforge.commons.io.Csv;
import net.simforge.commons.misc.Geo;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Str;
import net.simforge.fse.FSE;
import net.simforge.fse.FSEFeeder;
import net.simforge.fse.model.FSEFeederQuery;
import net.simforge.tracker.world.airports.Airport;
import net.simforge.tracker.world.airports.Airports;
import net.simforge.tracker.world.airports.AirportsLoader;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Probe {
    public static void main(String[] args) throws InterruptedException, IOException {
        try (SessionFactory sessionFactory = FSE.buildSessionFactory();
                Session session = sessionFactory.openSession()) {

//            final FSEFeederQuery aircraftsQuery = FSEFeeder.createAircraftByMakeModelQuery("XCHOXGAKWC", "Boeing 747-400");
            final FSEFeederQuery aircraftsQuery = FSEFeeder.createAircraftByMakeModelQuery("XCHOXGAKWC", "Boeing 737-800");

            HibernateUtils.transaction(session, () -> {
                session.save(aircraftsQuery);
            });

            waitForQuery(session, aircraftsQuery);

            Csv aircraftsCsv = Csv.fromContent(aircraftsQuery.getContent());

            Map<String, AircraftStatus> aircrafts = new TreeMap<>();

            for (int row = 0; row < aircraftsCsv.rowCount(); row++) {
                if (aircraftsCsv.rowWidth(row) < 10) {
                    continue;
                }

                String serialNumber = aircraftsCsv.value(row, "SerialNumber");

                AircraftStatus aircraftStatus = aircrafts.get(serialNumber);
                if (aircraftStatus == null) {
                    aircraftStatus = new AircraftStatus();
                    aircraftStatus.setSerial(serialNumber);
                    aircrafts.put(serialNumber, aircraftStatus);
                }

                aircraftStatus.setRegistration(aircraftsCsv.value(row, "Registration"));

                String location = aircraftsCsv.value(row, "Location");
                String rentedBy = aircraftsCsv.value(row, "RentedBy");

                aircraftStatus.setLocation(!"In Flight".equals(location) ? location : null);
                aircraftStatus.setStatus("In Flight".equals(location) ? "In Flight" : ("Not rented.".equals(rentedBy) ? null : "Locked"));
            }

            Set<String> icaos = new HashSet<>();
            for (AircraftStatus aircraftStatus : aircrafts.values()) {
                String location = aircraftStatus.getLocation();
                if (location == null)
                    continue;
                icaos.add(location);
            }



            final FSEFeederQuery assignmentsQuery = FSEFeeder.createAssignmentsByIcaosQuery("XCHOXGAKWC", icaos);

            HibernateUtils.transaction(session, () -> {
                session.save(assignmentsQuery);
            });

            Airports airports = AirportsLoader.load();

            waitForQuery(session, assignmentsQuery);

            Csv assignmentsCsv = Csv.fromContent(assignmentsQuery.getContent());

            Map<String, List<Map<String, String>>> icao2assignments = new HashMap<>();

            for (int row = 0; row < assignmentsCsv.rowCount(); row++) {
                if (assignmentsCsv.rowWidth(row) < 10) {
                    continue;
                }

                boolean allInOne = "true".equals(assignmentsCsv.value(row, "All-In"));
                if (!allInOne) {
                    continue;
                }

                Map<String, String> assignmentData = new HashMap<>();
                String location = assignmentsCsv.value(row, "Location");
                assignmentData.put("ToIcao", assignmentsCsv.value(row, "ToIcao"));
                assignmentData.put("Pay", assignmentsCsv.value(row, "Pay"));
                assignmentData.put("ExpireDateTime", assignmentsCsv.value(row, "ExpireDateTime"));

                List<Map<String, String>> assignments = icao2assignments.get(location);
                if (assignments == null) {
                    assignments = new ArrayList<>();
                    icao2assignments.put(location, assignments);
                }

                assignments.add(assignmentData);
            }

            for (AircraftStatus aircraftStatus : aircrafts.values()) {
                String location = aircraftStatus.getLocation();
                if (location == null) {
                    continue;
                }

                List<Map<String, String>> assignments = icao2assignments.get(location);
                if (assignments == null || assignments.isEmpty()) {
                    continue;
                }

                if (assignments.size() == 1) {
                    Map<String, String> assignment = assignments.get(0);

                    aircraftStatus.setAssignmentDestination(assignment.get("ToIcao"));

                    aircraftStatus.setAssignmentExpiration(null); // todo
                    aircraftStatus.setAssignmentPay(null); // // TODO: 01.02.2017

                    Airport locationAirport = airports.getByIcao(location);
                    Airport destAirport = airports.getByIcao(aircraftStatus.getAssignmentDestination());
                    if (locationAirport != null && destAirport != null) {
                        double distance = Geo.distance(locationAirport.getCoords(), destAirport.getCoords());
                        aircraftStatus.setAssignmentDistance((int) distance);

                        long eetMinutes = (long) ((distance / 450) * 60 + 30);
                        Duration eet = Duration.of(eetMinutes, ChronoUnit.MINUTES);
                        aircraftStatus.setAssignmentEET(JavaTime.toHhmm(eet));
                    }
                } else { // too many assignments
                    aircraftStatus.setAssignmentDestination("Unclear");
                }
            }

            System.out.println("Serial  Registration  Location  Status    Destination  Distance  EET");
            for (AircraftStatus aircraftStatus : aircrafts.values()) {
                System.out.println(
                        Str.al(aircraftStatus.getSerial(), 8) +
                        Str.al(aircraftStatus.getRegistration(), 14) +
                        Str.al(Str.mn(aircraftStatus.getLocation()), 10) +
                        Str.al(Str.mn(aircraftStatus.getStatus()), 10) +
                        Str.al(Str.mn(aircraftStatus.getAssignmentDestination()), 13) +
                        Str.al(aircraftStatus.getAssignmentDistance() != null ? Integer.toString(aircraftStatus.getAssignmentDistance()) : "", 10) +
                        Str.al(Str.mn(aircraftStatus.getAssignmentEET()), 5)
                );
            }
        }
    }

    private static void waitForQuery(Session session, FSEFeederQuery aircraftsQuery) throws InterruptedException {
        while (true) {
            Thread.sleep(5000);

            session.refresh(aircraftsQuery);

            if (aircraftsQuery.getStatus() == FSEFeederQuery.Status.Done) {
                break;
            }
        }
    }
}
