package net.simforge.fse.scanfbos.simple;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root
public class IcaoFboItems {
    @Attribute
    private int total;

    @ElementList(inline = true, required = false)
    private List<FBO> fbos;

    public List<FBO> getFBOs() {
        return fbos;
    }
}
