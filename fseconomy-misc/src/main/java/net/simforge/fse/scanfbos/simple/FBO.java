package net.simforge.fse.scanfbos.simple;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class FBO {
    @Element(name = "FboId", required = false)
    private String fboId;

    @Element(name = "Status")
    private String status;

    @Element(name = "Airport")
    private String airport;

    @Element(name = "Name")
    private String name;

    @Element(name = "Owner")
    private String owner;

    @Element(name = "Icao")
    private String icao;

    @Element(name = "Location")
    private String location;

    @Element(name = "Lots")
    private String lots;

    @Element(name = "RepairShop")
    private String repairShop;

    @Element(name = "Gates")
    private String gates;

    @Element(name = "GatesRented")
    private String gatesRented;

    @Element(name = "Fuel100LL")
    private String fuel100LL;

    @Element(name = "FuelJetA")
    private String fuelJetA;

    @Element(name = "BuildingMaterials")
    private String buildingMaterials;

    @Element(name = "Supplies")
    private String supplies;

    @Element(name = "SuppliesPerDay")
    private String suppliesPerDay;

    @Element(name = "SuppliedDays")
    private String suppliedDays;

    @Element(name = "SellPrice")
    private String sellPrice;

    public String getOwner() {
        return owner;
    }

    public String getSupplies() {
        return supplies;
    }
}
