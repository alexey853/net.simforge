package net.simforge.fse.scanfbos;

import net.simforge.commons.io.Csv;
import net.simforge.commons.io.IOHelper;
import net.simforge.commons.misc.Misc;
import net.simforge.fse.scanfbos.simple.FBO;
import net.simforge.fse.scanfbos.simple.IcaoFboItems;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class ScanFBOs {
    public static final int one_hour = 3600000;
    private static Logger log = LoggerFactory.getLogger("ScanFBOs");

    public static void main(String[] args) throws Exception {
//        String icaoPrefix = "EG";

        String icaoPrefix = "K";

        int minSize = 3501;

//        log.info("ICAO prefix is '{}'", icaoPrefix);

        Csv airportsCsv = Csv.load(new File("./data/icaodata.csv"));
        Set<String> icaos = new TreeSet<>();
        for (int i = 0; i < airportsCsv.rowCount(); i++) {
            String icao = airportsCsv.value(i, "icao");
            if (!icao.startsWith(icaoPrefix)) {
                continue;
            }

            int size = Integer.valueOf(airportsCsv.value(i, "size"));
            if (size < minSize) {
                continue;
            }

            icaos.add(icao);
        }

        log.info("Found {} airports", icaos.size());

        String urlTemplate = "http://server.fseconomy.net/data?userkey=XCHOXGAKWC&format=xml&query=icao&search=fbo&icao={}";

        new File("./scan-fbos").mkdirs();
        FileWriter fw = new FileWriter("./scan-fbos/report-" + System.currentTimeMillis() + ".txt");

        for (String icao : icaos) {
            log.info("Icao {}", icao);

            File icaoFolder = new File("./scan-fbos/xml/" + icao);
            icaoFolder.mkdirs();

            long maxTimestamp = 0;
            File[] files = icaoFolder.listFiles();
            for (File file : files) {
                String name = file.getName();
                if (!name.endsWith(".xml")) {
                    continue;
                }
                name = name.substring(0, name.length()-4);
                long timestamp = Long.parseLong(name);

                if (timestamp > maxTimestamp) {
                    maxTimestamp = timestamp;
                }
            }

            File file;
            boolean needPause = false;
            if (System.currentTimeMillis() - maxTimestamp < 3 * 24 * one_hour) {
                file = new File(icaoFolder, maxTimestamp + ".xml");
                log.info("\t\tLocal data is up-to-date");
            } else {
                while (true) {
                    String url = urlTemplate.replaceAll("\\{\\}", icao);

                    String content = IOHelper.download(url);

                    if (content.startsWith("<Error>")) {
                        log.warn("Data feed limit reached, 1 hour sleep - {}", content);
                        Misc.sleep(3600000); // 1 hour
                        continue;
                    }

                    file = new File(icaoFolder, System.currentTimeMillis() + ".xml");
                    IOHelper.saveFile(file, content);

                    needPause = true;

                    break;
                }
            }

            Serializer serializer = new Persister();
            IcaoFboItems root = serializer.read(IcaoFboItems.class, file);

            List<FBO> fbos = root.getFBOs();
            if (fbos == null) {
                fbos = Collections.emptyList();
            }

            boolean empty = true;
//                boolean closed = false;A
            for (FBO fbo : fbos) {
                boolean systemFbo = fbo.getOwner().equals("System");// && fbo.getSupplies().equals("Unlimited");
//                    boolean userClosedFbo = fbo.getOwner().equals("System") && !fbo.getSupplies().equals("Unlimited");

                if (systemFbo) {
                    continue;
                }

//                    if (userClosedFbo) {
//                        closed = true;
//                        break;
//                    }

                empty = false;
            }

            if (empty) {
                fw.write("\t" + icao + "\tEMPTY\r\n");
                log.info("\t\t{} Empty", icao);
//                } else if (closed) {
//                    fw.write("\t" + icao + "\tClosed\r\n");
            }
            fw.flush();

            if (needPause) {
                Misc.sleep(61000);
            }
        }
    }
}
