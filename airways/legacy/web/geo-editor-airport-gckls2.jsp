<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="java.util.List"%>
<%@ page import="forge.commons.gc_kls2_com.GC"%>
<%@ page import="forge.commons.gc_kls2_com.GCAirport"%>

<%
    Connection connx = DB.getConnection();

    String action = request.getParameter("action");
    String code = request.getParameter("code");
    code = code != null ? code.toUpperCase() : code;

    if ("Find".equals(action)) {
        List<Airport> airports = Persistence.loadByQuery(
                connx,
                Airport.class,
                "select * from %tn% where iata = '" + code + "' or icao = '" + code + "'");

        if (airports.size() != 0) {
            %>
[todo airport found in our DB]
<%
        } else {
            GCAirport gcAirport = GC.findAirport(connx, code);
            connx.commit();
            if (gcAirport != null) {
                %>
<h3>GC KLS2 data</h3>
<table>
    <tr><th>ICAO</th><td><%=gcAirport.getIcao()%></td></tr>
    <tr><th>IATA</th><td><%=gcAirport.getIata()%></td></tr>
    <tr><th>City</th><td><%=gcAirport.getCity()%></td></tr>
    <tr><th>Name</th><td><%=gcAirport.getName()%></td></tr>
    <tr><th>Lat</th><td><%=gcAirport.getLat()%></td></tr>
    <tr><th>Lon</th><td><%=gcAirport.getLon()%></td></tr>
    <tr><th>TZ1</th><td><%=gcAirport.getTimezone1()%></td></tr>
    <tr><th>TZ2</th><td><%=gcAirport.getTimezone2()%></td></tr>
</table>
<form action="geo-editor-airport-gckls2.jsp" method="POST">
    <input type="hidden" name="action" value="Import">
    <input type="hidden" name="icao" value="<%=gcAirport.getIcao()%>">
    <input type="button" value="Show on map" onclick="window.parent.showOnMap(<%=gcAirport.getLat()%>, <%=gcAirport.getLon()%>, 13)">
    <input type="submit" value="Import <%=gcAirport.getIcao()%> / <%=gcAirport.getIata()%>">
</form>
<br>
<br>
<%
            } else {
                %>
<h3>GC KLS2 data</h3>
<font color="red">Could not find airports in GC KLS2</font>
<br>
<br>
<%
            }
        }
    } else if ("Import".equals(action)) {
        String icao = request.getParameter("icao");
        GCAirport gcAirport = GC.findAirport(connx, icao);
        Airport airport = new Airport();
        airport.setIata(gcAirport.getIata());
        airport.setIcao(gcAirport.getIcao());
        airport.setName(gcAirport.getName());
        airport.setLat(gcAirport.getLat());
        airport.setLon(gcAirport.getLon());
        airport.setDataset(3);
        airport = Persistence.create(connx, airport);
        connx.commit();
        response.sendRedirect("geo-editor-airport.jsp?id=" + airport.getId() + "&refresh=yes");
    }

    connx.close();


%>

<h3>Import airport from GC KLS2</h3>
<form method="POST">
Specify ICAO or IATA code <input type="text" name="code" value="<%=code != null ? code : ""%>"> <input type="submit" name="action" value="Find">
</form>