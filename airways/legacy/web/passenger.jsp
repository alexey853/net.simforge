<jsp:include page="top-menu.jsp"/>

<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="java.util.List" %>
<%@ page import="net.simforge.legacy.airways.DT" %>
<%@ page import="net.simforge.legacy.airways.PassengerGroupLifecycle" %>
<%@ page import="net.simforge.legacy.airways.RefDataCache" %>
<%@ page import="net.simforge.legacy.airways.model.*" %>
<%@ page import="net.simforge.legacy.airways.Captions" %>
<%@ page import="org.joda.time.DateMidnight"%>
<%@ page import="org.joda.time.DateTime"%>
<%
    int id = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();
    PassengerGroup group = Persistence.load(connx, PassengerGroup.class, id);
    List<PassengerGroupItinerary> itineraries = Persistence.loadByQuery(connx, PassengerGroupItinerary.class, "select * from %tn% where group_id = " + id + " order by item_order");
    List<PassengerGroupEvent> events = Persistence.loadByQuery(connx, PassengerGroupEvent.class, "select * from %tn% where group_id = " + id + " order by dt");
    connx.close();

%>

<table>
    <tr>
        <th>Route</th>
        <td><%=PassengerGroupLifecycle.l(group)%></td>
    </tr>
    <tr>
        <th>Size</th>
        <td><%=group.getSize()%></td>
    </tr>
    <tr>
        <th>Current city</th>
        <td><%=group.getPositionCityId() == 0 ? "" : RefDataCache.getCity(group.getPositionCityId()).getName()%></td>
    </tr>
    <tr>
        <th>Current airport</th>
        <td><%=group.getPositionAirportId() == 0 ? "" : RefDataCache.getAirport(group.getPositionAirportId()).getIata()%></td>
    </tr>
    <tr>
        <th>Direction</th>
        <td><%=group.isRoundtrip() ? (!group.isPositionRoundtrip() ? "Heading to destination" : "Returning to origin") : "Oneway"%></td>
    </tr>
    <tr>
        <th>Status</th>
        <td><%=Captions.get(PassengerGroup.Status.class, group.getStatus())%></td>
        <td><%
            if (group.getStatus() == PassengerGroup.Status.CouldNotFindTickets) {
                %><a href="passenger-action.jsp?id=<%=group.getId()%>&action=makeItLookingForTickets">make it LookingForTickets</a><%
            }
        %></td>
    </tr>
</table>
<br>
<b>Itineraries</b><br>
<table>
    <tr>
        <th>#</th>
        <th>Number</th>
        <th>Date</th>
        <th>From</th>
        <th>Dep</th>
        <th>To</th>
        <th>Arr</th>
    </tr>

    <%

        for (PassengerGroupItinerary itinerary : itineraries) {
            Flight flight = RefDataCache.getFlight(itinerary.getFlightId());
            %>
    <tr>
        <td><%=itinerary.getItemOrder()%></td>
        <td><%=flight.getNumber()%></td>
        <td><%=DT.DF.print(new DateTime(flight.getDate()))%></td>
        <td><%=RefDataCache.getAirport(flight.getFromAirportId()).getIata()%></td>
        <td><%=DT.tF.print(flight.getDepTime())%></td>
        <td><%=RefDataCache.getAirport(flight.getToAirportId()).getIata()%></td>
        <td><%=DT.tF.print(flight.getArrTime())%></td>
    </tr>
    <%


        }

    %>
</table>
<br>
<b>Event log</b><br>
<table>
    <tr>
        <th colspan="2">DT</th>
        <th>City</th>
        <th>Airport</th>
        <th>Flight</th>
        <th>Message</th>
    </tr>

    <%

        DateMidnight lastDM = null;

        for (PassengerGroupEvent event : events) {
            City city = event.getCityId() != 0 ? RefDataCache.getCity(event.getCityId()) : null;
            Airport airport = event.getAirportId() != 0 ? RefDataCache.getAirport(event.getAirportId()) : null;
            Flight flight = event.getFlightId() != 0 ? RefDataCache.getFlight(event.getFlightId()) : null;

            DateMidnight currDM = event.getDt().toDateMidnight();
            boolean printDate = lastDM != null ? !currDM.isEqual(lastDM) : true;
            lastDM = currDM;

            %>
    <tr>
        <td><%=printDate ? DT.DF.print(event.getDt()) : ""%></td>
        <td><%=DT.tF.print(event.getDt())%></td>
        <td><%=city != null ? city.getName() : ""%></td>
        <td><%=airport != null ? airport.getIata() : ""%></td>
        <td><% if (flight != null) {
            %><a href="flight.jsp?id=<%=flight.getId()%>"><%=flight.getNumber()%></a> <%=DT.DtF.print(flight.getDepTime())%><%
        }%></td>
        <td><%=event.getMsg()%></td>
    </tr>
    <%
        }

    %>

</table>
