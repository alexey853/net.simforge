<jsp:include page="top-menu.jsp"/>

<%@ page import="net.simforge.legacy.airways.model.PassengerGroup" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.PassengerGroupLifecycle" %>
<%@ page import="net.simforge.legacy.airways.Captions" %>
<%

    String mode = request.getParameter("mode");

    Connection connx = DB.getConnection();

    List<PassengerGroup> passengers = null;

    if (mode.equals("city-status")) {
        int cityId = Integer.valueOf(request.getParameter("cityId"));
        int status = Integer.valueOf(request.getParameter("status"));
        passengers = Persistence.loadByQuery(connx, PassengerGroup.class, "select * from aw_pg where position_city_id = " + cityId + " and status = " + status);
    } else if (mode.equals("passengers-with-tickets")) {
        int flightId = Integer.valueOf(request.getParameter("flightId"));
        passengers = Persistence.loadByQuery(connx, PassengerGroup.class, "select g.* from aw_pg g join aw_pg_itinerary i on i.group_id = g.id where i.flight_id = " + flightId);
    } else if (mode.equals("passengers-on-board")) {
        int flightId = Integer.valueOf(request.getParameter("flightId"));
        passengers = Persistence.loadByQuery(connx, PassengerGroup.class, "select * from aw_pg where position_flight_id = " + flightId);
    }


    connx.close();

%>
<table>
    <tr>
        <th>Route</th>
        <th>Size</th>
        <th>Status</th>
    </tr>

    <%

        for (PassengerGroup group : passengers) {
            %>


    <tr>
        <td><%=PassengerGroupLifecycle.l(group)%></td>
        <td><%=group.getSize()%></td>
        <td><%=Captions.get(PassengerGroup.Status.class, group.getStatus())%></td>
        <td><a href="passenger.jsp?id=<%=group.getId()%>">view</a></td>
    </tr>

    <%
        }



    %>


</table>