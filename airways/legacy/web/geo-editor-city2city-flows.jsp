<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.City"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="net.simforge.legacy.airways.newflows.CityFlow"%>
<%@ page import="net.simforge.legacy.airways.newflows.NewFlowLogics"%>
<%@ page import="net.simforge.legacy.airways.newflows.City2CityFlow"%>
<%@ page import="java.util.List"%>
<%@ page import="net.simforge.legacy.airways.newflows.Calculations"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%@ page import="net.simforge.legacy.airways.RefDataCache"%>
<%@ page import="net.simforge.legacy.airways.model.Country"%>
<%

    int id = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();
    City city = Persistence.load(connx, City.class, id);
    CityFlow cityFlow = NewFlowLogics.loadCityFlow(connx, city);
    List<City2CityFlow> c2cFlows = Persistence.loadByQuery(
            connx,
            City2CityFlow.class,
            "select * from %tn% where from_flow_id = " + cityFlow.getId() + " order by active desc, percentage desc");
    connx.close();

    int dailyFlow = Calculations.getDailyFlow(city);

%>
<h3><%=T.mna(city.getName())%> - City-to-City Flows</h3>
Total Daily Flow <%=dailyFlow%>
<table>
    <tr>
        <th>Destination</th>
        <th>Active</th>
        <th>Units</th>
        <th>Flow<br>%</th>
        <th>Avail<br>%</th>
        <th>Theo<br>Flow</th>
        <th>Real<br>Flow</th>
    </tr>

<%

    for (City2CityFlow eachC2cFlow : c2cFlows) {
        CityFlow eachCityFlow = RefDataCache.get(CityFlow.class, eachC2cFlow.getToFlowId());
        City eachCity = RefDataCache.getCity(eachCityFlow.getCityId());

        %>
    <tr>
        <td><%=T.mna(eachCity.getName())%></td>
        <td><%=eachC2cFlow.isActive()%></td>
        <td><%=T.df0_o.format(eachC2cFlow.getUnits())%></td>
        <td><%=T.df0_o.format(eachC2cFlow.getPercentage() * 100)%></td>
        <td><%=T.df0_o.format(eachC2cFlow.getAvailability() * 100)%></td>
        <td><%=eachC2cFlow.isActive() ? Math.round(dailyFlow * eachC2cFlow.getPercentage()) : ""%></td>
        <td><%=eachC2cFlow.isActive() ? Math.round(dailyFlow * eachC2cFlow.getPercentage() * eachC2cFlow.getAvailability()) : ""%></td>
    </tr>
    <%
    }


%>

</table>