<jsp:include page="top-menu.jsp"/>

<%@ page import="forge.commons.db.DB" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.City" %>
<%@ page import="java.util.List" %>
<%@ page import="net.simforge.legacy.airways.model.Airport" %>
<%@ page import="net.simforge.legacy.airways.model.Airport2City" %>
<%

    int cityId = 0;
    String cityIdStr = request.getParameter("cityId");
    if (cityIdStr != null) {
        cityId = Integer.parseInt(cityIdStr);
    }

    int airportId = 0;
    String airportIdStr = request.getParameter("airportId");
    if (airportIdStr != null) {
        airportId = Integer.parseInt(airportIdStr);
    }

    Connection connx = DB.getConnection();

    if (airportId != 0) {
        List<Airport2City> airport2Cities = Persistence.loadByQuery(connx, Airport2City.class, "select * from %tn% where airport_id = " + airportId);
        if (airport2Cities.size() == 1) {
            cityId = airport2Cities.get(0).getCityId();
        }
    }


%>


<table>
    <tr>
        <td width="300px" align="left" valign="top">


            <%


//                List<City> cities = Persistence.loadAll(connx, City.class);
                List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn% where country_id = " + 141);

                for (City city : cities) {
                    %>
            <a href="index.jsp?cityId=<%=city.getId()%>"><%=city.getName()%></a><br>
            <%
                    if (city.getId() == cityId) {

                        List<Airport> airports = Persistence.loadByQuery(connx, Airport.class, "select * from aw_airport a join aw_airport2city c on a.id = c.airport_id where c.city_id = " + cityId);
                        for (Airport airport : airports) {
                            %>
            &nbsp;&nbsp;&nbsp;<a href="index.jsp?airportId=<%=airport.getId()%>"><%=airport.getName()%></a><br>
            <%
                        }
                    }
                }


%>

        </td>
        <td align="left" valign="top">


<%

    if (airportId != 0) {
        %><jsp:include page="airport.jsp"/><%
    } else if (cityId != 0) {
        %><jsp:include page="city.jsp"/><%
    } else {
        %><jsp:include page="empty.html"/><%
    }


%>

        </td>
    </tr>
</table>

<%

    connx.close();

%>
