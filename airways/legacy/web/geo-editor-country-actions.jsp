<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="net.simforge.legacy.airways.model.Country" %><%@
        page import="java.sql.Connection"%><%@
        page import="forge.commons.db.DB"%><%@
        page import="forge.commons.persistence.Persistence"%><%



    String action = request.getParameter("action");
    if ("Create".equals(action)) {
        String name = request.getParameter("name");
        String code = request.getParameter("code");

        Country country = new Country();
        country.setName(name);
        country.setCode(code);

        Connection connx = DB.getConnection();
        country = Persistence.create(connx, country);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-country.jsp?id=" + country.getId());
    } else if ("setName".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        String name = request.getParameter("name");

        Connection connx = DB.getConnection();
        Country country = Persistence.load(connx, Country.class, id);
        country.setName(name);
        Persistence.update(connx, country);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-country.jsp?id=" + id);
    }

%>