<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="net.simforge.legacy.airways.model.City"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="net.simforge.legacy.airways.model.Country"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="forge.commons.Geo"%>
<%@ page import="java.util.*"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%@ page import="net.simforge.legacy.airways.RefDataCache"%>
<%@ page import="net.simforge.legacy.airways.model.Airport2City"%>
<%@ page import="net.simforge.legacy.airways.newflows.CityFlow"%>
<%@ page import="net.simforge.legacy.airways.Captions"%>
<%@ page import="net.simforge.legacy.airways.newflows.NewFlowLogics"%>
<%@ page import="net.simforge.legacy.airways.DT"%>
<%@ page import="org.joda.time.Period"%>
<%@ page import="org.joda.time.format.PeriodFormat"%>
<%

    int id = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();

    City city = Persistence.load(connx, City.class, id);
    Country country = RefDataCache.getCountry(city.getCountryId());

    double delta = 2.0;
    String coordsCond = "where lat between " + (city.getLat() - delta) + " and " + (city.getLat() + delta) +
            " and lon between " + (city.getLon() - delta) + " and " + (city.getLon() + delta);
    List<Airport> airports = Persistence.loadByQuery(connx, Airport.class, "select * from %tn% " + coordsCond);
    List<Airport2City> a2cs = Persistence.loadByQuery(connx, Airport2City.class, "select * from %tn% where city_id = " + id);

    CityFlow cityFlow = NewFlowLogics.loadCityFlow(connx, city);

    connx.close();


    final Map<Airport, Double> airport2nm = new HashMap<Airport, Double>();
    for (Airport airport : airports) {
        airport2nm.put(airport, Geo.distanceNM(airport.getLat(), airport.getLon(), city.getLat(), city.getLon()));
    }
    Collections.sort(airports, new Comparator<Airport>() {
        public int compare(Airport o1, Airport o2) {
            Double d1 = airport2nm.get(o1);
            Double d2 = airport2nm.get(o2);
            return d1.compareTo(d2);
        }
    });

    Map<Integer, Airport2City> airport2a2c = new HashMap<Integer, Airport2City>();
    for (Airport2City a2c : a2cs) {
        airport2a2c.put(a2c.getAirportId(), a2c);
    }

%>

<script type="text/javascript">
    function edit(property, value) {
        var result = prompt("Specify " + property, value);
        if (result == null) {
            return;
        }
        if (value == result) {
            return;
        }
        if (result == "") {
            alert("It is not allowed to have empty " + property);
            return;
        }
        document.location.href = "geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=" + property + "&value=" + result;
    }
</script>



<h3><%=T.m(city.getName())%> <img src="img/lookup.gif" title="Show on map" onclick="window.parent.showOnMap(<%=city.getLat()%>, <%=city.getLon()%>)"></h3>
<table>
    <tr>
        <th>Name</th>
        <td><%=T.mna(city.getName())%>  <img src="img/edit.gif" onclick="edit('name', '<%=T.m(city.getName())%>')"></td>
    </tr>
    <tr>
        <th>Country</th>
        <td><a href="geo-editor-country.jsp?id=<%=country.getId()%>" class="asText"><%=country.getName()%></a></td>
    </tr>
    <tr>
        <th>Population</th>
        <td><%=T.dfMo.format(city.getPopulation())%> <img src="img/edit.gif" onclick="edit('population', <%=city.getPopulation()%>)"></td>
    </tr>
    <tr>
        <th>Dataset</th>
        <td><%
        if (city.getDataset() == 0) {
            %><span class="yes">#0</span><%
        } else {
            %><a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=setDS&dataset=0" class="off">#0</a><%
        } %> <%
        if (city.getDataset() == 1) {
            %><span class="yes">#1</span><%
        } else {
            %><a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=setDS&dataset=1" class="off">#1</a><%
        } %> <%
        if (city.getDataset() == 2) {
            %><span class="yes">#2</span><%
        } else {
            %><a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=setDS&dataset=2" class="off">#2</a><%
        }%></td>
    </tr>
</table>
<h3>City Flow</h3>
<%
    if (cityFlow == null) {
        %>
City flow is not created. <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=createFlow" class="yes">Create?</a><br>
<%
    } else {
        %>
<table>
    <tr>
        <th>Status</th>
        <td><%
            int status = cityFlow.getStatus();
            if (status == CityFlow.Status.Inactive) { %>
            <span class="no">Inactive</span>
            <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=cityFlow.status&value=<%=CityFlow.Status.RecalcThenActivate%>" class="off">make it <%=Captions.get(CityFlow.Status.class, CityFlow.Status.RecalcThenActivate)%></a>
            <% } else if (status == CityFlow.Status.RecalcThenActivate) { %>
            <span class="no"><%=Captions.get(CityFlow.Status.class, CityFlow.Status.RecalcThenActivate)%></span>
            <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=cityFlow.status&value=<%=CityFlow.Status.Inactive%>" class="off">make it Inactive</a>
            <% } else if (status == CityFlow.Status.ActiveButNeedsRecalc) { %>
            <span class="yes"><%=Captions.get(CityFlow.Status.class, CityFlow.Status.ActiveButNeedsRecalc)%></span>
            <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=cityFlow.status&value=<%=CityFlow.Status.Inactive%>" class="off">make it Inactive</a>
            <% } else if (status == CityFlow.Status.Active) { %>
            <span class="yes">Active</span>
            <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=cityFlow.status&value=<%=CityFlow.Status.ActiveButNeedsRecalc%>" class="off">make it <%=Captions.get(CityFlow.Status.class, CityFlow.Status.ActiveButNeedsRecalc)%></a>
            <a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=set&property=cityFlow.status&value=<%=CityFlow.Status.Inactive%>" class="off">make it Inactive</a>
            <% } else { %><span class="todo">todo</span><% } %></td>
    </tr>
    <tr>
        <th>Attraction</th>
        <td>
            <%=T.df0_o.format(cityFlow.getAttraction())%>
            <img src="img/edit.gif" onclick="edit('cityFlow.attraction', '<%=T.df0_o.format(cityFlow.getAttraction())%>')">
        </td>
    </tr>
    <tr>
        <th>Units Threshold</th>
        <td>
            <%=T.df0_000.format(cityFlow.getUnitsThreshold())%>
            <img src="img/edit.gif" onclick="edit('cityFlow.unitsThreshold', '<%=T.df0_000.format(cityFlow.getUnitsThreshold())%>')">
        </td>
    </tr>
    <tr>
        <th>Default Availability</th>
        <td>
            <%=T.df0_000000.format(cityFlow.getDefaultAvailability())%>
            <img src="img/edit.gif" onclick="edit('cityFlow.defaultAvailability', '<%=T.df0_000000.format(cityFlow.getDefaultAvailability())%>')">
        </td>
    </tr>
    <tr>
        <th>Last Recalc</th>
        <td>
            <%=PeriodFormat.getDefault().print(new Period(cityFlow.getLastRecalcDt(), DT.now()))%>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><a href="geo-editor-city2city-flows.jsp?id=<%=city.getId()%>" class="asText">view City-to-City Flows</a></td>
    </tr>
</table>
<%
    }

%>
<h3>Airports</h3>
<table>
    <tr>
        <th>Airport</th>
        <th>City</th>
        <th>Dist<br>nm</th>
        <th>A2C</th>
    </tr>
    <%

        for (Airport eachAirport : airports) {
            int dist = (int) Math.round(airport2nm.get(eachAirport));
            boolean isCityOfAirport = eachAirport.getCityId() == city.getId();
            String style = isCityOfAirport ? " class='highlight'" : "";
            Airport2City a2c = airport2a2c.get(eachAirport.getId());

    %>

    <tr>
        <td <%=style%>>
            <a href="geo-editor-airport.jsp?id=<%=eachAirport.getId()%>" class="asText">
                <%=eachAirport.getIcao()%> / <%=eachAirport.getIata()%> <%=T.limit(T.m(eachAirport.getName()), 20)%> (#<%=eachAirport.getDataset()%>)
            </a>
        </td>
        <td <%=style%>><%
            if (eachAirport.getCityId() != 0) {
                if (eachAirport.getCityId() != city.getId()) {
                    %><a href="geo-editor-city.jsp?id=<%=eachAirport.getCityId()%>" class="asText"><%=T.mna(RefDataCache.getCity(eachAirport.getCityId()).getName())%></a><%;
                } else {
                    %><%=T.mna(city.getName())%><%
                }
            } else {
                %>n/a<%
            }
                %></td>
        <td <%=style%>><%=dist%></td>
        <td <%=style%>><%
            if (a2c == null) {
                %><a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=addA2C&airportId=<%=eachAirport.getId()%>" class="off">no</a><%
            } else {
                %><a href="geo-editor-city-actions.jsp?id=<%=city.getId()%>&action=delA2C&airportId=<%=eachAirport.getId()%>" class="yes">Yes</a><%
            }
        %></td>
    </tr>
<%
    }
%>
</table>


<%
    if ("yes".equals(request.getParameter("refresh"))) {
%>
<script>
    window.parent.refreshData();
</script>
<%
    }
%>
