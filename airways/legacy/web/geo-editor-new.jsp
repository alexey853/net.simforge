<jsp:include page="geo-editor-editor-header.jsp"/>

<script>
    function newcountry() {
        window.parent.clearNewMarker();
        window.location.href = "geo-editor-country-new.jsp";
    }

    function newcity() {
        window.location.href = "geo-editor-city-new.jsp";
    }

    function nothing() {
        window.parent.clearNewMarker();
        window.location.href = "empty.html";
    }
</script>

<h3>What to create</h3>
<input type="button" value="City" onclick="newcity()"><br>
<br>
<br>
<input type="button" value="Airport"><br>
<br>
<br>
<input type="button" value="Country" onclick="newcountry()"><br>
<br>
<br>
<input type="button" value="Nothing" onclick="nothing()"><br>
