<meta http-equiv="refresh" content="10">
<jsp:include page="top-menu.jsp"/>

<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.Flight" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="net.simforge.legacy.airways.*"%>
<%

    Connection connx = DB.getConnection();

    List<Flight> departures = Persistence.loadByQuery(connx, Flight.class, "select * from aw_flight where dep_time - interval '3 hours' <= now() and now() <= dep_time order by dep_time, number");
    List<Flight> flying = Persistence.loadByQuery(connx, Flight.class, "select * from aw_flight where dep_time < now() and now() < arr_time order by dep_time, arr_time, number");
    List<Flight> arrivals = Persistence.loadByQuery(connx, Flight.class, "select * from aw_flight where arr_time <= now() and now() <= arr_time + interval '3 hours' order by arr_time, number");


    List<Flight> flights = new ArrayList<Flight>();
    flights.addAll(arrivals);
    flights.addAll(flying);
    flights.addAll(departures);


%>

<table>
    <tr>
        <th colspan="10">Flights</th>
    </tr>
    <tr>
        <th>Status</th>
        <th>Flight</th>
        <th>From</th>
        <th>Dep<br>time</th>
        <th>To</th>
        <th>Arr<br>time</th>
        <th>Seats<br>sold, %</th>
        <td>POB</td>
    </tr>

    <%

        for (Flight flight : flights) {
            Airport fromAirport = RefDataCache.getAirport(flight.getFromAirportId());
            Airport toAirport = RefDataCache.getAirport(flight.getToAirportId());
    %>
    <tr>
        <td><%=Captions.get(Flight.Status.class, flight.getStatus())%></td>
        <td><a href="flight.jsp?id=<%=flight.getId()%>"><%=flight.getNumber()%></a></td>
        <td><%=RefDataCache.getCity(fromAirport.getCityId()).getName()%>, <%=fromAirport.getIata()%></td>
        <td><%=DT.tF.print(flight.getDepTime())%></td>
        <td><%=RefDataCache.getCity(toAirport.getCityId()).getName()%>, <%=toAirport.getIata()%></td>
        <td><%=DT.tF.print(flight.getArrTime())%></td>
        <td><%=FlightHelper.getSoldPercentage(flight)%></td>
        <td><%=FlightHelper.getPOB(connx, flight)%></td>
    </tr>
    <%

        }

    %>

</table>

<%

    connx.close();

%>