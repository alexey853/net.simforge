<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.Country"%>
<%@ page import="java.util.List"%>
<%

    Connection connx = DB.getConnection();
    List<Country> countries = Persistence.loadByQuery(connx, Country.class, "select * from %tn% order by name");
    connx.close();


%>

<script>

    function readLatLon() {
        lat = window.parent.newMarker.getLatLng().lat();
        lon = window.parent.newMarker.getLatLng().lng();
        document.getElementById("lat").value = lat;
        document.getElementById("lon").value = lon;
    }

</script>

<form action="geo-editor-city-actions.jsp" method="POST">
<h3>New city</h3>
<table>
    <tr>
        <th>Country</th>
        <td><select name="countryId"><%
            for (Country country : countries) {
                %><option value="<%=country.getId()%>"><%=country.getName()%></option><%
            }

            %></select></td>
    </tr>
    <tr>
        <th>Name</th>
        <td><input type="text" name="name"></td>
    </tr>
    <tr>
        <th>Lat</th>
        <td><input type="text" id="lat" name="lat" readonly="true"></td>
    </tr>
    <tr>
        <th>Lon</th>
        <td><input type="text" id="lon" name="lon" readonly="true"></td>
    </tr>
    <tr>
        <th>Population</th>
        <td><input type="text" name="population"></td>
    </tr>
</table>
<br>
<input type="submit" name="action" value="Create">
</form>

<script>
    readLatLon();
</script>
