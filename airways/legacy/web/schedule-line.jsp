<jsp:include page="top-menu.jsp"/>

<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.ScheduleLine" %>
<%@ page import="net.simforge.legacy.airways.model.Flight" %>
<%@ page import="org.joda.time.DateMidnight" %>
<%@ page import="org.joda.time.DateTimeConstants" %>
<%@ page import="org.joda.time.format.DateTimeFormatter" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="net.simforge.legacy.airways.*"%>
<%

    int lineId = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();
    ScheduleLine line = Persistence.load(connx, ScheduleLine.class, lineId);
    List<Flight> flights = Persistence.loadByQuery(connx, Flight.class, "select * from %tn% where line_id = " + lineId);
    connx.close();

    Map<DateMidnight, Flight> date2flight = new HashMap<DateMidnight, Flight>();
    for (Flight flight : flights) {
        Date date = flight.getDate();
        DateMidnight dateMidnight = new DateMidnight(date);
        date2flight.put(dateMidnight, flight);
    }


%>
<table>
    <tr>
        <th>Flight</th>
        <td><%=line.getNumber()%></td>
    </tr>
    <tr>
        <th>Active</th>
        <td><% if (line.isActive()) {
            %><font color='green'>Yes</font> <a href="schedule-line-action.jsp?id=<%=line.getId()%>&action=deactivate">deactivate</a><%
        } else {
            %><font color='red'>NO</font> <a href="schedule-line-action.jsp?id=<%=line.getId()%>&action=activate">activate</a><%
        } %></td>
    </tr>
    <tr>
        <th>From</th>
        <td><%=RefDataCache.getAirport(line.getFromAirportId()).getIata()%><br>
            <%=RefDataCache.getAirport(line.getFromAirportId()).getName()%></td>
    </tr>
    <tr>
        <th>To</th>
        <td><%=RefDataCache.getAirport(line.getToAirportId()).getIata()%><br>
            <%=RefDataCache.getAirport(line.getToAirportId()).getName()%></td>
    </tr>
    <tr>
        <th>Dep time</th>
        <td><%=T.toHHmm(line.getDepTime())%></td>
    </tr>
    <tr>
        <th>Duration</th>
        <td><%=T.toHHmm(line.getDuration())%></td>
    </tr>
</table>
<br>
<b>Calendar</b><br>
<%

    DateMidnight today = new DateMidnight();
    DateMidnight sinceDay = DT.addDays(today, -7).toDateMidnight();
    while (sinceDay.getDayOfWeek() != DateTimeConstants.MONDAY) {
        sinceDay = DT.addDays(sinceDay, -1).toDateMidnight();
    }

    %>
<table>
    <tr>
        <th>Mon</th>
        <th>Tue</th>
        <th>Wen</th>
        <th>Thu</th>
        <th>Fri</th>
        <th>Sat</th>
        <th>Sun</th>
    </tr>
<%

    DateTimeFormatter dt = DateTimeFormat.forPattern("dd MMM").withLocale(Locale.UK);


    for (int week = 0; week < 6; week++) {
        %>
    <tr>
        <%

            for (int weekday = DateTimeConstants.MONDAY; weekday <= DateTimeConstants.SUNDAY; weekday++) {
                DateMidnight day = DT.addDays(sinceDay, week * 7 + (weekday - 1)).toDateMidnight();
                Flight flight = date2flight.get(day);

                boolean isToday = day.equals(DT.today());
                boolean isDateOfFlight = ScheduleLineLogics.isDateOfFlight(line, day);

                String bgcolor = null;

                if (isDateOfFlight) {
                    bgcolor = "#E0FFE0";
                } else {
                    bgcolor = "#E0E0E0";
                }

//                if (isToday) {
//                    bgcolor = "#FFE0D0";
//                }

                %>

        <td width="150" height="75" align="left" valign="top" <%=bgcolor != null ? "bgcolor='" + bgcolor + "'" : ""%>><%=dt.print(day)%><br>
            <%

                if (flight != null) {

            %>
            Status: <a href="flight.jsp?id=<%=flight.getId()%>"><%=Captions.get(Flight.Status.class, flight.getStatus())%></a><br>
            Seats sold: <%=FlightHelper.getSoldPercentage(flight)%>%
            <%

                }

            %>

        </td>
        
        <%
            }


        %>

    </tr>
    <%
    }


%>

</table>