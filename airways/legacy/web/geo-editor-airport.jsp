<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="net.simforge.legacy.airways.RefDataCache"%>
<%@ page import="net.simforge.legacy.airways.model.City"%>
<%@ page import="forge.commons.Geo"%>
<%@ page import="java.util.*"%>
<%@ page import="net.simforge.legacy.airways.model.Airport2City"%>
<%@ page import="net.simforge.legacy.airways.model.Country"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%

    int id = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();

    Airport airport = Persistence.load(connx, Airport.class, id);

    double delta = 2.0;
    String coordsCond = "where lat between " + (airport.getLat() - delta) + " and " + (airport.getLat() + delta) +
            " and lon between " + (airport.getLon() - delta) + " and " + (airport.getLon() + delta);
    List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn% " + coordsCond);
    List<Airport2City> a2cs = Persistence.loadByQuery(connx, Airport2City.class, "select * from %tn% where airport_id = " + airport.getId());
    connx.close();

    final Map<City, Double> city2nm = new HashMap<City, Double>();
    for (City city : cities) {
        city2nm.put(city, Geo.distanceNM(airport.getLat(), airport.getLon(), city.getLat(), city.getLon()));
    }
    Collections.sort(cities, new Comparator<City>() {
        public int compare(City o1, City o2) {
            Double d1 = city2nm.get(o1);
            Double d2 = city2nm.get(o2);
            return d1.compareTo(d2);
        }
    });

    Map<Integer, Airport2City> city2a2c = new HashMap<Integer, Airport2City>();
    for (Airport2City a2c : a2cs) {
        city2a2c.put(a2c.getCityId(), a2c);
    }

    City city = airport.getCityId() != 0 ? RefDataCache.getCity(airport.getCityId()) : null;
    Country country = city != null ? RefDataCache.getCountry(city.getCountryId()) : null;

%>

<script>
    function editname() {
        var oldname = "<%=T.m(airport.getName())%>";
        var result = prompt("Specify new name for this airport", oldname);
        if (result == null) {
            return;
        }
        if (oldname == result) {
            return;
        }
        if (result == "") {
            alert("It is not allowed to remove name");
            return;
        }
        document.location.href = "geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=setName&name=" + result;
    }
</script>

<h3><%=airport.getIcao()%> / <%=airport.getIata()%> <%=T.m(airport.getName())%> <img src="img/lookup.gif" title="Show on map" onclick="window.parent.showOnMap(<%=airport.getLat()%>, <%=airport.getLon()%>)"></h3>
<table>
    <tr><th>Name</th><td><%=T.mna(airport.getName())%> <img src="img/edit.gif" onclick="editname()"></td></tr>
    <tr><th>City</th><td><%
        if (city == null) {
            %>n/a<%
        } else {
            %><a href="geo-editor-city.jsp?id=<%=city.getId()%>" class="asText"><%=T.mna(city.getName())%></a>,
              <a href="geo-editor-country.jsp?id=<%=country.getId()%>" class="asText"><%=T.mna(country.getName())%></a><%
        }
            %></td></tr>
    <tr><th>ICAO</th><td><%=airport.getIcao()%></td></tr>
    <tr><th>IATA</th><td><%=airport.getIata()%></td></tr>
    <tr><th>Dataset</th><td><%
        if (airport.getDataset() == 0) {
            %><span class="yes">#0</span><%
        } else {
            %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=setDS&dataset=0" class="off">#0</a><%
        } %> <%
        if (airport.getDataset() == 1) {
            %><span class="yes">#1</span><%
        } else {
            %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=setDS&dataset=1" class="off">#1</a><%
        } %> <%
        if (airport.getDataset() == 3) {
            %><span class="yes">#3</span><%
        } else {
            %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=setDS&dataset=3" class="off">#3</a><%
        }%></td></tr>
</table>
<br>
<br>
<h3>Nearest cities</h3>
<table>
    <tr>
        <th>City</th>
        <th>Pop</th>
        <th>Dist<br>nm</th>
        <th>A2C</th>
    </tr>
    <%

        for (City eachCity : cities) {
            int dist = (int) Math.round(city2nm.get(eachCity));
            boolean isCityOfAirport = airport.getCityId() == eachCity.getId();
            String style = isCityOfAirport ? " class='highlight'" : "";
            Airport2City a2c = city2a2c.get(eachCity.getId());

             %>
    <tr>
        <td <%=style%>><%=eachCity.getName()%><%
            if (city != null && eachCity.getCountryId() != city.getCountryId()) {
                %>, <%=RefDataCache.getCountry(eachCity.getCountryId()).getName()%><%
            } %> (#<%=eachCity.getDataset()%>)</td>
        <td <%=style%>><%=new DecimalFormat("0.0").format(eachCity.getPopulation() / 1000000.)%></td>
        <td <%=style%>><%=dist%></td>
        <td <%=style%>><%
            if (a2c == null) {
                %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=addA2C&cityId=<%=eachCity.getId()%>" class="off">no</a><%
            } else {
                %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=delA2C&cityId=<%=eachCity.getId()%>" class="yes">Yes</a><%
            }
        %></td>
        <td <%=style%>><%
            if (isCityOfAirport) {
                %>City<%
            } else {
                %><a href="geo-editor-airport-actions.jsp?id=<%=airport.getId()%>&action=setAsCity&cityId=<%=eachCity.getId()%>">set as City</a><%
            } %></td>
    </tr>
    <%
        }

    %>
</table>

<%
    if ("yes".equals(request.getParameter("refresh"))) {
%>
<script>
    window.parent.refreshData();
</script>
<%
    }
%>
