<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.ScheduleLine" %>
<%@ page import="java.util.List" %>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="net.simforge.legacy.airways.model.Flight" %>
<%@ page import="net.simforge.legacy.airways.*" %>
<%@ page import="org.joda.time.DateTime" %>
<jsp:include page="top-menu.jsp"/>

<%


    Connection connx = DB.getConnection();

    List<ScheduleLine> lines = Persistence.loadByQuery(connx, ScheduleLine.class, "select * from %tn% order by number");


    int flightsPerLine = 10;

%>
<a href="schedule-line-new.jsp">add new</a>
<table>
    <tr>
        <th>Flight</th>
        <th>Active</th>
        <th>From</th>
        <th>To</th>
        <th>Dep time</th>
        <th>Duration</th>
        <th>Arr time</th>
        <th>Frequency</th>
        <th colspan="<%=flightsPerLine%>">Next flights seats sold</th>
    </tr>

<%

    for (ScheduleLine line : lines) {
        Airport fromAirport = RefDataCache.getAirport(line.getFromAirportId());
        Airport toAirport = RefDataCache.getAirport(line.getToAirportId());


        %>

    <tr>
        <td><a href="schedule-line.jsp?id=<%=line.getId()%>"><%=line.getNumber()%></a></td>
        <td><% if (line.isActive()) {
            %><font color='green'>Yes</font><%
        } else {
            %><font color='red'>NO</font><%
        } %></td>
        <td><%=RefDataCache.getCity(fromAirport.getCityId()).getName()%>, <%=fromAirport.getIata()%></td>
        <td><%=RefDataCache.getCity(toAirport.getCityId()).getName()%>, <%=toAirport.getIata()%></td>
        <td><%=T.toHHmm(line.getDepTime())%></td>
        <td><%=T.toHHmm(line.getDuration())%></td>
        <td><%=T.toHHmm(ScheduleLineLogics.sum(line.getDepTime().toDateTimeToday(), line.getDuration()).toTimeOfDay())%></td>
        <td><%=line.getWeekdays()%></td>

<%

        List<Flight> flights = Persistence.loadByQuery(connx, Flight.class, "select * from %tn% where line_id = " + line.getId() + " and date >= '" + DT.DF.print(DT.today()) + "' order by date limit " + flightsPerLine);
        for (Flight flight : flights) {
            int percentage = FlightHelper.getSoldPercentage(flight);
            String grade = "";
            if (percentage < 5)
                grade = "#FF8080";
            else if (percentage < 17)
                grade = "#FFA080";
            else if (percentage < 30)
                grade = "#FFC080";
            else if (percentage < 43)
                grade = "#FFE080";
            else if (percentage < 56)
                grade = "#FFFF80";
            else if (percentage < 69)
                grade = "#E0FF80";
            else if (percentage < 72)
                grade = "#C0FF80";
            else if (percentage < 95)
                grade = "#A0FF80";
            else
                grade = "#80FF80";
            %><td align="center" bgcolor="<%=grade%>"><a href="flight.jsp?id=<%=flight.getId()%>" title="<%
                DateTime dof = new DateTime(flight.getDate());
                if (dof.equals(DT.today())) {
                    %>Today, <%=Captions.get(Flight.Status.class, flight.getStatus())%><%
                } else if (dof.equals(DT.addDays(1))) {
                    %>Tomorrow, <%=Captions.get(Flight.Status.class, flight.getStatus())%><%
                } else {
                    %><%=DT.DF.print(dof)%><%
                }
            %>"><%=percentage%></a></td><%
        }


%>

    </tr>

    <%

    }


%>
</table>
<a href="schedule-line-new.jsp">add new</a>

<%

    connx.close();

%>

