<jsp:include page="geo-editor-editor-header.jsp"/>

<form action="geo-editor-country-actions.jsp" method="POST">
<h3>New country</h3>
<table>
    <tr>
        <th>Name</th>
        <td><input type="text" name="name"></td>
    </tr>
    <tr>
        <th>Code</th>
        <td><input type="text" name="code"></td>
    </tr>
</table>
<br>
<input type="submit" name="action" value="Create">
</form>
