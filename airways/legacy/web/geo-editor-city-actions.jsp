<%@ page import="net.simforge.legacy.airways.model.City" %><%@
        page import="java.sql.Connection"%><%@
        page import="forge.commons.db.DB"%><%@
        page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.Airport2City"%>
<%@ page import="net.simforge.legacy.airways.newflows.CityFlow"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%



    String action = request.getParameter("action");
    if ("Create".equals(action)) {
        int countryId = Integer.valueOf(request.getParameter("countryId"));
        String name = request.getParameter("name");
        double lat = Double.valueOf(request.getParameter("lat"));
        double lon = Double.valueOf(request.getParameter("lon"));
        int population = Integer.valueOf(request.getParameter("population"));

        City city = new City();
        city.setCountryId(countryId);
        city.setName(name);
        city.setLat(lat);
        city.setLon(lon);
        city.setPopulation(population);

        Connection connx = DB.getConnection();
        city = Persistence.create(connx, city);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + city.getId());
    } else if ("addA2C".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int airportId = Integer.valueOf(request.getParameter("airportId"));

        Connection connx = DB.getConnection();
        Airport2City a2c = new Airport2City();
        a2c.setAirportId(airportId);
        a2c.setCityId(id);
        Persistence.create(connx, a2c);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + id);
    } else if ("delA2C".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int airportId = Integer.valueOf(request.getParameter("airportId"));

        Connection connx = DB.getConnection();
        DB.executeUpdate(connx, "delete from aw_airport2city where airport_id = " + airportId + " and city_id = " + id);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + id);
    } else if ("setDS".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int dataset = Integer.valueOf(request.getParameter("dataset"));

        Connection connx = DB.getConnection();
        City city = Persistence.load(connx, City.class, id);
        city.setDataset(dataset);
        Persistence.update(connx, city);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + id + "&refresh=yes");
    } else if ("set".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        String property = request.getParameter("property");
        String value = request.getParameter("value");

        Connection connx = DB.getConnection();
        if (property.startsWith("cityFlow.")) {
            CityFlow cityFlow = Persistence.loadByQuery(connx, CityFlow.class, "select * from %tn% where city_id = " + id).get(0);
            if ("cityFlow.status".equals(property)) {
                cityFlow.setStatus(Integer.valueOf(value));
            } else if ("cityFlow.attraction".equals(property)) {
                cityFlow.setAttraction(T.df0_o.parse(value).doubleValue());
            } else if ("cityFlow.unitsThreshold".equals(property)) {
                cityFlow.setUnitsThreshold(T.df0_000.parse(value).doubleValue());
            } else if ("cityFlow.defaultAvailability".equals(property)) {
                cityFlow.setDefaultAvailability(T.df0_000000.parse(value).doubleValue());
            }
            Persistence.update(connx, cityFlow);
        } else {
            City city = Persistence.load(connx, City.class, id);
            if ("name".equals(property)) {
                city.setName(value);
            } else if ("population".equals(property)) {
                city.setPopulation(Integer.valueOf(value));
            }
            Persistence.update(connx, city);
        }
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + id);
    } else if ("createFlow".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));

        Connection connx = DB.getConnection();
        CityFlow cityFlow = new CityFlow();
        cityFlow.setCityId(id);
        cityFlow.setStatus(CityFlow.Status.Inactive);
        cityFlow.setAttraction(1);
        cityFlow.setUnitsThreshold(0.1);
        cityFlow.setDefaultAvailability(0.01);
        Persistence.create(connx, cityFlow);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-city.jsp?id=" + id);
    }

%>