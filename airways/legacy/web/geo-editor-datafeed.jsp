<%@ page import="forge.commons.db.DB"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.City"%>
<%@ page import="java.util.List"%>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%

    double fromLat = Double.parseDouble(request.getParameter("fromLat"));
    double fromLon = Double.parseDouble(request.getParameter("fromLon"));

    double toLat = Double.parseDouble(request.getParameter("toLat"));
    double toLon = Double.parseDouble(request.getParameter("toLon"));

    String coordsCond = "where lat between " + fromLat + " and " + toLat +
            " and lon between " + fromLon + " and " + toLon;

    String popFromStr = request.getParameter("popFrom");
    String popToStr = request.getParameter("popTo");
    String popCond = "";
    if (!popFromStr.equals("no")) {
        popCond += " and population >= " + popFromStr;
    }
    if (!popToStr.equals("no")) {
        popCond += " and population <= " + popToStr;
    }

    String datasets = request.getParameter("datasets");
    String datasetsCond = " and dataset in (" + (datasets.length() != 0 ? datasets : "0") + ")";

    Connection connx = DB.getConnection();

    List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn% " + coordsCond + popCond + datasetsCond);
    List<Airport> airports = Persistence.loadByQuery(connx, Airport.class, "select * from %tn% " + coordsCond + datasetsCond);

    connx.close();

%>
<script>
    var cityData = new Array( <%
            for (City city : cities) { %>
        <%=city.getId()%>, <%=city.getCountryId()%>, "<%=city.getName()%>", <%=city.getLat()%>, <%=city.getLon()%>, <%=city.getDataset()%>,<%
            } %> "EndOfCityData");
    var airportData = new Array( <%
            for (Airport airport : airports) { %>
        <%=airport.getId()%>, <%=airport.getCityId()%>, "<%=airport.getIcao()%>", "<%=airport.getIata()%>", "<%=airport.getName()%>", <%=airport.getLat()%>, <%=airport.getLon()%>, <%=airport.getDataset()%>,<%
            } %> "EndOfAirportData");
    window.parent.setData(cityData, airportData);
</script>

<table>
    <tr><th>Cities</th><td><%=cities.size()%></td></tr>
    <tr><th>Airports</th><td><%=airports.size()%></td></tr>
</table>
