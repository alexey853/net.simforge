<jsp:include page="geo-editor-editor-header.jsp"/>

<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.Country"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%
    int id = Integer.valueOf(request.getParameter("id"));
    Connection connx = DB.getConnection();
    Country country = Persistence.load(connx, Country.class, id);
    connx.close();


%>

<script>
    function editname() {
        var oldname = "<%=T.m(country.getName())%>";
        var result = prompt("Specify new name for this country", oldname);
        if (result == null) {
            return;
        }
        if (oldname == result) {
            return;
        }
        if (result == "") {
            alert("It is not allowed to remove name");
            return;
        }
        document.location.href = "geo-editor-country-actions.jsp?id=<%=country.getId()%>&action=setName&name=" + result;
    }
</script>

<h3><%=country.getName()%></h3>
<table>
    <tr>
        <th>Name</th>
        <td><%=country.getName()%> <img src="img/edit.gif" onclick="editname()"></td>
    </tr>
    <tr>
        <th>Code</th>
        <td><%=country.getCode()%></td>
    </tr>
</table>
