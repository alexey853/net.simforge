<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="net.simforge.legacy.airways.model.PassengerGroup"%>
<%@ page import="net.simforge.legacy.airways.DT"%>
<%
    int groupId = Integer.valueOf(request.getParameter("id"));
    String action = request.getParameter("action");

    Connection connx = DB.getConnection();
    PassengerGroup group = Persistence.load(connx, PassengerGroup.class, groupId);

    if ("makeItLookingForTickets".equals(action)) {
        group.setStatus(PassengerGroup.Status.LookingForTickets);
        group.setHeartbeatDt(DT.now());
        group.setExpireDt(DT.addDays(1));
        Persistence.update(connx, group);
    }

    connx.close();

    response.sendRedirect("passenger.jsp?id=" + group.getId());

%>