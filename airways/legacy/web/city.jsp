<%@ page import="net.simforge.legacy.airways.model.City" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="net.simforge.legacy.airways.Captions" %>
<%@ page import="net.simforge.legacy.airways.model.PassengerGroupStatus" %>
<%

    int cityId = Integer.valueOf(request.getParameter("cityId"));

    Connection connx = DB.getConnection();

    City city = Persistence.load(connx, City.class, cityId);

    %>

<table>
    <tr>
        <th>Name</th>
        <td><%=city.getName()%></td>
    </tr>
    <tr>
        <th>Population</th>
        <td><%=city.getPopulation()%></td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
        <th>Status</th>
        <th>Groups</th>
        <th>Passengers</th>
    </tr>
    <%
        String sql = "select status, count(id), sum(size) from aw_passenger_group where position_city_id = " + cityId + " group by status order by status";
        Statement st = connx.createStatement();
        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {
            int status = rs.getInt(1);%>
    <tr>
        <td><%=Captions.get(PassengerGroupStatus.class, status)%></td>
        <td><%=rs.getInt(2)%></td>
        <td><%=rs.getInt(3)%></td>
        <td><a href="passengers.jsp?mode=city-status&cityId=<%=cityId%>&status=<%=status%>">view</a></td>
    </tr>
    <%
        }

        rs.close();
        st.close();
    %>
    
</table>
<%

    connx.close();
%>