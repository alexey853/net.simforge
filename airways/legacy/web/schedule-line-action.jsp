<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="net.simforge.legacy.airways.model.ScheduleLine"%>
<%@ page import="forge.commons.persistence.Persistence"%>
<%
    int lineId = Integer.valueOf(request.getParameter("id"));
    String action = request.getParameter("action");

    Connection connx = DB.getConnection();
    ScheduleLine line = Persistence.load(connx, ScheduleLine.class, lineId);

    if ("activate".equals(action)) {
        line.setActive(true);
        Persistence.update(connx, line);
        connx.commit();
    } else if ("deactivate".equals(action)) {
        line.setActive(false);
        Persistence.update(connx, line);
        connx.commit();
    }

    connx.close();

    response.sendRedirect("schedule-line.jsp?id=" + line.getId());

%>