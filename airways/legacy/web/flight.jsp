<jsp:include page="top-menu.jsp"/>

<%@ page import="java.sql.Connection" %>
<%@ page import="net.simforge.legacy.airways.model.Flight" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.FlightEvent" %>
<%@ page import="java.util.List" %>
<%@ page import="net.simforge.legacy.airways.DT" %>
<%@ page import="org.joda.time.DateTime" %>
<%@ page import="net.simforge.legacy.airways.RefDataCache" %>
<%@ page import="net.simforge.legacy.airways.Captions" %>
<%@ page import="net.simforge.legacy.airways.T"%>
<%@ page import="org.joda.time.DateMidnight"%>
<%

    int id = Integer.valueOf(request.getParameter("id"));

    Connection connx = DB.getConnection();
    Flight flight = Persistence.load(connx, Flight.class, id);
    List<FlightEvent> events = Persistence.loadByQuery(connx, FlightEvent.class, "select * from %tn% where flight_id = " + id + " order by dt");
    connx.close();


%>

<table>
    <tr>
        <th>Flight</th>
        <td><%=flight.getNumber()%></td>
    </tr>
    <tr>
        <th>Date</th>
        <td><%=DT.DF.print(new DateTime(flight.getDate()))%></td>
    </tr>
    <tr>
        <th>Status</th>
        <td><%=Captions.get(Flight.Status.class, flight.getStatus())%></td>
    </tr>
    <tr>
        <th>From</th>
        <td><%=RefDataCache.getAirport(flight.getFromAirportId()).getIata()%><br>
            <%=RefDataCache.getAirport(flight.getFromAirportId()).getName()%></td>
    </tr>
    <tr>
        <th>To</th>
        <td><%=RefDataCache.getAirport(flight.getToAirportId()).getIata()%><br>
            <%=RefDataCache.getAirport(flight.getToAirportId()).getName()%></td>
    </tr>
    <tr>
        <th>Dep time</th>
        <td><%=DT.DtF.print(flight.getDepTime())%></td>
    </tr>
    <tr>
        <th>Arr time</th>
        <td><%=DT.DtF.print(flight.getArrTime())%></td>
    </tr>
    <tr>
        <th>Total tickets</th>
        <td><%=flight.getTotalTickets()%></td>
    </tr>
    <tr>
        <th>Free tickets</th>
        <td><%=flight.getFreeTickets()%></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><a href="schedule-line.jsp?id=<%=flight.getLineId()%>">schedule line</a><br>
            <a href="passengers.jsp?mode=passengers-with-tickets&flightId=<%=id%>">passengers with tickets</a><br>
            <a href="passengers.jsp?mode=passengers-on-board&flightId=<%=id%>">passengers on board</a>
        </td>
    </tr>
</table>
<br>
<b>Event log</b><br>
<table>
    <tr>
        <th colspan="2">DT</th>
        <th>Msg</th>
    </tr>

    <%

        DateMidnight lastDM = null;

        for (FlightEvent event : events) {
            DateMidnight currDM = event.getDt().toDateMidnight();
            boolean printDate = lastDM != null ? !currDM.isEqual(lastDM) : true;
            lastDM = currDM;

            %>
    <tr>
        <td><%=printDate ? DT.DF.print(event.getDt()) : ""%></td>
        <td><%=DT.tF.print(event.getDt())%></td>
        <td><%=event.getMsg()%></td>
    </tr>

    <%
        }


    %>
</table>
