<%@ page import="forge.commons.persistence.Persistence"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="forge.commons.db.DB"%>
<%@ page import="net.simforge.legacy.airways.model.Airport"%>
<%@ page import="net.simforge.legacy.airways.model.Airport2City"%>
<%
    String action = request.getParameter("action");

    if ("setAsCity".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int cityId = Integer.valueOf(request.getParameter("cityId"));

        Connection connx = DB.getConnection();
        Airport airport = Persistence.load(connx, Airport.class, id);
        airport.setCityId(cityId);
        Persistence.update(connx, airport);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-airport.jsp?id=" + id);
    } else if ("addA2C".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int cityId = Integer.valueOf(request.getParameter("cityId"));

        Connection connx = DB.getConnection();
        Airport2City a2c = new Airport2City();
        a2c.setAirportId(id);
        a2c.setCityId(cityId);
        Persistence.create(connx, a2c);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-airport.jsp?id=" + id);
    } else if ("delA2C".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int cityId = Integer.valueOf(request.getParameter("cityId"));

        Connection connx = DB.getConnection();
        DB.executeUpdate(connx, "delete from aw_airport2city where airport_id = " + id + " and city_id = " + cityId);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-airport.jsp?id=" + id);
    } else if ("setDS".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        int dataset = Integer.valueOf(request.getParameter("dataset"));

        Connection connx = DB.getConnection();
        Airport airport = Persistence.load(connx, Airport.class, id);
        airport.setDataset(dataset);
        Persistence.update(connx, airport);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-airport.jsp?id=" + id + "&refresh=yes");
    } else if ("setName".equals(action)) {
        int id = Integer.valueOf(request.getParameter("id"));
        String name = request.getParameter("name");

        Connection connx = DB.getConnection();
        Airport airport = Persistence.load(connx, Airport.class, id);
        airport.setName(name);
        Persistence.update(connx, airport);
        connx.commit();
        connx.close();

        response.sendRedirect("geo-editor-airport.jsp?id=" + id);
    }


%>