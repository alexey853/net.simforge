<jsp:include page="top-menu.jsp"/>
<%@ page import="forge.commons.Settings"%>
<!DOCTYPE html "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Airways - Geo Editor</title>
    <script src="http://maps.google.com/maps?file=api&v=2&sensor=true&key=<%=Settings.get("web.google-maps-key")%>" type="text/javascript"></script>
    <script type="text/javascript">

        var map;
        var newMarker;
        var currentStatus = 'nothing';
        var cities = new Object();
        var airports = new Object();

        function initialize() {
            if (GBrowserIsCompatible()) {
                map = new GMap2(document.getElementById("map_canvas"));
                map.setCenter(new GLatLng(51.5, -0.1), 9);
                map.setUIToDefault();

                GEvent.addListener(map, "click", function(overlay, latlng) {
//                    onMapClick(latlng);
                });

                GEvent.addListener(map, "moveend", function() {
                    onMapMoveEnd();
                });

                refreshData();
            }
        }

        function onMapClick(latlng) {
            if (currentStatus == 'nothing') {
                newMarker = new GMarker(latlng);
                newMarker.value = "New...";
                map.addOverlay(newMarker);
                document.getElementById("editor").src = "geo-editor-new.jsp";
                currentStatus = 'newMarker';
            }
        }

        function onMapMoveEnd() {
            refreshData();
        }

        function clearNewMarker() {
            if (currentStatus == 'newMarker') {
                map.removeOverlay(newMarker);
                newMarker = null;
                currentStatus = 'nothing';
            }
        }

        function getIcon(url) {
            var icon = new GIcon(G_DEFAULT_ICON, url);
            icon.iconSize = new GSize(32, 32);
            icon.shadowSize = new GSize(56, 32);
            icon.iconAnchor = new GPoint(16, 32);
            icon.infoWindowAnchor = new GPoint(16, 0);
            return icon;
        }

        function refreshData() {
            var bounds = map.getBounds();
            var southWest = bounds.getSouthWest();
            var northEast = bounds.getNorthEast();
            var coords = "fromLat=" + southWest.lat() + "&fromLon=" + southWest.lng() + "&" +
                         "toLat=" + northEast.lat() + "&toLon=" + northEast.lng()
            var pop = "popFrom=" + document.getElementById("popFrom").value + "&" + "popTo=" + document.getElementById("popTo").value;

            var datasets = "";
            if (document.getElementById("datasetMain").checked) {
                datasets += "0";
            }
            if (document.getElementById("datasetManual").checked) {
                if (datasets != "") {
                    datasets += ",";
                }
                datasets += "1";
            }
            if (document.getElementById("datasetTageo").checked) {
                if (datasets != "") {
                    datasets += ",";
                }
                datasets += "2";
            }
            if (document.getElementById("datasetGCKLS2").checked) {
                if (datasets != "") {
                    datasets += ",";
                }
                datasets += "3";
            }
            datasets = "datasets=" + datasets;

            document.getElementById("datafeed").src = "geo-editor-datafeed.jsp?" + coords + "&" + pop + "&" + datasets;
        }

        function addCity(id, countryId, name, lat, lon, dataset) {
            city = cities['city:' + id];
            if (city == null) {
                city = new Object();
                cities['city:' + id] = city;

                city['id'] = id;
                city['countryId'] = countryId;
                city['name'] = name;
                city['lat'] = lat;
                city['lon'] = lon;
                city['dataset'] = dataset;

                var icon;
                if (dataset == 0) {
                    icon = getIcon("http://maps.google.com/mapfiles/kml/pal3/icon23.png");
                } else if (dataset == 1) {
                    icon = getIcon("http://maps.google.com/mapfiles/kml/pal3/icon31.png");
                } else if (dataset == 2) {
                    icon = getIcon("http://google-maps-icons.googlecode.com/files/home.png");
                } else {
                    icon = G_DEFAULT_ICON;
                }
                markerOptions = { icon:icon };

                cityMarker = new GMarker(new GLatLng(lat, lon), markerOptions);
                cityMarker.value = name;

                GEvent.addListener(cityMarker, "click", function() {
                    document.getElementById("editor").src = "geo-editor-city.jsp?id=" + id;
                });

                map.addOverlay(cityMarker);
                city['marker'] = cityMarker;
            }
        }

        function removeCity(element) {
            var city = cities[element];
            cities[element] = null;
            if (city != null) {
                map.removeOverlay(city['marker']);
            }
        }

        function setCityData(cityData) {
            var oldCities = new Array();
            for (property in cities) {
                if (property.indexOf('city:') == 0) {
                    oldCities.push(property);
                }
            }

            cityData = cityData.reverse();
            while (cityData.length > 0) {
                var id = cityData.pop();
                if (id == "EndOfCityData") {
                    break;
                }

                var countryId = cityData.pop();
                var name = cityData.pop();
                var lat = cityData.pop();
                var lon = cityData.pop();
                var dataset = cityData.pop();

                var currCity = cities['city:' + id];
                if (currCity == null) {
                    addCity(id, countryId, name, lat, lon, dataset);
                } else {
                    if (currCity['dataset'] != dataset) {
                        removeCity('city:' + id);
                        addCity(id, countryId, name, lat, lon, dataset);
                    }
                }

                var index = oldCities.indexOf('city:' + id);
                oldCities.splice(index, 1);
            }

            while (oldCities.length > 0) {
                removeCity(oldCities.pop());
            }
        }

        function addAirport(id, cityId, icao, iata, name, lat, lon, dataset) {
            airport = airports['airport:' + id];
            if (airport == null) {
                airport = new Object();
                airports['airport:' + id] = airport;

                airport['id'] = id;
                airport['cityId'] = cityId;
                airport['icao'] = icao;
                airport['iata'] = iata;
                airport['name'] = name;
                airport['lat'] = lat;
                airport['lon'] = lon;
                airport['dataset'] = dataset;

                var icon;
                if (dataset == 0) {
                    icon = getIcon("http://maps.google.com/mapfiles/kml/pal2/icon48.png");
                } else if (dataset == 1) {
                    icon = getIcon("http://maps.google.com/mapfiles/kml/pal2/icon56.png");
                } else if (dataset == 3) {
                    icon = getIcon("http://google-maps-icons.googlecode.com/files/airport.png");
                } else {
                    icon = G_DEFAULT_ICON;
                }
                markerOptions = { icon:icon };

                airportMarker = new GMarker(new GLatLng(lat, lon), markerOptions);
                airportMarker.value = name;

                GEvent.addListener(airportMarker, "click", function() {
                    document.getElementById("editor").src = "geo-editor-airport.jsp?id=" + id;
                });

                map.addOverlay(airportMarker);
                airport['marker'] = airportMarker;
            }
        }

        function removeAirport(element) {
            var airport = airports[element];
            airports[element] = null;
            if (airport != null) {
                map.removeOverlay(airport['marker']);
            }
        }

        function setAirportData(airportData) {
            var oldAirports = new Array();
            for (property in airports) {
                if (property.indexOf('airport:') == 0) {
                    oldAirports.push(property);
                }
            }

            airportData = airportData.reverse();
            while (airportData.length > 0) {
                var id = airportData.pop();
                if (id == "EndOfAirportData") {
                    break;
                }

                var cityId = airportData.pop();
                var icao = airportData.pop();
                var iata = airportData.pop();
                var name = airportData.pop();
                var lat = airportData.pop();
                var lon = airportData.pop();
                var dataset = airportData.pop();

                var currAirport = airports['airport:' + id];
                if (currAirport == null) {
                    addAirport(id, cityId, icao, iata, name, lat, lon, dataset);
                } else {
                    if (currAirport['dataset'] != dataset) {
                        removeAirport('airport:' + id);
                        addAirport(id, cityId, icao, iata, name, lat, lon, dataset);
                    }
                }

                var index = oldAirports.indexOf('airport:' + id);
                oldAirports.splice(index, 1);
            }

            while (oldAirports.length > 0) {
                removeAirport(oldAirports.pop());
            }
        }

        function showOnMap(lat, lon, level) {
            map.setCenter(new GLatLng(lat, lon), level);
        }

        function setData(cityData, airportData) {
            setCityData(cityData);
            setAirportData(airportData);
        }

    </script>
  </head>
  <body onload="initialize()" onunload="GUnload()">
  Population from
  <select id="popFrom" onchange="refreshData()">
      <option value="1000000">1,000,000</option>
      <option value="750000">750,000</option>
      <option value="500000" selected>500,000</option>
      <option value="250000">250,000</option>
      <option value="100000">100,000</option>
      <option value="50000">50,000</option>
      <option value="25000">25,000</option>
      <option value="10000">10,000</option>
      <option value="5000">5,000</option>
      <option value="2500">2,500</option>
      <option value="1000">1,000</option>
      <option value="no">no limit</option>
  </select>
  to
  <select id="popTo" onchange="refreshData()">
      <option value="no" selected>no limit</option>
      <option value="1000000">1,000,000</option>
      <option value="750000">750,000</option>
      <option value="500000">500,000</option>
      <option value="250000">250,000</option>
      <option value="100000">100,000</option>
      <option value="50000">50,000</option>
      <option value="25000">25,000</option>
      <option value="10000">10,000</option>
      <option value="5000">5,000</option>
      <option value="2500">2,500</option>
      <option value="1000">1,000</option>
  </select>

  Data sets
      <input type="checkbox" id="datasetMain" checked onclick="refreshData()"> Main (#0)
      <input type="checkbox" id="datasetManual" checked onclick="refreshData()"> Manual (#1)
      <input type="checkbox" id="datasetTageo" onclick="refreshData()"> Tageo (#2)
      <input type="checkbox" id="datasetGCKLS2" onclick="refreshData()"> GC KLS2 (#3)

      <input type="button" value="Refresh" onclick="refreshData()">
        <table>
            <tr>
                <td><div id="map_canvas" style="width: 800px; height: 800px"></div></td>
                <td valign="top">
                    <table>
                        <tr><td><iframe id="editor" src="geo-editor-menu.jsp" style="width: 400px; height: 700px"></iframe></td></tr>
                        <tr><td><iframe id="datafeed" src="empty.html" style="width: 400px; height: 100px"></iframe></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
  </body>
</html>