<%@ page import="java.sql.Connection" %>
<%@ page import="forge.commons.db.DB" %>
<%@ page import="forge.commons.persistence.Persistence" %>
<%@ page import="net.simforge.legacy.airways.model.ScheduleLine" %>
<%@ page import="java.util.List" %>
<%@ page import="net.simforge.legacy.airways.model.Airport" %>
<%@ page import="net.simforge.legacy.airways.RefDataCache" %>
<%@ page import="org.joda.time.DateTime" %>
<%@ page import="org.joda.time.TimeOfDay" %>
<%@ page import="org.joda.time.DateTimeZone" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="org.joda.time.Duration" %>
<%@ page import="forge.commons.Geo"%>
<%@ page import="forge.commons.TimeMS"%>
<%@ page import="net.simforge.legacy.airways.T"%>
<%@ page import="net.simforge.legacy.airways.DT"%>
<%@ page import="forge.commons.Weekdays"%>
<jsp:include page="top-menu.jsp"/>

<%

    Connection connx = DB.getConnection();

    String numberStr = request.getParameter("number");
    List<ScheduleLine> linesWithNumber = Persistence.loadByQuery(connx, ScheduleLine.class, "select * from %tn% where number = '" + numberStr + "'");
    boolean numberIsOk = false;
    if (numberStr != null) {
        numberStr = numberStr.trim().toUpperCase();
    }
    if (linesWithNumber.isEmpty()) {
        numberIsOk = true;
    }

    String fromIata = request.getParameter("fromIata");
    if (fromIata != null) {
        fromIata = fromIata.trim().toUpperCase();
    }
    List<Airport> fromAirports = Persistence.loadByQuery(connx, Airport.class, "select * from %tn% where iata = '" + fromIata + "'");
    Airport fromAirport = null;
    if (fromAirports.size() == 1) {
        fromAirport = fromAirports.get(0);
    }
    boolean fromIataIsOk = fromAirport != null;

    String toIata = request.getParameter("toIata");
    if (toIata != null) {
        toIata = toIata.trim().toUpperCase();
    }
    List<Airport> toAirports = Persistence.loadByQuery(connx, Airport.class, "select * from %tn% where iata = '" + toIata + "'");
    Airport toAirport = null;
    if (toAirports.size() == 1) {
        toAirport = toAirports.get(0);
    }
    boolean toIataIsOk = toAirport != null;

    String depTime = request.getParameter("depTime");
    TimeOfDay timeOfDay = null;
    try {
        DateTime dt = DateTimeFormat.forPattern("HH:mm").withZone(DateTimeZone.UTC).parseDateTime(depTime);
        timeOfDay = new TimeOfDay(dt.getHourOfDay(), dt.getMinuteOfHour(), dt.getSecondOfMinute(), dt.getMillisOfSecond());
    } catch (Exception e) {
    }

    String durationStr = request.getParameter("duration");
    Duration duration = null;
    try {
        long millis = DateTimeFormat.forPattern("HH:mm").withZone(DateTimeZone.UTC).parseMillis(durationStr);
        duration = new Duration(millis);
    } catch (Exception e) {
    }

    String totalTicketsStr = request.getParameter("totalTickets");
    Integer totalTickets = null;
    try {
        totalTickets = Integer.valueOf(totalTicketsStr);
    } catch (Exception e) {
    }
    totalTicketsStr = totalTickets != null ? totalTickets.toString() : null;

    String horizonStr = request.getParameter("horizon");
    Integer horizon = null;
    try {
        horizon = Integer.valueOf(horizonStr);
    } catch (Exception e) {
    }
    horizonStr = horizon != null ? horizon.toString() : null;


    int dist = (int) (fromIataIsOk && toIataIsOk ? Geo.distanceNM(
                                    fromAirport.getLat(), fromAirport.getLon(),
                                    toAirport.getLat(), toAirport.getLon()) : 0);
    Duration estimatedDuration = fromIataIsOk && toIataIsOk ? new Duration((long) (dist / 389.0 * TimeMS.HOUR)) : null;


    boolean weekdayMon = "on".equals(request.getParameter("weekday_mon"));
    boolean weekdayTue = "on".equals(request.getParameter("weekday_tue"));
    boolean weekdayWen = "on".equals(request.getParameter("weekday_wen"));
    boolean weekdayThu = "on".equals(request.getParameter("weekday_thu"));
    boolean weekdayFri = "on".equals(request.getParameter("weekday_fri"));
    boolean weekdaySat = "on".equals(request.getParameter("weekday_sat"));
    boolean weekdaySun = "on".equals(request.getParameter("weekday_sun"));
    boolean weekdayAll = weekdayMon && weekdayTue && weekdayWen && weekdayThu && weekdayFri && weekdaySat && weekdaySun;


    boolean everythingOk = numberIsOk && fromIataIsOk && toIataIsOk && timeOfDay != null && duration != null && totalTickets != null;

    String action = request.getParameter("Action");
    if ("Create".equals(action)) {
        if (everythingOk) {
            ScheduleLine line = new ScheduleLine();
            line.setNumber(numberStr);
            line.setFromAirportId(fromAirport.getId());
            line.setToAirportId(toAirport.getId());
            line.setWeekdays(new Weekdays(weekdayMon, weekdayTue, weekdayWen, weekdayThu, weekdayFri, weekdaySat, weekdaySun));
            line.setDepTime(timeOfDay);
            line.setDuration(duration);
            line.setActive(false);
            line.setTotalTickets(totalTickets);
            line.setHorizon(horizon);
            line = Persistence.create(connx, line);
            connx.commit();
            connx.close();

            response.sendRedirect("schedule-line.jsp?id=" + line.getId());
            return;
        }
    }

    connx.close();

%>

<script type="text/javascript">
    function onvaluechanged() {
        document.getElementById('Create').disabled = 'disabled';
        document.getElementById('Validate').disabled = '';
    }

    function validate() {
        document.getElementById('Action').value = 'Validate';
        document.ScheduleLineForm.submit();
    }

    function create() {
        document.getElementById('Action').value = 'Create';
        document.ScheduleLineForm.submit();
    }

    function onweekday() {
        all =
            (document.getElementById('weekday_mon').checked) &&
            (document.getElementById('weekday_tue').checked) &&
            (document.getElementById('weekday_wen').checked) &&
            (document.getElementById('weekday_thu').checked) &&
            (document.getElementById('weekday_fri').checked) &&
            (document.getElementById('weekday_sat').checked) &&
            (document.getElementById('weekday_sun').checked);
        document.getElementById('weekday_all').checked = all;
        onvaluechanged();
    }

    function onweekdayall() {
        all = document.getElementById('weekday_all');
        document.getElementById('weekday_mon').checked = all.checked;
        document.getElementById('weekday_tue').checked = all.checked;
        document.getElementById('weekday_wen').checked = all.checked;
        document.getElementById('weekday_thu').checked = all.checked;
        document.getElementById('weekday_fri').checked = all.checked;
        document.getElementById('weekday_sat').checked = all.checked;
        document.getElementById('weekday_sun').checked = all.checked;
        onvaluechanged();
    }
</script>
<form name="ScheduleLineForm" action="schedule-line-new.jsp" method="post">
    <table>
        <tr>
            <th>Flight</th>
            <td><input type="text" name="number" value="<%=numberStr == null ? "" : numberStr%>" onchange="onvaluechanged()"></td>
            <td><%
                if (numberStr != null && numberStr.length() != 0) {
                    if (numberIsOk) {
                        %><font color="green">Ok</font><%
                    } else {
                        %><b><font color="red">Flight <%=linesWithNumber.get(0).getNumber()%> exists</font></b><%
                    }
                } else {
                    %><b><font color="orange">Specify flight number</font></b><%
                }
            %></td>
        </tr>
        <tr>
            <th>From</th>
            <td><input type="text" name="fromIata" value="<%=fromIata == null ? "" : fromIata%>" onchange="onvaluechanged()"></td>
            <td><%
                if (fromIata != null && fromIata.length() != 0) {
                    if (fromIataIsOk) {
                        %><font color="green"><%=fromAirport.getIata()%> <%=fromAirport.getName()%>, <%=RefDataCache.getCity(fromAirport.getCityId()).getName()%></font><%
                    } else {
                        %><b><font color="red"><%=fromIata%> is unknown IATA code</font></b><%
                    }
                } else {
                    %><b><font color="orange">Specify IATA code</font></b><%
                }
            %></td>
        </tr>
        <tr>
            <th>To</th>
            <td><input type="text" name="toIata" value="<%=toIata == null ? "" : toIata%>" onchange="onvaluechanged()"></td>
            <td><%
                if (toIata != null && toIata.length() != 0) {
                    if (toIataIsOk) {
                        %><font color="green"><%=toAirport.getIata()%> <%=toAirport.getName()%>, <%=RefDataCache.getCity(toAirport.getCityId()).getName()%></font><%
                    } else {
                        %><b><font color="red"><%=toIata%> is unknown IATA code</font></b><%
                    }
                } else {
                    %><b><font color="orange">Specify IATA code</font></b><%
                }
            %></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><%=(fromIataIsOk && toIataIsOk ? "Distance " + dist + " nm" : "&nbsp;")%></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><%=(fromIataIsOk && toIataIsOk ? "Est duration " + DateTimeFormat.forPattern("HH:mm").withZone(DateTimeZone.UTC).print(estimatedDuration.getMillis()) : "&nbsp;")%></td>
        </tr>
        <tr>
            <th>Weekdays</th>
            <td colspan="2">
                <table>
                    <tr>
                        <th>Mon</th>
                        <th>Tue</th>
                        <th>Wen</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                        <th>Sun</th>
                        <th>&nbsp;</th>
                        <th>All</th>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="weekday_mon" name="weekday_mon" onclick="onweekday()" <%=weekdayMon ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_tue" name="weekday_tue" onclick="onweekday()" <%=weekdayTue ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_wen" name="weekday_wen" onclick="onweekday()" <%=weekdayWen ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_thu" name="weekday_thu" onclick="onweekday()" <%=weekdayThu ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_fri" name="weekday_fri" onclick="onweekday()" <%=weekdayFri ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_sat" name="weekday_sat" onclick="onweekday()" <%=weekdaySat ? "checked" : ""%>></td>
                        <td><input type="checkbox" id="weekday_sun" name="weekday_sun" onclick="onweekday()" <%=weekdaySun ? "checked" : ""%>></td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="weekday_all" name="weekday_all" onclick="onweekdayall()" <%=weekdayAll ? "checked" : ""%>></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th>Dep time</th>
            <td><input type="text" name="depTime" value="<%=depTime == null ? "" : depTime%>" onchange="onvaluechanged()"></td>
            <td><%
                if (depTime != null && depTime.length() != 0) {
                    if (timeOfDay != null) {
                        %><font color="green"><%=T.toHHmm(timeOfDay)%></font><%
                    } else {
                        %><b><font color="red">Error! Please use format HH:MM</font></b><%
                    }
                } else {
                    %><b><font color="orange">Specify departure time using format HH:MM</font></b><%
                }


            %></td>
        </tr>
        <tr>
            <th>Duration</th>
            <td><input type="text" name="duration" value="<%=durationStr == null ? "" : durationStr%>" onchange="onvaluechanged()"></td>
            <td><%
                if (durationStr != null && durationStr.length() != 0) {
                    if (duration != null) {
                        %><font color="green"><%=T.toHHmm(duration)%></font><%
                    } else {
                        %><b><font color="red">Error! Please use format HH:MM</font></b><%
                    }
                } else {
                    %><b><font color="orange">Specify flight duration using format HH:MM</font></b><%
                }
            %></td>
        </tr>
        <tr>
            <th>Arr time</th>
            <td><%=fromIataIsOk && toIataIsOk && timeOfDay != null && duration != null ? DT.tF.print(timeOfDay.toDateTimeToday().plus(duration)) : "&nbsp;"%></td>
        </tr>
        <tr>
            <th>Total tickets</th>
            <td><input type="text" name="totalTickets" value="<%=totalTicketsStr == null ? "150" : totalTicketsStr%>" onchange="onvaluechanged()"></td>
        </tr>
        <tr>
            <th>Horizon</th>
            <td><input type="text" name="horizon" value="<%=horizonStr == null ? "15" : horizonStr%>" onchange="onvaluechanged()"></td>
        </tr>
    </table>
    <br>
    <br>
    <input type="hidden" id="Action" name="Action">
    <input type="button" id="Validate" name="Validate" value="Validate" <%=everythingOk ? "disabled" : "" %> onclick="validate()">
    <input type="button" id="Create" name="Create" value="Create" <%=!everythingOk ? "disabled" : "" %> onclick="create()">
</form>