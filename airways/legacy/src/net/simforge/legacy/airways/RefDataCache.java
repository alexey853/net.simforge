package net.simforge.legacy.airways;

import net.simforge.legacy.airways.model.City;
import net.simforge.legacy.airways.model.Flight;
import net.simforge.legacy.airways.model.Airport;
import net.simforge.legacy.airways.model.Country;

import java.util.Map;
import java.util.HashMap;
import java.sql.Connection;
import java.sql.SQLException;

import forge.commons.db.DB;
import forge.commons.TimeMS;
import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Persistence;
import net.simforge.commons.persistence.PersistenceListener;

public class RefDataCache {
    private static long TimeToKeepIntoCache = 60;

    private static Map<Class, Map<Integer, Object>> cache = new HashMap<Class, Map<Integer, Object>>();
    private static Map<Object, Long> object2ts = new HashMap<Object, Long>();

    static {
        Persistence.addListener(new Listener());
    }

    public static <T> T get(Class clazz, int id) {
        Map<Integer, Object> map = cache.get(clazz);
        if (map == null) {
            map = new HashMap<Integer, Object>();
            cache.put(clazz, map);
        }
        Object obj = map.get(id);
        if (obj != null) {
            Long loaded = object2ts.get(obj);
            if (loaded == null || System.currentTimeMillis() - loaded > TimeToKeepIntoCache * TimeMS.MINUTE) {
                remove(map, id, obj);
            } else {
                //noinspection unchecked
                return (T) obj;
            }
        }
        try {
            Connection connx = DB.getConnection();
            //noinspection unchecked
            obj = Persistence.load(connx, clazz, id);
            connx.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        put(map, id, obj);
        //noinspection unchecked
        return (T) obj;
    }

    private static <T>void remove(Map<Integer, Object> map, int id, Object obj) {
        map.remove(id);
        object2ts.remove(obj);
    }

    private static <T>void put(Map<Integer, Object> map, int id, Object obj) {
        map.put(id, obj);
        object2ts.put(obj, System.currentTimeMillis());
    }

    private static class Listener implements PersistenceListener {
        public void instanceUpdated(BaseEntity instance) {
            Map<Integer, Object> map = cache.get(instance.getClass());
            if (map == null) {
                return;
            }
            int id = instance.getId();
            Object obj = map.get(id);
            if (obj == null) {
                return;
            }
            remove(map, id, obj);
            put(map, id, instance);
        }
    }

    public static Country getCountry(int countryId) {
        return get(Country.class, countryId);
    }

    public static City getCity(int cityId) {
        return get(City.class, cityId);
    }

    public static Airport getAirport(int airportId) {
        return get(Airport.class, airportId);
    }

    public static Flight getFlight(int flightId) {
        return get(Flight.class, flightId);
    }

}
