package net.simforge.legacy.airways;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface WorldTask {
    int period() default 60;
}
