package net.simforge.legacy.airways.newflows;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name="nf_city2city_flow")
public class City2CityFlow extends BaseEntity {
    @Column
    private int fromFlowId;

    @Column
    private int toFlowId;

    @Column
    private boolean active;

    @Column
    private double units;

    @Column
    private double percentage;

    @Column
    private double availability;

    @Column
    private int nextGroupSize;

    @Column
    private double flowRemainder;

    @Column
    private DateTime tickDt;

    @Column
    private DateTime prevTickDt;

    public int getFromFlowId() {
        return fromFlowId;
    }

    public void setFromFlowId(int fromFlowId) {
        this.fromFlowId = fromFlowId;
    }

    public int getToFlowId() {
        return toFlowId;
    }

    public void setToFlowId(int toFlowId) {
        this.toFlowId = toFlowId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public double getAvailability() {
        return availability;
    }

    public void setAvailability(double availability) {
        this.availability = availability;
    }

    public int getNextGroupSize() {
        return nextGroupSize;
    }

    public void setNextGroupSize(int nextGroupSize) {
        this.nextGroupSize = nextGroupSize;
    }

    public double getFlowRemainder() {
        return flowRemainder;
    }

    public void setFlowRemainder(double flowRemainder) {
        this.flowRemainder = flowRemainder;
    }

    public DateTime getTickDt() {
        return tickDt;
    }

    public void setTickDt(DateTime tickDt) {
        this.tickDt = tickDt;
    }

    public DateTime getPrevTickDt() {
        return prevTickDt;
    }

    public void setPrevTickDt(DateTime prevTickDt) {
        this.prevTickDt = prevTickDt;
    }
}
