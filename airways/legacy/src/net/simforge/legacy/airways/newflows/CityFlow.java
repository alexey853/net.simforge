package net.simforge.legacy.airways.newflows;

import net.simforge.legacy.airways.Caption;
import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name="nf_city_flow")
public class CityFlow extends BaseEntity {
    @Column
    private int cityId;

    @Column
    private int status;

    @Column
    private DateTime lastRecalcDt;

    @Column
    private double attraction;

    @Column
    private double unitsThreshold;

    @Column
    private double defaultAvailability;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DateTime getLastRecalcDt() {
        return lastRecalcDt;
    }

    public void setLastRecalcDt(DateTime lastRecalcDt) {
        this.lastRecalcDt = lastRecalcDt;
    }

    public double getAttraction() {
        return attraction;
    }

    public void setAttraction(double attraction) {
        this.attraction = attraction;
    }

    public double getUnitsThreshold() {
        return unitsThreshold;
    }

    public void setUnitsThreshold(double unitsThreshold) {
        this.unitsThreshold = unitsThreshold;
    }

    public double getDefaultAvailability() {
        return defaultAvailability;
    }

    public void setDefaultAvailability(double defaultAvailability) {
        this.defaultAvailability = defaultAvailability;
    }

    public static class Status {
        public static final int Inactive = 1;
        public static final int Active = 2;
        @Caption("Inactive, will Activate after Recalc")
        public static final int RecalcThenActivate = 3;
        @Caption("Active, needs Recalc")
        public static final int ActiveButNeedsRecalc = 4;
    }
}
