package net.simforge.legacy.airways.newflows;

import net.simforge.legacy.airways.*;
import net.simforge.legacy.airways.model.City;
import net.simforge.legacy.airways.model.PassengerGroup;
import forge.commons.TimeMS;
import forge.commons.db.DB;
import net.simforge.commons.persistence.Persistence;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.MutableDateTime;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WorldTask(period = 10)
public class NewFlowLogics implements Runnable {

    private static Logger logger;

    public static Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger("NewFlows");
        }
        return logger;
    }

    public static void recalcCityFlow(CityFlow cityFlow) {
        Connection connx = null;
        try {
            connx = DB.getConnection();

            getLogger().info("Recalc: City " + RefDataCache.getCity(cityFlow.getCityId()).getName());

            cityFlow = Persistence.load(connx, CityFlow.class, cityFlow.getId());
            List<CityFlow> cityFlows = Persistence.loadAll(connx, CityFlow.class);

            double sumOfUnits = 0;
            List<CityFlow> activeCityFlows = new ArrayList<CityFlow>();
            for (CityFlow eachCityFlow : cityFlows) {
                if (cityFlow.getId() == eachCityFlow.getId()) {
                    continue;
                }

                int eachStatus = eachCityFlow.getStatus();
                if (!((eachStatus == CityFlow.Status.Active)
                        || (eachStatus == CityFlow.Status.ActiveButNeedsRecalc)
                        || (eachStatus == CityFlow.Status.RecalcThenActivate))) {
                    continue;
                }

                if (hasAirportConnections(connx, cityFlow, eachCityFlow)) {
                    continue;
                }

                double units = Calculations.getUnits(cityFlow, eachCityFlow);
                if (units >= cityFlow.getUnitsThreshold()) {
                    sumOfUnits += units;
                    activeCityFlows.add(eachCityFlow);
                }
            }

            List<City2CityFlow> newC2CFlows = new ArrayList<City2CityFlow>();
            for (CityFlow activeCityFlow : activeCityFlows) {
                double units = Calculations.getUnits(cityFlow, activeCityFlow);
                City2CityFlow newC2CFlow = new City2CityFlow();
                newC2CFlow.setFromFlowId(cityFlow.getId());
                newC2CFlow.setToFlowId(activeCityFlow.getId());
                newC2CFlow.setUnits(units);
                newC2CFlow.setPercentage(units / sumOfUnits);
                newC2CFlow.setAvailability(cityFlow.getDefaultAvailability());
                newC2CFlows.add(newC2CFlow);
            }

            List<City2CityFlow> dbC2CFlows = Persistence.loadByQuery(connx, City2CityFlow.class, "select * from %tn% where from_flow_id = " + cityFlow.getId());
            for (City2CityFlow eachDbC2CFlow : dbC2CFlows) {
                City2CityFlow newC2CFlow = null;
                for (City2CityFlow eachNewC2CFlow : newC2CFlows) {
                    if (eachNewC2CFlow.getToFlowId() == eachDbC2CFlow.getToFlowId()) {
                        newC2CFlow = eachNewC2CFlow;
                        newC2CFlows.remove(eachNewC2CFlow);
                        break;
                    }
                }

                if (newC2CFlow != null) {
                    eachDbC2CFlow.setUnits(newC2CFlow.getUnits());
                    eachDbC2CFlow.setPercentage(newC2CFlow.getPercentage());
                    eachDbC2CFlow.setActive(true);
                    eachDbC2CFlow.setTickDt(DT.now());
                } else {
                    eachDbC2CFlow.setActive(false);
                }
                Persistence.update(connx, eachDbC2CFlow);
            }

            for (City2CityFlow eachNewC2CFlow : newC2CFlows) {
                eachNewC2CFlow.setActive(true);
                eachNewC2CFlow.setTickDt(DT.now());
                Persistence.create(connx, eachNewC2CFlow);
            }

            cityFlow.setStatus(CityFlow.Status.Active);
            cityFlow.setLastRecalcDt(DT.now());
            Persistence.update(connx, cityFlow);
            connx.commit();
            connx.close();
        } catch (SQLException e) {
            getLogger().error("SQL exception happened", e);
            if (connx != null) {
                //noinspection EmptyCatchBlock
                try {
                    connx.rollback();
                    connx.close();
                } catch(SQLException e1) {
                }
            }
        }
    }

    private static boolean hasAirportConnections(Connection connx, CityFlow cityFlow1, CityFlow cityFlow2) throws SQLException {
        String sql = "select distinct(airport_id), count(city_id) " +
                "from aw_airport2city " +
                "where city_id = %cityId1% or city_id = %cityId2% " +
                "group by airport_id " +
                "having count(city_id) > 1";
        sql = sql.replaceAll("%cityId1%", String.valueOf(cityFlow1.getCityId()));
        sql = sql.replaceAll("%cityId2%", String.valueOf(cityFlow2.getCityId()));
        Statement st = connx.createStatement();
        ResultSet rs = st.executeQuery(sql);
        boolean result = rs.next(); // if we have at least one record in result then we have intercity airport connection
        rs.close();
        st.close();
        return result;
    }

    public static void tick(City2CityFlow flow) {
        Connection connx = null;
        try {
            connx = DB.getConnection();

            flow = Persistence.load(connx, City2CityFlow.class, flow.getId());

            DateTime tick = flow.getTickDt();
            DateTime prev = flow.getPrevTickDt();

            if (tick == null) {
                tick = DT.now();
            }
            if (prev == null) {
                prev = tick;
            }

            CityFlow cityFlow = RefDataCache.get(CityFlow.class, flow.getFromFlowId());
            City city = RefDataCache.getCity(cityFlow.getCityId());
            int dailyFlow = Calculations.getDailyFlow(city);
            double flowToDistribute = dailyFlow * (new Duration(prev, tick).getMillis() / (double) (TimeMS.DAY));

            double flowIncrement = flowToDistribute * flow.getPercentage() * Calculations.boundAvailability(flow.getAvailability());
            flow.setFlowRemainder(flow.getFlowRemainder() + flowIncrement);

            if (flow.getNextGroupSize() == 0) {
                flow.setNextGroupSize(Calculations.randomGroupSize());
            }

            if(flow.getFlowRemainder() >= flow.getNextGroupSize()) {
                createPassengerGroup(connx, flow);
                flow.setFlowRemainder(flow.getFlowRemainder() - flow.getNextGroupSize());
                flow.setNextGroupSize(Calculations.randomGroupSize());
            }

            flow.setPrevTickDt(flow.getTickDt());

            double requiredFlowToDistribute = (flow.getNextGroupSize() - flow.getFlowRemainder()) / flow.getPercentage() / Calculations.boundAvailability(flow.getAvailability());
            long requiredMillis = (long) (requiredFlowToDistribute * TimeMS.DAY / dailyFlow);
            requiredMillis = Math.min(requiredMillis, TimeMS.DAY);
            int seconds = (int) (requiredMillis / TimeMS.SECOND + 60);
            flow.setTickDt(DT.addSeconds(tick, seconds));

            if (flow.getTickDt().isBeforeNow()) {
                flow.setTickDt(DT.addMinutes(3));
                getLogger().warn("Flows: " + l(flow) + ": tickDt is calculated in past, remainder " + flow.getFlowRemainder());
            }

            Persistence.update(connx, flow);
            connx.commit();
            connx.close();
        } catch (SQLException e) {
            getLogger().error("SQL exception happened", e);
            if (connx != null) {
                //noinspection EmptyCatchBlock
                try {
                    connx.rollback();
                    connx.close();
                } catch(SQLException e1) {
                }
            }
        }
    }

    private static void createPassengerGroup(Connection connx, City2CityFlow flow) throws SQLException {
        int groupSize = flow.getNextGroupSize();
        boolean roundtrip = Calculations.randomRoundtrip();

        DateTime now = DT.now();

        CityFlow fromFlow = RefDataCache.get(CityFlow.class, flow.getFromFlowId());
        CityFlow toFlow = RefDataCache.get(CityFlow.class, flow.getToFlowId());

        PassengerGroup group = new PassengerGroup();
        group.setSize(groupSize);
        group.setPositionCityId(fromFlow.getCityId());
        group.setPositionRoundtrip(false);
        group.setFromCityId(fromFlow.getCityId());
        group.setToCityId(toFlow.getCityId());
        group.setRoundtrip(roundtrip);
        group.setC2cFlowId(flow.getId());
        group.setStatus(PassengerGroup.Status.LookingForTickets);
        group.setHeartbeatDt(now);
        group.setExpireDt(DT.addDays(7));
        group = Persistence.create(connx, group);

        getLogger().info("Flows: " + l(flow) + ": new group, size " + groupSize + ", " + (roundtrip ? "roundtrip" : "oneway"));
        PassengerGroupLifecycle.getLogger().info(PassengerGroupLifecycle.l(group) + " created");
        PassengerGroupLifecycle.makeLog(connx, group, "Created", group.getPositionCityId(), 0, 0);
    }

    private static String l(City2CityFlow flow) {
        CityFlow fromFlow = RefDataCache.get(CityFlow.class, flow.getFromFlowId());
        CityFlow toFlow = RefDataCache.get(CityFlow.class, flow.getToFlowId());
        return "Flow " + RefDataCache.getCity(fromFlow.getCityId()).getName() + "-" + RefDataCache.getCity(toFlow.getCityId()).getName();
    }

    public static City2CityFlowStats getCurrentStats(Connection connx, int flowId) throws SQLException {
        DateTime now = new DateTime();

        String sql = "select * from %tn% where flow_id = %id% and since <= '%dt%' and till > '%dt%'";
        sql = sql.replaceAll("%dt%", DT.DTF.print(now));
        sql = sql.replaceAll("%id%", String.valueOf(flowId));

        List<City2CityFlowStats> statsList = Persistence.loadByQuery(connx, City2CityFlowStats.class, sql);
        if (statsList.isEmpty()) {
            City2CityFlowStats stats = new City2CityFlowStats();
            stats.setFlowId(flowId);
            stats.setProcessed(false);
            stats.setSince(getSince(now));
            stats.setTill(getNextSince(getSince(now)));
            return Persistence.create(connx, stats);
        } else if (statsList.size() == 1) {
            return statsList.get(0);
        } else {
            throw new IllegalStateException();
        }
    }

    private static DateTime getNextSince(DateTime since) {
        MutableDateTime mdt = new MutableDateTime(since);
        mdt.addHours(1);
        return mdt.toDateTime();
    }

    private static DateTime getSince(DateTime dt) {
        MutableDateTime mdt = new MutableDateTime(dt);
        mdt.setMillisOfSecond(0);
        mdt.setSecondOfMinute(0);
        mdt.setMinuteOfHour(0);
        return mdt.toDateTime();
    }

    public void run() {
        runRecalc();
        runStats();
        runFlows();
    }

    private void runRecalc() {
        try {
            String sql =
                    "select * from %tn% " +
                    "where status in (" + CityFlow.Status.RecalcThenActivate + ", " + CityFlow.Status.ActiveButNeedsRecalc + ") " +
                    "or (status = " + CityFlow.Status.Active + " and last_recalc_dt < '" + DT.DTF.print(DT.addDays(-1)) + "') " +
                    "limit 10";

            Connection connx = DB.getConnection();
            List<CityFlow> cityFlows = Persistence.loadByQuery(connx, CityFlow.class, sql);
            connx.close();

            if (cityFlows.isEmpty()) {
                return;
            }

            getLogger().info("Recalc: " + cityFlows.size() + " to process");

            for (CityFlow cityFlow : cityFlows) {
                recalcCityFlow(cityFlow);
            }
        } catch (SQLException e) {
            getLogger().error("SQL exception happened", e);
        }
    }

    private void runStats() {
        try {
            DateTime threshold = DT.addMinutes(-5);

            String sql = "select * from %tn% where processed = false and till < '%dt%' order by flow_id, till limit 100";
            sql = sql.replaceAll("%dt%", DT.DTF.print(threshold));

            Connection connx = DB.getConnection();
            List<City2CityFlowStats> statsList = Persistence.loadByQuery(connx, City2CityFlowStats.class, sql);
            connx.close();

            if (statsList.isEmpty()) {
                return;
            }

            getLogger().info("Stats: " + statsList.size() + " to process");

            for (City2CityFlowStats stats : statsList) {
                calcStats(stats);
            }
        } catch (SQLException e) {
            getLogger().error("SQL exception happened", e);
        }
    }

    private void calcStats(City2CityFlowStats stats) {
        Connection connx = null;
        try {
            connx = DB.getConnection();

            City2CityFlow flow = Persistence.load(connx, City2CityFlow.class, stats.getFlowId());

            double availabilityBefore = flow.getAvailability();
            double availabilityChange = Calculations.calcAvailabilityChange(flow, stats);
            double availabilityAfter = Calculations.boundAvailability(availabilityBefore + availabilityChange);

            stats.setAvailabilityBefore(availabilityBefore);
            stats.setAvailabilityChange(availabilityChange);
            stats.setAvailabilityAfter(availabilityAfter);
            stats.setProcessed(true);
            Persistence.update(connx, stats);

            flow.setAvailability(availabilityAfter);
            if (flow.getTickDt().isAfterNow()) {
                flow.setTickDt(DT.now());
            }
            Persistence.update(connx, flow);
            connx.commit();
            connx.close();

            getLogger().info("Stats: " + l(flow) + ": availability " + T.df0_000000.format(availabilityAfter) + ", changed by " + T.df0_000000.format(availabilityChange));
        } catch(SQLException e) {
            getLogger().error("SQL exception happened", e);
            if (connx != null) {
                //noinspection EmptyCatchBlock
                try {
                    connx.rollback();
                    connx.close();
                } catch(SQLException e1) {
                }
            }
        }
    }

    private void runFlows() {
        try {
            String sql = "select * from %tn% where tick_dt <= '%dt%' and active = true order by tick_dt limit 1000";
            sql = sql.replaceAll("%dt%", DT.DTF.print(new DateTime()));

            Connection connx = DB.getConnection();
            List<City2CityFlow> flows = Persistence.loadByQuery(
                    connx,
                    City2CityFlow.class,
                    sql);
            connx.close();

            if (flows.isEmpty()) {
                return;
            }

            getLogger().info("Flows: " + flows.size() + " to process");

            for (City2CityFlow flow : flows) {
                tick(flow);
            }
        } catch (SQLException e) {
            getLogger().error("SQL exception happened", e);
        }
    }

    public static void main(String[] args) {
        new NewFlowLogics().run();
    }

    public static CityFlow loadCityFlow(Connection connx, City city) throws SQLException {
        List<CityFlow> cityFlows = Persistence.loadByQuery(connx, CityFlow.class, "select * from %tn% where city_id = " + city.getId());
        return cityFlows.isEmpty() ? null : cityFlows.get(0);
    }
}
