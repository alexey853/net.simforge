package net.simforge.legacy.airways.newflows;

import forge.commons.Geo;
import net.simforge.legacy.airways.RefDataCache;
import net.simforge.legacy.airways.model.City;

public class Calculations {
    private static double MinAvailability = 0.000001;
    private static double MaxAvailability = 1.0;

    public static double getUnits(CityFlow fromCityFlow, CityFlow toCityFlow) {
        double attractionUnits = toCityFlow.getAttraction();

        City fromCity = RefDataCache.getCity(fromCityFlow.getCityId());
        City toCity = RefDataCache.getCity(toCityFlow.getCityId());

        double dist = Geo.distanceNM(fromCity.getLat(), fromCity.getLon(), toCity.getLat(), toCity.getLon());
        double distUnits = dist / 500;
        if (distUnits < 1) {
            distUnits = 1;
        }

        return attractionUnits / distUnits;
    }

    public static int getDailyFlow(City city) {
        return (int) (city.getPopulation() * 0.001);
    }

    public static int randomGroupSize() {
        return (int) (Math.random() * 10) + 1;
    }

    public static boolean randomRoundtrip() {
        return Math.random() > 0.01;
    }

    public static double boundAvailability(double availability) {
        if (availability < MinAvailability)
            return MinAvailability;
        if (availability > MaxAvailability)
            return MaxAvailability;
        return availability;
    }

    public static double calcAvailabilityChange(City2CityFlow flow, City2CityFlowStats stats) {
        CityFlow cityFlow = RefDataCache.get(CityFlow.class, flow.getFromFlowId());
        double dailyFlow = getDailyFlow(RefDataCache.getCity(cityFlow.getCityId()));

        int couldNotFindTickets = stats.getCouldNotFindTickets();
        int ticketsBought = stats.getTicketsBought();
        int itinerariesDone = stats.getItinerariesDone();

        //noinspection UnnecessaryLocalVariable
        double totalChange = (-couldNotFindTickets * 1.0 + ticketsBought * 3.0 + itinerariesDone * 6.0) / (1.0 + 3.0 + 6.0);
        totalChange /= dailyFlow;

        return totalChange;
    }

}
