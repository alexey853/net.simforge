package net.simforge.legacy.airways.newflows;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name="nf_city2city_flow_stats")
public class City2CityFlowStats extends BaseEntity {
    @Column
    private int flowId;

    @Column
    private boolean processed;

    @Column
    private DateTime since;

    @Column
    private DateTime till;

    @Column
    private double availabilityBefore;

    @Column
    private double availabilityAfter;

    @Column
    private double availabilityChange;

    @Column
    private int couldNotFindTickets;

    @Column
    private int ticketsBought;

    @Column
    private int itinerariesDone;

    public int getFlowId() {
        return flowId;
    }

    public void setFlowId(int flowId) {
        this.flowId = flowId;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public DateTime getSince() {
        return since;
    }

    public void setSince(DateTime since) {
        this.since = since;
    }

    public DateTime getTill() {
        return till;
    }

    public void setTill(DateTime till) {
        this.till = till;
    }

    public double getAvailabilityBefore() {
        return availabilityBefore;
    }

    public void setAvailabilityBefore(double availabilityBefore) {
        this.availabilityBefore = availabilityBefore;
    }

    public double getAvailabilityAfter() {
        return availabilityAfter;
    }

    public void setAvailabilityAfter(double availabilityAfter) {
        this.availabilityAfter = availabilityAfter;
    }

    public double getAvailabilityChange() {
        return availabilityChange;
    }

    public void setAvailabilityChange(double availabilityChange) {
        this.availabilityChange = availabilityChange;
    }

    public int getCouldNotFindTickets() {
        return couldNotFindTickets;
    }

    public void setCouldNotFindTickets(int couldNotFindTickets) {
        this.couldNotFindTickets = couldNotFindTickets;
    }

    public int getTicketsBought() {
        return ticketsBought;
    }

    public void setTicketsBought(int ticketsBought) {
        this.ticketsBought = ticketsBought;
    }

    public int getItinerariesDone() {
        return itinerariesDone;
    }

    public void setItinerariesDone(int itinerariesDone) {
        this.itinerariesDone = itinerariesDone;
    }
}
