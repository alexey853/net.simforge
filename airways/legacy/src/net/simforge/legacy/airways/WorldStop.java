package net.simforge.legacy.airways;

import java.io.IOException;

public class WorldStop {
    public static void main(String[] args) throws IOException {
        String marker = args[0];
        WorldMarker.setMarkerToStop(marker);
    }
}
