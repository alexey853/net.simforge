package net.simforge.legacy.airways;

import net.simforge.legacy.lifecycle.LifecycleRunnable;
import net.simforge.legacy.lifecycle.LifecycleDefinition;

import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;

import forge.commons.TimeMS;
import forge.commons.Misc;
import org.apache.log4j.Logger;

public class World {
    private static Logger logger = Logger.getLogger("World");

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, ExecutionException, InterruptedException, IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        String marker = null;
        List<Task> tasks = new ArrayList<Task>();
        for (String arg : args) {
            if (arg.startsWith("marker:")) {
                marker = arg.substring("marker:".length());
            } else {
                tasks.add(new Task(arg));
            }
        }

        if (marker != null) {
            if (WorldMarker.isMarkerAlive(marker)) {
                logger.info("Marker '" + marker + "' seems alive, exiting");
                return;
            } else {
                logger.info("Marker '" + marker + "' is free, starting World...");
                WorldMarker.setMarker(marker);
                WorldMarker.clearMarkerToStop(marker);
            }
        } else {
            logger.info("Marker is not specified");
        }

        int secs = 0;
        while (true) {
            for (Task task : tasks) {
                if (task.future == null) {
                    if (System.currentTimeMillis() - task.futureDoneTS >= task.periodMS) {
                        task.future = executorService.submit(task.makeNew());
                    }
                } else {
                    if (task.future.isDone()) {
                        task.future.get();
                        task.future = null;
                        task.futureDoneTS = System.currentTimeMillis();
                    }
                }
            }

            Misc.sleep(1000);

            if (marker != null) {
                secs++;
                if (secs == 10) {
                    WorldMarker.setMarker(marker);
                    secs = 0;

                    if (WorldMarker.hasMarkerToStop(marker)) {
                        logger.info("Marker '" + marker + "' instructed to stop, stopping...");
                        executorService.shutdown();
                        break;
                    }
                }
            }
        }

        if (marker != null) {
            WorldMarker.clearMarker(marker);
            WorldMarker.clearMarkerToStop(marker);
            logger.info("Marker '" + marker + "' cleared");
        }
    }

    private static class Task {
        Class clazz;
        Future future;
        long futureDoneTS;
        long periodMS = 60 * TimeMS.SECOND;

        public Task(String classname) throws ClassNotFoundException {
            clazz = Class.forName(classname);
            //noinspection unchecked
            WorldTask worldTaskAnnotation = (WorldTask) clazz.getAnnotation(WorldTask.class);
            if (worldTaskAnnotation != null) {
                periodMS = Math.max(worldTaskAnnotation.period(), 1) * TimeMS.SECOND;
            }
        }

        Runnable makeNew() throws IllegalAccessException, InstantiationException {
            //noinspection unchecked
            if (clazz.getAnnotation(LifecycleDefinition.class) != null) {
                return new LifecycleRunnable(clazz);
            } else if (Runnable.class.isAssignableFrom(clazz)) {
                return (Runnable) clazz.newInstance();
            } else {
                throw new IllegalArgumentException();
            }
        }
    }
}
