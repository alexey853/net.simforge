package net.simforge.legacy.airways.model;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name = "aw_flight_event")
public class FlightEvent extends BaseEntity {
    @Column
    private int flightId;

    @Column
    private DateTime dt;

    @Column
    private String msg;

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public DateTime getDt() {
        return dt;
    }

    public void setDt(DateTime dt) {
        this.dt = dt;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
