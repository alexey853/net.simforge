package net.simforge.legacy.airways.model;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.SetNull;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name="aw_pg")
public class PassengerGroup extends BaseEntity {
    @Column
    private int c2cFlowId;

    @Column
    private int fromCityId;

    @Column
    private int toCityId;

    @Column
    private boolean roundtrip;

    @Column
    private int size;

    @Column
    private int status;

    @Column
    private DateTime heartbeatDt;

    @Column
    private DateTime expireDt;

    @Column
    @SetNull
    private int itineraryId;

    @Column
    @SetNull
    private int positionCityId;

    @Column
    @SetNull
    private int positionAirportId;

    @Column
    @SetNull
    private int positionFlightId;

    @Column
    private boolean positionRoundtrip;

    public int getC2cFlowId() {
        return c2cFlowId;
    }

    public void setC2cFlowId(int c2cFlowId) {
        this.c2cFlowId = c2cFlowId;
    }

    public int getFromCityId() {
        return fromCityId;
    }

    public void setFromCityId(int fromCityId) {
        this.fromCityId = fromCityId;
    }

    public int getToCityId() {
        return toCityId;
    }

    public void setToCityId(int toCityId) {
        this.toCityId = toCityId;
    }

    public boolean isRoundtrip() {
        return roundtrip;
    }

    public void setRoundtrip(boolean roundtrip) {
        this.roundtrip = roundtrip;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    public void setHeartbeatDt(DateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    public DateTime getExpireDt() {
        return expireDt;
    }

    public void setExpireDt(DateTime expireDt) {
        this.expireDt = expireDt;
    }

    public int getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(int itineraryId) {
        this.itineraryId = itineraryId;
    }

    public int getPositionCityId() {
        return positionCityId;
    }

    public void setPositionCityId(int positionCityId) {
        this.positionCityId = positionCityId;
    }

    public int getPositionAirportId() {
        return positionAirportId;
    }

    public void setPositionAirportId(int positionAirportId) {
        this.positionAirportId = positionAirportId;
    }

    public int getPositionFlightId() {
        return positionFlightId;
    }

    public void setPositionFlightId(int positionFlightId) {
        this.positionFlightId = positionFlightId;
    }

    public boolean isPositionRoundtrip() {
        return positionRoundtrip;
    }

    public void setPositionRoundtrip(boolean positionRoundtrip) {
        this.positionRoundtrip = positionRoundtrip;
    }

    public static class Status {
        public static final int LookingForTickets = 1000;
        public static final int CouldNotFindTickets = 1900;
        public static final int WaitingForFlight = 2000;
        public static final int TooLateToBoard = 2900;
        public static final int OnBoard = 3000;
        public static final int Arrived = 4000;
        public static final int ItinerariesDone = 5000;
        public static final int LivingAtDestination = 7000;
        public static final int Done = 9000;
        public static final int Died = 9999;
    }
}
