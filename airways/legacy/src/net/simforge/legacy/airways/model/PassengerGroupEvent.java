package net.simforge.legacy.airways.model;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.SetNull;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;

@Table(name="aw_pg_event")
public class PassengerGroupEvent extends BaseEntity {
    @Column
    private int groupId;

    @Column
    private DateTime dt;

    @Column
    private String msg;

    @Column
    @SetNull
    private int cityId;

    @Column
    @SetNull
    private int airportId;

    @Column
    @SetNull
    private int flightId;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public DateTime getDt() {
        return dt;
    }

    public void setDt(DateTime dt) {
        this.dt = dt;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getAirportId() {
        return airportId;
    }

    public void setAirportId(int airportId) {
        this.airportId = airportId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }
}
