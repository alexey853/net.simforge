package net.simforge.legacy.airways.model;

import java.util.Date;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;
import org.joda.time.DateTime;
import net.simforge.legacy.airways.Caption;

@Table(name = "aw_flight")
public class Flight extends BaseEntity {
    @Column
    private int lineId;

    @Column
    private Date date;

    @Column
    private String number;

    @Column
    private int fromAirportId;

    @Column
    private int toAirportId;

    @Column
    private DateTime depTime;

    @Column
    private DateTime arrTime;

    @Column
    private int status;

    @Column
    private DateTime statusDt;

    @Column
    private DateTime heartbeatDt;

    @Column
    private int totalTickets;

    @Column
    private int freeTickets;

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int scheduleLineId) {
        this.lineId = scheduleLineId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getFromAirportId() {
        return fromAirportId;
    }

    public void setFromAirportId(int fromAirportId) {
        this.fromAirportId = fromAirportId;
    }

    public int getToAirportId() {
        return toAirportId;
    }

    public void setToAirportId(int toAirportId) {
        this.toAirportId = toAirportId;
    }

    public DateTime getDepTime() {
        return depTime;
    }

    public void setDepTime(DateTime depTime) {
        this.depTime = depTime;
    }

    public DateTime getArrTime() {
        return arrTime;
    }

    public void setArrTime(DateTime arrTime) {
        this.arrTime = arrTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DateTime getStatusDt() {
        return statusDt;
    }

    public void setStatusDt(DateTime statusDt) {
        this.statusDt = statusDt;
    }

    public DateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    public void setHeartbeatDt(DateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    public int getTotalTickets() {
        return totalTickets;
    }

    public void setTotalTickets(int totalTickets) {
        this.totalTickets = totalTickets;
    }

    public int getFreeTickets() {
        return freeTickets;
    }

    public void setFreeTickets(int freeTickets) {
        this.freeTickets = freeTickets;
    }

    public static class Status {
        public static final int Scheduled = 1000;
        @Caption("Check-in")
        public static final int Checkin = 2000;
        @Caption("Check-in done")
        public static final int CheckinDone = 2100;
        public static final int Boarding = 3000;
        @Caption("Boarding done")
        public static final int BoardingDone = 3100;
        public static final int Departing = 4000;
        public static final int Flying = 5000;
        public static final int Landed = 6000;
        public static final int Unboarding = 7000;
        @Caption("Unboarding done")
        public static final int UnboardingDone = 7100;
        public static final int Done = 9000;
    }
}
