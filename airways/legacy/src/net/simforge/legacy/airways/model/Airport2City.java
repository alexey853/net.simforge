package net.simforge.legacy.airways.model;

import net.simforge.commons.persistence.BaseEntity;
import net.simforge.commons.persistence.Column;
import net.simforge.commons.persistence.Table;

@Table(name="aw_airport2city")
public class Airport2City extends BaseEntity {
    @Column
    private int airportId;
    
    @Column
    private int cityId;

    public int getAirportId() {
        return airportId;
    }

    public void setAirportId(int airportId) {
        this.airportId = airportId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
