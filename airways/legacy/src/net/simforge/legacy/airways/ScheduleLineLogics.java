package net.simforge.legacy.airways;

import net.simforge.legacy.airways.model.ScheduleLine;
import net.simforge.legacy.airways.model.Flight;
import forge.commons.db.DB;
import forge.commons.TimeMS;
import forge.commons.Weekdays;

import java.sql.SQLException;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import net.simforge.commons.persistence.Persistence;
import org.joda.time.*;

public class ScheduleLineLogics implements Runnable {
    public static void declareFlights(ScheduleLine scheduleLine) {
        Connection connx = null;
        try {
            if (!scheduleLine.isActive())
                return;

            connx = DB.getConnection();

            DateMidnight today = DT.today();
            DateMidnight till = DT.addDays(today, scheduleLine.getHorizon()).toDateMidnight();

            String sql = "select * from %tn% where line_id = %id% and date between '%dt1%' and '%dt2%'";
            sql = sql.replaceAll("%id%", String.valueOf(scheduleLine.getId()));
            sql = sql.replaceAll("%dt1%", DT.DTF.print(today));
            sql = sql.replaceAll("%dt2%", DT.DTF.print(till));

            List<Flight> flights = Persistence.loadByQuery(connx, Flight.class, sql);
            Map<DateMidnight, Flight> flightByDate = new HashMap<DateMidnight, Flight>();
            for (Flight flight : flights) {
                flightByDate.put(new DateMidnight(flight.getDate()), flight);
            }

            DateMidnight curr = today;
            while (curr.isBefore(till) || curr.isEqual(till)) {
                Weekdays weekdays = scheduleLine.getWeekdays();
                boolean weekdaysCheck;
                if (weekdays != null) {
                    if (!weekdays.isOn(curr.getDayOfWeek())) {
                        weekdaysCheck = false;
                    } else {
                        weekdaysCheck = true;
                    }
                } else {
                    weekdaysCheck = true;
                }

                Flight flight = flightByDate.get(curr);
                if (flight == null && weekdaysCheck) {
                    flight = new Flight();
                    flight.setLineId(scheduleLine.getId());
                    flight.setNumber(scheduleLine.getNumber());
                    flight.setDate(curr.toDate());
                    flight.setFromAirportId(scheduleLine.getFromAirportId());
                    flight.setToAirportId(scheduleLine.getToAirportId());
                    flight.setDepTime(sum(curr, scheduleLine.getDepTime()));
                    flight.setArrTime(sum(flight.getDepTime(), scheduleLine.getDuration()));
                    flight.setStatus(Flight.Status.Scheduled);
                    flight.setTotalTickets(scheduleLine.getTotalTickets());
                    flight.setFreeTickets(flight.getTotalTickets());
                    flight.setHeartbeatDt(DT.now());

                    if (!FlightHelper.getCheckinDT(flight).isBeforeNow()) {
                        flight = Persistence.create(connx, flight);
                        FlightLifecycle.makeLog(connx, flight, "Scheduled");
                    }
                }

                curr = DT.addDays(curr, 1).toDateMidnight();
            }
            connx.commit();
            connx.close();
        } catch (SQLException e) {
            FlightLifecycle.getLogger().error("SQL exception happened", e);
            if (connx != null) {
                //noinspection EmptyCatchBlock
                try {
                    connx.rollback();
                    connx.close();
                } catch(SQLException e1) {
                }
            }
        }
    }

    public static DateTime sum(DateTime dt, Duration duration) {
        MutableDateTime mdt = new MutableDateTime(dt);
        mdt.add(duration);
        return mdt.toDateTime();
    }

    private static DateTime sum(DateMidnight day, TimeOfDay timeOfDay) {
        return timeOfDay.toDateTime(day);
    }

    public static void undeclareFlights(Connection connx, ScheduleLine scheduleLine) {
        throw new UnsupportedOperationException();
    }

    public static void main(String[] args) throws SQLException {
        Connection connx = DB.getConnection();

        if (false) {
            int london = 1;
            int londonLhr = 1;
            int paris = 2;
            int parisCdg = 6;
            int frankfurt = 3;
            int frankfurtFra = 8;

            TimeOfDay T0800 = new TimeOfDay(8, 0);
            TimeOfDay T1030 = new TimeOfDay(10, 30);
            TimeOfDay T1200 = new TimeOfDay(12, 0);
            TimeOfDay T1330 = new TimeOfDay(13, 30);
            TimeOfDay T1500 = new TimeOfDay(15, 0);
            TimeOfDay T1630 = new TimeOfDay(16, 30);
            Duration D0130 = new Duration(TimeMS.HOUR + 30 * TimeMS.MINUTE);

            createScheduleLine(connx, "ZZ101", frankfurt, frankfurtFra, london, londonLhr, T0800, D0130);
            createScheduleLine(connx, "ZZ102", london, londonLhr, frankfurt, frankfurtFra, T1030, D0130);
            createScheduleLine(connx, "ZZ103", frankfurt, frankfurtFra, london, londonLhr, T1200, D0130);
            createScheduleLine(connx, "ZZ104", london, londonLhr, frankfurt, frankfurtFra, T1330, D0130);
            createScheduleLine(connx, "ZZ105", frankfurt, frankfurtFra, london, londonLhr, T1500, D0130);
            createScheduleLine(connx, "ZZ106", london, londonLhr, frankfurt, frankfurtFra, T1630, D0130);

            createScheduleLine(connx, "ZZ111", frankfurt, frankfurtFra, paris, parisCdg, T0800, D0130);
            createScheduleLine(connx, "ZZ112", paris, parisCdg, frankfurt, frankfurtFra, T1030, D0130);
            createScheduleLine(connx, "ZZ113", frankfurt, frankfurtFra, paris, parisCdg, T1200, D0130);
            createScheduleLine(connx, "ZZ114", paris, parisCdg, frankfurt, frankfurtFra, T1330, D0130);
            createScheduleLine(connx, "ZZ115", frankfurt, frankfurtFra, paris, parisCdg, T1500, D0130);
            createScheduleLine(connx, "ZZ116", paris, parisCdg, frankfurt, frankfurtFra, T1630, D0130);
        }

        List<ScheduleLine> list = Persistence.loadAll(connx, ScheduleLine.class);
        for (ScheduleLine line : list) {
            declareFlights(line);
        }
    }

    private static void createScheduleLine(Connection connx, String number, int fromCityId, int fromAirportId, int toCityId, int toAirportId, TimeOfDay depTime, Duration duration) throws SQLException {
        ScheduleLine line = new ScheduleLine();
        line.setNumber(number);
//        line.setFromCityId(fromCityId);
        line.setFromAirportId(fromAirportId);
//        line.setToCityId(toCityId);
        line.setToAirportId(toAirportId);
        line.setDepTime(depTime);
        line.setDuration(duration);
        Persistence.create(connx, line);
    }

    public void run() {
        try {
            Connection connx = DB.getConnection();
            List<ScheduleLine> list = Persistence.loadAll(connx, ScheduleLine.class);
            connx.close();

            for (ScheduleLine line : list) {
                declareFlights(line);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isDateOfFlight(ScheduleLine line, DateMidnight day) {
        Weekdays weekdays = line.getWeekdays();
        if (weekdays == null) {
            return true;
        }
        return weekdays.isOn(day.getDayOfWeek());
    }
}
