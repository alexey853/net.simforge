package net.simforge.legacy.airways;

import java.lang.reflect.Field;

public class Captions {
    public static String get(Class clazz, int statusCode) {
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                Object o = field.get(clazz);
                if (o instanceof Integer) {
                    int fieldCode = (Integer) o;
                    if (statusCode == fieldCode) {
                        return getCaption(field);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return String.valueOf(statusCode);
    }

    private static String getCaption(Field field) {
        Caption caption = field.getAnnotation(Caption.class);
        if (caption != null && !caption.value().equals("")) {
            return caption.value();
        }
        return field.getName();
    }
}
