package net.simforge.legacy.airways;

import forge.commons.io.IOHelper;

import java.io.File;
import java.io.IOException;

import org.joda.time.DateTime;

public class WorldMarker {
    private static String markerFN(String marker) {
        return marker + ".marker";
    }

    private static String markerToStopFN(String marker) {
        return marker + ".marker.stop";
    }

    public static void setMarker(String marker) throws IOException {
        IOHelper.saveFile(new File(markerFN(marker)), DT.DTF.print(DT.now()));
    }

    public static void clearMarker(String marker) {
        new File(markerFN(marker)).delete();
    }

    public static void clearMarkerToStop(String marker) {
        new File(markerToStopFN(marker)).delete();
    }

    public static boolean hasMarkerToStop(String marker) {
        return new File(markerToStopFN(marker)).exists();
    }

    public static boolean isMarkerAlive(String marker) throws IOException {
        File file = new File(markerFN(marker));
        if (!file.exists())
            return false;
        String content = IOHelper.loadFile(file);
        DateTime dt = DT.DTF.parseDateTime(content);
        return System.currentTimeMillis() - dt.getMillis() < 60000;
    }

    public static void setMarkerToStop(String marker) throws IOException {
        IOHelper.saveFile(new File(markerToStopFN(marker)), "Has to stop");
    }
}
