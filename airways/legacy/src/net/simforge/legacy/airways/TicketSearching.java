package net.simforge.legacy.airways;

import net.simforge.legacy.airways.model.Flight;
import net.simforge.legacy.airways.model.PassengerGroup;
import net.simforge.legacy.airways.routing.Journey;
import net.simforge.legacy.airways.routing.Routing;

import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;

public class TicketSearching {
/*    public static List<Flight> search_old(Connection connx, PassengerGroup group) throws SQLException {
        String sql = "select * from %tn% where dep_time > '%dt%' and from_city_id = %from% and to_city_id = %to% and free_tickets >= %size% order by dep_time";
        sql = sql.replaceAll("%dt%", DT.DTF.print(DT.addHours(DT.now(), 1)));
        sql = sql.replaceAll("%from%", String.valueOf(group.getFromCityId()));
        sql = sql.replaceAll("%to%", String.valueOf(group.getToCityId()));
        sql = sql.replaceAll("%size%", String.valueOf(group.getSize()));
        List<Flight> flights = Persistence.loadByQuery(connx, Flight.class, sql);
        if (flights.isEmpty()) {
            return null;
        } else {
            Flight flight = flights.get(0);
            return Arrays.asList(flight);
        }
    }*/

    public static List<Flight> search(Connection connx, PassengerGroup group) throws SQLException {
        int fromCityId = group.getPositionCityId();
        int toCityId = !group.isPositionRoundtrip() ? group.getToCityId() : group.getFromCityId();

        Routing routing = new Routing(connx, RefDataCache.getCity(fromCityId), RefDataCache.getCity(toCityId), group.getSize());
        routing.proceed();
        List<Journey> journeys = routing.getJourneys();
        if (journeys.isEmpty()) {
            return null;
        } else {
            return journeys.get(0).getFlights();
        }
    }

/*    public static List<Flight> search_old2(Connection connx, PassengerGroup group) throws SQLException {
        int fromCityId = group.getPositionCityId();
        int toCityId = !group.isPositionRoundtrip() ? group.getToCityId() : group.getFromCityId();

        Node rootNode = new Node(fromCityId, toCityId, group.getSize());
        rootNode.makeStep(connx);

        List<Node> journeys = rootNode.journeys;
        if (journeys == null || journeys.isEmpty()) {
            return null;
        } else {
            List<Flight> flights = new ArrayList<Flight>();
            Node curr = journeys.get(0);
            while (curr != rootNode) {
                flights.add(0, curr.flight);
                curr = curr.previousNode;
            }
            return flights;
        }
    }

    public static class Node {
        int size;
        int cityId;
        int toCityId;
        Node previousNode;
        Node rootNode;
        Flight flight;
        int stopovers;
        List<Node> nextNodes = new ArrayList<Node>();
        List<Node> journeys;

        public Node(int cityId, int toCityId, int size) {
            this.cityId = cityId;
            this.toCityId = toCityId;
            this.size = size;
            this.rootNode = this;
        }

        public Node(Node previousNode, Flight flight) {
            this.previousNode = previousNode;
            this.flight = flight;
            this.cityId = flight.getToCityId();
            stopovers = previousNode.stopovers + 1;
        }

        public void makeStep(Connection connx) throws SQLException {
            String sql = "select * from %tn% where dep_time > '%dt%' and from_city_id = %from% and free_tickets >= %size% %notInClause% order by dep_time limit 0, 10";
            sql = sql.replaceAll("%dt%", flight == null ? DT.DTF.print(DT.addHours(DT.now(), 1)) : DT.DTF.print(DT.addHours(flight.getArrTime(), 1)));
            sql = sql.replaceAll("%from%", String.valueOf(cityId));
            sql = sql.replaceAll("%size%", String.valueOf(rootNode.size));

            String inClause = "";
            Node curr = this;
            while (curr != rootNode) {
                inClause = inClause + cityId + ",";
                curr = curr.previousNode;
            }
            inClause = inClause + rootNode.cityId + ",";
            if (inClause.endsWith(",")) {
                inClause = inClause.substring(0, inClause.length()-1);
                sql = sql.replace("%notInClause%", "and to_city_id not in (" + inClause + ")");
            } else {
                sql = sql.replace("%notInClause%", "");
            }

            List<Flight> flights = Persistence.loadByQuery(connx, Flight.class, sql);

            for (Flight eachFlight : flights) {
                Node node = new Node(this, eachFlight);
                node.rootNode = rootNode;
                nextNodes.add(node);
            }

            for (Node nextNode : nextNodes) {
                if (nextNode.stopovers > 2) {
                    continue;
                }

                if (rootNode.toCityId == nextNode.cityId) {
                    rootNode.addJourney(nextNode);
                    continue;
                }

                nextNode.makeStep(connx);
            }
        }

        private void addJourney(Node journey) {
            if (journeys == null) {
                journeys = new ArrayList<Node>();
            }
            journeys.add(journey);
        }
    }*/
}
