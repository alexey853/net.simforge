package net.simforge.legacy.airways;

import org.joda.time.TimeOfDay;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;

public class T {
    public static DecimalFormat df0_o = new DecimalFormat("0.#");
    public static DecimalFormat df0_000 = new DecimalFormat("0.000");
    public static DecimalFormat df0_000000 = new DecimalFormat("0.000000");
    public static DecimalFormat dfMo = new DecimalFormat("#,###,##0");

    public static String limit(String s, int maxSize) {
        if (s.length() > maxSize) {
            return s.substring(0, maxSize-3) + "...";
        } else {
            return s;
        }
    }

    public static String m(String s) {
        return s != null ? s : "";
    }

    public static String m(String s, String n) {
        return s != null ? s : n;
    }

    public static String mna(String s) {
        return m(s, "n/a");
    }
}
