package net.simforge.legacy.airways.worldbuilder;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import net.simforge.commons.io.IOHelper;
import net.simforge.legacy.airways.model.Country;
import net.simforge.legacy.airways.model.City;
import org.joda.time.DateTime;

public class TageoCom {
    private static final String LOCAL_ROOT = "./airways/data/tageo.com/";
    private static final String TAGEO_ROOT = "http://www.tageo.com/";
    private static final int DEPTH = 4;

    public static void main(String[] args) throws IOException, SQLException {
        String countriesStr = IOHelper.loadFile(new File(LOCAL_ROOT + "_countries.txt"));
        String[] countriesStrs = countriesStr.split(";");

        for (int i = 0; i < countriesStrs.length / 2; i++) {
            System.out.println(new DateTime() + " " + countriesStrs[i*2+1]);
//            downloadCountry(countriesStrs[i*2]);
            importCountry(countriesStrs[i*2+1], countriesStrs[i*2]);
        }
    }

    private static void importCountry(String name, String countryUrl) throws SQLException, IOException {
        name = mask(name);

        System.out.println(new DateTime() +  " importCountry " + countryUrl);

        String countryContent = getContent(countryUrl);
        String citiesUrl = getCitiesUrl(countryContent);

        Connection connx = DB.getConnection();

        List<Country> countries = Persistence.loadByQuery(connx, Country.class, "select * from %tn% where name = '" + name + "'");
        Country country;
        if (countries.isEmpty()) {
            int index = citiesUrl.lastIndexOf(".htm");
            String code = citiesUrl.substring(index-2, index);

            country = new Country();
            country.setName(name);
            country.setCode(code);
            country = Persistence.create(connx, country);
            connx.commit();
        } else if (countries.size() == 1) {
            country = countries.get(0);
        } else {
            throw new RuntimeException(name + " " + countryUrl);
        }

        List<String> dataUrls = new ArrayList<String>();
        dataUrls.add(citiesUrl);
        for (int i = 1; i <= DEPTH; i++) {
            String stepUrl = citiesUrl.replace(".", "-step-" + i + ".");
            dataUrls.add(stepUrl);
        }

        boolean firstCity = true;
        for (String dataUrl : dataUrls) {
            String data = getContent(dataUrl);
            if (data == null) {
                continue;
            }
            String str = Html.toPlainText(data);

            int index = str.indexOf("Rank;City;Population (2000);Latitude (DD);Longitude (DD)");
            str = str.substring(index);
            String[] strs = str.split("\r\n");

            int strIndex = 1;
            while (true) {
                str = strs[strIndex];
                String[] cityStrs = str.split(";");

                if (cityStrs.length != 5) {
                    break;
                }

                strIndex++;

                String cityName = mask(cityStrs[1]);
                int population;
                double lat;
                double lon;
                try {
                    population = Integer.valueOf(cityStrs[2]);
                    lat = Double.valueOf(cityStrs[3]);
                    lon = Double.valueOf(cityStrs[4]);
                } catch (Exception e) {
                    continue;
                }

                List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn% where country_id = " + country.getId() + " and name = '" + cityName.replace("'", "''") + "'");
                if (cities.isEmpty()) {
                    City city = new City();
                    city.setCountryId(country.getId());
                    city.setName(cityName);
                    city.setPopulation(population);
                    city.setLat(lat);
                    city.setLon(lon);
                    city.setDataset(2);
                    Persistence.create(connx, city);
                    firstCity = false;
                    connx.commit();
                } else if (cities.size() == 1) {
                    // todo
                } else {
                    throw new RuntimeException();
                }
            }
        }

        connx.close();
    }

    private static void downloadCountry(String countryPage) throws IOException {
        String content = getContent(countryPage);
        String citiesUrl = getCitiesUrl(content);
        getContent(citiesUrl);
        for (int i = 1; i <= DEPTH; i++) {
            String stepUrl = citiesUrl.replace(".", "-step-" + i + ".");
            try {
                getContent(stepUrl);
            } catch (IOException e) {
                break;
            }
        }
    }

    private static void sleep() {
        try {
            double seconds = Math.random() * 30 + 30;
            System.out.println(new DateTime() + " Sleep for " + (int) seconds);
            Thread.sleep((long) (1000 * seconds));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String getContent(String page) throws IOException {
        File file = new File(LOCAL_ROOT + page);
        if (file.exists()) {
            return IOHelper.loadFile(file);
        }

        return null;
//        sleep();
//
//        System.out.println(new DateTime() +  " Downloading " + page);
//        String url = TAGEO_ROOT + page;
//        String content = IOHelper.download(url);
//        IOHelper.saveFile(file, content);
//        return content;
    }

    private static String getCitiesUrl(String content) {
        int index = content.indexOf("City & Town Population");
        if (index != -1) {
            content = content.substring(0, index);
            index = content.lastIndexOf("index-");
            if (index != -1) {
                //noinspection RedundantStringConstructorCall
                content = new String(content.substring(index, content.lastIndexOf('\'')));
                return content;
            }
        }
        return null;
    }

    private static String mask(String name) {
        boolean changed = false;

        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                buf.append(c);
            } else if (c >= 'a' && c <= 'z') {
                buf.append(c);
            } else if (c == '-' || c == ' ' || c == '\'') {
                buf.append(c);
            } else {
                buf.append('#');
                changed = true;
            }
        }

        if (changed) {
            System.out.println(name);
            System.out.println(buf.toString());
            System.out.println();
        }

        return buf.toString();
    }
}
