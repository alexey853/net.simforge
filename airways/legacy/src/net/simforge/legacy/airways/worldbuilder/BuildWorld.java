package net.simforge.legacy.airways.worldbuilder;

import net.simforge.legacy.airways.model.City;
import net.simforge.legacy.airways.model.Airport;
import net.simforge.legacy.airways.model.Airport2City;
import forge.commons.db.DB;
import net.simforge.commons.persistence.Persistence;

import java.sql.SQLException;
import java.sql.Connection;
import java.util.List;

public class BuildWorld {
    private static Connection connx;

    public static void main(String[] args) throws SQLException {
        connx = DB.getConnection();

        clearTables();

        City city_london = newCity("London", 51.5, -0.125, 7556900);
        City city_paris = newCity("Paris", 48.85, 2.35, 2203817);
        City city_frankfurt = newCity("Frankfurt", 50.1, 8.67, 651899);

        createCity2CityFlows();

        Airport airport_egll = newAirport("LHR", "EGLL", "Heathrow", 51.47, -0.45);
        Airport airport_egkk = newAirport("LGW", "EGKK", "Gatwick", 51.15, -0.18);
        Airport airport_eglc = newAirport("LCY", "EGLC", "London City", 51.50, 0.05);
        Airport airport_egss = newAirport("STN", "EGSS", "Stansted", 51.89, 0.23);
        Airport airport_eggw = newAirport("LTN", "EGGW", "Luton", 51.88, -0.37);

        linkAirport2City(airport_egll, city_london);
        linkAirport2City(airport_egkk, city_london);
        linkAirport2City(airport_eglc, city_london);
        linkAirport2City(airport_egss, city_london);
        linkAirport2City(airport_eggw, city_london);

        Airport airport_lfpg = newAirport("CDG", "LFPG", "Charles de Gaulle", 49.02, 2.54);
        Airport airport_lfpo = newAirport("ORY", "LFPO", "Orly", 48.73, 2.37);

        linkAirport2City(airport_lfpg, city_paris);
        linkAirport2City(airport_lfpo, city_paris);

        Airport airport_eddf = newAirport("FRA", "EDDF", "Frankfurt", 50.03, 8.57);

        linkAirport2City(airport_eddf, city_frankfurt);
    }

    private static Airport newAirport(String iata, String icao, String name, double lat, double lon) throws SQLException {
        Airport airport = new Airport();
        airport.setIata(iata);
        airport.setIcao(icao);
        airport.setName(name);
        airport.setLat(lat);
        airport.setLon(lon);
        return Persistence.create(connx, airport);
    }

    private static void linkAirport2City(Airport airport, City city) throws SQLException {
        Airport2City airport2City = new Airport2City();
        airport2City.setAirportId(airport.getId());
        airport2City.setCityId(city.getId());
        Persistence.create(connx, airport2City);
    }

    private static void createCity2CityFlows() throws SQLException {
        List<City> cities = Persistence.loadAll(connx, City.class);
        for (int i = 0; i < cities.size(); i++) {
            City c1 = cities.get(i);
            for (int j = 0; j < cities.size(); j++) {
                if (i == j)
                    continue;
                City c2 = cities.get(j);

/*                City2CityFlow c2cFlow = new City2CityFlow();
                c2cFlow.setFromCityId(c1.getId());
                c2cFlow.setToCityId(c2.getId());
                c2cFlow.setFlowWeight(1);
                c2cFlow.setFlowRemainder(0);
                Persistence.create(connx, c2cFlow);*/
            }
        }
    }

    private static void clearTables() throws SQLException {
        DB.executeUpdate(connx, "truncate table aw_city");
        DB.executeUpdate(connx, "truncate table aw_city2city_flow");
        DB.executeUpdate(connx, "truncate table aw_airport");
        DB.executeUpdate(connx, "truncate table aw_airport2city");
    }

    private static City newCity(String name, double lat, double lon, int population) throws SQLException {
        City city = new City();
        city.setName(name);
        city.setLat(lat);
        city.setLon(lon);
        city.setPopulation(population);
        return Persistence.create(connx, city);
    }
}
