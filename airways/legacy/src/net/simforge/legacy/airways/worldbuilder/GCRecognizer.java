package net.simforge.legacy.airways.worldbuilder;

import forge.commons.gc_kls2_com.GCAirport;
import forge.commons.gc_kls2_com.GC;
import forge.commons.db.DB;
import forge.commons.Geo;

import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;

import net.simforge.legacy.airways.model.City;
import net.simforge.legacy.airways.model.Airport;
import net.simforge.legacy.airways.model.Airport2City;
import net.simforge.commons.persistence.Persistence;

public class GCRecognizer {
    public static void main(String[] args) throws SQLException {
        Connection connx = DB.getConnection();
        DB.executeUpdate(connx, "use schedules");
        List<GCAirport> gcAirports = DB.readList(connx, "select * from " + GC.airport_tablename, GCAirport.class);

        DB.executeUpdate(connx, "use airways");
/*
        for (GCAirport gcAirport : gcAirports) {
            Airport airport = new Airport();
            airport.setName(gcAirport.getName());
            airport.setIata(gcAirport.getIata());
            airport.setIcao(gcAirport.getIcao());
            airport.setLat(gcAirport.getLat());
            airport.setLon(gcAirport.getLon());
            Persistence.create(connx, airport);
        }*/

        List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn% where population >= 100000 and country_id = 141");
//        List<City> cities = Persistence.loadByQuery(connx, City.class, "select * from %tn%");

        int found = 0;
        List<Airport> airports = Persistence.loadAll(connx, Airport.class);
        for (Airport airport : airports) {
            if (airport.getCityId() != 0)
                continue;

            double minDist = 0;
            City nearest = null;
            for (City city : cities) {
                double dist = Geo.distanceNM(airport.getLat(), airport.getLon(), city.getLat(), city.getLon());
                if (nearest == null) {
                    minDist = dist;
                    nearest = city;
                } else {
                    if (dist < minDist) {
                        minDist = dist;
                        nearest = city;
                    }
                }
            }

            if (nearest != null) {
                if (minDist > 50) {
                    nearest = null;
                }
            }

            if (nearest != null) {
                found++;

                airport.setCityId(nearest.getId());
                Persistence.update(connx, airport);

                Airport2City a2c = new Airport2City();
                a2c.setCityId(nearest.getId());
                a2c.setAirportId(airport.getId());
                Persistence.create(connx, a2c);
            }

/*            System.out.println(
                    a(airport.getName(), 120) +
                    (nearest != null ? nearest.getName() + ", " + ((Country)RefDataCache.get(Country.class, nearest.getCountryId())).getName() + ", D=" + (int) minDist : "not found"));*/
        }

        System.out.println("Found = " + found);
        connx.close();
    }

    private static String a(String s, int i) {
        while (s.length() < i) {
            s = s + " ";
        }
        return s;
    }
}
