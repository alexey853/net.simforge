package net.simforge.legacy.lifecycle;

import java.sql.SQLException;

public class LifecycleRunnable implements Runnable {
    private final Class lifecycleClazz;

    public LifecycleRunnable(Class lifecycleClazz) {
        this.lifecycleClazz = lifecycleClazz;
    }

    public void run() {
        try {
            Lifecycle.heartbeat(lifecycleClazz);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
