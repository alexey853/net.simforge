package net.simforge.legacy.lifecycle;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface LifecycleDefinition {
    Class clazz();

    int finalStatus();

    int batchSize() default 100;
}
