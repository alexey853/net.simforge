package net.simforge.legacy.lifecycle;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.sql.Connection;
import java.sql.SQLException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import org.joda.time.DateTime;
import net.simforge.legacy.airways.DT;

public class Lifecycle {
    private Class lifecycleClazz;
    private LifecycleDefinition definition;
    private Class clazz;
    private int finalStatus;
    private int batchSize;

    private Field heartbeatDtField;
    private Field statusField;
    private Map<Integer, Method> statusHandlers = new HashMap<Integer, Method>();

    private static final String HeartbeatDtFieldname = "heartbeatDt";
    private static final String StatusFieldname = "status";

    public static void heartbeat(Class lifecycleClazz) throws SQLException {
        Lifecycle lifecycle = new Lifecycle(lifecycleClazz);
        lifecycle.heartbeat();
    }

    public Lifecycle(Class lifecycleClazz) {
        this.lifecycleClazz = lifecycleClazz;

        //noinspection unchecked
        definition = (LifecycleDefinition) lifecycleClazz.getAnnotation(LifecycleDefinition.class);
        clazz = definition.clazz();
        finalStatus = definition.finalStatus();
        batchSize = definition.batchSize();

        heartbeatDtField = getField(HeartbeatDtFieldname);
        statusField = getField(StatusFieldname);

        Method[] methods = lifecycleClazz.getDeclaredMethods();
        for (Method method : methods) {
            StatusHandler statusHandlerAnnotation = method.getAnnotation(StatusHandler.class);
            if (statusHandlerAnnotation == null) {
                continue;
            }

            int statusCode = statusHandlerAnnotation.code();
            if (statusHandlers.get(statusCode) != null) {
                throw new RuntimeException("Duplicated status handlers for status " + statusCode);
            }

            statusHandlers.put(statusCode, method);
        }
    }

    private Field getField(String fieldname) {
        Field field;
        try {
            field = clazz.getDeclaredField(fieldname);
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Could not find field " + fieldname, e);
        }
        return field;
    }

    private void heartbeat() throws SQLException {
        Logger logger = getLogger();
        Connection connx = DB.getConnection();
        List objects;
        try {
            String sql = "select * from %tn% where (heartbeat_dt <= '%dt%' or heartbeat_dt is null) and status != %final% order by heartbeat_dt asc limit " + batchSize;
            sql = sql.replaceAll("%dt%", DT.DTF.print(new DateTime()));
            sql = sql.replaceAll("%final%", String.valueOf(finalStatus));
            //noinspection unchecked
            objects = Persistence.loadByQuery(
                    connx,
                    clazz,
                    sql);
            connx.close();
        } catch (SQLException e) {
            if (logger != null) {
                getLogger().error("SQL exception happened", e);
            }

            if (connx != null) {
                //noinspection EmptyCatchBlock
                try {
                    connx.rollback();
                    connx.close();
                } catch(SQLException e1) {
                }
            }

            return;
        }

        if (logger != null) {
            if (!objects.isEmpty()) {
                logger.info("Processing queue: " + objects.size());
            }
        }

        for (Object object : objects) {
            if (isHeartbeatInFuture(object)) {
                break;
            }

            try {
                heartbeat(object);
            } catch(Throwable t) {
                if (logger != null) {
                    logger.error("Error during lifecycle", t);
                }
            }
        }
    }

    private Logger getLogger() {
        try {
            Method getLoggerMethod = lifecycleClazz.getMethod("getLogger");
            if (getLoggerMethod != null) {
                return (Logger) getLoggerMethod.invoke(lifecycleClazz);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void heartbeat(Object object) {
        Integer status = (Integer) getValue(statusField, object);
        Method method = getStatusHandler(status);
        try {
            BaseLifecycle baseLifecycle = (BaseLifecycle) lifecycleClazz.newInstance();
            baseLifecycle.setObject(object);
            method.invoke(baseLifecycle);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    private Method getStatusHandler(int statusCode) {
        Method method = statusHandlers.get(statusCode);
        if (method == null) {
            throw new RuntimeException("Could not find status handler for status " + statusCode);
        }
        return method;
    }

    private boolean isHeartbeatInFuture(Object object) {
        DateTime heartbeatDt = (DateTime) getValue(heartbeatDtField, object);
        //noinspection SimplifiableIfStatement
        if (heartbeatDt == null)
            return false;
        return heartbeatDt.getMillis() > new DateTime().getMillis();
    }

    private Object getValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could not get value of field " + field.getName());
        }
    }
}
