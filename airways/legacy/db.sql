-- phpMyAdmin SQL Dump
-- version 2.6.1
-- http://www.phpmyadmin.net
--
-- ����: localhost
-- ����� ��������: ��� 24 2010 �., 17:12
-- ������ �������: 5.0.45
-- ������ PHP: 5.2.4
--
-- ��: `airways`
--

-- --------------------------------------------------------

--
-- ��������� ������� `aw_airport`
--

CREATE TABLE aw_airport (
  id int(11) NOT NULL auto_increment,
  iata varchar(30) default NULL,
  icao varchar(4) default NULL,
  `name` varchar(30) default NULL,
  lat float default NULL,
  lon float default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_airport2city`
--

CREATE TABLE aw_airport2city (
  id int(11) NOT NULL auto_increment,
  airport_id int(11) NOT NULL,
  city_id int(11) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_city`
--

CREATE TABLE aw_city (
  id int(11) NOT NULL auto_increment,
  country_id int(11) default NULL,
  `name` varchar(50) default NULL,
  lat float default NULL,
  lon float default NULL,
  population bigint(20) default NULL,
  last_flow_tick_dt datetime default NULL,
  attraction double default NULL,
  activity double default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_city2city_flow`
--

CREATE TABLE aw_city2city_flow (
  id int(11) NOT NULL auto_increment,
  from_city_id int(11) NOT NULL,
  to_city_id int(11) NOT NULL,
  flow_weight double default NULL,
  flow_remainder double default NULL,
  next_group_size int(11) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_city2city_flow_stats`
--

CREATE TABLE aw_city2city_flow_stats (
  id int(11) NOT NULL auto_increment,
  flow_id int(11) NOT NULL,
  processed tinyint(4) default NULL,
  since datetime NOT NULL,
  till datetime NOT NULL,
  weight_before double default NULL,
  weight_after double default NULL,
  weight_change double default NULL,
  could_not_find_tickets int(11) default NULL,
  PRIMARY KEY  (id),
  KEY idx_flow_id_till (flow_id,till)
) ENGINE=MyISAM AUTO_INCREMENT=339 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_country`
--

CREATE TABLE aw_country (
  id int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `code` varchar(2) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY `name` (`name`,`code`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_flight`
--

CREATE TABLE aw_flight (
  id int(11) NOT NULL auto_increment,
  schedule_line_id int(11) NOT NULL,
  number varchar(10) default NULL,
  `date` date NOT NULL,
  from_city_id int(11) NOT NULL,
  from_airport_id int(11) NOT NULL,
  to_city_id int(11) NOT NULL,
  to_airport_id int(11) NOT NULL,
  dep_time datetime default NULL,
  arr_time datetime default NULL,
  `status` int(11) default NULL,
  heartbeat_dt datetime default NULL,
  total_tickets smallint(4) default NULL,
  free_tickets smallint(4) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=277 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_flight_event`
--

CREATE TABLE aw_flight_event (
  id int(11) NOT NULL auto_increment,
  flight_id int(11) NOT NULL,
  dt datetime NOT NULL,
  msg varchar(200) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=235 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_passenger_group`
--

CREATE TABLE aw_passenger_group (
  id int(11) NOT NULL auto_increment,
  created_dt datetime NOT NULL,
  size mediumint(9) NOT NULL,
  position_city_id int(11) default NULL,
  position_airport_id int(11) default NULL,
  position_flight_id int(11) default NULL,
  position_roundtrip tinyint(4) default NULL,
  from_city_id int(11) NOT NULL,
  to_city_id int(11) NOT NULL,
  roundtrip tinyint(4) default NULL,
  c2c_flow_id int(11) NOT NULL,
  `status` int(11) default NULL,
  itinerary_id int(11) default NULL,
  heartbeat_dt datetime default NULL,
  expire_dt datetime default NULL,
  PRIMARY KEY  (id),
  KEY idx_heartbeat_dt (heartbeat_dt)
) ENGINE=MyISAM AUTO_INCREMENT=6291 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_passenger_group_event`
--

CREATE TABLE aw_passenger_group_event (
  id int(11) NOT NULL auto_increment,
  group_id int(11) NOT NULL,
  dt datetime NOT NULL,
  msg varchar(200) default NULL,
  city_id int(11) default NULL,
  airport_id int(11) default NULL,
  flight_id int(11) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=7346 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_passenger_group_itinerary`
--

CREATE TABLE aw_passenger_group_itinerary (
  id int(11) NOT NULL auto_increment,
  group_id int(11) NOT NULL,
  item_order int(11) default NULL,
  flight_id int(11) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=3995 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- ��������� ������� `aw_schedule_line`
--

CREATE TABLE aw_schedule_line (
  id int(11) NOT NULL auto_increment,
  number varchar(10) default NULL,
  from_city_id int(11) NOT NULL,
  from_airport_id int(11) NOT NULL,
  to_city_id int(11) NOT NULL,
  to_airport_id int(11) NOT NULL,
  dep_time varchar(5) NOT NULL,
  duration varchar(5) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=cp1251;
        