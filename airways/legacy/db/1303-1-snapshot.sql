
CREATE SEQUENCE aw_airport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE aw_airport (
    id integer DEFAULT nextval('aw_airport_id_seq'::regclass) NOT NULL,
    version smallint NOT NULL,
    city_id integer,
    iata character varying(3),
    icao character varying(4),
    name character varying(50),
    lat real,
    lon real,
    dataset smallint
);

CREATE SEQUENCE aw_airport2city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE aw_airport2city (
    id integer DEFAULT nextval('aw_airport2city_id_seq'::regclass) NOT NULL,
    version smallint,
    airport_id integer,
    city_id integer
);

CREATE SEQUENCE aw_city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE aw_city (
    id integer DEFAULT nextval('aw_city_id_seq'::regclass) NOT NULL,
    version smallint NOT NULL,
    country_id integer,
    name character varying(50),
    lat real,
    lon real,
    population bigint,
    last_flow_tick_dt timestamp without time zone,
    attraction real,
    activity real,
    dataset smallint
);

CREATE SEQUENCE aw_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE aw_country (
    id integer DEFAULT nextval('aw_country_id_seq'::regclass) NOT NULL,
    version smallint NOT NULL,
    name character varying(50) NOT NULL,
    code character varying(2) NOT NULL
);

CREATE SEQUENCE nf_city_flow_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE nf_city_flow (
    id integer DEFAULT nextval('nf_city_flow_id_seq'::regclass) NOT NULL,
    version smallint NOT NULL,
    city_id integer,
    status smallint,
    last_recalc_dt timestamp without time zone,
    attraction real,
    units_threshold real,
    default_availability real
);

ALTER TABLE ONLY aw_airport
    ADD CONSTRAINT pk_aw_airport PRIMARY KEY (id);

ALTER TABLE ONLY aw_airport2city
    ADD CONSTRAINT pk_aw_airport2city PRIMARY KEY (id);

ALTER TABLE ONLY aw_city
    ADD CONSTRAINT pk_aw_city PRIMARY KEY (id);

ALTER TABLE ONLY aw_country
    ADD CONSTRAINT pk_aw_country PRIMARY KEY (id);

ALTER TABLE ONLY nf_city_flow
    ADD CONSTRAINT pk_nf_city_flow PRIMARY KEY (id);

ALTER TABLE ONLY aw_country
    ADD CONSTRAINT uq_name UNIQUE (name);

ALTER TABLE ONLY aw_city
    ADD CONSTRAINT fk_country_id FOREIGN KEY (country_id) REFERENCES aw_country(id);
