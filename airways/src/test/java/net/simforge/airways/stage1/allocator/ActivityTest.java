package net.simforge.airways.stage1.allocator;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ActivityTest {
    @Test
    public void testIntersectYes1() {
        // -----==========-------
        // --=====---------------
        Activity activity1 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10,  0), null, LocalDateTime.of(2016, 1, 1, 12,  0));
        Activity activity2 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1,  9,  0), null, LocalDateTime.of(2016, 1, 1, 11,  0));
        assertTrue(activity1.isIntersecting(activity2));
        assertTrue(activity2.isIntersecting(activity1));
    }

    @Test
    public void testIntersectYes2() {
        // -----==========-------
        // -------====-----------
        Activity activity1 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10,  0), null, LocalDateTime.of(2016, 1, 1, 12,  0));
        Activity activity2 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10, 30), null, LocalDateTime.of(2016, 1, 1, 11, 30));
        assertTrue(activity1.isIntersecting(activity2));
        assertTrue(activity2.isIntersecting(activity1));
    }

    @Test
    public void testIntersectYes3() {
        // -----==========-------
        // -------------====-----
        Activity activity1 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10,  0), null, LocalDateTime.of(2016, 1, 1, 12,  0));
        Activity activity2 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 11,  0), null, LocalDateTime.of(2016, 1, 1, 13,  0));
        assertTrue(activity1.isIntersecting(activity2));
        assertTrue(activity2.isIntersecting(activity1));
    }

    @Test
    public void testIntersectNo1() {
        // ------==========------
        // -====-----------------
        Activity activity1 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10,  0), null, LocalDateTime.of(2016, 1, 1, 12,  0));
        Activity activity2 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1,  8,  0), null, LocalDateTime.of(2016, 1, 1,  9,  0));
        assertFalse(activity1.isIntersecting(activity2));
        assertFalse(activity2.isIntersecting(activity1));
    }

    @Test
    public void testIntersectNo2() {
        // ------==========------
        // -----------------====-
        Activity activity1 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 10,  0), null, LocalDateTime.of(2016, 1, 1, 12,  0));
        Activity activity2 = Activity.forFlight(null, LocalDateTime.of(2016, 1, 1, 13,  0), null, LocalDateTime.of(2016, 1, 1, 14,  0));
        assertFalse(activity1.isIntersecting(activity2));
        assertFalse(activity2.isIntersecting(activity1));
    }
}
