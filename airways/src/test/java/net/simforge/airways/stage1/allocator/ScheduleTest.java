package net.simforge.airways.stage1.allocator;

import net.simforge.airways.stage1.model.geo.Airport;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ScheduleTest {
    private Airport airportA = new Airport();
    private Airport airportB = new Airport();
    private Airport airportC = new Airport();

    private LocalDateTime time_10_00 = LocalDateTime.of(2016, 1, 1, 10, 0);
    private LocalDateTime time_12_00 = LocalDateTime.of(2016, 1, 1, 12, 0);
    private LocalDateTime time_13_00 = LocalDateTime.of(2016, 1, 1, 13, 0);
    private LocalDateTime time_14_00 = LocalDateTime.of(2016, 1, 1, 14, 0);
    private LocalDateTime time_15_00 = LocalDateTime.of(2016, 1, 1, 15, 0);
    private LocalDateTime time_16_00 = LocalDateTime.of(2016, 1, 1, 16, 0);
    private LocalDateTime time_18_00 = LocalDateTime.of(2016, 1, 1, 18, 0);
    private LocalDateTime time_19_00 = LocalDateTime.of(2016, 1, 1, 19, 0);
    private LocalDateTime time_20_00 = LocalDateTime.of(2016, 1, 1, 20, 0);
    private LocalDateTime time_21_00 = LocalDateTime.of(2016, 1, 1, 21, 0);
    private LocalDateTime time_22_00 = LocalDateTime.of(2016, 1, 1, 22, 0);

    @Before
    public void before() {
        airportA.setIcao("AAAA");
        airportB.setIcao("BBBB");
        airportC.setIcao("CCCC");
    }

    @Test
    public void testEmptySchedule() {
        // S: --[A]------------------------
        // A: --------{A-----B}------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));

        Activity activity = Activity.forFlight(airportA, time_10_00, airportB, time_12_00);

        assertTrue(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testEmptySchedule_wrongAirport() {
        // S: --[A]------------------------
        // A: --------{B-----С}------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));

        Activity activity = Activity.forFlight(airportB, time_10_00, airportC, time_12_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testSomeSchedule_addingNewFlight() {
        // S: --{A--B}--{B-C}-------------------------
        // A: -------------------{C-----B}------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_10_00, airportB, time_12_00));
        schedule.add(Activity.forFlight(airportB, time_13_00, airportC, time_15_00));

        Activity activity = Activity.forFlight(airportC, time_16_00, airportB, time_18_00);

        assertTrue(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testSomeSchedule_addingNewFlight_wrongAirport() {
        // S: --{A--B}--{B-C}-------------------------
        // A: -------------------{A-----B}------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_10_00, airportB, time_12_00));
        schedule.add(Activity.forFlight(airportB, time_13_00, airportC, time_15_00));

        Activity activity = Activity.forFlight(airportA, time_16_00, airportB, time_18_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testSomeSchedule_addingNewFlight_intersection() {
        // S: --{A--B}--{B-C}-------------------------
        // A: ------------{C-----B}-------------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_10_00, airportB, time_12_00));
        schedule.add(Activity.forFlight(airportB, time_13_00, airportC, time_15_00));

        Activity activity = Activity.forFlight(airportC, time_14_00, airportB, time_16_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testComplexSchedule_addingNewFlight_incompatibleState() {
        // S: --{A--B}--{B-C}----------------{C---A}--
        // A: -------------------{C-----B}------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_10_00, airportB, time_12_00));
        schedule.add(Activity.forFlight(airportB, time_13_00, airportC, time_15_00));
        schedule.add(Activity.forFlight(airportC, time_19_00, airportA, time_20_00));

        Activity activity = Activity.forFlight(airportC, time_16_00, airportB, time_18_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testComplexSchedule_addingNewFlight_incompatibleState2() {
        // S: --{A--B}--{B-C}------------{C-B}--{B-A}--
        // A: -------------------{C--B}----------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_10_00, airportB, time_12_00));
        schedule.add(Activity.forFlight(airportB, time_13_00, airportC, time_15_00));
        schedule.add(Activity.forFlight(airportC, time_19_00, airportB, time_20_00));
        schedule.add(Activity.forFlight(airportB, time_21_00, airportA, time_22_00));

        Activity activity = Activity.forFlight(airportC, time_16_00, airportB, time_18_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }

    @Test
    public void testSomeSchedule_addingNewFlight_incompatibleWithCurrentState() {
        // S: --[A]--------------------{A--B}---------
        // A: ----------{C-----A}---------------------

        Schedule schedule = new Schedule();
        schedule.setCurrentState(new InAirportState(airportA));
        schedule.add(Activity.forFlight(airportA, time_16_00, airportB, time_18_00));

        Activity activity = Activity.forFlight(airportC, time_12_00, airportA, time_14_00);

        assertFalse(schedule.isActivitySuitable(activity));
    }
}
