package net.simforge.airways.stage1;

import net.simforge.airways.stage1.model.aircraft.AircraftType;

public class TestRefData {
    static AircraftType getA320Data() {
        AircraftType data = new AircraftType();
        data.setTypicalCruiseAltitude(36000);
        data.setTypicalCruiseSpeed(444);
        data.setTakeoffSpeed(160);
        data.setLandingSpeed(150);
        data.setClimbVerticalSpeed(2000);
        data.setDescentVerticalSpeed(1200);
        return data;
    }

    static AircraftType getC152Data() {
        AircraftType data = new AircraftType();
        data.setTypicalCruiseAltitude(6000);
        data.setTypicalCruiseSpeed(107);
        data.setTakeoffSpeed(60);
        data.setLandingSpeed(60);
        data.setClimbVerticalSpeed(715);
        data.setDescentVerticalSpeed(500);
        return data;
    }
}
