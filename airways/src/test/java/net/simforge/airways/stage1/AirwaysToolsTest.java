package net.simforge.airways.stage1;

import junit.framework.TestCase;

public class AirwaysToolsTest extends TestCase {
    public void testMakeFlightNumber() throws Exception {
        assertEquals("ZZ1", AirwaysTools.makeFlightNumber("ZZ", 1));
        assertEquals("ZZ101", AirwaysTools.makeFlightNumber("ZZ", 101));
    }

    public void testIncreaseFlightNumber() throws Exception {
        assertEquals("ZZ101", AirwaysTools.increaseFlightNumber("ZZ100"));
    }
}