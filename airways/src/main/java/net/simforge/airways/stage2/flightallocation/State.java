package net.simforge.airways.stage2.flightallocation;

public interface State {
    boolean isCompatibleWith(State state);
}
