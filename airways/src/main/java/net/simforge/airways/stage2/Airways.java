package net.simforge.airways.stage2;

import net.simforge.airways.stage2.model.*;
import net.simforge.airways.stage2.model.aircraft.Aircraft;
import net.simforge.airways.stage2.model.aircraft.AircraftType;
import net.simforge.airways.stage2.model.flight.*;
import net.simforge.airways.stage2.model.flow.City2CityFlow;
import net.simforge.airways.stage2.model.flow.City2CityFlowStats;
import net.simforge.airways.stage2.model.flow.CityFlow;
import net.simforge.airways.stage2.model.geo.Airport;
import net.simforge.airways.stage2.model.geo.Airport2City;
import net.simforge.airways.stage2.model.geo.City;
import net.simforge.airways.stage2.model.geo.Country;
import net.simforge.commons.hibernate.SessionFactoryBuilder;
import net.simforge.commons.legacy.BM;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class Airways {

    public static final int ACTIVE_DATASET = 0;
    public static final int INACTIVE_DATASET = 1;
    public static final int TAGEO_COM_DATASET = 2;
    public static final int FSECONOMY_DATASET = 3;

    public static final Class[] entities = {
            Airport.class,
            Airport2City.class,
            Country.class,
            City.class,

            EventLogEntry.class,

            CityFlow.class,
            City2CityFlow.class,
            City2CityFlowStats.class,

            Journey.class,
            Person.class,

            Pilot.class,

            AircraftType.class,
            Aircraft.class,

            Airline.class,

            TimetableRow.class,
            TransportFlight.class,

            Flight.class,
            PilotAssignment.class,
            AircraftAssignment.class,
    };

    public static SessionFactory buildSessionFactory() {
        return SessionFactoryBuilder
                .forDatabase("airways")
                .entities(entities)
                .build();
    }

    public static Country countryByName(Session session, String countryName) {
        BM.start("Airways.countryByName");
        try {

            //noinspection JpaQlInspection
            return (Country) session
                    .createQuery("from Country c where name = :name")
                    .setString("name", countryName)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public static City cityByNameAndCountry(Session session, String cityName, Country country) {
        BM.start("Airways.cityByNameAndCountry");
        try {
            //noinspection JpaQlInspection
            return (City) session
                    .createQuery("from City c where name = :name and country = :country")
                    .setString("name", cityName)
                    .setEntity("country", country)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public static Airport airportByIcao(Session session, String icao) {
        BM.start("Airways.airportByIcao");
        try {
            //noinspection JpaQlInspection
            return (Airport) session
                    .createQuery("from Airport c where icao = :icao")
                    .setString("icao", icao)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }


    public static Airline airlineByIata(Session session, String iata) {
        BM.start("Airways.airlineByIata");
        try {
            //noinspection JpaQlInspection
            return (Airline) session
                    .createQuery("from Airline c where iata = :iata")
                    .setString("iata", iata)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public static AircraftType aircraftTypeByIcao(Session session, String icao) {
        BM.start("Airways.aircraftTypeByIcao");
        try {
            //noinspection JpaQlInspection
            return (AircraftType) session
                    .createQuery("from AircraftType c where icao = :icao")
                    .setString("icao", icao)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public static String makeFlightNumber(String iataCode, int number) {
        return iataCode + String.valueOf(number);
    }

    public static String increaseFlightNumber(String number) {
        String iataCode = number.substring(0, 2);
        String digits = number.substring(2);
        int flightNumber = Integer.parseInt(digits);
        return makeFlightNumber(iataCode, flightNumber + 1);
    }
}
