package net.simforge.airways.stage2.worldbuilder;

import net.simforge.airways.stage2.Airways;
import net.simforge.airways.stage2.model.Airline;
import net.simforge.airways.stage2.model.aircraft.Aircraft;
import net.simforge.airways.stage2.model.aircraft.AircraftType;
import net.simforge.airways.stage2.model.geo.Airport;
import net.simforge.commons.hibernate.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@Deprecated
public class AddAircraftFleet {

    private static final Logger logger = LoggerFactory.getLogger(AddAircraftFleet.class.getName());

    public static void main(String[] args) throws IOException {
        try (SessionFactory sessionFactory = Airways.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

/*            buildAicraftFleet(session, "ZZ", "Russia", "Moskva", 200, 799,  "A320", 30);
            buildMidRangeHub(session, "ZZ", "Russia", "Moskva", 800, 1500, "A320", 20);
            buildMidRangeHub(session, "ZZ", "United kingdom", "London", 100, 1500, "A320", 50);
            buildMidRangeHub(session, "ZZ", "United states", "New york", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "United states", "Los angeles", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Venezuela", "Caracas", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Brazil", "Sao paulo", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "India", "Dilli", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "China", "Shanghai", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Singapore", "Singapore", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Australia", "Sydney", 100, 2000, "A320", 50);*/

            //B744
            addAircraftFleet(session, "WW", "B744", "EGLL", "G-BN??", 5);
            addAircraftFleet(session, "WW", "B744", "YSSY", "VH-B??", 2);
            addAircraftFleet(session, "WW", "B744", "ZSPD", "X-CB??", 2);
        }
    }

    private static void addAircraftFleet(Session session, String airlineIata, String aircraftTypeIcao, String airportIcao, String regNoPattern, int count) {
        Airline airline = Airways.airlineByIata(session, airlineIata);
        AircraftType aircraftType = Airways.aircraftTypeByIcao(session, aircraftTypeIcao);
        Airport airport = Airways.airportByIcao(session, airportIcao);

        for (int number = 0; number < count; number++) {
            String regNo = regNoPattern;
            int remainder = number;
            int index;
            while ((index = regNo.lastIndexOf('?')) != -1) {
                int letterCode = remainder % 26;
                remainder = remainder / 26;

                regNo = regNo.substring(0, index) + (char) ('A' + letterCode) + regNo.substring(index + 1);
            }

            logger.info("Aircraft Reg No: " + regNo);

            //noinspection JpaQlInspection
            Aircraft aircraft = (Aircraft) session
                    .createQuery("from Aircraft a where regNo = :regNo")
                    .setString("regNo", regNo)
                    .setMaxResults(1)
                    .uniqueResult();

            if (aircraft != null) {
                logger.info("Aircraft " + regNo + " exists");
                continue;
            }

            aircraft = new Aircraft();
            aircraft.setType(aircraftType);
            aircraft.setRegNo(regNo);
            aircraft.setAirline(airline);
            aircraft.setPositionAirport(airport);
            aircraft.setStatus(Aircraft.Status.Idle);

            HibernateUtils.saveAndCommit(session, aircraft);
        }
    }
}
