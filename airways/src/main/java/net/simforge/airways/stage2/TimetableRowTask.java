package net.simforge.airways.stage2;

import net.simforge.airways.stage2.model.flight.Flight;
import net.simforge.airways.stage2.model.flight.TimetableRow;
import net.simforge.airways.stage2.model.flight.TransportFlight;
import net.simforge.commons.HeartbeatTask;
import net.simforge.commons.hibernate.HibernateUtils;
import net.simforge.commons.legacy.BM;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Weekdays;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class TimetableRowTask extends HeartbeatTask<TimetableRow> {

    private final SessionFactory sessionFactory;

    @SuppressWarnings("unused")
    public TimetableRowTask() {
        this(AirwaysApp.getSessionFactory());
    }

    private TimetableRowTask(SessionFactory sessionFactory) {
        super("TimetableRow", sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected void startup() {
        super.startup();

        BM.setLoggingPeriod(ChronoUnit.HOURS.getDuration().toMillis());
    }

    @Override
    protected TimetableRow heartbeat(TimetableRow timetableRow) {
        BM.start("TimetableRowTask.heartbeat");
        try (Session session = sessionFactory.openSession()) {
            timetableRow = session.get(TimetableRow.class, timetableRow.getId());

            scheduleFlights(session, timetableRow);

            return timetableRow;
        } finally {
            BM.stop();
        }

    }

    private void scheduleFlights(Session session, TimetableRow timetableRow) {
        BM.start("TimetableRowTask.scheduleFlights");
        try {

            logger.debug("Scheduling flights for " + timetableRow + "...");

            Integer horizon = timetableRow.getHorizon();
            if (horizon == null) {
                horizon = 7; // default horizon
            }

            LocalDate today = JavaTime.todayUtc();
            LocalDate till = today.plusDays(horizon);

            List<TransportFlight> transportFlights;

            BM.start("TimetableRowTask.scheduleFlights#loadFlights");
            try {
                //noinspection JpaQlInspection,unchecked
                transportFlights = session
                        .createQuery("select tf from TransportFlight tf where tf.timetableRow = :timetableRow and tf.dateOfFlight between :today and :till")
                        .setEntity("timetableRow", timetableRow)
                        .setParameter("today", today)
                        .setParameter("till", till)
                        .list();
            } finally {
                BM.stop();
            }

            logger.debug("Loaded " + transportFlights.size() + " flights for horizon " + horizon + " days");

            Map<LocalDate, TransportFlight> flightByDate = new HashMap<>();
            for (TransportFlight flight : transportFlights) {
                flightByDate.put(flight.getDateOfFlight(), flight);
            }

            Weekdays weekdays = Weekdays.valueOf(timetableRow.getWeekdays());

            boolean someFlightFailed = false;
            for (LocalDate curr = today; curr.isBefore(till) || curr.isEqual(till); curr = curr.plusDays(1)) {
                if (!weekdays.isOn(curr.getDayOfWeek())) {
                    logger.debug("Date " + curr + " - skip due to weekdays");
                    continue;
                }

                TransportFlight _transportFlight = flightByDate.get(curr);
                if (_transportFlight != null) {
                    logger.debug("Date " + curr + " - flight exists");
                    continue;
                }

                logger.debug("Date " + curr + " - creating...");

                TransportFlight transportFlight = initTransportFlight(curr, timetableRow);
                Flight flight = initFlight(transportFlight, timetableRow);

                try {
                    String msg = String.format("Flight for %s scheduled", curr);

                    HibernateUtils.transaction(session, "TimetableRowTask.scheduleFlights#createFlight", () -> {
                        session.save(flight);

                        transportFlight.setFlight(flight);
                        session.save(transportFlight);

                        flight.setTransportFlight(transportFlight);
                        session.update(flight);

                        EventLog.saveLog(session, timetableRow, msg, flight, transportFlight);
                        EventLog.saveLog(session, flight, "Scheduled", timetableRow, transportFlight);
                        EventLog.saveLog(session, transportFlight, "Scheduled", timetableRow, flight);
                    });

                    logger.info(String.format("Flight %s %s-%s departing at %s is scheduled",
                            timetableRow.getNumber(),
                            timetableRow.getFromAirport().getIcao(),
                            timetableRow.getToAirport().getIcao(),
                            timetableRow.getDepartureTime()));


                } catch (Exception e) {
                    logger.error("Unable to create a flight, timetableRow " + timetableRow, e);
                    someFlightFailed = true;
                }
            }

            if (!someFlightFailed) {
                // lets process it on next day
                timetableRow.setHeartbeatDt(JavaTime.nowUtc().plusDays(1));
            } else {
                // in case of any failure we are going to retry some minutes later
                timetableRow.setHeartbeatDt(JavaTime.nowUtc().plusMinutes(10));
            }

            HibernateUtils.updateAndCommit(session, timetableRow);

        } finally {
            BM.stop();
        }
    }

    private static TransportFlight initTransportFlight(LocalDate dateOfFlight, TimetableRow timetableRow) {
        TransportFlight transportFlight = new TransportFlight();

        transportFlight.setTimetableRow(timetableRow);
        transportFlight.setDateOfFlight(dateOfFlight);
        transportFlight.setNumber(timetableRow.getNumber());
        transportFlight.setFromAirport(timetableRow.getFromAirport());
        transportFlight.setToAirport(timetableRow.getToAirport());
        transportFlight.setDepartureDt(dateOfFlight.atTime(LocalTime.parse(timetableRow.getDepartureTime())));
        transportFlight.setArrivalDt(transportFlight.getDepartureDt().plus(JavaTime.hhmmToDuration(timetableRow.getDuration())));
        transportFlight.setStatus(100 /*Scheduled*/);
        transportFlight.setTotalTickets(timetableRow.getTotalTickets());
        transportFlight.setFreeTickets(transportFlight.getTotalTickets());
        transportFlight.setHeartbeatDt(JavaTime.nowUtc());

        return transportFlight;
    }

    private static Flight initFlight(TransportFlight transportFlight, TimetableRow timetableRow) {
        Flight flight = new Flight();

        flight.setDateOfFlight(transportFlight.getDateOfFlight());
        flight.setCallsign("TODO"); // todo AK
        flight.setAircraftType(timetableRow.getAircraftType());
        flight.setNumber(transportFlight.getNumber());
        flight.setFromAirport(transportFlight.getFromAirport());
        flight.setToAirport(transportFlight.getToAirport());
        flight.setAlternativeAirport(null); // todo AK from kind of typical flights

        flight.setScheduledDepartureTime(transportFlight.getDepartureDt());
        flight.setScheduledArrivalTime(transportFlight.getArrivalDt());

        FlightTimeline flightTimeline = FlightTimeline.byScheduledDepartureArrivalTime(transportFlight.getDepartureDt(), transportFlight.getArrivalDt());

        flight.setScheduledTakeoffTime(flightTimeline.getTakeoff().getScheduledTime());
        flight.setScheduledLandingTime(flightTimeline.getLanding().getScheduledTime());

        flight.setStatus(Flight.Status.Planned);
        flight.setHeartbeatDt(JavaTime.nowUtc());

        return flight;
    }
}
