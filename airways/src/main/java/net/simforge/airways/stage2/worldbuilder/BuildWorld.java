package net.simforge.airways.stage2.worldbuilder;

import java.io.IOException;
import java.sql.SQLException;

public class BuildWorld {
    public static void main(String[] args) throws IOException, SQLException {
        TageoCom.main(args);
        ImportFSEconomyAirports.main(args);

        ActivateCities.main(args);
        ActivateAirports.main(args);

        UpdateAirport2CityLinks.main(args);

        UpdateCityFlows.main(args);

        AddMiscReferenceData.main(args);
    }
}
