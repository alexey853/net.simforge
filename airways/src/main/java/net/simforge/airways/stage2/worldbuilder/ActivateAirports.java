package net.simforge.airways.stage2.worldbuilder;

import net.simforge.airways.stage1.Util;
import net.simforge.airways.stage2.Airways;
import net.simforge.airways.stage2.model.geo.Airport;
import net.simforge.airways.stage2.model.geo.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class ActivateAirports {
    private static final Logger logger = LoggerFactory.getLogger(ActivateAirports.class.getName());

    public static void main(String[] args) throws IOException {
        logger.info("Activate airports");

        try (SessionFactory sessionFactory = Airways.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            //noinspection JpaQlInspection,unchecked
            List<Airport> airports = session
                    .createQuery("from Airport where dataset != :activeDataset")
                    .setInteger("activeDataset", Airways.ACTIVE_DATASET)
                    .list();

            logger.info("found {} airports to activate", airports.size());

            for (Airport airport : airports) {
                airport.setDataset(Airways.ACTIVE_DATASET);

                Util.update(session, airport);

                logger.info("airport {} activated", airport.getName());
            }
        }
    }
}
