package net.simforge.airways.stage2.worldbuilder;

import net.simforge.airways.stage1.Util;
import net.simforge.airways.stage2.Airways;
import net.simforge.airways.stage2.model.Airline;
import net.simforge.airways.stage2.model.aircraft.AircraftType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AddMiscReferenceData {
    private static final Logger logger = LoggerFactory.getLogger(AddMiscReferenceData.class.getName());

    public static void main(String[] args) throws IOException {
        logger.info("Add misc reference data");

        try (SessionFactory sessionFactory = Airways.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            Util.transaction(session, () -> {
                addAirline(session, "ZZ", "ZZA", "ZZ Airways");
                addAirline(session, "WW", "WWA", "Worldwide Airways");
                addAirline(session, "PH", "PHA", "PhantomAir");

                addAircraftType(session, "A320", "320", 36000, 444, 160, 150, 2000, 1200);
                addAircraftType(session, "B744", "744", 36000, 480, 160, 150, 1800, 1000);
            });
        }
    }

    private static void addAirline(Session session, String iata, String icao, String name) {
        if (Airways.airlineByIata(session, iata) != null) {
            return;
        }

        Airline airline = new Airline();
        airline.setIata(iata);
        airline.setIcao(icao);
        airline.setName(name);

        Util.save(session, airline, "createAirline");
    }

    private static void addAircraftType(Session session,
                                        String icaoCode, String iataCode,
                                        int typicalCruiseAltitude, int typicalCruiseSpeed,
                                        int takeoffSpeed, int landingSpeed,
                                        int climbVerticalSpeed, int descentVerticalSpeed) {
        if (Airways.aircraftTypeByIcao(session, icaoCode) != null) {
            return;
        }

        AircraftType type = new AircraftType();
        type.setIcao(icaoCode);
        type.setIata(iataCode);
        type.setTypicalCruiseAltitude(typicalCruiseAltitude);
        type.setTypicalCruiseSpeed(typicalCruiseSpeed);
        type.setTakeoffSpeed(takeoffSpeed);
        type.setLandingSpeed(landingSpeed);
        type.setClimbVerticalSpeed(climbVerticalSpeed);
        type.setDescentVerticalSpeed(descentVerticalSpeed);

        Util.save(session, type, "createType");
    }
}
