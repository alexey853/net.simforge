package net.simforge.airways.stage2.status;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

public class StatusHandler {
    private Map<Integer, Method> statusHandlers = new HashMap<>();
    private Method otherwiseHandler;
    private Object statusHandlersScope;

    public static StatusHandler create(Object statusHandlers) {
        StatusHandler result = new StatusHandler();

        result.statusHandlersScope = statusHandlers;

        Method[] methods = statusHandlers.getClass().getDeclaredMethods();
        for (Method eachMethod : methods) {
            Status eachStatusAnnotation = eachMethod.getAnnotation(Status.class);
            if (eachStatusAnnotation != null) {
                int eachStatusCode = eachStatusAnnotation.code();
                if (result.statusHandlers.get(eachStatusCode) != null) {
                    throw new RuntimeException("Duplicated status handlers for status " + eachStatusCode);
                }

                result.statusHandlers.put(eachStatusCode, eachMethod);
            }

            Otherwise eachOtherwiseAnnotation = eachMethod.getAnnotation(Otherwise.class);
            if (eachOtherwiseAnnotation != null) {
                result.otherwiseHandler = eachMethod;
            }
        }

        return result;
    }

    private StatusHandler() {
    }

    public void perform(Context ctx) {
        Object subject = ctx.getSubject();

        try {
            Method getStatusMethod = subject.getClass().getDeclaredMethod("getStatus");
            int statusCode = (Integer) getStatusMethod.invoke(subject);
            Method statusHandlerMethod = statusHandlers.get(statusCode);

            if (statusHandlerMethod != null) {
                statusHandlerMethod.setAccessible(true);
                statusHandlerMethod.invoke(statusHandlersScope, ctx);
            } else if (otherwiseHandler != null) {
                otherwiseHandler.setAccessible(true);
                otherwiseHandler.invoke(statusHandlersScope, ctx);
            } else {
                throw new RuntimeException("Could not find handler for status " + statusCode);
            }
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e); // todo
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e); // todo
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e); // todo
        }
    }

    public static <T> Context<T> context(T subject, Object... additionalObjects) {
        return new Context<T>(subject, additionalObjects);
    }

    public static class Context<T> {
        private final T subject;
        private final Object[] additionalObjects;

        private Context(T subject, Object[] additionalObjects) {
            this.subject = subject;
            this.additionalObjects = additionalObjects;
        }

        public T getSubject() {
            return subject;
        }

        public <K> K get(Class clazz) {
            for (Object each : additionalObjects) {
                if (clazz.isAssignableFrom(each.getClass())) {
                    //noinspection unchecked
                    return (K) each;
                }
            }
            return null;
        }
    }
}
