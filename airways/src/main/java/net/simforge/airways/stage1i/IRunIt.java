package net.simforge.airways.stage1i;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

public class IRunIt {
    public static void main(String[] args) throws InterruptedException {
        try (Ignite ignite = Ignition.start("airways/ignite/ignite.xml")) {

            TimetableRowTask timetableRowTask = new TimetableRowTask();
            Thread timetableRowThread = new Thread(timetableRowTask);
            timetableRowThread.start();

//        PilotTask pilotTask = new PilotTask(sessionFactory);
//        Thread pilotThread = new Thread(pilotTask);
//        pilotThread.start();

//        InPlaceAllocatorTask allocatorTask = new InPlaceAllocatorTask(sessionFactory);
//        Thread allocatorThread = new Thread(allocatorTask);
//        allocatorThread.start();

            timetableRowThread.join();
//        pilotThread.join();
//        allocatorThread.join();

        }
    }
}
