package net.simforge.airways.stage1i;

import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.runtime.BaseTask;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.cache.Cache;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;

/**
 * It processes entities by heartbeatDt field.
 * Entities with heartbeatDt = null are out of processing queue.
 */
public abstract class IHeartbeatTask<T> extends BaseTask {
    private final String cacheName;

    protected IHeartbeatTask(String cacheName) {
        this(cacheName, cacheName);
    }

    protected IHeartbeatTask(String cacheName, String loggerName) {
        super(loggerName);
        this.cacheName = cacheName;
    }

    @Override
    protected void process() {
        if (cacheName == null) {
            throw new Error("Cache name should be specified");
        }

        IgniteCache<Object, Object> cache = Ignition.ignite().cache(cacheName);

        String sql = "heartbeatDt < ?";

        SqlQuery<Object, T> query = new SqlQuery<Object, T>(cacheName, sql)
                .setArgs(JavaTime.nowUtc());
        List<Cache.Entry<Object, T>> objects = cache.query(query).getAll();

        if (objects == null || objects.isEmpty()) {
            logger.debug("No objects found");
            return;
        }

        logger.debug("Processing queue: " + objects.size());

        for (Cache.Entry<Object, T> entry : objects) {
            T object = entry.getValue();
            logger.debug("Heartbeat for " + object);

            LocalDateTime before = getHeartbeatDt(object);

            try {
                heartbeat(object);
            } catch (Throwable t) {
                logger.error("Error during Heartbeat for " + object, t);
            }

            //noinspection unchecked
            T objectAfter = (T) cache.get(entry.getKey());
            LocalDateTime after = getHeartbeatDt(objectAfter);

            if (after != null
                    && (after.equals(before)
                    || after.isBefore(after))) {
                logger.warn(String.format("HeartbeatDt for %s is not changed or changed in wrong way: before %s, after %s", object, before, after));
            }
        }
    }

    protected abstract void heartbeat(T object);

    private LocalDateTime getHeartbeatDt(T object) {
        Field heartbeatDtField = getField(object.getClass(), "heartbeatDt");
        return (LocalDateTime) getValue(heartbeatDtField, object);
    }

    private Object getValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could not get value of field " + field.getName());
        }
    }

    private Field getField(Class<?> clazz, String fieldname) {
        Field field;
        try {
            field = clazz.getDeclaredField(fieldname);
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Could not find field " + fieldname, e);
        }
        return field;
    }
}
