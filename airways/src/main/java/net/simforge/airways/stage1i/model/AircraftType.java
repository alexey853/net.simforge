package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.util.List;

public class AircraftType {
    private int id;

    @QuerySqlField(index = true)
    private String icao;

    @QuerySqlField(index = true)
    private String iata;

    private int typicalCruiseAltitude;
    private int typicalCruiseSpeed;

    private int climbVerticalSpeed;
    private int descentVerticalSpeed;

    private int takeoffSpeed;
    private int landingSpeed;

    //******************************************************************************************************************
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public int getTypicalCruiseAltitude() {
        return typicalCruiseAltitude;
    }

    public void setTypicalCruiseAltitude(int typicalCruiseAltitude) {
        this.typicalCruiseAltitude = typicalCruiseAltitude;
    }

    public int getTypicalCruiseSpeed() {
        return typicalCruiseSpeed;
    }

    public void setTypicalCruiseSpeed(int typicalCruiseSpeed) {
        this.typicalCruiseSpeed = typicalCruiseSpeed;
    }

    public int getClimbVerticalSpeed() {
        return climbVerticalSpeed;
    }

    public void setClimbVerticalSpeed(int climbVerticalSpeed) {
        this.climbVerticalSpeed = climbVerticalSpeed;
    }

    public int getDescentVerticalSpeed() {
        return descentVerticalSpeed;
    }

    public void setDescentVerticalSpeed(int descentVerticalSpeed) {
        this.descentVerticalSpeed = descentVerticalSpeed;
    }

    public int getTakeoffSpeed() {
        return takeoffSpeed;
    }

    public void setTakeoffSpeed(int takeoffSpeed) {
        this.takeoffSpeed = takeoffSpeed;
    }

    public int getLandingSpeed() {
        return landingSpeed;
    }

    public void setLandingSpeed(int landingSpeed) {
        this.landingSpeed = landingSpeed;
    }

    @Override
    public String toString() {
        return "AircraftType{" +
                "id=" + id +
                ", icao='" + icao + '\'' +
                '}';
    }

    //******************************************************************************************************************
    public static IgniteCache<Integer, AircraftType> cache() {
        CacheConfiguration<Integer, AircraftType> cfg = new CacheConfiguration<>("AircraftType");
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, AircraftType.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static AircraftType byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence("AircraftTypeID", 0, true);
        return (int) seq.incrementAndGet();
    }

    public static AircraftType byIcao(String icaoCode) {
        String sql = "icao = ?";

        SqlQuery<Integer, AircraftType> query = new SqlQuery<Integer, AircraftType>(AircraftType.class, sql)
                .setArgs(icaoCode);
        List<Cache.Entry<Integer, AircraftType>> types = cache().query(query).getAll();

        if (types.isEmpty()) {
            return null;
        }

        return types.get(0).getValue();
    }

    public static AircraftType byIata(String iata) {
        String sql = "iata = ?";

        SqlQuery<Integer, AircraftType> query = new SqlQuery<Integer, AircraftType>(AircraftType.class, sql)
                .setArgs(iata);
        List<Cache.Entry<Integer, AircraftType>> objects = cache().query(query).getAll();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0).getValue();
    }
}
