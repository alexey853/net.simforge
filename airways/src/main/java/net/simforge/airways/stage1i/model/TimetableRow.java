package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimetableRow {
    private int id;

    @QuerySqlField(index = true)
    private String number;

    @QuerySqlField(index = true)
    private int airlineId;
    private transient Airline airline;

    private int fromAirportId;
    private transient Airport fromAirport;

    private int toAirportId;
    private transient Airport toAirport;

    private int aircraftTypeId;
    private transient AircraftType aircraftType;

    private String weekdays;
    private String departureTime;
    private String duration;
    private int totalTickets;
    private int horizon;

    @QuerySqlField(index = true)
    private int status;
    @QuerySqlField(index = true)
    private LocalDateTime heartbeatDt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(int airlineId) {
        this.airlineId = airlineId;

        airline = null;
    }

    public Airline getAirline() {
        if (airline != null) {
            return airline;
        }

        airline = Airline.byId(airlineId);
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;

        airlineId = airline != null ? airline.getId() : 0;
    }

    public int getFromAirportId() {
        return fromAirportId;
    }

    public void setFromAirportId(int fromAirportId) {
        this.fromAirportId = fromAirportId;

        fromAirport = null;
    }

    public Airport getFromAirport() {
        if (fromAirport != null) {
            return fromAirport;
        }

        fromAirport = Airport.byId(fromAirportId);
        return fromAirport;
    }

    public void setFromAirport(Airport fromAirport) {
        this.fromAirport = fromAirport;

        fromAirportId = fromAirport != null ? fromAirport.getId() : 0;
    }

    public int getToAirportId() {
        return toAirportId;
    }

    public void setToAirportId(int toAirportId) {
        this.toAirportId = toAirportId;

        toAirport = null;
    }

    public Airport getToAirport() {
        if (toAirport != null) {
            return toAirport;
        }

        toAirport = Airport.byId(toAirportId);
        return toAirport;
    }

    public void setToAirport(Airport toAirport) {
        this.toAirport = toAirport;

        toAirportId = toAirport != null ? toAirport.getId() : 0;
    }

    public int getAircraftTypeId() {
        return aircraftTypeId;
    }

    public void setAircraftTypeId(int aircraftTypeId) {
        this.aircraftTypeId = aircraftTypeId;

        aircraftType = null;
    }

    public AircraftType getAircraftType() {
        if (aircraftType != null) {
            return aircraftType;
        }

        aircraftType = AircraftType.byId(aircraftTypeId);
        return aircraftType;
    }

    public void setAircraftType(AircraftType aircraftType) {
        this.aircraftType = aircraftType;

        aircraftTypeId = aircraftType != null ? aircraftType.getId() : 0;
    }

    public String getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(String weekdays) {
        this.weekdays = weekdays;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getTotalTickets() {
        return totalTickets;
    }

    public void setTotalTickets(int totalTickets) {
        this.totalTickets = totalTickets;
    }

    public int getHorizon() {
        return horizon;
    }

    public void setHorizon(int horizon) {
        this.horizon = horizon;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LocalDateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    public void setHeartbeatDt(LocalDateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    @Override
    public String toString() {
        return "TimetableRow{" +
                "id=" + id +
                ", number='" + number + '\'' +
                '}';
    }

    public static class Status {
        public static final int Stopped = 0;
        public static final int Active = 1;
    }

    //******************************************************************************************************************
    public static final String CacheName = "TimetableRow";
    public static final String SequenceName = "TimetableRowID";

    public static IgniteCache<Integer, TimetableRow> cache() {
        CacheConfiguration<Integer, TimetableRow> cfg = new CacheConfiguration<>(CacheName);
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, TimetableRow.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static TimetableRow byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence(SequenceName, 0, true);
        return (int) seq.incrementAndGet();
    }

    public static List<TimetableRow> listByAirline(Airline airline) {
        String sql = "airlineId = ?";

        SqlQuery<Integer, TimetableRow> query = new SqlQuery<Integer, TimetableRow>(TimetableRow.class, sql)
                .setArgs(airline.getId());
        List<Cache.Entry<Integer, TimetableRow>> objects = cache().query(query).getAll();

        List<TimetableRow> result = new ArrayList<>();
        objects.forEach((e) -> { result.add(e.getValue()); });
        return Collections.unmodifiableList(result);
    }
}
