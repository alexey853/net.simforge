package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.cache.query.annotations.QueryTextField;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.util.List;

public class Airport {
    private int id;

    @QuerySqlField(index = true)
    private String iata;

    @QuerySqlField(index = true)
    private String icao;

    @QueryTextField
    private String name;

    private double latitude;
    private double longitude;

    //******************************************************************************************************************
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", icao='" + icao + '\'' +
                '}';
    }

    //******************************************************************************************************************
    public static IgniteCache<Integer, Airport> cache() {
        CacheConfiguration<Integer, Airport> cfg = new CacheConfiguration<>("Airport");
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, Airport.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static Airport byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence("AirportID", 0, true);
        return (int) seq.incrementAndGet();
    }

    public static Airport byIcao(String icao) {
        String sql = "icao = ?";

        SqlQuery<Integer, Airport> query = new SqlQuery<Integer, Airport>(Airport.class, sql)
                .setArgs(icao);
        List<Cache.Entry<Integer, Airport>> objects = cache().query(query).getAll();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0).getValue();
    }

    public static Airport byIata(String iata) {
        String sql = "iata = ?";

        SqlQuery<Integer, Airport> query = new SqlQuery<Integer, Airport>(Airport.class, sql)
                .setArgs(iata);
        List<Cache.Entry<Integer, Airport>> objects = cache().query(query).getAll();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0).getValue();
    }

}
