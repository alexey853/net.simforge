package net.simforge.airways.stage1i;

import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;

public class IgniteNode {
    public static void main(String[] args) throws IgniteException {
        Ignition.start("airways/ignite/ignite.xml");
    }
}
