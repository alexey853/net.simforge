package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.cache.query.annotations.QueryTextField;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.util.List;

public class Airline {
    private int id;

    @QuerySqlField(index = true)
    private String iata;

    @QuerySqlField(index = true)
    private String icao;

    @QueryTextField
    private String name;

    //******************************************************************************************************************
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "id=" + id +
                ", iata='" + iata + '\'' +
                '}';
    }

    //******************************************************************************************************************
    public static IgniteCache<Integer, Airline> cache() {
        CacheConfiguration<Integer, Airline> cfg = new CacheConfiguration<>("Airline");
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, Airline.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static Airline byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence("AirlineID", 0, true);
        return (int) seq.incrementAndGet();
    }

    public static Airline byIata(String iata) {
        String sql = "iata = ?";

        SqlQuery<Integer, Airline> query = new SqlQuery<Integer, Airline>(Airline.class, sql)
                .setArgs(iata);
        List<Cache.Entry<Integer, Airline>> objects = cache().query(query).getAll();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0).getValue();
    }

    public static Airline byIcao(String icao) {
        String sql = "icao = ?";

        SqlQuery<Integer, Airline> query = new SqlQuery<Integer, Airline>(Airline.class, sql)
                .setArgs(icao);
        List<Cache.Entry<Integer, Airline>> objects = cache().query(query).getAll();

        if (objects.isEmpty()) {
            return null;
        }

        return objects.get(0).getValue();
    }

}
