package net.simforge.airways.stage1i;

import net.simforge.airways.stage1.AirwaysTools;
import net.simforge.airways.stage1.FlightTimeline;
import net.simforge.airways.stage1.SimpleFlight;
import net.simforge.airways.stage1i.model.*;
import net.simforge.commons.misc.Geo;
import net.simforge.commons.misc.JavaTime;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;

import static net.simforge.commons.misc.JavaTime.nowUtc;

public class IMiniWorldCreator {
    private static Logger logger = LoggerFactory.getLogger("MiniWorldCreator");

    public static void main(String[] args) {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start("airways/ignite/ignite.xml")) {

            addAircraftType("A320", "320", 36000, 444, 160, 150, 2000, 1200);

            addAirline("ZZ", "ZZA", "ZZ Airways");
            addAirline("WW", "WWA", "Worldwide Airways");

            // todo AK rework it using GC
            addAirport("EGLL", "LHR", "Heathrow", 51.4775, -0.461388);
            addAirport("LFPG", "CDG", "Charles de Gaulle (Roissy)", 49.009722, 2.547778);
            addAirport("EDDF", "FRA", "Rhein-Main", 50.033306, 8.570456);
            addAirport("EDDB", "SXF", "Berlin Sch�nefeld Airport (Berlin Brandenburg International)", 52.378333, 13.520556);
            addAirport("LEMD", "MAD", "Adolfo Su�rez Madrid-Barajas International Airport", 40.493556, -3.566763);
            addAirport("LIRF", "FCO", "Leonardo da Vinci International (Fiumicino)", 41.804475, 12.250797);
            addAirport("LMML", "MLA", "Malta International (Luqa Airport)", 35.857497, 14.4775);
            addAirport("LKPR", "PRG", "V�clav Havel Airport Prague (Prague Ruzyne International)", 50.100833, 14.26);
            addAirport("LOWW", "VIE", "Vienna International (Schwechat)", 48.110278, 16.569722);
            addAirport("LBSF", "SOF", "Sofia International(Vrajdebna, Vrazhdebna)", 42.695194, 23.406167);
            addAirport("LGAV", "ATH", "Eleftherios Venizelos International Airport", 37.936358, 23.944467);
            addAirport("EPWA", "WAW", "Fryderyk Chopin International (Okecie Airport)", 52.16575, 20.967122);
            addAirport("EFHK", "HEL", "Vantaa", 60.317222, 24.963333);
            addAirport("ESSA", "ARN", "Arlanda", 59.651944, 17.918611);
            addAirport("EKCH", "CPH", "K�benhavn Airport - Kastrup", 55.617917, 12.655972);
            addAirport("EINN", "SNN", "Shannon International Airport", 52.701978, -8.924816);
            addAirport("EGBB", "BHX", "Birmingham Airport (Birmingham International)", 52.453856, -1.748027);
            addAirport("EGCC", "MAN", "Ringway International Airport", 53.353889, -2.274999);
            addAirport("UUDD", "DME", "Domodedovo International Airport", 55.408611, 37.906389);

            addAircraft("G-AAAA", "A320", "WW", "EGLL");
            addAircraft("G-AAAB", "A320", "WW", "EGLL");
            addAircraft("G-AAAC", "A320", "WW", "EGLL");
            addAircraft("G-AAAD", "A320", "WW", "EGLL");
            addAircraft("G-AAAE", "A320", "WW", "EGLL");
            addAircraft("G-AAAF", "A320", "WW", "EGLL");
            addAircraft("G-AAAG", "A320", "WW", "EGLL");
            addAircraft("G-AAAH", "A320", "WW", "EGLL");
            addAircraft("G-AAAI", "A320", "WW", "EGLL");
            addAircraft("G-AAAJ", "A320", "WW", "EGLL");

            addPilot("John", "Smith", "EGLL");
            addPilot("Mark", "Twain", "EGLL");
            addPilot("Paul", "Caney", "EGLL");
            addPilot("Dmitry", "Ivanov", "EGLL");
            addPilot("Li Si", "Tzin", "EGLL");
            addPilot("Andrew", "James", "EGLL");
            addPilot("Brandon", "Lee", "EGLL");

            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "04:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "06:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "08:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "10:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "12:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "14:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "16:00", "A320");
            addRoundtripTimetableRow("WW", "EGLL", "EGCC", "20:00", "A320");

        }
    }

    private static void addAircraftType(String icao, String iata,
                                        int typicalCruiseAltitude, int typicalCruiseSpeed,
                                        int takeoffSpeed, int landingSpeed,
                                        int climbVerticalSpeed, int descentVerticalSpeed) {
        AircraftType type = AircraftType.byIcao(icao);
        if (type != null) {
            logger.warn(String.format("Aircraft with ICAO code %s already exists", icao));
            return;
        }

        type = AircraftType.byIata(iata);
        if (type != null) {
            logger.warn(String.format("Aircraft with IATA code %s already exists", iata));
            return;
        }

        type = new AircraftType();
        type.setId(AircraftType.nextId());
        type.setIcao(icao);
        type.setIata(iata);
        type.setTypicalCruiseAltitude(typicalCruiseAltitude);
        type.setTypicalCruiseSpeed(typicalCruiseSpeed);
        type.setTakeoffSpeed(takeoffSpeed);
        type.setLandingSpeed(landingSpeed);
        type.setClimbVerticalSpeed(climbVerticalSpeed);
        type.setDescentVerticalSpeed(descentVerticalSpeed);

        AircraftType.cache().put(type.getId(), type);

        logger.info("Aircraft type added: " + type);
    }

    private static void addAirline(String iata, String icao, String name) {
        Airline airline = Airline.byIcao(icao);
        if (airline != null) {
            logger.warn(String.format("Airline with ICAO code %s already exists", icao));
            return;
        }

        airline = Airline.byIata(iata);
        if (airline != null) {
            logger.warn(String.format("Airline with IATA code %s already exists", iata));
            return;
        }

        airline = new Airline();
        airline.setId(Airline.nextId());
        airline.setIata(iata);
        airline.setIcao(icao);
        airline.setName(name);

        Airline.cache().put(airline.getId(), airline);

        logger.info("Airline added: " + airline);
    }

    private static void addAirport(String icao, String iata, String name, double lat, double lon) {
        Airport airport = Airport.byIcao(icao);
        if (airport != null) {
            logger.warn(String.format("Airport with ICAO code %s already exists", icao));
            return;
        }

        airport = Airport.byIata(iata);
        if (airport != null) {
            logger.warn(String.format("Airport with IATA code %s already exists", iata));
            return;
        }

        airport = new Airport();
        airport.setId(Airport.nextId());
        airport.setIcao(icao);
        airport.setIata(iata);
        airport.setName(name);
        airport.setLatitude(lat);
        airport.setLongitude(lon);

        Airport.cache().put(airport.getId(), airport);

        logger.info("Airport added: " + airport);
    }


    private static void addAircraft(String regNo, String typeIcao, String airlineIata, String airportIcao) {
        Aircraft aircraft = Aircraft.byRegNo(regNo);
        if (aircraft != null) {
            logger.warn(String.format("Aircraft with Reg Number %s already exists", regNo));
            return;
        }

        AircraftType type = AircraftType.byIcao(typeIcao);
        if (type == null) {
            throw new IllegalArgumentException(String.format("Could not find aircraft type %s", typeIcao));
        }

        Airline airline = Airline.byIata(airlineIata);
        if (airline == null) {
            throw new IllegalArgumentException(String.format("Could not find airline %s", airlineIata));
        }

        Airport airport = Airport.byIcao(airportIcao);
        if (airport == null) {
            throw new IllegalArgumentException(String.format("Could not find airport %s", airportIcao));
        }

        aircraft = new Aircraft();
        aircraft.setId(Aircraft.nextId());
        aircraft.setType(type);
        aircraft.setRegNo(regNo);
        aircraft.setAirline(airline);
        aircraft.setPositionAirport(airport);
        aircraft.setStatus(Aircraft.Status.Idle);

        Aircraft.cache().put(aircraft.getId(), aircraft);

        logger.info("Aircraft added: " + aircraft);
    }

    private static void addPilot(String name, String surname, String airportIcao) {
        Airport airport = Airport.byIcao(airportIcao);
        if (airport == null) {
            throw new IllegalArgumentException(String.format("Could not find airport %s", airportIcao));
        }

        Pilot pilot = new Pilot();
        pilot.setId(Pilot.nextId());
        pilot.setName(name);
        pilot.setSurname(surname);
        pilot.setPositionAirport(airport);
        pilot.setStatus(Pilot.Status.Idle);
        pilot.setHeartbeatDt(nowUtc());

        Pilot.cache().put(pilot.getId(), pilot);

        logger.info("Pilot added: " + pilot);
    }

    private static void addRoundtripTimetableRow(String airlineIata, String fromIcao, String toIcao, String departureTime, String aircraftTypeIcao) {
        Airline airline = Airline.byIata(airlineIata);
        if (airline == null) {
            throw new IllegalArgumentException(String.format("Could not find airline %s", airlineIata));
        }

        List<TimetableRow> timetableRows = TimetableRow.listByAirline(airline);
        TimetableRow latestAirlineTimetableRow = timetableRows
                .stream()
                .max(Comparator.comparing(TimetableRow::getNumber))
                .orElse(null);

        String number = latestAirlineTimetableRow != null
                ? AirwaysTools.increaseFlightNumber(latestAirlineTimetableRow.getNumber())
                : AirwaysTools.makeFlightNumber(airlineIata, 100);

        Airport fromAirport = Airport.byIcao(fromIcao);
        if (fromAirport == null) {
            throw new IllegalArgumentException(String.format("Could not find airport %s", fromIcao));
        }

        Airport toAirport = Airport.byIcao(toIcao);
        if (toAirport == null) {
            throw new IllegalArgumentException(String.format("Could not find airport %s", toIcao));
        }

        AircraftType aircraftType = AircraftType.byIcao(aircraftTypeIcao);
        if (aircraftType == null) {
            throw new IllegalArgumentException(String.format("Could not find aircraft type %s", aircraftTypeIcao));
        }


        SimpleFlight simpleFlight = SimpleFlight.forRoute(
                new Geo.Coords(fromAirport.getLatitude(), fromAirport.getLongitude()),
                new Geo.Coords(toAirport.getLatitude(), toAirport.getLongitude()),
                convertAircraftType(aircraftType));


        Duration flyingTime = simpleFlight.getTotalTime();
        FlightTimeline timeline = FlightTimeline.byFlyingTime(flyingTime);
        Duration flightDuration = timeline.getScheduledDuration(timeline.getBlocksOff(), timeline.getBlocksOn());


        TimetableRow flight1row = new TimetableRow();
        flight1row.setId(TimetableRow.nextId());
        flight1row.setAirline(airline);
        flight1row.setNumber(number);
        flight1row.setFromAirport(fromAirport);
        flight1row.setToAirport(toAirport);
        flight1row.setAircraftType(aircraftType);
        flight1row.setWeekdays("1234567");
        flight1row.setDepartureTime(departureTime);
        flight1row.setDuration(JavaTime.toHhmm(flightDuration));
        flight1row.setStatus(TimetableRow.Status.Active);
        flight1row.setHeartbeatDt(JavaTime.nowUtc());
        flight1row.setTotalTickets(160);
        flight1row.setHorizon(30);

        TimetableRow.cache().put(flight1row.getId(), flight1row);

        logger.info("Timetable row added: " + flight1row);


        LocalTime flight2departureTime = LocalTime.parse(departureTime).plus(flightDuration).plusHours(1);
        int step = 5;
        int remainder = flight2departureTime.getMinute() % step;
        flight2departureTime = flight2departureTime.plusMinutes(remainder != 0 ? (step - remainder) : 0);

        TimetableRow flight2row = new TimetableRow();
        flight2row.setId(TimetableRow.nextId());
        flight2row.setAirline(airline);
        flight2row.setNumber(AirwaysTools.increaseFlightNumber(number));
        flight2row.setFromAirport(toAirport);
        flight2row.setToAirport(fromAirport);
        flight2row.setAircraftType(aircraftType);
        flight2row.setWeekdays("1234567");
        flight2row.setDepartureTime(JavaTime.toHhmm(flight2departureTime));
        flight2row.setDuration(JavaTime.toHhmm(flightDuration));
        flight2row.setStatus(net.simforge.airways.stage1.model.TimetableRow.Status.Active);
        flight2row.setHeartbeatDt(JavaTime.nowUtc());
        flight2row.setTotalTickets(160);
        flight2row.setHorizon(30);

        TimetableRow.cache().put(flight2row.getId(), flight2row);

        logger.info("Timetable row added: " + flight2row);
    }

    private static net.simforge.airways.stage1.model.aircraft.AircraftType convertAircraftType(AircraftType aircraftType) {
        net.simforge.airways.stage1.model.aircraft.AircraftType result = new net.simforge.airways.stage1.model.aircraft.AircraftType();
        result.setTypicalCruiseSpeed(aircraftType.getTypicalCruiseSpeed());
        result.setTypicalCruiseAltitude(aircraftType.getTypicalCruiseAltitude());
        result.setClimbVerticalSpeed(aircraftType.getClimbVerticalSpeed());
        result.setDescentVerticalSpeed(aircraftType.getDescentVerticalSpeed());
        result.setTakeoffSpeed(aircraftType.getTakeoffSpeed());
        result.setLandingSpeed(aircraftType.getLandingSpeed());
        return result;
    }
}
