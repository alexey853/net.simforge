package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.Cache;
import java.util.List;

public class Aircraft {
    private int id;

    @QuerySqlField(index = true)
    private int typeId;
    private transient AircraftType type;

    @QuerySqlField(index = true)
    private String regNo;

    @QuerySqlField(index = true)
    private int airlineId;
    private transient Airline airline;

    @QuerySqlField(index = true)
    private int status;

    private double positionLatitude;
    private double positionLongitude;

    @QuerySqlField(index = true)
    private int positionAirportId;
    private transient Airport positionAirport;

    //******************************************************************************************************************
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;

        type = null;
    }

    public AircraftType getType() {
        if (type != null) {
            return type;
        }

        type = AircraftType.byId(typeId);
        return type;
    }

    public void setType(AircraftType type) {
        this.type = type;

        typeId = type != null ? type.getId() : 0;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public int getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(int airlineId) {
        this.airlineId = airlineId;

        airline = null;
    }

    public Airline getAirline() {
        if (airline != null) {
            return airline;
        }

        airline = Airline.byId(airlineId);
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;

        airlineId = airline != null ? airline.getId() : 0;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getPositionLatitude() {
        return positionLatitude;
    }

    public void setPositionLatitude(double positionLatitude) {
        this.positionLatitude = positionLatitude;
    }

    public double getPositionLongitude() {
        return positionLongitude;
    }

    public void setPositionLongitude(double positionLongitude) {
        this.positionLongitude = positionLongitude;
    }

    public int getPositionAirportId() {
        return positionAirportId;
    }

    public void setPositionAirportId(int positionAirportId) {
        this.positionAirportId = positionAirportId;

        positionAirport = null;
    }

    public Airport getPositionAirport() {
        if (positionAirport != null) {
            return positionAirport;
        }

        positionAirport = Airport.byId(positionAirportId);
        return positionAirport;
    }

    public void setPositionAirport(Airport positionAirport) {
        this.positionAirport = positionAirport;

        positionAirportId = positionAirport != null ? positionAirport.getId() : 0;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "id=" + id +
                ", regNo='" + regNo + '\'' +
                '}';
    }

    public static class Status {
        public final static int Idle = 100;
        public final static int PreFlight = 200;
        public final static int TaxiingOut = 300;
        public final static int Flying = 400;
        public final static int TaxiingIn = 500;
        public final static int PostFlight = 600;
    }

    //******************************************************************************************************************
    public static IgniteCache<Integer, Aircraft> cache() {
        CacheConfiguration<Integer, Aircraft> cfg = new CacheConfiguration<>("Aircraft");
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, Aircraft.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static Aircraft byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence("AircraftID", 0, true);
        return (int) seq.incrementAndGet();
    }

    public static Aircraft byRegNo(String regNo) {
        String sql = "regNo = ?";

        SqlQuery<Integer, Aircraft> query = new SqlQuery<Integer, Aircraft>(Aircraft.class, sql)
                .setArgs(regNo);
        List<Cache.Entry<Integer, Aircraft>> aircrafts = cache().query(query).getAll();

        if (aircrafts.isEmpty()) {
            return null;
        }

        return aircrafts.get(0).getValue();
    }
}
