package net.simforge.airways.stage1i.model;

import org.apache.ignite.IgniteAtomicSequence;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.configuration.CacheConfiguration;

import java.time.LocalDateTime;

public class Pilot {
    private int id;

    private String name;
    private String surname;

    private Double positionLatitude;
    private Double positionLongitude;

    @QuerySqlField(index = true)
    private int positionAirportId;
    private transient Airport positionAirport;

    @QuerySqlField(index = true)
    private int status;

    @QuerySqlField(index = true)
    private LocalDateTime heartbeatDt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Double getPositionLatitude() {
        return positionLatitude;
    }

    public void setPositionLatitude(Double positionLatitude) {
        this.positionLatitude = positionLatitude;
    }

    public Double getPositionLongitude() {
        return positionLongitude;
    }

    public void setPositionLongitude(Double positionLongitude) {
        this.positionLongitude = positionLongitude;
    }

    public int getPositionAirportId() {
        return positionAirportId;
    }

    public void setPositionAirportId(int positionAirportId) {
        this.positionAirportId = positionAirportId;
    }

    public Airport getPositionAirport() {
        return positionAirport;
    }

    public void setPositionAirport(Airport positionAirport) {
        this.positionAirport = positionAirport;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LocalDateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    public void setHeartbeatDt(LocalDateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public static class Status {
        public static final int Idle = 100;
        public static final int OnDuty = 200;
    }

    //******************************************************************************************************************
    public static IgniteCache<Integer, Pilot> cache() {
        CacheConfiguration<Integer, Pilot> cfg = new CacheConfiguration<>("Pilot");
        cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cfg.setIndexedTypes(Integer.class, Pilot.class);
        return Ignition.ignite().getOrCreateCache(cfg);
    }

    public static Pilot byId(int typeId) {
        return cache().get(typeId);
    }

    public static int nextId() {
        IgniteAtomicSequence seq = Ignition.ignite().atomicSequence("PilotID", 0, true);
        return (int) seq.incrementAndGet();
    }
}
