package net.simforge.airways.stage1i.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Flight {
    private int id;

    private LocalDate dateOfFlight;
    private String callsign;

    private AircraftType aircraftType;

    private TransportFlight transportFlight;
    private String number;

    private Airport fromAirport;

    private Airport toAirport;

    private Airport alternativeAirport;

    private LocalDateTime scheduledDepartureTime;
    private LocalDateTime actualDepartureTime;
    private LocalDateTime scheduledTakeoffTime;
    private LocalDateTime actualTakeoffTime;
    private LocalDateTime scheduledLandingTime;
    private LocalDateTime actualLandingTime;
    private LocalDateTime scheduledArrivalTime;
    private LocalDateTime actualArrivalTime;

    private int status;
    private LocalDateTime statusDt;
    private LocalDateTime heartbeatDt;

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", scheduledDepartureTime=" + scheduledDepartureTime +
                '}';
    }

    public static class Status {
        public final static int Planned = 100;
        public final static int Assigned = 200;
        public final static int PreFlight = 300;
        public final static int Departure = 400;
        public final static int Flying = 500;
        public final static int Arrival = 600;
        public final static int PostFlight = 700;
        public final static int Finished = 1000;
        public final static int Cancelled = 9999;
    }
}
