package net.simforge.airways.stage1i.model;

import net.simforge.airways.stage1i.model.Airport;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TransportFlight {
    private int id;

    private TimetableRow timetableRow;

    private Flight flight;

    private LocalDate dateOfFlight;
    private String number;

    private Airport fromAirport;

    private Airport toAirport;

    private LocalDateTime departureDt;

    private LocalDateTime arrivalDt;
    private int status;
    private LocalDateTime heartbeatDt;
    private Integer totalTickets;
    private Integer freeTickets;


    @Override
    public String toString() {
        return "TransportFlight{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", dateOfFlight=" + dateOfFlight +
                '}';
    }
}
