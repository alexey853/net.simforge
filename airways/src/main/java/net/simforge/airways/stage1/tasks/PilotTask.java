package net.simforge.airways.stage1.tasks;

import net.simforge.airways.stage1.FlightContext;
import net.simforge.airways.stage1.FlightTimeline;
import net.simforge.airways.stage1.SimpleFlight;
import net.simforge.airways.stage1.Util;
import net.simforge.airways.stage1.model.AircraftAssignment;
import net.simforge.airways.stage1.model.Flight;
import net.simforge.airways.stage1.model.Pilot;
import net.simforge.airways.stage1.model.PilotAssignment;
import net.simforge.airways.stage1.model.aircraft.Aircraft;
import net.simforge.airways.stage1.model.aircraft.AircraftType;
import net.simforge.airways.stage1.model.geo.Airport;
import net.simforge.commons.misc.Geo;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.HeartbeatTask;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public class PilotTask extends HeartbeatTask<Pilot> {

    private final SessionFactory sessionFactory;
    private Session session;

    public PilotTask(SessionFactory sessionFactory) {
        super("Pilot", sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected Pilot heartbeat(Pilot pilot) {
        try (Session session = sessionFactory.openSession()) {
            this.session = session;

            pilot = session.get(Pilot.class, pilot.getId());

            logger.debug(String.format("Pilot %s heartbeat - status %s", pilot, pilot.getStatus()));

            switch (pilot.getStatus()) {
                case Pilot.Status.Idle:
                    idle(pilot);
                    break;
                case Pilot.Status.OnDuty:
                    onDuty(pilot);
                    break;
                default:
                    throw new IllegalStateException("Unsupported pilot status " + pilot.getStatus());
            }

            return pilot;
        } finally {
            this.session = null;
        }
    }

    private void idle(Pilot pilot) {
        List<PilotAssignment> pilotAssignments = loadPilotAssignments(pilot);

        if (pilotAssignments.isEmpty()) {
            logger.debug(String.format("Pilot %s does not have any assignment", pilot));
            planHeartbeat(pilot);
            return;
        }

        logger.debug(String.format("Pilot %s has %s assignment(s)", pilot, pilotAssignments.size()));

        PilotAssignment pilotAssignment = pilotAssignments.get(0);
        Flight flight = pilotAssignment.getFlight();
        FlightContext ctx = FlightContext.load(session, flight);

        if (flight.getStatus() == Flight.Status.Assigned) {

            FlightTimeline timeline = FlightTimeline.byFlight(flight);
            LocalDateTime preflightStartsAtDt = timeline.getStart().getScheduledTime();

            if (preflightStartsAtDt.isBefore(JavaTime.nowUtc())) {
                startFlight(ctx);
            } else {
                planHeartbeat(pilot);
            }
        }
    }

    private void onDuty(Pilot pilot) {
        //noinspection JpaQlInspection,unchecked
        PilotAssignment pilotAssignment = loadInProgressPilotAssignment(pilot);

        logger.debug(String.format("Pilot %s is on %s assignment", pilot, pilotAssignment));

        Flight flight = pilotAssignment.getFlight();
        FlightContext ctx = FlightContext.load(session, flight);

        FlightTimeline timeline = FlightTimeline.byFlight(flight);
        LocalDateTime now = JavaTime.nowUtc();

        if (flight.getStatus() == Flight.Status.Planned) {

            // error
            throw new IllegalArgumentException(); // todo AK

        } else if (flight.getStatus() == Flight.Status.Assigned) {

            // error
            throw new IllegalArgumentException(); // todo AK

        } else if (flight.getStatus() == Flight.Status.PreFlight) {

            if (timeline.getBlocksOff().getEstimatedTime().isBefore(now)) {
                blocksOff(ctx);
            } else {
                planHeartbeat(pilot);
            }

        } else if (flight.getStatus() == Flight.Status.Departure) {

            if (timeline.getTakeoff().getEstimatedTime().isBefore(now)) {
                takeoff(ctx);
            } else {
                planHeartbeat(pilot);
            }

        } else if (flight.getStatus() == Flight.Status.Flying) {

            fly(ctx);

        } else if (flight.getStatus() == Flight.Status.Arrival) {

            if (timeline.getBlocksOn().getEstimatedTime().isBefore(now)) {
                blocksOn(ctx);
            } else {
                planHeartbeat(pilot);
            }

        } else if (flight.getStatus() == Flight.Status.PostFlight) {

            if (timeline.getFinish().getEstimatedTime().isBefore(now)) {
                finishFlight(ctx);
            } else {
                planHeartbeat(pilot);
            }

        } else if (flight.getStatus() == Flight.Status.Finished) {

            // todo AK search for next assignment or go back to Idle status
            // error
            throw new IllegalArgumentException(); // todo AK

        }
    }

    private void startFlight(FlightContext ctx) {
        ctx.getFlight().setStatus(Flight.Status.PreFlight);

        ctx.getPilot().setStatus(Pilot.Status.OnDuty);
        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        ctx.getAircraft().setStatus(Aircraft.Status.PreFlight);

        ctx.getPilotAssignment().setStatus(PilotAssignment.Status.InProgress);
        ctx.getAircraftAssignment().setStatus(AircraftAssignment.Status.InProgress);

        Util.update(session, ctx.getPilot(), ctx.getPilotAssignment(), ctx.getFlight(), ctx.getAircraft(), ctx.getAircraftAssignment());

        logger.info(String.format("Pilot %s has started flight %s", ctx.getPilot(), ctx.getFlight()));
    }

    private void blocksOff(FlightContext ctx) {
        ctx.getFlight().setStatus(Flight.Status.Departure);
        ctx.getFlight().setActualDepartureTime(JavaTime.nowUtc());

        ctx.getAircraft().setStatus(Aircraft.Status.TaxiingOut);

        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        Util.update(session, ctx.getFlight(), ctx.getAircraft(), ctx.getPilot());

        logger.info(String.format("Pilot %s has started taxiing out", ctx.getPilot()));
    }

    private void takeoff(FlightContext ctx) {
        ctx.getFlight().setStatus(Flight.Status.Flying);
        ctx.getFlight().setActualTakeoffTime(JavaTime.nowUtc());

        ctx.getAircraft().setStatus(Aircraft.Status.Flying);

        Airport positionAirport = ctx.getAircraft().getPositionAirport();
        ctx.getAircraft().setPositionAirport(null);
        ctx.getAircraft().setPositionLatitude(positionAirport.getLatitude());
        ctx.getAircraft().setPositionLongitude(positionAirport.getLongitude());

        ctx.getPilot().setPositionAirport(null);
        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        Util.update(session, ctx.getFlight(), ctx.getAircraft(), ctx.getPilot());

        logger.info(String.format("Pilot %s has took aircraft %s off at %s", ctx.getPilot(), ctx.getAircraft(), ctx.getFlight().getFromAirport()));
    }

    private void fly(FlightContext ctx) {
        Flight flight = ctx.getFlight();

        Airport fromAirport = flight.getFromAirport();
        Airport toAirport = flight.getToAirport();

        Geo.Coords fromCoords = new Geo.Coords(fromAirport.getLatitude(), fromAirport.getLongitude());
        Geo.Coords toCoords = new Geo.Coords(toAirport.getLatitude(), toAirport.getLongitude());

        AircraftType aircraftType = ctx.getAircraft().getType();
        SimpleFlight simpleFlight = SimpleFlight.forRoute(
                fromCoords,
                toCoords,
                aircraftType);

        Duration actualTimeSinceTakeoff = Duration.between(flight.getActualTakeoffTime(), JavaTime.nowUtc());

        SimpleFlight.Position aircraftPosition = simpleFlight.getAircraftPosition(actualTimeSinceTakeoff);

        if (aircraftPosition.getStage() != SimpleFlight.Position.Stage.AfterLanding) {
            Geo.Coords coords = aircraftPosition.getCoords();

            ctx.getAircraft().setPositionLatitude(coords.getLat());
            ctx.getAircraft().setPositionLongitude(coords.getLon());

            ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

            Util.update(session, ctx.getAircraft(), ctx.getPilot());
        } else {
            landing(ctx);
        }
    }

    private void landing(FlightContext ctx) {
        ctx.getAircraft().setPositionLatitude(null);
        ctx.getAircraft().setPositionLongitude(null);
        ctx.getAircraft().setPositionAirport(ctx.getFlight().getToAirport());
        ctx.getAircraft().setStatus(Aircraft.Status.TaxiingOut);

        ctx.getFlight().setActualLandingTime(JavaTime.nowUtc());
        ctx.getFlight().setStatus(Flight.Status.Arrival);

        ctx.getPilot().setPositionAirport(ctx.getFlight().getToAirport());
        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        Util.update(session, ctx.getFlight(), ctx.getAircraft(), ctx.getPilot());

        logger.info(String.format("Pilot %s has landed aircraft %s off at %s", ctx.getPilot(), ctx.getAircraft(), ctx.getFlight().getFromAirport()));
    }

    private void blocksOn(FlightContext ctx) {
        ctx.getFlight().setStatus(Flight.Status.PostFlight);
        ctx.getFlight().setActualArrivalTime(JavaTime.nowUtc());

        ctx.getAircraft().setStatus(Aircraft.Status.PostFlight);

        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        Util.update(session, ctx.getFlight(), ctx.getAircraft(), ctx.getPilot());
    }

    private void finishFlight(FlightContext ctx) {
        ctx.getFlight().setStatus(Flight.Status.Finished);

        ctx.getAircraft().setStatus(Aircraft.Status.Idle);

        ctx.getPilotAssignment().setStatus(PilotAssignment.Status.Done);
        ctx.getAircraftAssignment().setStatus(AircraftAssignment.Status.Done);

        ctx.getPilot().setStatus(Pilot.Status.Idle);
        ctx.getPilot().setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));

        Util.update(session, ctx.getFlight(), ctx.getAircraft(), ctx.getPilotAssignment(), ctx.getAircraftAssignment(), ctx.getPilot());
    }

    private List<PilotAssignment> loadPilotAssignments(Pilot pilot) {
        //noinspection JpaQlInspection,unchecked
        return session
                .createQuery("select pa " +
                        "from PilotAssignment as pa " +
                        "inner join pa.flight as flight " +
                        "where pa.pilot = :pilot " +
                        "  and pa.status = :status " +
                        "order by flight.scheduledDepartureTime asc")
                .setEntity("pilot", pilot)
                .setInteger("status", PilotAssignment.Status.Assigned)
                .list();
    }

    private PilotAssignment loadInProgressPilotAssignment(Pilot pilot) {
        //noinspection JpaQlInspection
        return (PilotAssignment) session
                .createQuery("select pa " +
                        "from PilotAssignment as pa " +
                        "inner join pa.flight as flight " +
                        "where pa.pilot = :pilot " +
                        "and pa.status = :status")
                .setEntity("pilot", pilot)
                .setInteger("status", PilotAssignment.Status.InProgress)
                .setMaxResults(1)
                .uniqueResult();
    }

    private void planHeartbeat(Pilot pilot) {
        pilot.setHeartbeatDt(JavaTime.nowUtc().plusMinutes(1));
        Util.update(session, pilot);
    }
}
