package net.simforge.airways.stage1.model.aircraft;

import javax.persistence.*;

@Entity
@Table(name = "aircraft_type")
public class AircraftType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_aircraft_type_id")
    @SequenceGenerator(name = "pk_aircraft_type_id", sequenceName = "aircraft_type_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    private String icao;
    private String iata;

    private Integer typicalCruiseAltitude;
    private Integer typicalCruiseSpeed;

    private Integer climbVerticalSpeed;
    private Integer descentVerticalSpeed;

    private Integer takeoffSpeed;
    private Integer landingSpeed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public Integer getTypicalCruiseAltitude() {
        return typicalCruiseAltitude;
    }

    public void setTypicalCruiseAltitude(Integer typicalCruiseAltitude) {
        this.typicalCruiseAltitude = typicalCruiseAltitude;
    }

    public Integer getTypicalCruiseSpeed() {
        return typicalCruiseSpeed;
    }

    public void setTypicalCruiseSpeed(Integer typicalCruiseSpeed) {
        this.typicalCruiseSpeed = typicalCruiseSpeed;
    }

    public Integer getClimbVerticalSpeed() {
        return climbVerticalSpeed;
    }

    public void setClimbVerticalSpeed(Integer climbVerticalSpeed) {
        this.climbVerticalSpeed = climbVerticalSpeed;
    }

    public Integer getDescentVerticalSpeed() {
        return descentVerticalSpeed;
    }

    public void setDescentVerticalSpeed(Integer descentVerticalSpeed) {
        this.descentVerticalSpeed = descentVerticalSpeed;
    }

    public Integer getTakeoffSpeed() {
        return takeoffSpeed;
    }

    public void setTakeoffSpeed(Integer takeoffSpeed) {
        this.takeoffSpeed = takeoffSpeed;
    }

    public Integer getLandingSpeed() {
        return landingSpeed;
    }

    public void setLandingSpeed(Integer landingSpeed) {
        this.landingSpeed = landingSpeed;
    }
}
