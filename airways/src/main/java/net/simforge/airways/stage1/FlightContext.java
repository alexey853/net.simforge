package net.simforge.airways.stage1;

import net.simforge.airways.stage1.model.AircraftAssignment;
import net.simforge.airways.stage1.model.Flight;
import net.simforge.airways.stage1.model.Pilot;
import net.simforge.airways.stage1.model.PilotAssignment;
import net.simforge.airways.stage1.model.aircraft.Aircraft;
import org.hibernate.Session;

public class FlightContext {
    private Flight flight;
    private Pilot pilot;
    private PilotAssignment pilotAssignment;
    private Aircraft aircraft;
    private AircraftAssignment aircraftAssignment;

    public static FlightContext load(Session session, Flight flight) {
        FlightContext ctx = new FlightContext();

        ctx.flight = session.load(flight.getClass(), flight.getId());

        //noinspection JpaQlInspection
        ctx.aircraftAssignment = (AircraftAssignment) session
                .createQuery("select aa " +
                        "from AircraftAssignment as aa " +
                        "where aa.flight = :flight" +
                        "  and aa.status != :cancelled")
                .setEntity("flight", flight)
                .setInteger("cancelled", AircraftAssignment.Status.Cancelled)
                .setMaxResults(1)
                .uniqueResult();
        if (ctx.aircraftAssignment != null) {
            ctx.aircraft = ctx.aircraftAssignment.getAircraft();
        }

        //noinspection JpaQlInspection
        ctx.pilotAssignment = (PilotAssignment) session
                .createQuery("select pa " +
                        "from PilotAssignment as pa " +
                        "where pa.flight = :flight" +
                        "  and pa.status != :cancelled")
                .setEntity("flight", flight)
                .setInteger("cancelled", PilotAssignment.Status.Cancelled)
                .setMaxResults(1)
                .uniqueResult();
        if (ctx.pilotAssignment != null) {
            ctx.pilot = ctx.pilotAssignment.getPilot();
        }

        return ctx;
    }

    public Flight getFlight() {
        return flight;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public PilotAssignment getPilotAssignment() {
        return pilotAssignment;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public AircraftAssignment getAircraftAssignment() {
        return aircraftAssignment;
    }

    public boolean isFullyAssigned() {
        return pilotAssignment != null && aircraftAssignment != null;
    }
}
