package net.simforge.airways.stage1.model.event;

import javax.persistence.*;

public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_event_id")
    @SequenceGenerator(name = "pk_event_id", sequenceName = "event_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    private Integer typeId;
    private Integer objectId;

}
