package net.simforge.airways.stage1.model;

import net.simforge.airways.stage1.model.geo.Airport;
import net.simforge.commons.HeartbeatObject;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "pilot",
        indexes = {@Index(name = "idx_pilot_heartbeat_dt", columnList = "heartbeat_dt")})
public class Pilot implements HeartbeatObject {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_pilot_id")
    @SequenceGenerator(name = "pk_pilot_id", sequenceName = "pilot_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    private String name;
    private String surname;

    @Column(name = "position_latitude")
    private Double positionLatitude;
    @Column(name = "position_longitude")
    private Double positionLongitude;
    @ManyToOne
    @JoinColumn(name = "position_airport_id")
    private Airport positionAirport;

    private Integer status;
    @Column(name = "heartbeat_dt")
    private LocalDateTime heartbeatDt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Double getPositionLatitude() {
        return positionLatitude;
    }

    public void setPositionLatitude(Double positionLatitude) {
        this.positionLatitude = positionLatitude;
    }

    public Double getPositionLongitude() {
        return positionLongitude;
    }

    public void setPositionLongitude(Double positionLongitude) {
        this.positionLongitude = positionLongitude;
    }

    public Airport getPositionAirport() {
        return positionAirport;
    }

    public void setPositionAirport(Airport positionAirport) {
        this.positionAirport = positionAirport;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    public void setHeartbeatDt(LocalDateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }

    public static class Status {
        public static final int Idle = 100;
        public static final int OnDuty = 200;
    }

}
