package net.simforge.airways.stage1.model;

import net.simforge.airways.stage1.model.aircraft.Aircraft;

import javax.persistence.*;

@Entity
@Table(name = "aircraft_assignment")
public class AircraftAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_aircraft_assignment_id")
    @SequenceGenerator(name = "pk_aircraft_assignment_id", sequenceName = "aircraft_assignment_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "flight_id")
    private Flight flight;
    @ManyToOne
    @JoinColumn(name = "aircraft_id")
    private Aircraft aircraft;

    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AircraftAssignment{" +
                "id=" + id +
                '}';
    }

    public static class Status {
        public static final int Assigned = 100;
        public static final int InProgress = 200;
        public static final int Done = 1000;
        public static final int Cancelled = 9999;
    }
}
