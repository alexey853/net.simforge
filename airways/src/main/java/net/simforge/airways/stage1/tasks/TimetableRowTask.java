package net.simforge.airways.stage1.tasks;

import net.simforge.airways.stage1.FlightTimeline;
import net.simforge.airways.stage1.Util;
import net.simforge.airways.stage1.model.Flight;
import net.simforge.airways.stage1.model.TimetableRow;
import net.simforge.airways.stage1.model.TransportFlight;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.misc.Weekdays;
import net.simforge.commons.HeartbeatTask;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimetableRowTask extends HeartbeatTask<TimetableRow> {

    private final SessionFactory sessionFactory;

    public TimetableRowTask(SessionFactory sessionFactory) {
        super("TimetableRow", sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected TimetableRow heartbeat(TimetableRow timetableRow) {
        try (Session session = sessionFactory.openSession()) {
            timetableRow = session.get(TimetableRow.class, timetableRow.getId());

            scheduleFlights(session, timetableRow);

            return timetableRow;
        }
    }

    private void scheduleFlights(Session session, TimetableRow timetableRow) {
        logger.debug("Scheduling flights for " + timetableRow + "...");

        LocalDate today = JavaTime.todayUtc();
        LocalDate till = today.plusDays(timetableRow.getHorizon());

        //noinspection JpaQlInspection,unchecked
        List<TransportFlight> transportFlights = session
                .createQuery("select tf from TransportFlight tf where tf.timetableRow = :timetableRow and tf.dateOfFlight between :today and :till")
                .setEntity("timetableRow", timetableRow)
                .setParameter("today", today)
                .setParameter("till", till)
                .list();

        logger.debug("Loaded " + transportFlights.size() + " flights for horizon " + timetableRow.getHorizon() + " days");

        Map<LocalDate, TransportFlight> flightByDate = new HashMap<>();
        for (TransportFlight flight : transportFlights) {
            flightByDate.put(flight.getDateOfFlight(), flight);
        }

        Weekdays weekdays = Weekdays.valueOf(timetableRow.getWeekdays());

        boolean someFlightFailed = false;
        for (LocalDate curr = today; curr.isBefore(till) || curr.isEqual(till); curr = curr.plusDays(1)) {
            if (!weekdays.isOn(curr.getDayOfWeek())) {
                logger.debug("Date " + curr + " - skip due to weekdays");
                continue;
            }

            TransportFlight transportFlight = flightByDate.get(curr);
            if (transportFlight != null) {
                logger.debug("Date " + curr + " - flight exists");
                continue;
            }

            logger.debug("Date " + curr + " - creating...");
            try {
                session.getTransaction().begin();

                transportFlight = initTransportFlight(curr, timetableRow);
                Flight flight = initFlight(transportFlight, timetableRow);

                session.save(flight);

                transportFlight.setFlight(flight);
                session.save(transportFlight);

                flight.setTransportFlight(transportFlight);
                session.update(flight);

                // todo AK FlightLifecycle.makeLog(connx, flight, "Scheduled");

                session.getTransaction().commit();

                logger.info(String.format("Flight %s %s-%s departing at %s is created",
                        timetableRow.getNumber(),
                        timetableRow.getFromAirport().getIcao(),
                        timetableRow.getToAirport().getIcao(),
                        timetableRow.getDepartureTime()));
            } catch (HibernateException e) {
                Util.rollback(session);
                logger.error("Unable to create a flight, timetableRow " + timetableRow, e);
                someFlightFailed = true;
            }
        }

        if (!someFlightFailed) {
            // lets process it on next day
            timetableRow.setHeartbeatDt(JavaTime.nowUtc().plusDays(1));
        } else {
            // in case of any failure we are going to retry some minutes later
            timetableRow.setHeartbeatDt(JavaTime.nowUtc().plusMinutes(10));
        }

        Util.update(session, timetableRow);
    }

    private static TransportFlight initTransportFlight(LocalDate dateOfFlight, TimetableRow timetableRow) {
        TransportFlight transportFlight = new TransportFlight();

        transportFlight.setTimetableRow(timetableRow);
        transportFlight.setDateOfFlight(dateOfFlight);
        transportFlight.setNumber(timetableRow.getNumber());
        transportFlight.setFromAirport(timetableRow.getFromAirport());
        transportFlight.setToAirport(timetableRow.getToAirport());
        transportFlight.setDepartureDt(dateOfFlight.atTime(LocalTime.parse(timetableRow.getDepartureTime())));
        transportFlight.setArrivalDt(transportFlight.getDepartureDt().plus(JavaTime.hhmmToDuration(timetableRow.getDuration())));
//         todo AK transportFlight.setStatus(Flight.Status.Scheduled);
        transportFlight.setTotalTickets(timetableRow.getTotalTickets());
        transportFlight.setFreeTickets(transportFlight.getTotalTickets());
        transportFlight.setHeartbeatDt(JavaTime.nowUtc());

        return transportFlight;
    }

    private static Flight initFlight(TransportFlight transportFlight, TimetableRow timetableRow) {
        Flight flight = new Flight();

        flight.setDateOfFlight(transportFlight.getDateOfFlight());
        flight.setCallsign("TODO"); // todo AK
        flight.setAircraftType(timetableRow.getAircraftType());
        flight.setNumber(transportFlight.getNumber());
        flight.setFromAirport(transportFlight.getFromAirport());
        flight.setToAirport(transportFlight.getToAirport());
        flight.setAlternativeAirport(null); // todo AK from kind of typical flights

        flight.setScheduledDepartureTime(transportFlight.getDepartureDt());
        flight.setScheduledArrivalTime(transportFlight.getArrivalDt());

        FlightTimeline flightTimeline = FlightTimeline.byScheduledDepartureArrivalTime(transportFlight.getDepartureDt(), transportFlight.getArrivalDt());

        flight.setScheduledTakeoffTime(flightTimeline.getTakeoff().getScheduledTime());
        flight.setScheduledLandingTime(flightTimeline.getLanding().getScheduledTime());

        flight.setStatus(Flight.Status.Planned);
        flight.setHeartbeatDt(JavaTime.nowUtc());

        return flight;
    }
}
