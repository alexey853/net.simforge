package net.simforge.airways.stage1;

public class AirwaysTools {
    public static String makeFlightNumber(String iataCode, int number) {
        return iataCode + String.valueOf(number);
    }

    public static String increaseFlightNumber(String number) {
        String iataCode = number.substring(0, 2);
        String digits = number.substring(2);
        int flightNumber = Integer.parseInt(digits);
        return makeFlightNumber(iataCode, flightNumber + 1);
    }
}
