package net.simforge.airways.stage1.model;

import javax.persistence.*;

@Entity
@Table(name = "airline")
public class Airline {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_airline_id")
    @SequenceGenerator(name = "pk_airline_id", sequenceName = "airline_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    private String iata;
    private String icao;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
