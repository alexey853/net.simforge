package net.simforge.airways.stage1;

import net.simforge.airways.stage1.allocator.InPlaceAllocatorTask;
import net.simforge.airways.stage1.tasks.PilotTask;
import net.simforge.airways.stage1.tasks.TimetableRowTask;
import org.hibernate.SessionFactory;

public class RunIt {
    public static void main(String[] args) throws InterruptedException {
        SessionFactory sessionFactory = Persistence.createSessionFactory(false);

        TimetableRowTask timetableRowTask = new TimetableRowTask(sessionFactory);
        Thread timetableRowThread = new Thread(timetableRowTask);
        timetableRowThread.start();

        PilotTask pilotTask = new PilotTask(sessionFactory);
        Thread pilotThread = new Thread(pilotTask);
        pilotThread.start();

        InPlaceAllocatorTask allocatorTask = new InPlaceAllocatorTask(sessionFactory);
        Thread allocatorThread = new Thread(allocatorTask);
        allocatorThread.start();

        timetableRowThread.join();
        pilotThread.join();
        allocatorThread.join();
    }
}
