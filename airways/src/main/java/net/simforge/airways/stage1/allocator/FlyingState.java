package net.simforge.airways.stage1.allocator;

public class FlyingState implements State {
    @Override
    public boolean isCompatibleWith(State state) {
        return false;
    }

    @Override
    public String toString() {
        return "FlyingState{}";
    }
}
