package net.simforge.airways.stage1;

import net.simforge.commons.hibernate.HibernateUtils;
import net.simforge.commons.legacy.BM;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

@Deprecated
public class Util {
    private static Logger logger = LoggerFactory.getLogger(Util.class);


    public static void save(Session session, Object object) {
        try {
            session.getTransaction().begin();
            session.save(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error(MarkerFactory.getMarker(object.toString()), "Error on save", e);
            rollback(session);
            throw new RuntimeException("Error on save", e);
        }
    }

    public static void update(Session session, Object object) {
        try {
            session.getTransaction().begin();
            session.update(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error(MarkerFactory.getMarker(object.toString()), "Error on update", e);
            rollback(session);
            throw new RuntimeException("Error on update", e);
        }
    }

    public static void update(Session session, Object... objects) {
        try {
            session.getTransaction().begin();
            for (Object object : objects) {
                if (object == null) {
                    continue;
                }

                session.update(object);
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error(MarkerFactory.getMarker(objects[0].toString()), "Error on update", e);
            rollback(session);
            throw new RuntimeException("Error on update", e);
        }
    }

    public static void saveOrUpdate(Session session, Object... objects) {
        BM.start("Util.saveOrUpdate");
        try {
            session.getTransaction().begin();
            for (Object object : objects) {
                if (object == null) {
                    continue;
                }

                session.update(object);
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            logger.error("Error on save-or-update", e);
            rollback(session);
            throw new RuntimeException("Error on save-or-update", e);
        } finally {
            BM.stop();
        }
    }




    @Deprecated
    public static void transaction(Session session, Runnable action) {
        HibernateUtils.transaction(session, action);
    }

    @Deprecated
    public static void rollback(Session session) {
        HibernateUtils.rollback(session);
    }

    public static void save(Session session, Object entity, String tag) {
        BM.start("Util.save/" + tag);
        try {
            session.save(entity);
        } finally {
            BM.stop();
        }
    }


    public static void update(Session session, Object entity, String tag) {
        BM.start("Util.update/" + tag);
        try {
            session.update(entity);
        } finally {
            BM.stop();
        }
    }
}
