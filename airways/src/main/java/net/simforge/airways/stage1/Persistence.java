package net.simforge.airways.stage1;

import net.simforge.airways.stage1.model.*;
import net.simforge.airways.stage1.model.aircraft.Aircraft;
import net.simforge.airways.stage1.model.aircraft.AircraftType;
import net.simforge.airways.stage1.model.geo.Airport;
import net.simforge.commons.legacy.misc.Settings;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Persistence {
    public static synchronized SessionFactory createSessionFactory(boolean create) {
        Configuration configuration = new Configuration();

        String driverClass = Settings.get("airways.db.driver-class");
        String url         = Settings.get("airways.db.url");
        String username    = Settings.get("airways.db.username");
        String password    = Settings.get("airways.db.password");
        String poolSize    = Settings.get("airways.db.pool-size");

        configuration.setProperty("hibernate.connection.driver_class", driverClass);
        configuration.setProperty("hibernate.connection.url", url);
        configuration.setProperty("hibernate.connection.username", username);
        configuration.setProperty("hibernate.connection.password", password);
        configuration.setProperty("hibernate.connection.pool_size", poolSize);

        if (create) {
            configuration.setProperty("hibernate.hbm2ddl.auto", "create");
        }

        configuration.addAnnotatedClass(Airport.class);

        configuration.addAnnotatedClass(Airline.class);

        configuration.addAnnotatedClass(TimetableRow.class);

        configuration.addAnnotatedClass(TransportFlight.class);

        configuration.addAnnotatedClass(Flight.class);
        configuration.addAnnotatedClass(PilotAssignment.class);
        configuration.addAnnotatedClass(AircraftAssignment.class);

        configuration.addAnnotatedClass(AircraftType.class);
        configuration.addAnnotatedClass(Aircraft.class);

        configuration.addAnnotatedClass(Pilot.class);

        return configuration.buildSessionFactory();
    }
}
