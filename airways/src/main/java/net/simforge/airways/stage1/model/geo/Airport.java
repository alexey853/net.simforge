package net.simforge.airways.stage1.model.geo;

import javax.persistence.*;

@Entity
@Table(name = "geo_airport")
public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_airport_id")
    @SequenceGenerator(name = "pk_airport_id", sequenceName = "airport_id_seq", allocationSize = 1)
    private Long id;
    @Version
    private Integer version;

    private String iata;
    private String icao;
    private String name;
    private Double latitude;
    private Double longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", icao='" + icao + '\'' +
                '}';
    }
}
