package net.simforge.airways.stage1.allocator;

public interface State {
    boolean isCompatibleWith(State state);
}
