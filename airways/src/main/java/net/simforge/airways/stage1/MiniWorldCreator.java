package net.simforge.airways.stage1;

import net.simforge.airways.stage1.model.Airline;
import net.simforge.airways.stage1.model.Pilot;
import net.simforge.airways.stage1.model.TimetableRow;
import net.simforge.airways.stage1.model.aircraft.Aircraft;
import net.simforge.airways.stage1.model.aircraft.AircraftType;
import net.simforge.airways.stage1.model.geo.Airport;
import net.simforge.commons.gckls2com.GC;
import net.simforge.commons.gckls2com.GCAirport;
import net.simforge.commons.misc.Geo;
import net.simforge.commons.misc.JavaTime;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;

public class MiniWorldCreator {
    private static Session session;

    public static void main(String[] args) throws IOException {
        try (SessionFactory sessionFactory = Persistence.createSessionFactory(true)) {
            try (Session _session = sessionFactory.openSession()) {
                session = _session;

                addAirline("ZZ", "ZZA", "ZZ Airways");
                addAirline("WW", "WWA", "Worldwide Airways");

                addAirport("EGLL");
                addAirport("EGBB");
                addAirport("EGCC");
                addAirport("LFPG");
                addAirport("EDDF");
                addAirport("EDDB");
                addAirport("LEMD");
                addAirport("LIRF");
                addAirport("LMML");
                addAirport("LKPR");
                addAirport("LOWW");
                addAirport("LBSF");
                addAirport("LGAV");
                addAirport("EPWA");
                addAirport("EFHK");
                addAirport("ESSA");
                addAirport("EKCH");
                addAirport("EINN");
                addAirport("UUDD");

                addAircraftType("A320", "320", 36000, 444, 160, 150, 2000, 1200);

                addRoundtripTimetableRow("WW", "EGLL", "EGCC", "04:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGBB", "05:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "UUDD", "05:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EDDF", "06:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGCC", "07:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "LFPG", "07:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EINN", "08:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "LEMD", "09:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGBB", "09:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGCC", "10:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "LFPG", "10:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EFHK", "11:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EDDF", "11:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "ESSA", "12:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGCC", "13:00", "A320");
                addRoundtripTimetableRow("WW", "EGLL", "EGBB", "13:00", "A320");

                addAircraft("G-AAAA", "A320", "WW", "EGLL");
                addAircraft("G-AAAB", "A320", "WW", "EGLL");
                addAircraft("G-AAAC", "A320", "WW", "EGLL");
                addAircraft("G-AAAD", "A320", "WW", "EGLL");
                addAircraft("G-AAAE", "A320", "WW", "EGLL");
                addAircraft("G-AAAF", "A320", "WW", "EGLL");
                addAircraft("G-AAAG", "A320", "WW", "EGLL");
                addAircraft("G-AAAH", "A320", "WW", "EGLL");
                addAircraft("G-AAAI", "A320", "WW", "EGLL");
                addAircraft("G-AAAJ", "A320", "WW", "EGLL");

                addPilot("John", "Smith", "EGLL");
                addPilot("Mark", "Twain", "EGLL");
                addPilot("Paul", "Caney", "EGLL");
                addPilot("Dmitry", "Ivanov", "EGLL");
                addPilot("Li Si", "Tzin", "EGLL");
                addPilot("Andrew", "James", "EGLL");
                addPilot("Brandon", "Lee", "EGLL");
            }
        }
    }

    private static void addAirline(String iata, String icao, String name) {
        Airline airline = new Airline();
        airline.setIata(iata);
        airline.setIcao(icao);
        airline.setName(name);

        session.getTransaction().begin();
        session.save(airline);
        session.getTransaction().commit();
    }

    private static void addAirport(String code) throws IOException {
        GCAirport gcAirport = GC.findAirport(code);

        Airport airport = new Airport();
        airport.setIcao(gcAirport.getIcao());
        airport.setIata(gcAirport.getIata());
        airport.setName(gcAirport.getName());
        airport.setLatitude(gcAirport.getLat());
        airport.setLongitude(gcAirport.getLon());

        session.getTransaction().begin();
        session.save(airport);
        session.getTransaction().commit();
    }

    private static void addRoundtripTimetableRow(String iataCode, String fromIcao, String toIcao, String departureTime, String aircraftTypeIcao) {
        session.getTransaction().begin();


        //noinspection JpaQlInspection
        Airline airline = (Airline) session
                .createQuery("select a from Airline a where a.iata = :iata")
                .setString("iata", iataCode)
                .setMaxResults(1)
                .uniqueResult();


        //noinspection JpaQlInspection
        TimetableRow latestAirlineTimetableRow = (TimetableRow) session
                .createQuery("select t from TimetableRow t where t.airline = :airline order by t.number desc")
                .setEntity("airline", airline)
                .setMaxResults(1)
                .uniqueResult();
        String number = latestAirlineTimetableRow != null
                ? AirwaysTools.increaseFlightNumber(latestAirlineTimetableRow.getNumber())
                : AirwaysTools.makeFlightNumber(iataCode, 100);


        //noinspection JpaQlInspection
        Query airportQuery = session
                .createQuery("select a from Airport a where a.icao = :icao")
                .setMaxResults(1);

        Airport fromAirport = (Airport) airportQuery
                .setString("icao", fromIcao)
                .uniqueResult();
        Airport toAirport = (Airport) airportQuery
                .setString("icao", toIcao)
                .uniqueResult();


        //noinspection JpaQlInspection
        AircraftType aircraftType = (AircraftType) session
                .createQuery("select t from AircraftType t where t.icao = :icao")
                .setString("icao", aircraftTypeIcao)
                .setMaxResults(1)
                .uniqueResult();

        SimpleFlight simpleFlight = SimpleFlight.forRoute(
                new Geo.Coords(fromAirport.getLatitude(), fromAirport.getLongitude()),
                new Geo.Coords(toAirport.getLatitude(), toAirport.getLongitude()),
                aircraftType);

        Duration flyingTime = simpleFlight.getTotalTime();
        FlightTimeline timeline = FlightTimeline.byFlyingTime(flyingTime);
        Duration flightDuration = timeline.getScheduledDuration(timeline.getBlocksOff(), timeline.getBlocksOn());

        TimetableRow flight1row = new TimetableRow();
        flight1row.setAirline(airline);
        flight1row.setNumber(number);
        flight1row.setFromAirport(fromAirport);
        flight1row.setToAirport(toAirport);
        flight1row.setAircraftType(aircraftType);
        flight1row.setWeekdays("1234567");
        flight1row.setDepartureTime(departureTime);
        flight1row.setDuration(JavaTime.toHhmm(flightDuration));
        flight1row.setStatus(TimetableRow.Status.Active);
        flight1row.setHeartbeatDt(JavaTime.nowUtc());
        flight1row.setTotalTickets(160);
        flight1row.setHorizon(30);

        session.save(flight1row);


        LocalTime flight2departureTime = LocalTime.parse(departureTime).plus(flightDuration).plusHours(1);
        int step = 5;
        int remainder = flight2departureTime.getMinute() % step;
        flight2departureTime = flight2departureTime.plusMinutes(remainder != 0 ? (step - remainder) : 0);

        TimetableRow flight2row = new TimetableRow();
        flight2row.setAirline(airline);
        flight2row.setNumber(AirwaysTools.increaseFlightNumber(number));
        flight2row.setFromAirport(toAirport);
        flight2row.setToAirport(fromAirport);
        flight2row.setAircraftType(aircraftType);
        flight2row.setWeekdays("1234567");
        flight2row.setDepartureTime(JavaTime.toHhmm(flight2departureTime));
        flight2row.setDuration(JavaTime.toHhmm(flightDuration));
        flight2row.setStatus(TimetableRow.Status.Active);
        flight2row.setHeartbeatDt(JavaTime.nowUtc());
        flight2row.setTotalTickets(160);
        flight2row.setHorizon(30);

        session.save(flight2row);


        session.getTransaction().commit();
    }

    private static void addAircraftType(String icaoCode, String iataCode,
                                        int typicalCruiseAltitude, int typicalCruiseSpeed,
                                        int takeoffSpeed, int landingSpeed,
                                        int climbVerticalSpeed, int descentVerticalSpeed) {
        AircraftType type = new AircraftType();
        type.setIcao(icaoCode);
        type.setIata(iataCode);
        type.setTypicalCruiseAltitude(typicalCruiseAltitude);
        type.setTypicalCruiseSpeed(typicalCruiseSpeed);
        type.setTakeoffSpeed(takeoffSpeed);
        type.setLandingSpeed(landingSpeed);
        type.setClimbVerticalSpeed(climbVerticalSpeed);
        type.setDescentVerticalSpeed(descentVerticalSpeed);

        session.getTransaction().begin();
        session.save(type);
        session.getTransaction().commit();
    }

    private static void addAircraft(String regNo, String typeIcaoCode, String airlineIataCode, String airportCode) {
        session.getTransaction().begin();


        //noinspection JpaQlInspection
        AircraftType type = (AircraftType) session
                .createQuery("select t from AircraftType t where t.icao = :icao")
                .setString("icao", typeIcaoCode)
                .setMaxResults(1)
                .uniqueResult();

        //noinspection JpaQlInspection
        Airline airline = (Airline) session
                .createQuery("select a from Airline a where a.iata = :iata")
                .setString("iata", airlineIataCode)
                .setMaxResults(1)
                .uniqueResult();

        //noinspection JpaQlInspection
        Airport airport = (Airport) session
                .createQuery("select a from Airport a where a.icao = :icao")
                .setString("icao", airportCode)
                .setMaxResults(1)
                .uniqueResult();


        Aircraft aircraft = new Aircraft();
        aircraft.setType(type);
        aircraft.setRegNo(regNo);
        aircraft.setAirline(airline);
        aircraft.setPositionAirport(airport);
        aircraft.setStatus(Aircraft.Status.Idle);
        aircraft.setHeartbeatDt(JavaTime.nowUtc());

        session.save(aircraft);


        session.getTransaction().commit();
    }

    private static void addPilot(String name, String surname, String airportCode) {
        session.getTransaction().begin();


        //noinspection JpaQlInspection
        Airport airport = (Airport) session
                .createQuery("select a from Airport a where a.icao = :icao")
                .setString("icao", airportCode)
                .setMaxResults(1)
                .uniqueResult();


        Pilot pilot = new Pilot();
        pilot.setName(name);
        pilot.setSurname(surname);
        pilot.setPositionAirport(airport);
        pilot.setStatus(Pilot.Status.Idle);
        pilot.setHeartbeatDt(JavaTime.nowUtc());


        session.save(pilot);


        session.getTransaction().commit();
    }
}
