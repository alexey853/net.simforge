package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.Airline;
import net.simforge.commons.hibernate.BaseEntity;
import net.simforge.airways.stage3.model.aircraft.Aircraft;
import net.simforge.airways.stage3.model.aircraft.AircraftType;
import net.simforge.airways.stage3.model.flight.AircraftAssignment;
import net.simforge.airways.stage3.model.flight.Flight;
import net.simforge.airways.stage3.model.flight.PilotAssignment;
import net.simforge.airways.stage3.model.flight.TransportFlight;
import net.simforge.airways.stage3.model.geo.Airport;
import net.simforge.airways.stage3.model.person.Person;
import net.simforge.airways.stage3.model.person.Pilot;
import net.simforge.commons.HeartbeatObject;
import net.simforge.commons.legacy.BM;
import net.simforge.commons.misc.JavaTime;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

public class EntityStorage {
    private SessionFactory sessionFactory;
    private CacheManager cacheManager;
    private final Map<Class, Cache> classToCache = new HashMap<>();
    private final Map<String, Cache> nameToCache = new HashMap<>();
    private final List<CacheContext> caches = new LinkedList<>();

    public EntityStorage(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

        URL myUrl = this.getClass().getResource("/stage3-ehcache.xml");
        Configuration xmlConfig = new XmlConfiguration(myUrl);
        CacheManager cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig);
        cacheManager.init();

        // todo nov17 determine eviction pocilies
        caches.add(
                CacheContext
                        .forCache(Airport.class, cacheManager.getCache("AirportCache", Integer.class, Airport.class))
                        .populateBy(All.create())
                        //.invalidateBy(ByModifyDt.create())
                        .build());

        caches.add(
                CacheContext
                        .forCache(Airline.class, cacheManager.getCache("AirlineCache", Integer.class, Airline.class))
                        .populateBy(All.create())
                        .invalidateBy(ByModifyDt.create())
                        .build());
        caches.add(
                CacheContext
                        .forCache(Aircraft.class, cacheManager.getCache("AircraftCache", Integer.class, Aircraft.class))
                        .populateBy(All.create())
                        // todo nov17 byheartbeatdt
                        .invalidateBy(ByModifyDt.create())
                        .build());
        caches.add(
                CacheContext
                        .forCache(AircraftType.class, cacheManager.getCache("AircraftTypeCache", Integer.class, AircraftType.class))
                        .populateBy(All.create())
                        .build());

        caches.add(
                CacheContext
                        .forCache(Person.class, cacheManager.getCache("PersonCache", Integer.class, Person.class))
                        .populateBy(ByType.create(Person.Type.Excluded))
                        .invalidateBy(ByHeartbeatDt.create())
                        .invalidateBy(ByModifyDt.create())
                        .build());
        caches.add(
                CacheContext
                        .forCache(Pilot.class, cacheManager.getCache("PilotCache", Integer.class, Pilot.class))
                        .populateBy(All.create())
                        .invalidateBy(ByHeartbeatDt.create())
                        .invalidateBy(ByModifyDt.create())
                        .build());

        caches.add(
                CacheContext
                        .forCache(Flight.class, cacheManager.getCache("FlightCache", Integer.class, Flight.class))
                        .populateBy(ByStatus.except(Flight.Status.Cancelled, Flight.Status.Finished))
                        .invalidateBy(ByHeartbeatDt.create())
                        .invalidateBy(ByModifyDt.create())
                        .build());
/*        caches.add(
                CacheContext
                        .forCache(TransportFlight.class, cacheManager.getCache("TransportFlightCache", Integer.class, TransportFlight.class))
                        .populateBy(ByStatus.except(TransportFlight.Status.Cancelled, TransportFlight.Status.Finished))
                        .invalidateBy(ByHeartbeatDt.create())
//                        .invalidateBy(ByModifyDt.create())
                        .build());*/
        caches.add(
                CacheContext
                        .forCache(PilotAssignment.class, cacheManager.getCache("PilotAssignmentCache", Integer.class, PilotAssignment.class))
                        .populateBy(ByStatus.create(PilotAssignment.Status.Assigned, PilotAssignment.Status.InProgress))
                        .invalidateBy(ByModifyDt.create())
                        .build());
        caches.add(
                CacheContext
                        .forCache(AircraftAssignment.class, cacheManager.getCache("AircraftAssignmentCache", Integer.class, AircraftAssignment.class))
                        .populateBy(ByStatus.create(AircraftAssignment.Status.Assigned, AircraftAssignment.Status.InProgress))
                        .invalidateBy(ByModifyDt.create())
                        .build());
    }

    public <T> List<T> filter(Class clazz, Predicate<T> predicate) {
        return filter(clazz, predicate, Integer.MAX_VALUE);
    }

    public <T> List<T> filter(Class clazz, Predicate<T> predicate, int resultLimit) {
        //noinspection unchecked
        Cache<Integer, T> cache = getCacheByClass(clazz);

        final List<T> result = new ArrayList<>();
        Iterator<Cache.Entry<Integer, T>> iterator = cache.<T>iterator();
        while (iterator.hasNext()
                && (result.size() < resultLimit || resultLimit == Integer.MAX_VALUE)) {
            Cache.Entry<Integer, T> entry = iterator.next();
            if (predicate.test(entry.getValue())) {
                result.add(Util.makeImmutable(entry.getValue()));
            }
        }

        return result;
    }

    public <T extends BaseEntity> T get(T entity) {
        return get(entity.getClass(), entity.getId());
    }

    public <T extends BaseEntity> T get(Class clazz, Integer id) {
        Cache<Integer, T> cache = getCacheByClass(clazz);
        T result = cache.get(id);
        if (result == null) {
            throw new IllegalArgumentException("Could not get " + clazz.getSimpleName() + " entity by id " + id);
        }
        return Util.makeImmutable(result);
    }

    public <T extends BaseEntity> T find(Class clazz, Integer id) {
        Cache<Integer, T> cache = getCacheByClass(clazz);
        T result = cache.get(id);
        if (result == null) {
            return null;
        }
        return Util.makeImmutable(result);
    }

    public <T extends BaseEntity> void putIfKnown(T object) {
        Cache<Integer, T> cache = findCacheByClass(object.getClass());
        if (cache != null) {
            cache.put(object.getId(), object);
        }
    }

    public <T extends BaseEntity> void removeIfKnown(T object) {
        Cache<Integer, T> cache = findCacheByClass(object.getClass());
        if (cache != null) {
            cache.remove(object.getId());
        }
    }

    public void invalidate() {
        BM.start("EntityStorage.invalidate");
        try {
            synchronized (caches) {
                for (CacheContext cacheContext : caches) {
                    cacheContext.invalidate();
                }
            }
        } finally {
            BM.stop();
        }
    }

    public FlightOps getFlightOps() {
        return new FlightOps(this, sessionFactory);
    }

    @NotNull
    private <T> Cache<Integer, T> getCacheByClass(Class clazz) {
        Cache<Integer, T> cache = findCacheByClass(clazz);
        if (cache == null) {
            throw new IllegalArgumentException("Could not find cache for entity class " + clazz);
        }
        return cache;
    }

    private <T> Cache<Integer, T> findCacheByClass(Class clazz) {
        synchronized (classToCache) {
            //noinspection unchecked
            Cache cache = classToCache.get(clazz);
            if (cache == null) {
                for (CacheContext cacheContext : caches) {
                    if (cacheContext.acceptsClass(clazz)) {
                        cache = cacheContext.getCache();
                        classToCache.put(clazz, cache);
                        break;
                    }
                }
            }
            return cache;
        }
    }

    private static class CacheContext {
        private Class<?> clazz;
        private Cache cache;
        private boolean populated = false;
        private Invalidator initialPopulator;
        private List<Invalidator> invalidators = new ArrayList<>();

        public boolean acceptsClass(Class clazz) {
            return this.clazz.isAssignableFrom(clazz);
        }

        public Cache getCache() {
            return cache;
        }

        public static Builder forCache(Class clazz, Cache cache) {
            Builder builder = new Builder();
            builder.ctx = new CacheContext();
            builder.ctx.clazz = clazz;
            builder.ctx.cache = cache;
            return builder;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public void invalidate() {
            BM.start("CacheContext.invalidate#" + clazz.getSimpleName());
            try {
                if (!populated && initialPopulator != null) {
                    initialPopulator.invalidate();
                }
                populated = true;

                for (Invalidator invalidator : invalidators) {
                    invalidator.invalidate();
                }
            } finally {
                BM.stop();
            }
        }

        public static class Builder {
            private CacheContext ctx;

            public Builder populateBy(Invalidator invalidator) {
                invalidator.setCacheContext(ctx);
                ctx.initialPopulator = invalidator;
                return this;
            }

            public Builder invalidateBy(Invalidator invalidator) {
                invalidator.setCacheContext(ctx);
                ctx.invalidators.add(invalidator);
                return this;
            }

            public CacheContext build() {
                return this.ctx;
            }
        }
    }

    private abstract static class Invalidator {
        protected CacheContext cacheContext;

        void setCacheContext(CacheContext cacheContext) {
            this.cacheContext = cacheContext;
        }

        abstract void invalidate();
    }

    private static class ByHeartbeatDt extends Invalidator {
        private LocalDateTime lastLoadedHeartbeatDt = LocalDateTime.of(1900, 1, 1, 0, 0);

        public static Invalidator create() {
            return new ByHeartbeatDt();
        }

        @Override
        void invalidate() {
            BM.start("ByHeartbeatDt.invalidate");
            try (Session session = Airways3App.getSessionFactory().openSession()) { // todo!!!
                LocalDateTime nowUtc = JavaTime.nowUtc();

                LocalDateTime fromDt = lastLoadedHeartbeatDt.minusMinutes(1);
                LocalDateTime toDt = nowUtc.plusMinutes(10);

                //noinspection JpaQlInspection,unchecked
                List<BaseEntity> list = session
                        .createQuery("from " + cacheContext.getClazz().getSimpleName() + " where heartbeatDt between :fromDt and :toDt order by heartbeatDt")
                        .setParameter("fromDt", fromDt)
                        .setParameter("toDt", toDt)
                        .setMaxResults(10000)
                        .list();

                //noinspection unchecked
                Cache<Integer, BaseEntity> cache = cacheContext.getCache();

                for (BaseEntity object : list) {
                    cache.put(object.getId(), object);

                    LocalDateTime eachHeartbeatDt = ((HeartbeatObject) object).getHeartbeatDt();
                    if (lastLoadedHeartbeatDt.isBefore(eachHeartbeatDt)) {
                        lastLoadedHeartbeatDt = eachHeartbeatDt; // todo nov17 there is a bug!!
                    }
                }

                lastLoadedHeartbeatDt = nowUtc;
            } finally {
                BM.stop();
            }
        }
    }

    private static class ByModifyDt extends Invalidator {
        private LocalDateTime lastLoadedModifyDt = JavaTime.nowUtc().minusMinutes(10);

        public static Invalidator create() {
            return new ByModifyDt();
        }

        // todo nov17 there is a bug!!
        @Override
        void invalidate() {
            BM.start("ByModifyDt.invalidate");
            try (Session session = Airways3App.getSessionFactory().openSession()) { // todo!!!
                LocalDateTime nowUtc = JavaTime.nowUtc();

                LocalDateTime fromDt = lastLoadedModifyDt.minusMinutes(1);
                LocalDateTime toDt = nowUtc.plusMinutes(10);

                //noinspection JpaQlInspection,unchecked
                List<BaseEntity> list = session
                        .createQuery("from " + cacheContext.getClazz().getSimpleName() + " where modifyDt between :fromDt and :toDt order by modifyDt")
                        .setParameter("fromDt", fromDt)
                        .setParameter("toDt", toDt)
                        .list();

                //noinspection unchecked
                Cache<Integer, BaseEntity> cache = cacheContext.getCache();

                for (BaseEntity object : list) {
                    cache.put(object.getId(), object);
                }

                lastLoadedModifyDt = nowUtc;
            } finally {
                BM.stop();
            }
        }
    }

    private static class ByStatus extends Invalidator {
        private String statusesStr;
        private boolean exceptMode;

        public static Invalidator create(int... statusList) {
            return create(false, statusList);
        }

        public static Invalidator except(int... statusList) {
            return create(true, statusList);
        }

        private static Invalidator create(boolean exceptMode, int... statusList) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < statusList.length; i++) {
                sb.append(statusList[i]);
                if (i != statusList.length - 1) {
                    sb.append(", ");
                }
            }

            ByStatus invalidator = new ByStatus();
            invalidator.statusesStr = sb.toString();
            invalidator.exceptMode = exceptMode;
            return invalidator;
        }

        @Override
        void invalidate() {
            BM.start("ByStatus.invalidate");
            try (Session session = Airways3App.getSessionFactory().openSession()) { // todo!!!

                //noinspection JpaQlInspection,unchecked
                List<BaseEntity> list = session
                        .createQuery("from " + cacheContext.getClazz().getSimpleName() + " where status " + (exceptMode ? "not" : "") + " in (" + statusesStr + ")")
                        .list();

                //noinspection unchecked
                Cache<Integer, BaseEntity> cache = cacheContext.getCache();

                for (BaseEntity object : list) {
                    cache.put(object.getId(), object);
                }
            } finally {
                BM.stop();
            }
        }
    }

    private static class ByType extends Invalidator {
        private String typesStr;

        public static Invalidator create(int... typeList) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < typeList.length; i++) {
                sb.append(typeList[i]);
                if (i != typeList.length - 1) {
                    sb.append(", ");
                }
            }

            ByType invalidator = new ByType();
            invalidator.typesStr = sb.toString();
            return invalidator;
        }

        @Override
        void invalidate() {
            BM.start("ByType.invalidate");
            try (Session session = Airways3App.getSessionFactory().openSession()) { // todo!!!

                //noinspection JpaQlInspection,unchecked
                List<BaseEntity> list = session
                        .createQuery("from " + cacheContext.getClazz().getSimpleName() + " where type in (" + typesStr + ")")
                        .list();

                //noinspection unchecked
                Cache<Integer, BaseEntity> cache = cacheContext.getCache();

                for (BaseEntity object : list) {
                    cache.put(object.getId(), object);
                }
            } finally {
                BM.stop();
            }
        }
    }

    private static class All extends Invalidator {

        public static Invalidator create() {
            return new All();
        }

        @Override
        void invalidate() {
            BM.start("All.invalidate");
            try (Session session = Airways3App.getSessionFactory().openSession()) { // todo!!!

                //noinspection JpaQlInspection,unchecked
                List<BaseEntity> list = session
                        .createQuery("from " + cacheContext.getClazz().getSimpleName())
                        .list();

                //noinspection unchecked
                Cache<Integer, BaseEntity> cache = cacheContext.getCache();

                for (BaseEntity object : list) {
                    cache.put(object.getId(), object);
                }
            } finally {
                BM.stop();
            }
        }
    }
}
