package net.simforge.airways.stage3.model;

import net.simforge.commons.HeartbeatObject;
import net.simforge.commons.hibernate.BaseEntity;

public interface BaseHeartbeatEntity extends BaseEntity, HeartbeatObject {
}
