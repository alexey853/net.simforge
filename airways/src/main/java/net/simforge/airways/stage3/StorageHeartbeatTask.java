package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.BaseHeartbeatEntity;
import net.simforge.commons.misc.JavaTime;
import net.simforge.commons.runtime.BaseTask;

import java.time.LocalDateTime;
import java.util.List;

public abstract class StorageHeartbeatTask<T extends BaseHeartbeatEntity> extends BaseTask {
    protected final EntityStorage storage;

    public StorageHeartbeatTask(String taskName) {
        super(taskName);
        storage = Airways3App.getStorage();
    }

    @Override
    protected void startup() {
        super.startup();

        setBaseSleepTime(5000);
    }

    @Override
    protected void process() {
        BaseOps<T> ops = getBaseOps();

        List<T> objects = ops.whereHeartbeatDtBelow(JavaTime.nowUtc(), 100);
        for (T object : objects) {
            LocalDateTime before = object.getHeartbeatDt();

            try {
                object = heartbeat(object);
            } catch (Throwable t) {
                logger.error("Error during Heartbeat for " + object, t);
            }

            LocalDateTime after = storage.get(object).getHeartbeatDt();

            if (after != null
                    && (after.equals(before)
                    || after.isBefore(after))) {
                logger.warn(String.format("HeartbeatDt for %s is not changed or changed in wrong way: before %s, after %s", object, before, after));
            }
        }
    }

    protected abstract T heartbeat(T object);

    protected abstract BaseOps<T> getBaseOps();
}
