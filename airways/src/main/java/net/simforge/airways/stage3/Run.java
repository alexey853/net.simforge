package net.simforge.airways.stage3;

public class Run {
    public static void main(String[] args) {
        new Airways3App.StartupAction().run();

//        FlightTask task = new FlightTask();
//        PilotTask task = new PilotTask();
        TransportFlightTask task = new TransportFlightTask();
        task.run();
    }
}
