package net.simforge.airways.stage3.model.person;

import net.simforge.airways.stage3.model.geo.Airport;
import net.simforge.airways.stage3.model.geo.AirportEntity;
import net.simforge.airways.stage3.model.geo.City;
import net.simforge.airways.stage3.model.geo.CityEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "Person")
@Table(name="aw_person")
public class PersonEntity implements Person {
    @SuppressWarnings("unused")
    public static final String EventLogCode = "person";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aw_person_id")
    @SequenceGenerator(name = "aw_person_id", sequenceName = "aw_person_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;

    @SuppressWarnings("unused")
    @Column(name = "create_dt")
    private LocalDateTime createDt;
    @SuppressWarnings("unused")
    @Column(name = "modify_dt")
    private LocalDateTime modifyDt;

    @Column(name = "heartbeat_dt")
    private LocalDateTime heartbeatDt;

    @Column
    private Integer type;
    @Column
    private Integer status;
    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private String sex;
    @ManyToOne(targetEntity = CityEntity.class)
    @JoinColumn(name = "origin_city_id")
    private City originCity;
    @ManyToOne(targetEntity = CityEntity.class)
    @JoinColumn(name = "position_city_id")
    private City positionCity;
    @ManyToOne(targetEntity = AirportEntity.class)
    @JoinColumn(name = "position_airport_id")
    private Airport positionAirport;
//    @ManyToOne
//    @JoinColumn(name = "journey_id")
//    private Journey journey;

    @Override
    public String getEventLogCode() {
        return EventLogCode;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public LocalDateTime getCreateDt() {
        return createDt;
    }

    @Override
    public LocalDateTime getModifyDt() {
        return modifyDt;
    }

    @Override
    public LocalDateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    @Override
    public void setHeartbeatDt(LocalDateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    @Override
    public Integer getType() {
        return type;
    }

    @Override
    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String getSex() {
        return sex;
    }

    @Override
    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public City getOriginCity() {
        return originCity;
    }

    @Override
    public void setOriginCity(City originCity) {
        this.originCity = originCity;
    }

    @Override
    public City getPositionCity() {
        return positionCity;
    }

    @Override
    public void setPositionCity(City positionCity) {
        this.positionCity = positionCity;
    }

    @Override
    public Airport getPositionAirport() {
        return positionAirport;
    }

    @Override
    public void setPositionAirport(Airport positionAirport) {
        this.positionAirport = positionAirport;
    }

//    @Override
//    public Journey getJourney() {
//        return journey;
//    }
//
//    @Override
//    public void setJourney(Journey journey) {
//        this.journey = journey;
//    }

}
