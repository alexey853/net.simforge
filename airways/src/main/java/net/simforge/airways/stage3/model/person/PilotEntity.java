package net.simforge.airways.stage3.model.person;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "Pilot")
@Table(name = "aw_pilot")
public class PilotEntity implements Pilot {
    @SuppressWarnings("unused")
    public static final String EventLogCode = "pilot";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aw_pilot_id")
    @SequenceGenerator(name = "aw_pilot_id", sequenceName = "aw_pilot_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;

    @SuppressWarnings("unused")
    @Column(name = "create_dt")
    private LocalDateTime createDt;
    @SuppressWarnings("unused")
    @Column(name = "modify_dt")
    private LocalDateTime modifyDt;

    @Column(name = "heartbeat_dt")
    private LocalDateTime heartbeatDt;

    private Integer status;
    @ManyToOne(targetEntity = PersonEntity.class)
    @JoinColumn(name = "person_id")
    private Person person;

    @Override
    public String getEventLogCode() {
        return EventLogCode;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public LocalDateTime getCreateDt() {
        return createDt;
    }

    @Override
    public LocalDateTime getModifyDt() {
        return modifyDt;
    }

    @Override
    public LocalDateTime getHeartbeatDt() {
        return heartbeatDt;
    }

    @Override
    public void setHeartbeatDt(LocalDateTime heartbeatDt) {
        this.heartbeatDt = heartbeatDt;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }

}
