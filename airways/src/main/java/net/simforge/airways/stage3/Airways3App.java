package net.simforge.airways.stage3;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Airways3App {
    private static Logger logger = LoggerFactory.getLogger(Airways3App.class.getName());

    private static SessionFactory sessionFactory;
    private static EntityStorage storage;

    public static class StartupAction implements Runnable {
        @Override
        public void run() {
            logger.info("creating session factory");
            sessionFactory = Airways3.buildSessionFactory();
            storage = new EntityStorage(sessionFactory);

            storage.invalidate();
        }
    }

    public static class ShutdownAction implements Runnable {
        @Override
        public void run() {
            logger.info("killing session factory");
            SessionFactory _sessionFactory = sessionFactory;
            sessionFactory = null;
            _sessionFactory.close();
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static EntityStorage getStorage() {
        return storage;
    }
}
