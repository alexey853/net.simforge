package net.simforge.airways.stage3.model.flight;

import net.simforge.airways.stage3.model.person.Pilot;
import net.simforge.airways.stage3.model.person.PilotEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "PilotAssignment")
@Table(name = "aw_pilot_assignment")
public class PilotAssignmentEntity implements PilotAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aw_pilot_assignment_id")
    @SequenceGenerator(name = "aw_pilot_assignment_id", sequenceName = "aw_pilot_assignment_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;

    @SuppressWarnings("unused")
    @Column(name = "create_dt")
    private LocalDateTime createDt;
    @SuppressWarnings("unused")
    @Column(name = "modify_dt")
    private LocalDateTime modifyDt;

    @ManyToOne(targetEntity = FlightEntity.class)
    @JoinColumn(name = "flight_id")
    private Flight flight;
    @ManyToOne(targetEntity = PilotEntity.class)
    @JoinColumn(name = "pilot_id")
    private Pilot pilot;
    private String role; // Captain, First Officer, etc

    private Integer status;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public LocalDateTime getCreateDt() {
        return createDt;
    }

    @Override
    public LocalDateTime getModifyDt() {
        return modifyDt;
    }

    @Override
    public Flight getFlight() {
        return flight;
    }

    @Override
    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public Pilot getPilot() {
        return pilot;
    }

    @Override
    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PilotAssignment{" +
                "id=" + id +
                '}';
    }
}
