package net.simforge.airways.stage3.model.geo;

import net.simforge.commons.hibernate.BaseEntity;

public interface Country extends BaseEntity {

    String getName();

    void setName(String name);

    String getCode();

    void setCode(String code);

}
