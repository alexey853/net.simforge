package net.simforge.airways.stage3;

import net.simforge.commons.legacy.BM;
import net.simforge.commons.runtime.BaseTask;

public class CacheInvalidationTask extends BaseTask {

    private final EntityStorage storage;

    public CacheInvalidationTask() {
        super("CacheInvalidationTask");
        storage = Airways3App.getStorage();
    }

    @Override
    protected void startup() {
        super.startup();

        setBaseSleepTime(5000);
        BM.setLoggingPeriod(600000);
    }

    @Override
    protected void process() {
        BM.start("CacheInvalidationTask.process");
        try {

            storage.invalidate();

        } finally {
            BM.stop();
        }
    }
}
