package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.BaseHeartbeatEntity;

import java.time.LocalDateTime;
import java.util.List;

public interface BaseOps<T extends BaseHeartbeatEntity> {

    List<T> whereHeartbeatDtBelow(LocalDateTime threshold, int resultLimit);

    void arrangeHeartbeatAt(T entity, LocalDateTime nextHeartbeatDt);

    void arrangeHeartbeatAt(T entity, LocalDateTime nextHeartbeatDt, String msg);

    void arrangeHeartbeatIn(T entity, long millisTillNextHeartbeat);

}
