package net.simforge.airways.stage3.worldbuilder;

import net.simforge.airways.stage3.Airways3;
import net.simforge.airways.stage3.EventLog;
import net.simforge.airways.stage3.MiscOps;
import net.simforge.airways.stage3.PersonOps;
import net.simforge.airways.stage3.model.geo.Airport;
import net.simforge.airways.stage3.model.geo.City;
import net.simforge.airways.stage3.model.geo.Country;
import net.simforge.airways.stage3.model.person.Person;
import net.simforge.airways.stage3.model.person.Pilot;
import net.simforge.airways.stage3.model.person.PilotEntity;
import net.simforge.commons.hibernate.HibernateUtils;
import net.simforge.commons.misc.JavaTime;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class AddPilots {

    private static final Logger logger = LoggerFactory.getLogger(AddPilots.class.getName());

    public static void main(String[] args) throws IOException {
        try (SessionFactory sessionFactory = Airways3.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            addPilots(session, "United kingdom", "London", "EGLL", 100);
            addPilots(session, "Australia", "Sydney", "YSSY", 20);
            addPilots(session, "China", "Shanghai", "ZSPD", 20);
            addPilots(session, "Russia", "Moskva", "UUDD", 100);
        }
    }

    private static void addPilots(Session session, String countryName, String cityName, String airportIcao, int count) {
        MiscOps ops = new MiscOps(session);

        Country country = ops.countryByName(countryName);
        if (country == null) {
            throw new IllegalArgumentException("Could not find country '" + countryName + "'");
        }

        City city = ops.cityByNameAndCountry(cityName, country);
        if (city == null) {
            throw new IllegalArgumentException("Could not find city '" + cityName + "'");
        }

        Airport airport = ops.airportByIcao(airportIcao);

        //noinspection JpaQlInspection,unchecked
        List<Pilot> pilots = session
                .createQuery("from Pilot pilot where pilot.person.originCity = :city")
                .setEntity("city", city)
                .list();

        if (pilots.size() >= count) {
            return;
        }

        int countToCreate = count - pilots.size();

        for (int i = 0; i < countToCreate; i++) {
            HibernateUtils.transaction(session, () -> {
                Person person = PersonOps.create(session, city);
                person.setType(Person.Type.Excluded);
                person.setPositionAirport(airport);
                person.setPositionCity(null);
                session.update(person);

                Pilot pilot = new PilotEntity();
                pilot.setPerson(person);
                pilot.setStatus(Pilot.Status.Idle);
                pilot.setHeartbeatDt(JavaTime.nowUtc());
                session.save(pilot);

                session.save(EventLog.make(pilot, "Pilot created", person));
            });
        }
    }
}
