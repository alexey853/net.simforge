package net.simforge.airways.stage3.model.flight;

import net.simforge.airways.stage3.model.aircraft.Aircraft;
import net.simforge.airways.stage3.model.aircraft.AircraftEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "AircraftAssignment")
@Table(name = "aw_aircraft_assignment")
public class AircraftAssignmentEntity implements AircraftAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aw_aircraft_assignment_id")
    @SequenceGenerator(name = "aw_aircraft_assignment_id", sequenceName = "aw_aircraft_assignment_id_seq", allocationSize = 1)
    private Integer id;
    @Version
    private Integer version;

    @SuppressWarnings("unused")
    @Column(name = "create_dt")
    private LocalDateTime createDt;
    @SuppressWarnings("unused")
    @Column(name = "modify_dt")
    private LocalDateTime modifyDt;

    @ManyToOne(targetEntity = FlightEntity.class)
    @JoinColumn(name = "flight_id")
    private Flight flight;
    @ManyToOne(targetEntity = AircraftEntity.class)
    @JoinColumn(name = "aircraft_id")
    private Aircraft aircraft;

    private Integer status;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public LocalDateTime getCreateDt() {
        return createDt;
    }

    @Override
    public LocalDateTime getModifyDt() {
        return modifyDt;
    }

    @Override
    public Flight getFlight() {
        return flight;
    }

    @Override
    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public Aircraft getAircraft() {
        return aircraft;
    }

    @Override
    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AircraftAssignment{" +
                "id=" + id +
                '}';
    }

    public static class Status {
        public static final int Assigned = 100;
        public static final int InProgress = 200;
        public static final int Done = 1000;
        public static final int Cancelled = 9999;
    }
}
