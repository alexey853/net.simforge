package net.simforge.airways.stage3.model;

import net.simforge.commons.hibernate.Auditable;
import net.simforge.commons.hibernate.BaseEntity;

public interface Airline extends BaseEntity, Auditable {
    String getIata();

    void setIata(String iata);

    String getIcao();

    void setIcao(String icao);

    String getName();

    void setName(String name);
}
