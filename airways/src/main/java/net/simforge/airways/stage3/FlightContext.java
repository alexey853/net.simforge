package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.aircraft.Aircraft;
import net.simforge.airways.stage3.model.aircraft.AircraftEntity;
import net.simforge.airways.stage3.model.flight.*;
import net.simforge.airways.stage3.model.person.Pilot;
import net.simforge.airways.stage3.model.person.PilotEntity;
import net.simforge.commons.legacy.BM;
import org.hibernate.Session;

import java.util.List;

public class FlightContext {
    private Flight flight;
    private Pilot pilot;
    private PilotAssignment pilotAssignment;
    private Aircraft aircraft;
    private AircraftAssignment aircraftAssignment;

    public static FlightContext fromCache(Flight flight) {
        BM.start("FlightContext.fromCache");
        try {
            EntityStorage storage = Airways3App.getStorage();

            FlightContext ctx = new FlightContext();

            ctx.flight = storage.get(Flight.class, flight.getId());

            List<PilotAssignment> pilotAssignments = storage.filter(
                    PilotAssignment.class,
                    (assignment) -> assignment.getFlight().getId().equals(flight.getId())
                            && (assignment.getStatus() == PilotAssignment.Status.Assigned
                                || assignment.getStatus() == PilotAssignment.Status.InProgress));
            if (!pilotAssignments.isEmpty()) {
                ctx.pilotAssignment = pilotAssignments.get(0);
                ctx.pilot = storage.get(ctx.pilotAssignment.getPilot());
            }

            List<AircraftAssignment> aircraftAssignments = storage.filter(
                    AircraftAssignment.class,
                    (assignment) -> assignment.getFlight().getId().equals(flight.getId())
                            && (assignment.getStatus() == AircraftAssignment.Status.Assigned
                            || assignment.getStatus() == AircraftAssignment.Status.InProgress));
            if (!aircraftAssignments.isEmpty()) {
                ctx.aircraftAssignment = aircraftAssignments.get(0);
                ctx.aircraft = storage.get(ctx.aircraftAssignment.getAircraft());
            }

            return ctx;
        } finally {
            BM.stop();
        }
    }

    public static FlightContext load(Session session, Flight flight) {
        BM.start("FlightContext.load#direct");
        try {
            FlightContext ctx = new FlightContext();

            ctx.flight = session.load(FlightEntity.class, flight.getId());

            //noinspection JpaQlInspection
            ctx.aircraftAssignment = (AircraftAssignment) session
                    .createQuery("select aa " +
                            "from AircraftAssignment as aa " +
                            "where aa.flight = :flight" +
                            "  and aa.status != :cancelled")
                    .setEntity("flight", flight)
                    .setInteger("cancelled", AircraftAssignment.Status.Cancelled)
                    .setMaxResults(1)
                    .uniqueResult();
            if (ctx.aircraftAssignment != null) {
                ctx.aircraft = ctx.aircraftAssignment.getAircraft();
            }

            //noinspection JpaQlInspection
            ctx.pilotAssignment = (PilotAssignment) session
                    .createQuery("select pa " +
                            "from PilotAssignment as pa " +
                            "where pa.flight = :flight" +
                            "  and pa.status != :cancelled")
                    .setEntity("flight", flight)
                    .setInteger("cancelled", PilotAssignment.Status.Cancelled)
                    .setMaxResults(1)
                    .uniqueResult();
            if (ctx.pilotAssignment != null) {
                ctx.pilot = ctx.pilotAssignment.getPilot();
            }

            return ctx;
        } finally {
            BM.stop();
        }
    }

    public static FlightContext load(Session session, FlightContext flightContext) {
        BM.start("FlightContext.load#byContext");
        try {
            FlightContext ctx = new FlightContext();

            ctx.flight = session.load(FlightEntity.class, flightContext.getFlight().getId());

            if (flightContext.getPilotAssignment() != null) {
                ctx.pilotAssignment = session.load(PilotAssignmentEntity.class, flightContext.getPilotAssignment().getId());
                ctx.pilot = session.load(PilotEntity.class, flightContext.getPilotAssignment().getPilot().getId());
            }

            if (flightContext.getAircraftAssignment() != null) {
                ctx.aircraftAssignment = session.load(AircraftAssignmentEntity.class, flightContext.getAircraftAssignment().getId());
                ctx.aircraft = session.load(AircraftEntity.class, flightContext.getAircraft().getId());
            }

            return ctx;
        } finally {
            BM.stop();
        }
    }

    public Flight getFlight() {
        return flight;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public PilotAssignment getPilotAssignment() {
        return pilotAssignment;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public AircraftAssignment getAircraftAssignment() {
        return aircraftAssignment;
    }

    public boolean isFullyAssigned() {
        return pilotAssignment != null && aircraftAssignment != null;
    }
}
