package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.EventLogEntry;
import net.simforge.commons.misc.JavaTime;

public class EventLog {
    public interface Loggable {
        Integer getId();
        String getEventLogCode();
    }

    public static EventLogEntry make(Loggable primaryObject, String msg, Loggable... secondaryObjects) {
        EventLogEntry entry = new EventLogEntry();

        entry.setDt(JavaTime.nowUtc());
        entry.setPrimaryId(getId(primaryObject));
        entry.setMsg(msg);

        if (secondaryObjects.length > 0) entry.setSecondaryId1(getId(secondaryObjects[0]));
        if (secondaryObjects.length > 1) entry.setSecondaryId2(getId(secondaryObjects[1]));
        if (secondaryObjects.length > 2) entry.setSecondaryId3(getId(secondaryObjects[2]));

        return entry;
    }

    private static String getId(Loggable object) {
        try {
            int id = object.getId();
            String objectType = object.getEventLogCode();
            return objectType + ":" + id;
        } catch (Exception e) {
            throw new RuntimeException("Unable to make id for object of class " + object.getClass(), e);
        }
    }
}
