package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.AirlineEntity;
import net.simforge.airways.stage3.model.EventLogEntry;
import net.simforge.airways.stage3.model.aircraft.AircraftEntity;
import net.simforge.airways.stage3.model.flight.*;
import net.simforge.airways.stage3.model.aircraft.AircraftTypeEntity;
import net.simforge.airways.stage3.model.geo.AirportEntity;
import net.simforge.airways.stage3.model.geo.CityEntity;
import net.simforge.airways.stage3.model.geo.CountryEntity;
import net.simforge.airways.stage3.model.person.PersonEntity;
import net.simforge.airways.stage3.model.person.PilotEntity;
import net.simforge.commons.hibernate.SessionFactoryBuilder;
import org.hibernate.SessionFactory;

public class Airways3 {

    public static final Class[] entities = {
            AirportEntity.class,
//            Airport2City.class,
            CountryEntity.class,
            CityEntity.class,

            EventLogEntry.class,

//            CityFlow.class,
//            City2CityFlow.class,
//            City2CityFlowStats.class,

//            Journey.class,
            PersonEntity.class,

            PilotEntity.class,

            AircraftTypeEntity.class,
            AircraftEntity.class,

            AirlineEntity.class,

            TimetableRowEntity.class,
            TransportFlightEntity.class,

            FlightEntity.class,
            PilotAssignmentEntity.class,
            AircraftAssignmentEntity.class,
    };

    public static SessionFactory buildSessionFactory() {
        return SessionFactoryBuilder
                .forDatabase("airways")
                .entities(entities)
                .build();
    }
}
