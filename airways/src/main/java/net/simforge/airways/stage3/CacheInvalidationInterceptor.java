package net.simforge.airways.stage3;

import net.simforge.commons.hibernate.BaseEntity;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Interceptor;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class CacheInvalidationInterceptor extends EmptyInterceptor {
    private EntityStorage storage;
    private Interceptor chainedInterceptor;

    private List<BaseEntity> created = new LinkedList<>();
    private List<BaseEntity> updated = new LinkedList<>();
    private List<BaseEntity> deleted = new LinkedList<>();

    public CacheInvalidationInterceptor(EntityStorage storage) {
        this.storage = storage;
    }

    public CacheInvalidationInterceptor(EntityStorage storage, Interceptor chainedInterceptor) {
        this.storage = storage;
        this.chainedInterceptor = chainedInterceptor;
    }

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        created.add((BaseEntity) entity);

        if (chainedInterceptor != null) {
            return chainedInterceptor.onSave(entity, id, state, propertyNames, types);
        } else {
            return super.onSave(entity, id, state, propertyNames, types);
        }
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        updated.add((BaseEntity) entity);

        if (chainedInterceptor != null) {
            return chainedInterceptor.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
        } else {
            return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
        }
    }

    @Override
    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        deleted.add((BaseEntity) entity);

        if (chainedInterceptor != null) {
            chainedInterceptor.onDelete(entity, id, state, propertyNames, types);
        } else {
            super.onDelete(entity, id, state, propertyNames, types);
        }
    }

    @Override
    public void afterTransactionCompletion(Transaction tx) {
        TransactionStatus status = tx.getStatus();
        if (status == TransactionStatus.COMMITTED) {
            created.stream().forEach(storage::putIfKnown);
            updated.stream().forEach(storage::putIfKnown);
            deleted.stream().forEach(storage::removeIfKnown);
        }

        if (chainedInterceptor != null) {
            chainedInterceptor.afterTransactionCompletion(tx);
        } else {
            super.afterTransactionCompletion(tx);
        }
    }
}
