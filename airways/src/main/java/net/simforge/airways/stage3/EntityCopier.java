package net.simforge.airways.stage3;

import org.ehcache.spi.copy.Copier;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.ManyToOne;
import java.lang.reflect.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class EntityCopier implements Copier<Object> {
    @Override
    public Object copyForRead(Object src) {
        return src;
    }

    @Override
    public Object copyForWrite(Object src) {
        return copyObject(src);
    }

    private Object copyObject(Object src) {
        try {
            Object dst = src.getClass().newInstance();

            Field[] fields = src.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }

                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }

                Class<?> fieldClass = field.getType();
                ManyToOne[] manyToOnes = field.getAnnotationsByType(ManyToOne.class);
                boolean isReference = manyToOnes.length >= 1;

                if (fieldClass == Integer.class
                        || fieldClass == Double.class
                        || fieldClass == String.class
                        || fieldClass == LocalDate.class
                        || fieldClass == LocalDateTime.class) {
                    Object value = field.get(src);
                    field.set(dst, value);
                } else if (isReference) {
                    Object srcValue = field.get(src);
                    Object dstValue;
                    if (srcValue != null) {
                        Integer id = getProxyId(srcValue);
                        dstValue = makeReferenceProxy(id, fieldClass);
                    } else {
                        dstValue = null;
                    }
                    field.set(dst, dstValue);
                } else {
                    throw new IllegalArgumentException("Unsupported class of field: " + fieldClass);
                }
            }

            return dst;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Object makeReferenceProxy(final Integer id, final Class<?> clazz) {
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),
                new Class[]{clazz},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().equals("getId")) {
                            return id;
                        } else if (method.getName().equals("toString")) {
                            return "Proxy: " + clazz.getSimpleName() + " #" + id;
                        } else {
                            throw new IllegalStateException("Semantic fields are not allowed on proxy object");
                        }
                    }
                }
        );
    }

    private Integer getProxyId(Object object) {
        if (object instanceof HibernateProxy) {
            return (Integer) ((HibernateProxy)object).getHibernateLazyInitializer().getIdentifier();
        } else {
            try {
                Method method = object.getClass().getDeclaredMethod("getId");
                return (Integer) method.invoke(object);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
