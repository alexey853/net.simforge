package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.Airline;
import net.simforge.airways.stage3.model.aircraft.AircraftType;
import net.simforge.airways.stage3.model.geo.Airport;
import net.simforge.airways.stage3.model.geo.City;
import net.simforge.airways.stage3.model.geo.Country;
import net.simforge.commons.legacy.BM;
import org.hibernate.Session;

public class MiscOps {

    private Session session;

    public MiscOps(Session session) {
        this.session = session;
    }

    public Country countryByName(String countryName) {
        BM.start("MiscOps.countryByName");
        try {

            //noinspection JpaQlInspection
            return (Country) session
                    .createQuery("from Country c where name = :name")
                    .setString("name", countryName)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public City cityByNameAndCountry(String cityName, Country country) {
        BM.start("MiscOps.cityByNameAndCountry");
        try {
            //noinspection JpaQlInspection
            return (City) session
                    .createQuery("from City c where name = :name and country = :country")
                    .setString("name", cityName)
                    .setEntity("country", country)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public Airport airportByIcao(String icao) {
        BM.start("MiscOps.airportByIcao");
        try {
            //noinspection JpaQlInspection
            return (Airport) session
                    .createQuery("from Airport c where icao = :icao")
                    .setString("icao", icao)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }


    public Airline airlineByIata(String iata) {
        BM.start("MiscOps.airlineByIata");
        try {
            //noinspection JpaQlInspection
            return (Airline) session
                    .createQuery("from Airline c where iata = :iata")
                    .setString("iata", iata)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public AircraftType aircraftTypeByIcao(String icao) {
        BM.start("MiscOps.aircraftTypeByIcao");
        try {
            //noinspection JpaQlInspection
            return (AircraftType) session
                    .createQuery("from AircraftType c where icao = :icao")
                    .setString("icao", icao)
                    .setMaxResults(1)
                    .uniqueResult();
        } finally {
            BM.stop();
        }
    }

    public String makeFlightNumber(String iataCode, int number) {
        return iataCode + String.valueOf(number);
    }

    public String increaseFlightNumber(String number) {
        String iataCode = number.substring(0, 2);
        String digits = number.substring(2);
        int flightNumber = Integer.parseInt(digits);
        return makeFlightNumber(iataCode, flightNumber + 1);
    }

}
