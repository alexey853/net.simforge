package net.simforge.airways.stage3.model.flight;

import net.simforge.airways.stage3.EventLog;
import net.simforge.airways.stage3.model.Airline;
import net.simforge.airways.stage3.model.BaseHeartbeatEntity;
import net.simforge.airways.stage3.model.aircraft.AircraftType;
import net.simforge.airways.stage3.model.geo.Airport;

public interface TimetableRow extends BaseHeartbeatEntity, EventLog.Loggable/*, Auditable*/ {

    Airline getAirline();

    void setAirline(Airline airline);

    String getNumber();

    void setNumber(String number);

    Airport getFromAirport();

    void setFromAirport(Airport fromAirport);

    Airport getToAirport();

    void setToAirport(Airport toAirport);

    AircraftType getAircraftType();

    void setAircraftType(AircraftType aircraftType);

    String getWeekdays();

    void setWeekdays(String weekdays);

    String getDepartureTime();

    void setDepartureTime(String departureTime);

    String getDuration();

    void setDuration(String duration);

    Integer getStatus();

    void setStatus(Integer status);

    Integer getTotalTickets();

    void setTotalTickets(Integer totalTickets);

    Integer getHorizon();

    void setHorizon(Integer horizon);

    class Status {
        public static final int Active = 0;
        public static final int Stopped = 1;
    }
}
