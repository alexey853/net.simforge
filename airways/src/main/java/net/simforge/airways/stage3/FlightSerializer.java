package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.flight.Flight;
import org.ehcache.spi.serialization.Serializer;
import org.ehcache.spi.serialization.SerializerException;

import javax.persistence.ManyToOne;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;

public class FlightSerializer implements Serializer<Flight> {
    public FlightSerializer(ClassLoader classLoader) {
    }

    @Override
    public ByteBuffer serialize(Flight flight) throws SerializerException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oout = new ObjectOutputStream(bout);

            Field[] fields = flight.getClass().getDeclaredFields();
            for (Field field : fields) {
                Class<?> fieldClass = field.getDeclaringClass();
                ManyToOne[] manyToOnes = field.getAnnotationsByType(ManyToOne.class);
                boolean isReference = manyToOnes.length >= 1;

                if (fieldClass == Integer.class) {
                    Integer boxedValue = (Integer) field.get(flight);
                    int value;
                    if (boxedValue == null) {
                        value = Integer.MIN_VALUE;
                    } else {
                        value = boxedValue;
                    }
                    oout.writeInt(value);
                } else {
                    throw new IllegalArgumentException("Unsupported class of field: " + fieldClass);
                }
            }

            bout.close();
        } catch (Exception e) {
            throw new SerializerException(e);
        }

        return ByteBuffer.wrap(bout.toByteArray());
    }

    @Override
    public Flight read(ByteBuffer binary) throws ClassNotFoundException, SerializerException {
        return null;
    }

    @Override
    public boolean equals(Flight object, ByteBuffer binary) throws ClassNotFoundException, SerializerException {
        return false;
    }
}
