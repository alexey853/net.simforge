package net.simforge.airways.stage3;

import net.simforge.airways.stage3.model.BaseHeartbeatEntity;
import net.simforge.commons.hibernate.HibernateUtils;
import net.simforge.commons.misc.JavaTime;
import org.hibernate.Session;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class BaseOpsImpl<T extends BaseHeartbeatEntity> implements BaseOps<T> {

    @Override
    public void arrangeHeartbeatAt(T entity, LocalDateTime nextHeartbeatDt) {
        arrangeHeartbeatAt(entity, nextHeartbeatDt, null);
    }

    @Override
    public void arrangeHeartbeatAt(T entity, LocalDateTime nextHeartbeatDt, String msg) {
        try (Session session = openSession()) {
            HibernateUtils.transaction(session, "BaseOpsImpl.arrangeHeartbeatAt", () -> {
                T _entity = (T) session.get(getEntityClass(), entity.getId());
                _entity.setHeartbeatDt(nextHeartbeatDt);
                session.update(_entity);

                if (msg != null && _entity instanceof EventLog.Loggable) {
                    session.save(EventLog.make((EventLog.Loggable) _entity, msg));
                }
            });
        }
    }

    @Override
    public void arrangeHeartbeatIn(T entity, long millisTillNextHeartbeat) {
        try (Session session = openSession()) {
            HibernateUtils.transaction(session, "BaseOpsImpl.arrangeHeartbeatIn", () -> {
                T _entity = (T) session.get(getEntityClass(), entity.getId());
                _entity.setHeartbeatDt(JavaTime.nowUtc().plus(millisTillNextHeartbeat, ChronoUnit.MILLIS));
                session.update(_entity);
            });
        }
    }

    protected abstract Class getEntityClass(); // todo nov17 automatic determination of entity class

    protected abstract Session openSession();

}
