package net.simforge.airways.stage3.worldbuilder;

import net.simforge.airways.stage3.Airways3;
import net.simforge.airways.stage3.MiscOps;
import net.simforge.airways.stage3.model.Airline;
import net.simforge.airways.stage3.model.aircraft.Aircraft;
import net.simforge.airways.stage3.model.aircraft.AircraftEntity;
import net.simforge.airways.stage3.model.aircraft.AircraftType;
import net.simforge.airways.stage3.model.geo.Airport;
import net.simforge.commons.hibernate.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AddAircraftFleet {

    private static final Logger logger = LoggerFactory.getLogger(AddAircraftFleet.class.getName());

    public static void main(String[] args) throws IOException {
        try (SessionFactory sessionFactory = Airways3.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            // A320
            addAircraftFleet(session, "ZZ", "A320", "UUDD", "VP-B??", 50);
/*            buildAicraftFleet(session, "ZZ", "Russia", "Moskva", 200, 799,  "A320", 30);
            buildMidRangeHub(session, "ZZ", "Russia", "Moskva", 800, 1500, "A320", 20);
            buildMidRangeHub(session, "ZZ", "United kingdom", "London", 100, 1500, "A320", 50);
            buildMidRangeHub(session, "ZZ", "United states", "New york", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "United states", "Los angeles", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Venezuela", "Caracas", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Brazil", "Sao paulo", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "India", "Dilli", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "China", "Shanghai", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Singapore", "Singapore", 100, 2000, "A320", 50);
            buildMidRangeHub(session, "ZZ", "Australia", "Sydney", 100, 2000, "A320", 50);*/

            //B744
            addAircraftFleet(session, "WW", "B744", "EGLL", "G-BN??", 5);
            addAircraftFleet(session, "WW", "B744", "YSSY", "VH-B??", 2);
            addAircraftFleet(session, "WW", "B744", "ZSPD", "X-CB??", 2);
        }
    }

    private static void addAircraftFleet(Session session, String airlineIata, String aircraftTypeIcao, String airportIcao, String regNoPattern, int count) {
        MiscOps miscOps = new MiscOps(session);

        Airline airline = miscOps.airlineByIata( airlineIata);
        AircraftType aircraftType = miscOps.aircraftTypeByIcao(aircraftTypeIcao);
        Airport airport = miscOps.airportByIcao(airportIcao);

        for (int number = 0; number < count; number++) {
            String regNo = regNoPattern;
            int remainder = number;
            int index;
            while ((index = regNo.lastIndexOf('?')) != -1) {
                int letterCode = remainder % 26;
                remainder = remainder / 26;

                regNo = regNo.substring(0, index) + (char) ('A' + letterCode) + regNo.substring(index + 1);
            }

            logger.info("Aircraft Reg No: " + regNo);

            //noinspection JpaQlInspection
            Aircraft aircraft = (Aircraft) session
                    .createQuery("from Aircraft a where regNo = :regNo")
                    .setString("regNo", regNo)
                    .setMaxResults(1)
                    .uniqueResult();

            if (aircraft != null) {
                logger.info("Aircraft " + regNo + " exists");
                continue;
            }

            aircraft = new AircraftEntity();
            aircraft.setType(aircraftType);
            aircraft.setRegNo(regNo);
            aircraft.setAirline(airline);
            aircraft.setPositionAirport(airport);
            aircraft.setStatus(Aircraft.Status.Idle);

            HibernateUtils.saveAndCommit(session, aircraft);
        }
    }
}
