package net.simforge.airways.stage3;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Util {
    public static <T> T makeImmutable(final T src) {
        String className = src.getClass().getName();
        if (className.endsWith("Entity")) {
            className = className.substring(0, className.length() - "Entity".length());
        }
        Class clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        //noinspection unchecked
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(),
                new Class[]{clazz},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().startsWith("get")
                                || method.getName().equals("toString")) {
                            return method.invoke(src, args);
                        } else {
                            throw new IllegalStateException("Only get-methods are allowed on immutable object");
                        }
                    }
                }
        );
    }
}
