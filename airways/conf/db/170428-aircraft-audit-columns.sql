
alter table aw_aircraft add column create_dt timestamp without time zone;
update aw_aircraft set create_dt = '2000-01-01 00:00:00' where create_dt is null;
alter table aw_aircraft alter column create_dt set not null;

alter table aw_aircraft add column modify_dt timestamp without time zone;
update aw_aircraft set modify_dt = '2000-01-01 00:00:00' where modify_dt is null;
alter table aw_aircraft alter column modify_dt set not null;

create index aw_aircraft_modify_dt on aw_aircraft (modify_dt);
