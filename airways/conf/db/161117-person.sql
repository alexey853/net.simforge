
CREATE SEQUENCE aw_person_id_seq;

CREATE TABLE aw_person (
    id integer DEFAULT nextval('aw_person_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_person PRIMARY KEY,
    version smallint NOT NULL,
    type smallint NOT NULL,
    status smallint NOT NULL,
    heartbeat_dt timestamp without time zone,
    name character varying(20) NOT NULL,
    surname character varying(20) NOT NULL,
    sex character varying(1) NOT NULL,
    origin_city_id integer NOT NULL CONSTRAINT fk_origin_city_id REFERENCES aw_city(id),
    current_city_id integer CONSTRAINT fk_current_city_id REFERENCES aw_city(id),
    current_journey_id integer CONSTRAINT fk_current_journey_id REFERENCES aw_journey(id)
);

CREATE INDEX aw_person_heartbeat_dt ON aw_person (heartbeat_dt);
