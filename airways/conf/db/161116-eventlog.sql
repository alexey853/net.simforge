
CREATE SEQUENCE aw_event_log_entry_id_seq;

CREATE TABLE aw_event_log_entry (
    id integer DEFAULT nextval('aw_event_log_entry_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_event_log_entry PRIMARY KEY,
    version smallint NOT NULL,
    dt timestamp without time zone NOT NULL,
    primary_id character varying(30) NOT NULL,
    msg character varying(100) NOT NULL,
    secondary_id_1 character varying(30),
    secondary_id_2 character varying(30),
    secondary_id_3 character varying(30)
);
