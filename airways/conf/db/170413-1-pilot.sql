
CREATE SEQUENCE aw_pilot_id_seq;

CREATE TABLE aw_pilot (
    id integer DEFAULT nextval('aw_pilot_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_pilot PRIMARY KEY,
    version smallint NOT NULL,

    status smallint NOT NULL,
    status_dt timestamp without time zone,
    heartbeat_dt timestamp without time zone,

    person_id integer NOT NULL CONSTRAINT fk_aw_pilot_person_id REFERENCES aw_person(id)
);

CREATE INDEX aw_pilot_heartbeat_dt ON aw_pilot (heartbeat_dt);
