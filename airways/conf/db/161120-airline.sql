
CREATE SEQUENCE aw_airline_id_seq;

CREATE TABLE aw_airline (
    id integer DEFAULT nextval('aw_airline_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_airline PRIMARY KEY,
    version smallint NOT NULL,
    icao character varying(3) NOT NULL CONSTRAINT uq_airline_icao UNIQUE,
    iata character varying(2) NOT NULL CONSTRAINT uq_airline_iata UNIQUE,
    name character varying(30) NOT NULL
);
