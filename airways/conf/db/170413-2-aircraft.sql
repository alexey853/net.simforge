
CREATE SEQUENCE aw_aircraft_id_seq;

CREATE TABLE aw_aircraft (
    id integer DEFAULT nextval('aw_aircraft_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_aircraft PRIMARY KEY,
    version smallint NOT NULL,

    status smallint NOT NULL,
    status_dt timestamp without time zone,
    heartbeat_dt timestamp without time zone,

    aircraft_type_id integer NOT NULL CONSTRAINT fk_aw_aircraft_aircraft_type_id REFERENCES aw_aircraft_type(id),
    reg_no character varying(10) NOT NULL CONSTRAINT uq_aw_aircraft_reg_no UNIQUE,
    airline_id integer CONSTRAINT fk_aw_aircraft_airline_id REFERENCES aw_airline(id),

    position_latitude real,
    position_longitude real,
    position_airport_id integer CONSTRAINT fk_aw_aircraft_position_airport_id REFERENCES aw_airport(id)
);

CREATE INDEX aw_aircraft_heartbeat_dt ON aw_aircraft (heartbeat_dt);
