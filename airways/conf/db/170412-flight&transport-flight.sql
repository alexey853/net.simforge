
CREATE SEQUENCE aw_flight_id_seq;

CREATE TABLE aw_flight (
    id integer DEFAULT nextval('aw_flight_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_flight PRIMARY KEY,
    version smallint NOT NULL,

    date_of_flight date,
    callsign character varying(10),
    aircraft_type_id integer CONSTRAINT fk_aw_flight_aircraft_type_id REFERENCES aw_aircraft_type(id),

    number character varying(10),
    transport_flight_id integer, -- foreign key will be created below, after creation of aw_transport_flight table

    from_airport_id integer CONSTRAINT fk_aw_flight_from_airport_id REFERENCES aw_airport(id),
    to_airport_id integer CONSTRAINT fk_aw_flight_to_airport_id REFERENCES aw_airport(id),
    alternative_airport_id integer CONSTRAINT fk_aw_flight_alternative_airport_id REFERENCES aw_airport(id),

    scheduled_departure_time timestamp without time zone,
    actual_departure_time timestamp without time zone,
    scheduled_takeoff_time timestamp without time zone,
    actual_takeoff_time timestamp without time zone,
    scheduled_landing_time timestamp without time zone,
    actual_landing_time timestamp without time zone,
    scheduled_arrival_time timestamp without time zone,
    actual_arrival_time timestamp without time zone,

    status smallint NOT NULL,
    status_dt timestamp without time zone,
    heartbeat_dt timestamp without time zone
);

CREATE INDEX aw_flight_heartbeat_dt ON aw_flight (heartbeat_dt);



CREATE SEQUENCE aw_transport_flight_id_seq;

CREATE TABLE aw_transport_flight (
    id integer DEFAULT nextval('aw_transport_flight_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_transport_flight PRIMARY KEY,
    version smallint NOT NULL,

    timetable_row_id integer CONSTRAINT fk_aw_transport_flight_timetable_row_id REFERENCES aw_timetable_row(id),
    flight_id integer CONSTRAINT fk_aw_transport_flight_flight_id REFERENCES aw_flight(id),

    date_of_flight date NOT NULL,
    number character varying(10) NOT NULL,

    from_airport_id integer NOT NULL CONSTRAINT fk_aw_transport_flight_from_airport_id REFERENCES aw_airport(id),
    to_airport_id integer NOT NULL CONSTRAINT fk_aw_transport_flight_to_airport_id REFERENCES aw_airport(id),

    departure_dt timestamp without time zone NOT NULL,
    arrival_dt timestamp without time zone NOT NULL,

    status smallint NOT NULL,
    status_dt timestamp without time zone,
    heartbeat_dt timestamp without time zone,

    total_tickets integer NOT NULL,
    free_tickets integer NOT NULL
);

CREATE INDEX aw_transport_flight_heartbeat_dt ON aw_transport_flight (heartbeat_dt);



ALTER TABLE aw_flight
  ADD CONSTRAINT fk_aw_flight_transport_flight_id FOREIGN KEY (transport_flight_id)
      REFERENCES aw_transport_flight (id);
