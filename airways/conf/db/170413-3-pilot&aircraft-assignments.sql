
CREATE SEQUENCE aw_aircraft_assignment_id_seq;

CREATE TABLE aw_aircraft_assignment (
    id integer DEFAULT nextval('aw_aircraft_assignment_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_aircraft_assignment PRIMARY KEY,
    version smallint NOT NULL,

    flight_id integer NOT NULL CONSTRAINT fk_aw_aircraft_assignment_flight_id REFERENCES aw_flight(id),
    aircraft_id integer NOT NULL CONSTRAINT fk_aw_aircraft_assignment_aircraft_id REFERENCES aw_aircraft(id),
    
    status smallint NOT NULL
);




CREATE SEQUENCE aw_pilot_assignment_id_seq;

CREATE TABLE aw_pilot_assignment (
    id integer DEFAULT nextval('aw_pilot_assignment_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_pilot_assignment PRIMARY KEY,
    version smallint NOT NULL,

    flight_id integer NOT NULL CONSTRAINT fk_aw_pilot_assignment_flight_id REFERENCES aw_flight(id),
    pilot_id integer NOT NULL CONSTRAINT fk_aw_pilot_assignment_pilot_id REFERENCES aw_pilot(id),
    
    status smallint NOT NULL,
    role character varying(20)
);
