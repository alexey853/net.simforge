
alter table aw_flight add column create_dt timestamp without time zone;
update aw_flight set create_dt = '2000-01-01 00:00:00' where create_dt is null;
alter table aw_flight alter column create_dt set not null;

alter table aw_flight add column modify_dt timestamp without time zone;
update aw_flight set modify_dt = '2000-01-01 00:00:00' where modify_dt is null;
alter table aw_flight alter column modify_dt set not null;

create index aw_flight_modify_dt on aw_flight (modify_dt);
