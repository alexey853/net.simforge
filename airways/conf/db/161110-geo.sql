
CREATE SEQUENCE aw_country_id_seq;

CREATE TABLE aw_country (
    id integer DEFAULT nextval('aw_country_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_country PRIMARY KEY,
    version smallint NOT NULL,
    name character varying(50) NOT NULL CONSTRAINT uq_name UNIQUE,
    code character varying(2) NOT NULL
);



CREATE SEQUENCE aw_city_id_seq;

CREATE TABLE aw_city (
    id integer DEFAULT nextval('aw_city_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_city PRIMARY KEY,
    version smallint NOT NULL,
    country_id integer NOT NULL CONSTRAINT fk_country_id REFERENCES aw_country(id),
    name character varying(50) NOT NULL,
    latitude real NOT NULL,
    longitude real NOT NULL,
    population integer NOT NULL,
    dataset smallint NOT NULL
);



CREATE SEQUENCE aw_airport_id_seq;

CREATE TABLE aw_airport (
    id integer DEFAULT nextval('aw_airport_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_airport PRIMARY KEY,
    version smallint NOT NULL,
    iata character varying(3) CONSTRAINT uq_iata UNIQUE,
    icao character varying(4) NOT NULL CONSTRAINT uq_icao UNIQUE,
    name character varying(50),
    latitude real NOT NULL,
    longitude real NOT NULL,
    dataset smallint NOT NULL
);



CREATE SEQUENCE aw_airport2city_id_seq;

CREATE TABLE aw_airport2city (
    id integer DEFAULT nextval('aw_airport2city_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_airport2city PRIMARY KEY,
    version smallint NOT NULL,
    airport_id integer NOT NULL CONSTRAINT fk_airport_id REFERENCES aw_airport(id),
    city_id integer NOT NULL CONSTRAINT fk_city_id REFERENCES aw_city(id),
    dataset smallint NOT NULL
);
