
CREATE SEQUENCE aw_aircraft_type_id_seq;

CREATE TABLE aw_aircraft_type (
    id integer DEFAULT nextval('aw_aircraft_type_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_aircraft_type PRIMARY KEY,
    version smallint NOT NULL,
    icao character varying(4) NOT NULL CONSTRAINT uq_aircraft_type_icao UNIQUE,
    iata character varying(3) NOT NULL CONSTRAINT uq_aircraft_type_iata UNIQUE,
    typical_cruise_altitude integer NOT NULL,
    typical_cruise_speed smallint NOT NULL,
    climb_vertical_speed smallint NOT NULL,
    descent_vertical_speed smallint NOT NULL,
    takeoff_speed smallint NOT NULL,
    landing_speed smallint NOT NULL
);
