
-- It renames current_journey_id to journey_id
ALTER TABLE aw_person
  DROP CONSTRAINT fk_current_journey_id;

ALTER TABLE aw_person
  RENAME COLUMN current_journey_id TO journey_id;

ALTER TABLE aw_person
  ADD CONSTRAINT fk_aw_person_journey_id FOREIGN KEY (journey_id)
      REFERENCES aw_journey (id);



-- It renames current_city_id to position_city_id
ALTER TABLE aw_person
  DROP CONSTRAINT fk_current_city_id;

ALTER TABLE aw_person
  RENAME COLUMN current_city_id TO position_city_id;

ALTER TABLE aw_person
  ADD CONSTRAINT fk_aw_person_position_city_id FOREIGN KEY (position_city_id)
      REFERENCES aw_city (id);



-- It adds position_airport_id
ALTER TABLE aw_person
  ADD COLUMN position_airport_id INTEGER;

ALTER TABLE aw_person
  ADD CONSTRAINT fk_aw_person_position_airport_id FOREIGN KEY (position_airport_id)
      REFERENCES aw_airport (id);
