
alter table aw_airline add column create_dt timestamp without time zone;
update aw_airline set create_dt = '2000-01-01 00:00:00' where create_dt is null;
alter table aw_airline alter column create_dt set not null;

alter table aw_airline add column modify_dt timestamp without time zone;
update aw_airline set modify_dt = '2000-01-01 00:00:00' where modify_dt is null;
alter table aw_airline alter column modify_dt set not null;

create index aw_airline_modify_dt on aw_airline (modify_dt);
