
CREATE SEQUENCE aw_timetable_row_id_seq;

CREATE TABLE aw_timetable_row (
    id integer DEFAULT nextval('aw_timetable_row_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_timetable_row PRIMARY KEY,
    version smallint NOT NULL,
    airline_id integer NOT NULL CONSTRAINT fk_timetable_row_airline_id REFERENCES aw_airline(id),
    number character varying(7) NOT NULL CONSTRAINT uq_timetable_row_number UNIQUE,
    from_airport_id integer NOT NULL CONSTRAINT fk_timetable_row_from_airport_id REFERENCES aw_airport(id),
    to_airport_id integer NOT NULL CONSTRAINT fk_timetable_row_to_airport_id REFERENCES aw_airport(id),
    aircraft_type_id integer NOT NULL CONSTRAINT fk_timetable_row_aircraft_type_id REFERENCES aw_aircraft_type(id),
    weekdays character varying(7) NOT NULL,
    departure_time character varying(5) NOT NULL,
    duration character varying(5) NOT NULL,
    status smallint NOT NULL,
    heartbeat_dt timestamp without time zone,
    total_tickets smallint NOT NULL,
    horizon smallint
);

CREATE INDEX aw_timetable_row_heartbeat_dt ON aw_timetable_row (heartbeat_dt);
