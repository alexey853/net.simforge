

CREATE SEQUENCE aw_city_flow_id_seq;

CREATE TABLE aw_city_flow (
    id integer DEFAULT nextval('aw_city_flow_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_city_flow PRIMARY KEY,
    version smallint NOT NULL,
    city_id integer NOT NULL CONSTRAINT uq_city_id UNIQUE CONSTRAINT fk_city_id REFERENCES aw_city(id),
    heartbeat_dt timestamp without time zone,
    status smallint NOT NULL,
    last_redistribution_dt timestamp without time zone,
    attraction real,
    units_threshold real,
    default_availability real
);



CREATE SEQUENCE aw_city2city_flow_id_seq;

CREATE TABLE aw_city2city_flow (
    id integer DEFAULT nextval('aw_city2city_flow_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_city2city_flow PRIMARY KEY,
    version smallint NOT NULL,
    from_flow_id integer NOT NULL CONSTRAINT fk_from_flow_id REFERENCES aw_city_flow(id),
    to_flow_id integer NOT NULL CONSTRAINT fk_to_flow_id REFERENCES aw_city_flow(id),
    heartbeat_dt timestamp without time zone,
    active boolean NOT NULL,
    units real NOT NULL,
    percentage real NOT NULL,
    availability real NOT NULL,
    next_group_size smallint NOT NULL,
    accumulated_flow real NOT NULL,
    accumulated_flow_dt timestamp without time zone NOT NULL,
    CONSTRAINT uq_from_to_flows_id UNIQUE (from_flow_id, to_flow_id)
);

CREATE INDEX idx_aw_city2city_flow_heartbeat_dt ON aw_city2city_flow (heartbeat_dt);



CREATE SEQUENCE aw_city2city_flow_stats_id_seq;

CREATE TABLE aw_city2city_flow_stats (
    id integer DEFAULT nextval('aw_city2city_flow_stats_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_city2city_flow_stats PRIMARY KEY,
    version smallint NOT NULL,
    c2c_flow_id integer NOT NULL CONSTRAINT fk_c2c_flow_id REFERENCES aw_city2city_flow(id),
    date date NOT NULL,
    heartbeat_dt timestamp without time zone,
    availability_before real NOT NULL,
    availability_after real NOT NULL,
    availability_delta real NOT NULL,
    no_tickets integer NOT NULL,
    tickets_bought integer NOT NULL,
    travelled integer NOT NULL,
    CONSTRAINT uq_c2c_flow_id_date UNIQUE (c2c_flow_id, date)
);

CREATE INDEX idx_aw_city2city_flow_stats_heartbeat_dt ON aw_city2city_flow_stats (heartbeat_dt);
