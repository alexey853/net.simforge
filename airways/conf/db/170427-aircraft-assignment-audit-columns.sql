
alter table aw_aircraft_assignment add column create_dt timestamp without time zone;
update aw_aircraft_assignment set create_dt = '2000-01-01 00:00:00' where create_dt is null;
alter table aw_aircraft_assignment alter column create_dt set not null;

alter table aw_aircraft_assignment add column modify_dt timestamp without time zone;
update aw_aircraft_assignment set modify_dt = '2000-01-01 00:00:00' where modify_dt is null;
alter table aw_aircraft_assignment alter column modify_dt set not null;

create index aw_aircraft_assignment_modify_dt on aw_aircraft_assignment (modify_dt);
