
CREATE SEQUENCE aw_journey_id_seq;

CREATE TABLE aw_journey (
    id integer DEFAULT nextval('aw_journey_id_seq'::regclass) NOT NULL CONSTRAINT pk_aw_journey PRIMARY KEY,
    version smallint NOT NULL,
    c2c_flow_id integer NOT NULL CONSTRAINT fk_c2c_flow_id REFERENCES aw_city2city_flow(id),
    from_city_id integer NOT NULL CONSTRAINT fk_from_city_id REFERENCES aw_city(id),
    to_city_id integer NOT NULL CONSTRAINT fk_to_city_id REFERENCES aw_city(id),
    group_size smallint NOT NULL,
    status smallint NOT NULL,
    heartbeat_dt timestamp without time zone,
    expiration_dt timestamp without time zone,
    current_city_id integer CONSTRAINT fk_current_city_id REFERENCES aw_city(id),
    current_airport_id integer CONSTRAINT fk_current_airport_id REFERENCES aw_airport(id)
);

CREATE INDEX idx_aw_journey_heartbeat_dt ON aw_journey (heartbeat_dt);
